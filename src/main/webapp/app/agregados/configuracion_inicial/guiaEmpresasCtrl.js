app.controller("guiaEmpresasCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    
    $scope.nuevaEmpresa = {empresaID:"",razonSocial:"",ruc:"",estado:'A',telefono:"",pagWeb:"",existe:true};
    
    $scope.empresaSel = {};
    $scope.oSel = {};
    
    //tabla de Empresas
    var paramsEmpresa = {count: 10};
    var settingEmpresa = { counts: []};
    $scope.tablaEmpresas = new NgTableParams(paramsEmpresa, settingEmpresa);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"activo"},{id:'I',title:"inactivo"},{id:'E',title:"eliminado"}];
    
    
    $scope.buscarEmpresa = function(){
        
        if($scope.nuevaEmpresa.ruc==''){
            modal.mensaje("ALERTA","ingrese RUC");
            return;
        }
        //preparamos un objeto request
        var request = crud.crearRequest('empresas',1,'buscarEmpresa');
        request.setData({ruc:$scope.nuevaEmpresa.ruc});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            modal.mensaje("CONFIRMACION",data.responseMsg);
            if(data.responseSta){
                $scope.nuevaEmpresa = data.data;
            }
            else{
                $scope.nuevaEmpresa.existe = false;
                
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarEmpresas = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('empresas',1,'listarEmpresas');
        crud.listar("/configuracionInicial",request,function(data){
            settingEmpresa.dataset = data.data;
            iniciarPosiciones(settingEmpresa.dataset);
            $scope.tablaEmpresas.settings(settingEmpresa);
        },function(data){
            console.info(data);
        });
    };
    $scope.reiniciarDatos = function(){
        var p = $scope.nuevaEmpresa;
        
        p.existe = true;
        p.razonSocial="";
        p.pagWeb="";
        p.telefono="";
        p.estado="";
        p.ruc="";
        $scope.oSel = {};
    };
    $scope.prepararAgregar = function(){
        
        $scope.nuevaEmpresa = {razonSocial:"",ruc:"",estado:'A',telefono:"",pagWeb:"",existe:true};
        $scope.oSel = {};
        
        $("#modalNuevo").modal('show');
    };    
    $scope.agregarEmpresa = function(){
        
        
        var request = crud.crearRequest('empresas',1,'insertarEmpresa');
        request.setData({empresa:$scope.nuevaEmpresa});
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                var u = {};
                
                u.empresaID = response.data.empresaID;                
                u.razonSocial = response.data.razonSocial;
                u.ruc = response.data.ruc;
                u.estado = response.data.estado;
                u.pagWeb = response.data.pagWeb;
                u.telefono = response.data.telefono;
                
                //insertamos el elemento a la lista
                insertarElemento(settingEmpresa.dataset,u);
                $scope.tablaEmpresas.reload();
                
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarEmpresa = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('empresas',1,'eliminarEmpresa');
            request.setData({empresaID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingEmpresa.dataset,i);
                    $scope.tablaEmpresas.reload();
                }
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.empresaSel = JSON.parse(JSON.stringify(t));
        
        $('#modalEditar').modal('show');
    };
    $scope.editarEmpresa = function(){
        
        var request = crud.crearRequest('empresas',1,'actualizarEmpresa');
        request.setData($scope.empresaSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                //$scope.usuarioSel.organizacion = buscarContenido($scope.organizaciones,"organizacionID","nombre",$scope.usuarioSel.organizacionID);
                $scope.empresaSel.empresaID=response.data.empresaID;
                settingEmpresa.dataset[$scope.empresaSel.i] = $scope.empresaSel;
                $scope.tablaEmpresas.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };

    
    
}]);
