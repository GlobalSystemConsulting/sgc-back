app.controller("areaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    $scope.tipoAreas = [];
    $scope.areas = [];    
    $scope.nuevaArea = {codigo:"",nombre:"",estado:'A',organizacionID:"0",areaPadreID:"0"};
    $scope.areaSel = {};
    
    //tabla de areas
    var paramsArea = {count: 10};
    var settingArea = { counts: []};
    $scope.tablaArea = new NgTableParams(paramsArea, settingArea);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"activo"},{id:'I',title:"inactivo"},{id:'E',title:"eliminado"}];
    $scope.areasSelect = [];
    
    $scope.listarAreas = function(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas = settingArea.dataset = data.data;
            iniciarPosiciones(settingArea.dataset);            
            
            $scope.areasSelect.splice(0);
            $scope.areasSelect.push({id:'',title:""});
            
            data.data.forEach(function(item){
                var o = item;
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.areasSelect.push(o);
            });
            $scope.tablaArea.settings(settingArea);
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarArea = function(organizacionID){
        
        $scope.nuevaArea.organizacionID = organizacionID;
        
        $scope.nuevaArea.nombre = buscarContenido($scope.tipoAreas,"tipoAreaID","nombre",$scope.nuevaArea.tipoAreaID);
        
        var request = crud.crearRequest('area',1,'insertarArea');
        request.setData($scope.nuevaArea);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevaArea.areaID = response.data.areaID;
                $scope.nuevaArea.areaPadre = buscarContenido(settingArea.dataset,"areaID","nombre",$scope.nuevaArea.areaPadreID);
                
                //insertamos el elemento a la lista
                insertarElemento(settingArea.dataset,$scope.nuevaArea);
                
                var o = {};
                o.id = $scope.nuevaArea.nombre;
                o.title = $scope.nuevaArea.nombre;
                $scope.areasSelect.push(o);
                
                $scope.tablaArea.reload();
                //reiniciamos las variables
                $scope.nuevaArea = {codigo:"",nombre:"",estado:'A',organizacionID:"0",areaPadreID:"0"};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarArea = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar la area",function(){
            
            var request = crud.crearRequest('area',1,'eliminarArea');
            request.setData({areaID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingArea.dataset,i);
                    $scope.tablaArea.reload();
                }

            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.areaSel = JSON.parse(JSON.stringify(t));
        $('#modalEditar').modal('show');
    };
    $scope.editarArea = function(){
        
        $scope.areaSel.nombre = buscarContenido($scope.tipoAreas,"tipoAreaID","nombre",$scope.areaSel.tipoAreaID);
        
        var request = crud.crearRequest('area',1,'actualizarArea');
        request.setData($scope.areaSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                $scope.areaSel.areaPadre = buscarContenido(settingArea.dataset,"areaID","nombre",$scope.areaSel.areaPadreID);
                settingArea.dataset[$scope.areaSel.i] = $scope.areaSel;
                
                var o = $scope.areasSelect[$scope.areaSel.i];
                o.id = o.title = $scope.areaSel.nombre;
                
                $scope.tablaArea.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
    listarTipoAreas ();
    
    function listarTipoAreas (){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarTipoAreas');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoAreas  = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    $scope.verUsuarios = function(area){
        $scope.areaSel = area;
        $scope.usuariosSel = [];
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuariosPorArea');
        request.setData({areaID:area.areaID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.usuariosSel = data.data;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
              
    };
    
    $scope.elegirResponsable = function(responsableID){
        
        modal.mensajeConfirmacion($scope,"seguro que desea cambiar de responsable",function(){
            var request = crud.crearRequest('area',1,'actualizarResponsableArea');
            request.setData({areaID:$scope.areaSel.areaID,responsableID:responsableID});
            
            crud.actualizar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.areaSel.responsableID = responsableID;
                }
            },function(data){
                console.info(data);
            });
        
        });
              
    };
    
    
}]);
