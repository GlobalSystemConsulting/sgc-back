app.controller("alertaSistemaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.funciones = [];
    
    var paramsAlerta = {count: 10};
    var settingAlerta = { counts: []};
    $scope.tablaAlerta = new NgTableParams(paramsAlerta, settingAlerta);
    
    $scope.listarAlertas = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('alertaSistema',1,'listarAlertas');
        crud.listar("/configuracionInicial",request,function(data){
            settingAlerta.dataset = data.data;
            $scope.tablaAlerta.settings(settingAlerta);
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararAgregar = function(){
        $scope.agregar = true;
        $scope.alerta = {nombre:"",codigo:"",descripcion:"",tipo:'I',funcionID:0};
        $('#modal').modal('show');
    };
    $scope.prepararEditar = function(t){
        $scope.agregar = false;
        $scope.alerta = JSON.parse(JSON.stringify(t));        
        $scope.alerta.funcion = buscarObjeto($scope.funciones,"funcionID",$scope.alerta.funcionID);        
        $scope.alerta.ID = $scope.alerta.alertaID%100;
        $scope.alerta.modulo = $scope.alerta.funcion.subModuloID;
        $scope.alerta.modulo = $scope.alerta.modulo>9?$scope.alerta.modulo:'0'+$scope.alerta.modulo;
        $('#modal').modal('show');
    };
    $scope.cambiarModulo = function(modulo){
        $scope.alerta.modulo = modulo>9?modulo:'0'+modulo;
    };
    function generarCodigo(a){
        var codigo = a.tipo=='I'?1:a.tipo=='A'?2:3;
        codigo += ""+a.modulo; 
        codigo += a.ID>9?a.IDid:'0'+a.ID;
        
        return codigo;
    }
    $scope.mergeAlerta = function(){
        
        if(!$scope.alerta.ID || $scope.alerta.ID.length > 2){
            modal.mensaje("ALERTA","el id debe ser de tamaño de 2 digitos");
            return;
        }
        if(!$scope.alerta.funcion ){
            modal.mensaje("ALERTA","seleccione una funcion");
            return;
        }
        $scope.alerta.alertaID = generarCodigo($scope.alerta);
        $scope.alerta.funcionID = $scope.alerta.funcion.funcionID;
        
        if($scope.agregar){
            var request = crud.crearRequest('alertaSistema',1,'insertarAlerta');
            request.setData($scope.alerta);

            crud.insertar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.alerta.alertaID = response.data.alertaID;

                    //insertamos el elemento a la lista
                    insertarElemento(settingAlerta.dataset,$scope.alerta);
                    $scope.tablaAlerta.reload();
                    //cerramos la ventana modal
                    $('#modal').modal('hide');
                }            
            },function(data){
                console.info(data);
            });            
        }
        else{
            var request = crud.crearRequest('alertaSistema',1,'actualizarAlerta');
            request.setData($scope.alerta);

            crud.actualizar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){                
                    //actualizando
                    settingAlerta.dataset[$scope.alerta.i] = $scope.alerta;
                    $scope.tablaAlerta.reload();
                    $('#modal').modal('hide');
                }
            },function(data){
                console.info(data);
            });            
        }
        
    };
    $scope.eliminarAlerta = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('alertaSistema',1,'eliminarAlerta');
            request.setData({alertaID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingAlerta.dataset,i);
                    $scope.tablaAlerta.reload();
                }

            },function(data){
                console.info(data);
            });
            
        },'350');
    };
    
    listarFunciones();
    
    function listarFunciones(){
        //preparamos un objeto request
        var request = crud.crearRequest('funcionSistema',1,'listarFunciones');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.funciones = data.data;
        },function(data){
            console.info(data);
        });
    }; 
    
    
}]);