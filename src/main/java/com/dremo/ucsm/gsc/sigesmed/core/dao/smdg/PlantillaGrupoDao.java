/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface PlantillaGrupoDao extends GenericDao<PlantillaGrupo>{
    public String buscarUltimoCodigo();
    public PlantillaGrupo obtenerPlantillaGrupo(int gruId);        
    public List<PlantillaIndicadores> obtenerIndicadores(int gruId);    
}
