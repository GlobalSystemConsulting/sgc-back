package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity(name = "MatriculaMaestro")
@Table(name = "matricula", schema = "pedagogico")
public class Matricula implements java.io.Serializable {

    @Id
    @Column(name = "mat_id")
    @SequenceGenerator(name = "matricula_mat_id_seq",sequenceName = "pedagogico.matricula_mat_id_seq")
    @GeneratedValue(generator = "matricula_mat_id_seq")
    private int id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "org_des_id",insertable = false, updatable = false)
    private Organizacion ie;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "est_id",insertable = false, updatable = false)
    private Estudiante estudiante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "apo_id",insertable = false, updatable = false)
    private Persona apoderado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_ori_id", nullable = true)
    private Organizacion orgOrigen;

    @OneToMany(mappedBy = "matriculaEstudiante",fetch = FetchType.LAZY)
    private List<GradoIEEstudiante> gradosMatricula = new ArrayList<>();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini")
    private Date fecIni;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin")
    private Date fecFin;
    @Column(name = "act")
    private Boolean act;

    @OneToMany(mappedBy = "matricula", fetch = FetchType.LAZY)
    private List<AsistenciaEstudiante> asistencias = new ArrayList<>();
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public Matricula() {
    }

    public Matricula(Organizacion ie, Estudiante estudiante, Date fecIni, Date fecFin, Boolean act) {
        this.ie = ie;
        this.estudiante = estudiante;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.act = act;
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Organizacion getIe() {
        return ie;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public Organizacion getOrgOrigen() {
        return orgOrigen;
    }

    public void setOrgOrigen(Organizacion orgOrigen) {
        this.orgOrigen = orgOrigen;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Boolean getAct() {
        return act;
    }

    public void setAct(Boolean act) {
        this.act = act;
    }

    public List<GradoIEEstudiante> getGradosMatricula() {
        return gradosMatricula;
    }

    public void setGradosMatricula(List<GradoIEEstudiante> gradosMatricula) {
        this.gradosMatricula = gradosMatricula;
    }

    public List<AsistenciaEstudiante> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<AsistenciaEstudiante> asistencias) {
        this.asistencias = asistencias;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

}
