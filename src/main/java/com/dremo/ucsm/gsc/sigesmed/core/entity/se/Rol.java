/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity(name = "RolSE")
@Table(name="rol")

public class Rol implements Serializable {
    
    @Id
    @Column(name="rol_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_rol", sequenceName="rol_rol_id_seq")
    @GeneratedValue(generator="secuencia_rol")
    private int rolId;
    
    @Column(name="abr", length=8)
    private String abr;
    
    @Column(name="nom", nullable=false, length=32)
    private String nom;
    
    @Column(name="des", length=256)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Column(name="est_reg", length=1)
    private char estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "rol")
    List<Trabajador> trabajadores;

    public Rol() {
    }
    public Rol(int rolId) {
        this.rolId = rolId;
    }
	
    public Rol(int rolId, String nom) {
        this.rolId = rolId;
        this.nom = nom;
    }
   
    public int getRolId() {
        return this.rolId;
    }
    
    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    
    public String getAbr() {
        return this.abr;
    }
    
    public void setAbr(String abr) {
        this.abr = abr;
    }

    
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public char getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "Rol{" + "rolId=" + rolId + ", abr=" + abr + ", nom=" + nom + ", des=" + des + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }
    
    

}