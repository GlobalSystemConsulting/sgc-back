/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author gscadmin
 */
public interface FichaEscalafonariaDao extends GenericDao<FichaEscalafonaria>{
    FichaEscalafonaria obtenerDatosPersonales(int orgId, int perId);
    FichaEscalafonaria buscarPorTraId(Integer traId);
    FichaEscalafonaria buscarPorDNI(String perDNI);
    List<FichaEscalafonaria> listarxOrganizacion(int orgId);
    FichaEscalafonaria buscarPorId(Integer ficEscId);
    FichaEscalafonaria buscarPorUsuId(Integer usuId);
    
    //Busca segun cadena de inicio
    List<FichaEscalafonaria> listarxOrgxCadInicio(int orgId, String cadenaInicio);
    
    List<FichaEscalafonaria> listarxOrgTopTen(int orgId, int limit, int offset);
    List<FichaEscalafonaria> busquedaParametrizada(int orgId,int limit, int offset, String campo, String data);
    long canRegBusqPar(int orgId,String nomCampo, String dataCampo);
    
    List<FichaEscalafonaria> searchWithFilters(int orgId, int limit, int offset, JSONArray filtros);
    long canRegBusqxFiltros(int orgId, JSONArray filtros);
    
    FichaEscalafonaria buscarPorDNIyTraId(String perDNI, int traId);
    
}
