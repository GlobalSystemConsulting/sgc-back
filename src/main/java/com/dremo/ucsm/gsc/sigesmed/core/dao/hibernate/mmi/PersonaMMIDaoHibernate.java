package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import org.hibernate.Query;
import org.hibernate.Session;

public class PersonaMMIDaoHibernate extends GenericMMIDaoHibernate<PersonaMMI> {

    public PersonaMMI find4Dni(String dni) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        PersonaMMI personaMMI = null;
        String hql;
        Query query;
        try {
            hql = "FROM PersonaMMI per WHERE per.estReg != 'E' and per.dni= :hdlDni";
            query = session.createQuery(hql);
            query.setString("hdlDni", dni);
            query.setMaxResults(1);
            personaMMI = (PersonaMMI) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se encontro la persona con el DNI: " + dni);
            throw ex;
        } finally {
            session.close();
        }
        return personaMMI;
    }

}
