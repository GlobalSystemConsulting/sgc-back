/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.PrioridadExpediente;
/**
 *
 * @author abel
 */
public interface PrioridadExpedienteDao extends GenericDao<PrioridadExpediente>{
    
}
