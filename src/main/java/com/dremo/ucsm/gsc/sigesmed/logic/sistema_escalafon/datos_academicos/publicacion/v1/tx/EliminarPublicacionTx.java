/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarPublicacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarPublicacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer pubId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            pubId = requestData.getInt("pubId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarPublicacion",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        PublicacionDao publicacionDao = (PublicacionDao)FactoryDao.buildDao("se.PublicacionDao");
        try{
            publicacionDao.delete(new Publicacion(pubId));
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino publicacion", "Publ_ID: "+pubId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar la publicacion\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la publicacion", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La publicacion se elimino correctamente");
    }
    
}
