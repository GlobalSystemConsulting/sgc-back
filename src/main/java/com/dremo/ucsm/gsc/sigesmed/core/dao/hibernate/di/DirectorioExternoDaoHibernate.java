/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class DirectorioExternoDaoHibernate extends GenericDaoHibernate<DirectorioExterno> implements DirectorioExternoDao {

    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List ListarDirectorioExterno(String s){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
//        String qry = " WHERE t.org_id = " + orgId;                
//        qry += (tipo.isEmpty()?"":(" AND t.tra_tip = '" + tipo + "'"));
        
        List data = null;
        
        try{
            SQLQuery query = session.createSQLQuery("select "+s+" from institucional.directorio_externo");
            query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);            
            data = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Externo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Externo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }        
        
        return data;         
    }
    
//    @Override
//    public List<DirectorioExterno> listarDirectorioExterno() {
//        
//        List<DirectorioExterno> direx = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try{
//            //listar FuncionSistemas
//            //String hql = "SELECT p FROM Persona p" ;
//            String hql = "SELECT d FROM DirectorioExterno d WHERE d.estReg = 'A'";// 
//            //String hql = "SELECT t FROM Trabajador t join fetch t.persona p WHERE t.orgId =:p1"; 
//            Query query = session.createQuery(hql);                        
//            direx = query.list();
//            t.commit();
//        
//        }catch(Exception e){
//            t.rollback();
//            System.out.println("No se pudo Listar el Directorio Externo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo Listar el Directorio Externo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
//        return direx;
//    }
    
//    @Override
//    public boolean eliminarxId(Integer dexId){
//        
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction tx = session.beginTransaction();
//        
//        DirectorioExterno d = (DirectorioExterno)session.load(DirectorioExterno.class, dexId);//new DirectorioExterno(3);
//        d.setEstReg("I");
//        
//        try {
//                session.update(d);                                
//                tx.commit();                                
//            } catch (Exception e) {
//                tx.rollback();
//                return false;
//            }
//        
//        session.close();
//        return true;
//        
//    }
}
