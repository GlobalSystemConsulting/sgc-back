/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public interface UsuarioDao extends GenericDao<Usuario>{
    
    public List<Usuario> buscarConRolYOrganizacion(int limit, int offset);
    public List<Usuario> buscarConRolPorOrganizacion(int orgID, int limit, int offset);
    public List<UsuarioSession> buscarConRolPorArea(int areID);
    public List<UsuarioSession> buscarPorUsuario(String nombre);    
    public Usuario buscarPorUsuarioYPassword(String nombre , String password);
    public Usuario buscarPorId(int userId);
    
    public void eliminarSessiones(int usuarioID,int usuarioModificadorID);
    public void insertarSession(UsuarioSession session);
    public void cambiarPassword(int usuarioID,String password);
    
    public List<Usuario> busquedaParametrizada(int limit, int offset, String nomTabla, String nomCampo, String dataCampo);
    public long canRegBusqPar(String nomTabla, String nomCampo, String dataCampo);
    
    List<Usuario> searchWithFilters(int limit, int offset, JSONArray filters);
    long canRegBusqxFiltros(JSONArray filters);
}

