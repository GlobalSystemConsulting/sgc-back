/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

//import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Organizacion;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "gestion_educativa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GestionEducativa.findAll", query = "SELECT g FROM GestionEducativa g"),
    @NamedQuery(name = "GestionEducativa.findByGesId", query = "SELECT g FROM GestionEducativa g WHERE g.gesId = :gesId"),
    @NamedQuery(name = "GestionEducativa.findByGesAli", query = "SELECT g FROM GestionEducativa g WHERE g.gesAli = :gesAli"),
    @NamedQuery(name = "GestionEducativa.findByGesNom", query = "SELECT g FROM GestionEducativa g WHERE g.gesNom = :gesNom")})
public class GestionEducativa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ges_id")
    private Short gesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ges_ali")
    private String gesAli;
    @Size(max = 100)
    @Column(name = "ges_nom")
    private String gesNom;
    @OneToMany(mappedBy = "gesId")
    private List<Organizacion> organizacionList;

    public GestionEducativa() {
    }

    public GestionEducativa(Short gesId) {
        this.gesId = gesId;
    }

    public GestionEducativa(Short gesId, String gesAli) {
        this.gesId = gesId;
        this.gesAli = gesAli;
    }

    public Short getGesId() {
        return gesId;
    }

    public void setGesId(Short gesId) {
        this.gesId = gesId;
    }

    public String getGesAli() {
        return gesAli;
    }

    public void setGesAli(String gesAli) {
        this.gesAli = gesAli;
    }

    public String getGesNom() {
        return gesNom;
    }

    public void setGesNom(String gesNom) {
        this.gesNom = gesNom;
    }

    @XmlTransient
    public List<Organizacion> getOrganizacionList() {
        return organizacionList;
    }

    public void setOrganizacionList(List<Organizacion> organizacionList) {
        this.organizacionList = organizacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gesId != null ? gesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GestionEducativa)) {
            return false;
        }
        GestionEducativa other = (GestionEducativa) object;
        if ((this.gesId == null && other.gesId != null) || (this.gesId != null && !this.gesId.equals(other.gesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.GestionEducativa[ gesId=" + gesId + " ]";
    }
    
}
