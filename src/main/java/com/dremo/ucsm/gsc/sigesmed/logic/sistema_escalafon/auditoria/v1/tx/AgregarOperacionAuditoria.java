/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.LogAuditoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.LogAuditoria;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class AgregarOperacionAuditoria {
    private static final Logger logger = Logger.getLogger(AgregarOperacionAuditoria.class.getName());
    public static boolean agregarAltaAuditoria(String obsOpe, String conOpe, int usuId, String usuEmi, String ipAcc){
        LogAuditoria logAuditoria = null;
        try {
            //Character tipOpe, String obsOpe, String conOpe, Date fecEmi, int usuId, String usuEmi, String ipAcc
            logAuditoria = new LogAuditoria('A', obsOpe, conOpe, new Date(), usuId, usuEmi, ipAcc);
            LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao) FactoryDao.buildDao("LogAuditoriaDao");
            logAuditoriaDao.insert(logAuditoria);
            return true;
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar Alta de log de auditoria",e);
            System.out.println(e);
            return false;
        }
    }
    public static boolean agregarBajaAuditoria(String obsOpe, String conOpe, int usuId, String usuEmi, String ipAcc){
        LogAuditoria logAuditoria = null;
        try {
            //Character tipOpe, String obsOpe, String conOpe, Date fecEmi, int usuId, String usuEmi, String ipAcc
            logAuditoria = new LogAuditoria('B', obsOpe, conOpe, new Date(), usuId, usuEmi, ipAcc);
            LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao) FactoryDao.buildDao("LogAuditoriaDao");
            logAuditoriaDao.insert(logAuditoria);
            return true;
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar Baja de log de auditoria",e);
            System.out.println(e);
            return false;
        }
    }
    public static boolean agregarCambioAuditoria(String obsOpe, String conOpe, int usuId, String usuEmi, String ipAcc){
        LogAuditoria logAuditoria = null;
        try {
            //Character tipOpe, String obsOpe, String conOpe, Date fecEmi, int usuId, String usuEmi, String ipAcc
            logAuditoria = new LogAuditoria('C', obsOpe, conOpe, new Date(), usuId, usuEmi, ipAcc);
            LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao) FactoryDao.buildDao("LogAuditoriaDao");
            logAuditoriaDao.insert(logAuditoria);
            return true;
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar Cambio de log de auditoria",e);
            System.out.println(e);
            return false;
        }
    }
    public static boolean agregarLecturaAuditoria(String obsOpe, String conOpe, int usuId, String usuEmi, String ipAcc){
        LogAuditoria logAuditoria = null;
        try {
            //Character tipOpe, String obsOpe, String conOpe, Date fecEmi, int usuId, String usuEmi, String ipAcc
            logAuditoria = new LogAuditoria('L', obsOpe, conOpe, new Date(), usuId, usuEmi, ipAcc);
            LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao) FactoryDao.buildDao("LogAuditoriaDao");
            logAuditoriaDao.insert(logAuditoria);
            return true;
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar Lectura de log de auditoria",e);
            System.out.println(e);
            return false;
        }
    }
}
