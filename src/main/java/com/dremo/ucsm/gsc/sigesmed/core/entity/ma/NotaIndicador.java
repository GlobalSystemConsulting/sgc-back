package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

/**
 * Created by Administrador on 17/01/2017.
 */
public class NotaIndicador implements java.io.Serializable {
    private Integer notevaindid;
    private Integer indaprid;
    private String nomind;
    private Integer graieestid;
    private String notaeva;
    private Character tipnot;

    public NotaIndicador() {
    }

    public NotaIndicador(Integer notevaindid, Integer indaprid, String nomind, Integer graieestid, String notaeva, Character tipnot) {
        this.notevaindid = notevaindid;
        this.indaprid = indaprid;
        this.nomind = nomind;
        this.graieestid = graieestid;
        this.notaeva = notaeva;
        this.tipnot = tipnot;
    }

    public Integer getNotevaindid() {
        return notevaindid;
    }

    public void setNotevaindid(Integer notevaindid) {
        this.notevaindid = notevaindid;
    }

    public Integer getIndaprid() {
        return indaprid;
    }

    public void setIndaprid(Integer indaprid) {
        this.indaprid = indaprid;
    }

    public String getNomind() {
        return nomind;
    }

    public void setNomind(String nomind) {
        this.nomind = nomind;
    }

    public Integer getGraieestid() {
        return graieestid;
    }

    public void setGraieestid(Integer graieestid) {
        this.graieestid = graieestid;
    }

    public String getNotaeva() {
        return notaeva;
    }

    public void setNotaeva(String notaeva) {
        this.notaeva = notaeva;
    }

    public Character getTipnot() {
        return tipnot;
    }

    public void setTipnot(Character tipnot) {
        this.tipnot = tipnot;
    }
}
