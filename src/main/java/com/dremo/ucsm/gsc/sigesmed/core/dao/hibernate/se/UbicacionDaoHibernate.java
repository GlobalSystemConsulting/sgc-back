/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class UbicacionDaoHibernate extends GenericDaoHibernate<Ubicacion> implements UbicacionDao{

    @Override
    public List<Ubicacion> listarxZona() {
        List<Ubicacion> zonas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT z from Ubicacion as z "
                    + "WHERE z.estReg='A'";
            Query query = session.createQuery(hql);
            zonas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los zonas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las zonas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return zonas;
    }
    @Override
    public Ubicacion buscarPorId(Integer zonId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Ubicacion item = (Ubicacion)session.get(Ubicacion.class, zonId);
        session.close();
        return item;
    }
    
    @Override
    public Ubicacion obtenerDetalle(int ubiId) {
        Ubicacion ubicacion = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT u from Ubicacion as u "
                    + "WHERE u.catId='"+ubiId+"' "
                    + "AND u.estReg='A'";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            ubicacion = (Ubicacion) query.uniqueResult();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los ubicacion \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las ubicacion \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return ubicacion;
    }
}
