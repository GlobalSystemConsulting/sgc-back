///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;
//
//import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
//import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
//import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
//import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
//import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
//import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
//import com.itextpdf.io.font.FontConstants;
//import com.itextpdf.kernel.font.PdfFont;
//import com.itextpdf.kernel.font.PdfFontFactory;
//import com.itextpdf.layout.element.Paragraph;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.json.JSONObject;
//
///**
// *
// * @author gscadmin
// */
//public class Modelos12ReporteFichaEscalafonariaTx implements ITransaction{
//
//    private static final Logger logger = Logger.getLogger(ReporteFichaEscalafonariaTx.class.getName());
//
//    @Override
//    public WebResponse execute(WebRequest wr) {
//
//        JSONObject data = (JSONObject) wr.getData();
//        Integer ficEscId = data.optInt("ficEscId");
//        Integer traId = data.optInt("traId");
//        String perDni = data.optString("perDni");
//        Integer selected = data.optInt("selected");
//        
//        return crearReporte(ficEscId, traId, perDni, selected);
//    }
//
//    private WebResponse crearReporte(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) {
//        int tipoReporte = seleccionados;
//        String data = "";
//        try{
//            switch(tipoReporte){
//            case 1:  data = crearArchivo1(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 2:  data = crearArchivo2(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 3:  data = crearArchivo3(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 4:  data = crearArchivo4(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 5:  data = crearArchivo5(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 6:  data = crearArchivo6(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 7:  data = crearArchivo7(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 8:  data = crearArchivo8(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 9:  data = crearArchivo9(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 10: data = crearArchivo10(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 11: data = crearArchivo11(ficEscId, traId, perDni, seleccionados);
//                     break;
//            case 12: data = crearArchivo12(ficEscId, traId, perDni, seleccionados);
//                     break;
//            default: data = crearArchivo1(ficEscId, traId, perDni, seleccionados);
//                     break;
//            }  
//            
//            return WebResponse.crearWebResponseExito("", new JSONObject().put("file",data));
//        }catch (Exception e){
//            logger.log(Level.SEVERE,"crearReporte",e);
//            return WebResponse.crearWebResponseError("No se puede crear el reporte");
//        }
//    }
//    private String crearArchivo1(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("LICENCIAS SIN GOCE DE REMUNERACIONES POR MOTIVOS PARTICULARES (DOCENTES Y AUXILIARES)");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. SITUACION ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//       if(true){
//                Paragraph p = new Paragraph("VIII. LICENCIAS POR MOTIVOS PARTICULARES DE LOS ULTIMOS DOCE MESES A LA FECHA DE SOLICITUD DE LA LICENCIA");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo2(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("SUBSIDIO POR FALLECIMIENTO Y/O GASTOS DE SEPELIO (PROFESORES Y AUXILIARES DE EDUCACION - NOMBRADO O PENSIONISTA)");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. CARGO ACTUAL O DESEMPEÑADO AL CESE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. GRUPO OCUPACIONAL O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION QUE OTORGA PENSION DEFINITIVA DE CESANTIA");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE CARGO ACTUAL O AL CESE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo3(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("CESE");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. LUGAR Y FECHA DE NACIMIENTO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.setWidthPercent(100); 
//                gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. DOCUMENTO NACIONAL DE IDENTIDAD");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. TITULO PROFESIONAL Y/O TECNICO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. RESOLUCION(ES) DE CONTRATOS O NOMBRAMIENTOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. RESOLUCION DE CARGOS DESEMPEÑADOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("VIII. CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("X. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XI. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIII. REGIMEN PENSIONARIO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "REGIMEN", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIV. RESOLUCION(ES) DE QUINQUENIOS O BONIFICACION PERSONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "QUINQUENIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XV. RESOLUCION(ES) DE LICENCIAS SIN GOCE DE REMUNERACIONES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "LICENCIAS", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVI. RESOLUCION DE ACUMULACION DE AÑOS DE FORMACION PROFESIONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVII. RESOLUCION DE INCORPORACION AL DECRETO DE LEY 20530");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XVIII. RESOLUCION DE DEMERITOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle = {10, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D", 
//                    "RESUELVE SEPARACION DEFINITIVA Y/O TEMPORAL DEL CARGO","FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});
//
//                if (demeritos != null){
//                    for(Demerito d:demeritos){
//                        gtabla.processLine(new String[]{d.getEntEmi(), d.getNumDoc(), d.getFecDoc().toString(), d.getSep()?"SI":"NO", d.getFecIni().toString(), 
//                            d.getFecFin().toString(), d.getMot()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XIX. OTROS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo4(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("OTORGAMIENTO DE PENSION DE CESANTIA");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. LUGAR Y FECHA DE NACIMIENTO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.setWidthPercent(100); 
//                gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. DOCUMENTO NACIONAL DE IDENTIDAD");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. TITULO PROFESIONAL Y/O TECNICO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. RESOLUCION(ES) DE CONTRATOS O NOMBRAMIENTOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. RESOLUCION DE CARGOS DESEMPEÑADOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION DE CARGO AL CESE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("X. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XI. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIII. REGIMEN PENSIONARIO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "REGIMEN", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIV. RESOLUCION(ES) DE QUINQUENIOS O BONIFICACION PERSONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "QUINQUENIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XV. RESOLUCION(ES) DE LICENCIAS SIN GOCE DE REMUNERACIONES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "LICENCIAS", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVI. RESOLUCION DE ACUMULACION DE AÑOS DE FORMACION PROFESIONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVII. RESOLUCION DE INCORPORACION AL DECRETO DE LEY 20530");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XVIII. RESOLUCION DE DEMERITOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle = {10, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D", 
//                    "RESUELVE SEPARACION DEFINITIVA Y/O TEMPORAL DEL CARGO","FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});
//
//                if (demeritos != null){
//                    for(Demerito d:demeritos){
//                        gtabla.processLine(new String[]{d.getEntEmi(), d.getNumDoc(), d.getFecDoc().toString(), d.getSep()?"SI":"NO", d.getFecIni().toString(), 
//                            d.getFecFin().toString(), d.getMot()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XIV. RESOLUCION DE CESE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle = {10, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D", 
//                    "FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});
//
//                if (demeritos != null){
//                    for(Demerito d:demeritos){
//                        gtabla.processLine(new String[]{d.getEntEmi(), d.getNumDoc(), d.getFecDoc().toString(), d.getSep()?"SI":"NO", d.getFecIni().toString(), 
//                            d.getFecFin().toString(), d.getMot()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XX. OTROS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo5(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("LICENCIAS CON GOCE DE REMUNERACIONES POR INCAPACIDAD TEMPORAL");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. SITUACION ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//       if(true){
//                Paragraph p = new Paragraph("VIII. LICENCIAS POR INCAPACIDAD TEMPORAL PARA EL TRABAJO CONCEDIDO EN LOS ULTIMOS 12 MESES A LA FECHA DE SOLICITUD (DESAGREGADO MINEDU Y ESSALUD)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo6(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("OTRAS LICENCIAS CON GOCE DE REMUNERACIONES");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. SITUACION ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//       if(true){
//                Paragraph p = new Paragraph("VIII. LICENCIAS CON GOCE DE REMUNERACIONES CONCEDIDAS EN LOS 12 ULTIMOS MESES A LA FECHA DE SOLICITUD");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo7(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("PENSION DE SOBREVIVIENTE");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. LUGAR Y FECHA DE NACIMIENTO DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.setWidthPercent(100); 
//                gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. DOCUMENTO NACIONAL DE IDENTIDAD DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. TITULO PROFESIONAL Y/O TECNICO DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. RESOLUCION(ES) DE CONTRATOS O NOMBRAMIENTOS DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. RESOLUCION DE CARGOS DESEMPEÑADOS DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION DEL CARGO AL CESE DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("IX. REGIMEN LABORAL DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "REGIMEN", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("X. GRUPO OCUPACIONAL Y/O ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XI. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XII. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIII. REGIMEN PENSIONARIO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        
//        
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIV. RESOLUCION(ES) DE QUINQUENIOS O BONIFICACION PERSONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "QUINQUENIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XV. RESOLUCION(ES) DE LICENCIAS SIN GOCE DE REMUNERACIONES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "LICENCIAS", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVI. RESOLUCION DE ACUMULACION DE AÑOS DE FORMACION PROFESIONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XVII. RESOLUCION DE INCORPORACION AL DECRETO DE LEY 20530");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XVIII. RESOLUCION DE DEMERITOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle = {10, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D", 
//                    "RESUELVE SEPARACION DEFINITIVA Y/O TEMPORAL DEL CARGO","FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});
//
//                if (demeritos != null){
//                    for(Demerito d:demeritos){
//                        gtabla.processLine(new String[]{d.getEntEmi(), d.getNumDoc(), d.getFecDoc().toString(), d.getSep()?"SI":"NO", d.getFecIni().toString(), 
//                            d.getFecFin().toString(), d.getMot()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//        if(true){
//                Paragraph p = new Paragraph("XIX. RESOLUCION DE CESE DEL CAUSANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "REGIMEN", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }//Datos personales
//        if(true){
//                Paragraph p = new Paragraph("XX. APELLIDOS Y NOMBRES DEL SOLICITANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("XXI. DOCUMENTO NACIONAL DE IDENTIDAD DEL SOLICITANTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Demeritos
//        if(true){
//                Paragraph p = new Paragraph("XXII. OTROS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo8(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("ASCENSO DE ESCALA MAGISTERIAL Y/O REUBICACION");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. DOCUMENTO NACIONAL DE IDENTIDAD");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.setWidthPercent(100); 
//                gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("IV. NUMERO RESOLUCION INDICANDO EL CARGO SEGUN EL NOMBRAMIENTO O CARGO ACTUAL, INDICAR SI SE ENCUENTRA DESTACADO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("V. MODALIDAD SEGUN SU NOMBRAMIENTO O CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VI. NIVEL/CICLO SEGUN SU NOMBRAMIENTO O CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("VII. AREA/FAMILIA PROFESIONAL SEGUN SU NOMBRAMIENTO O CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VIII. INSTITUCION EDUCATIVA, JURISDICCION Y AMBITO DONDE SE ENCUENTRE NOMRADO O CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("IX. ESCALA MAGISTERIAL ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("X. TITULO PEDAGOGICO Y ESPECIALIDAD");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("XI. REGIMEN LABORAL ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("XII. REGIMEN LABORAL ANTERIOR (NOMBRADO O INCORPORADO LEYES N°24029 Y 29082)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("XIII. JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("XIV. LICENCIAS SIN GOCE DE REMUNERACIONES, SI SE ENCUENTRA HACIENDO USO, EN CASO DE EXISTIR LICENCIA SIN GOCE, PRECISAR EL MOTIVO Y VIGENCIA");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XV. TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo9(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("PASE DRE O UGEL PARA REASIGNACION Y PERMUTA");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("III. TITULO PROFESIONAL O TECNICO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
//                gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
//
//                if (formacionesEducativas != null){
//                   for(FormacionEducativa fe:formacionesEducativas){
//                        gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. CARGO ACTUAL ");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("V. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("VI. ESCALA MAGISTERIAL Y/O GRUPO OCUPACIONAL ");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VII. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VIII. UBICACION DEL CENTRO EDUCATIVO; ESPECIFICAR ZONA RURAL O URBANA");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. TIEMPO DE SERVICIOS EN EL ULTIMO CARGO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("X. TIEMPO DE SERVICIOS TOTAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XI. ANTECEDENTES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XII. RESOLUCION DEL ULTIMO CARGO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        /*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIII. RESOLUCION DE LICENCIA SIN GOCE DE REMUNERACIONES(SI SE ENCUENTRA EN USO)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("XIV. RESOLUCION DE DEMERITO (SI SE ENCUENTRA CUMPLIENDO ACTUALMENTE)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo10(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("RESOLUCION DE ASIGNACIONES E INCENTIVO LEY N°29944");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. CARGO DESEMPEÑADO ACTUALMENTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. TIEMPO DE SERVICIOS (FECHA EN LA QUE CUMPLE EL RECONOCIMIENTO POR ASIGNACION POR TIEMPO DE SERVICIOS POR 25 O 30 AÑOS)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       //Exposiciones y/o Ponencias
//       if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION ED LA ULTIMA ASIGNACION POR TIEMPO DE SERVICIOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        if(true){
//                Paragraph p = new Paragraph("X. RESOLUCION DE PAGO POR MAESTRIA (LEY N°24029)");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        if(true){
//                Paragraph p = new Paragraph("XI. GRADO ALCANZADO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo11(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("RESOLUCION DE INSTAURACION DE PROCESO ADMINISTRATIVO, DEMERITOS Y RECURSOS ADMINISTRATIVOS");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. CARGO DESEMPEÑADO ACTUALMENTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. ESCALA MAGISTERIAL Y/O GRUPO OCUPACIONAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. NIVEL REMUNERATIVO Y/O JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. RESOLUCION DE DEMERITOS QUE SE ENCUENTRA CUMPLIENDO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "DEMERITO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE INSTAURACION DE PROCESO ADMINISTRATIVO");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        if(true){
//                Paragraph p = new Paragraph("X. RESOLUCION DE RECURSOS ADMINISTRATIVOS");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String crearArchivo12(Integer ficEscId, Integer traId, String perDni, Integer seleccionados) throws Exception{
//        System.out.println("Seleccionados: " + seleccionados); 
//        
//        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
//        
//        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
//        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
//        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
//        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
//        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
//        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
//        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
//        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
//        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
//        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
//        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
//        
//        
//        FichaEscalafonaria datosGenerales = null;
//        
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
//        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
//        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
//        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
//        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
//        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
//        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
//        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
//        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
//        List<Demerito> demeritos = new ArrayList<Demerito>();
//        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
//        List<Ascenso> ascensos = new ArrayList<Ascenso>();
//        
//        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
//        
//        parientes = parientesDao.listarxTrabajador(traId);
//        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
//        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
//        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
//        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
//        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
//        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
//        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
//        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
//        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
//        
//        
//        Mitext mitext = new Mitext(true,"ESCALAFON");
//        mitext.agregarTitulo("INFORME POR PERMANENCIA ANUAL");
//
//        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
//        
//        //Datos personales
//        if(true){
//                Paragraph p = new Paragraph("I. APELLIDOS Y NOMBRES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float [] columnsWidths = new float[] {5, 5, 5};//15
//                GTabla gtabla = new GTabla(columnsWidths);
//                gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
//                gtabla.setWidthPercent(100);
//                gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
//                    datosGenerales.getTrabajador().getPersona().getNom() });
//                /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
//                mitext.agregarTabla(gtabla);
//
//        }/*
//        
//        */
//        //Datos familiares
//        if(true){
//                Paragraph p = new Paragraph("II. CODIGO MODULAR");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Formaciones educativas
//        if(true){
//                Paragraph p = new Paragraph("III. CARGO DESEMPEÑADO ACTUALMENTE");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Colegiaturas
//        if(true){
//                Paragraph p = new Paragraph("IV. ESCALA MAGISTERIAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//        }/*
//        
//        */
//        //Estudios de especializacion
//        if(true){
//                Paragraph p = new Paragraph("V. JORNADA LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Conocimientos informaticos
//        if(true){
//                Paragraph p = new Paragraph("VI. REGIMEN LABORAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//        }/*
//        
//        */
//        //Idiomas
//        if(true){
//                Paragraph p = new Paragraph("VII. RESOLUCION DE DEMERITOS EN LOS ULTIMOS 12 MESES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
//                gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "DEMERITO", "ENTIDAD QUE EMITE RESOLUCION"});
//
//                if (bonificaciones != null){
//                    for(Reconocimiento b:bonificaciones){
//                        gtabla.processLine(new String[]{b.getNumDoc(), b.getFecDoc().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
//                    } 
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//       
//        //Publicaciones
//        if(true){
//                Paragraph p = new Paragraph("VIII. RESOLUCION DE CARGO ACTUAL");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }/*
//        
//        */
//        if(true){
//                Paragraph p = new Paragraph("IX. RESOLUCION DE INSTAURACION DE PROCESO ADMINISTRATIVO EN LOS ULTIMOS 12 MESES");
//                p.setKeepTogether(true);
//                p.setFont(bold).setFontSize(12);
//                mitext.getDocument().add(p);
//
//                float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
//                GTabla gtabla = new GTabla(colWidthsTitle1);
//                gtabla.setWidthPercent(100);
//                gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
//                "HORAS", "FECHA INICIO", "FECHA TERMINO"});
//
//                if (desplazamientos != null){
//                    for(Desplazamiento d:desplazamientos){
//                        gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumDoc(), d.getFecDoc().toString(), d.getInsEdu(), d.getCar(), 
//                            d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
//                    }
//                }
//                mitext.agregarTabla(gtabla);
//        }
//        
//        mitext.cerrarDocumento();
//        return mitext.encodeToBase64();
//    }
//    
//    private String mostrarMotivo(Character m){
//        switch (m){
//            case '1': return "Mérito";
//            case '2': return "Felicitación";
//            case '3': return "25 años de servicios";
//            case '4': return "30 años de servicios";
//            case '5': return "Luto y sepelio";
//            default: return "Otros";
//        }
//    }
//    private String mostrarTipoDesplazamiento(Character t){
//        switch (t){
//            case '1': return "Designación";
//            case '2': return "Rotación";
//            case '3': return "Reasignación";
//            case '4': return "Destaque";
//            case '5': return "Permuta";
//            case '6': return "Encargo";
//            case '7': return "Comisión de servicio";
//            case '8': return "Transferencia";
//            default: return "Otros";
//        }
//    }
//    
//    private String mostrarTipoEstudioComplementario(Character ec){
//        switch (ec){
//            case '1': return "Informatica";
//            case '2': return "Idiomas";
//            case '3': return "Certificación";
//            case '4': return "Diplomado";
//            case '5': return "Especialización";
//            default: return "Otros";
//        }
//    }
//    
//    private String mostrarNivel(Character n){
//        switch (n){
//            case 'B': return "Básico";
//            case 'I': return "Intermedio";
//            case 'A': return "Avanzado";
//            default: return "Ninguno";
//        }
//    }  
//    
//    private String mostrarTipoFormacionEducativa(Character t){
//        switch (t){
//            case '1': return "Estudios Basicos Regulares";
//            case '2': return "Estudios Tecnicos";
//            case '3': return "Bachiller";
//            case '4': return "Universitario";
//            case '5': return "Licenciatura";
//            case '6': return "Maestria";
//            case '7': return "Doctorado";
//            default: return "Otros";
//        }
//    }
//    
//    private Integer calcularEdad(Date fecNac) {
//        Date fechaActual = new Date();
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
//        String fecha_nac = formato.format(fecNac);
//        String hoy = formato.format(fechaActual);
//        String[] dat1 = fecha_nac.split("/");
//        String[] dat2 = hoy.split("/");
//        int anos = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]);
//        int mes = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);
//        if (mes < 0) {
//            anos = anos - 1;
//        } else if (mes == 0) {
//            int dia = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
//            if (dia > 0) {
//                anos = anos - 1;
//            }
//        }
//        return anos;
//    }
//
//}
