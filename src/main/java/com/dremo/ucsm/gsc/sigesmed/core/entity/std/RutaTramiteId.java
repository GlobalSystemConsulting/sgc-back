/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class RutaTramiteId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer tipoTramite;
    private int rutTraId;

    public RutaTramiteId() {
    }

    public RutaTramiteId(Integer tipoTramite, int rutTraId) {
        this.setTipoTramite(tipoTramite);
        this.setRutTraId(rutTraId);
    }

    @Override
    public int hashCode() {
        return ((this.getTipoTramite() == null
                ? 0 : this.getTipoTramite().hashCode())
                ^ ((int) this.getRutTraId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof RutaTramiteId)) {
            return false;
        }
        RutaTramiteId other = (RutaTramiteId) otherOb;
        return ((this.getTipoTramite() == null
                ? other.getTipoTramite() == null : this.getTipoTramite()
                .equals(other.getTipoTramite()))
                && (this.getRutTraId() == other.getRutTraId()));
    }

    @Override
    public String toString() {
        return "" + getTipoTramite() + "-" + getRutTraId();
    }

    public Integer getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(Integer tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public int getRutTraId() {
        return rutTraId;
    }

    public void setRutTraId(int rutTraId) {
        this.rutTraId = rutTraId;
    }
    
}
