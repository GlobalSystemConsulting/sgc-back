/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoLicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoLicencia;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TipoLicenciaDaoHibernate extends GenericDaoHibernate<TipoLicencia> implements TipoLicenciaDao{

    @Override
    public List<TipoLicencia> listarAll() {
        List<TipoLicencia> licencias = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from TipoLicencia as d "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            licencias = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las licencias \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las licencias \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return licencias;
    }

    @Override
    public TipoLicencia buscarPorId(Integer tipLicId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoLicencia des = (TipoLicencia)session.get(TipoLicencia.class, tipLicId);
        session.close();
        return des;
    }
    
}
