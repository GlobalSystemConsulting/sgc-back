/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarOrganigramaToTrabajadorTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarOrganigramaToTrabajadorTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();

            int traId = requestData.getInt("traId");
            int orgiId = requestData.getInt("orgiId");
            //int ubiId = requestData.getInt("ubiId");
            int plaId = requestData.getInt("plaId");
            int catId = requestData.getInt("catId");
            int carId = requestData.getInt("carId");
            
            return actualizarTrabajadorOrganigramaDet(traId,orgiId, plaId,catId,carId);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar trabajadorOrganigramaDetalle",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarTrabajadorOrganigramaDet(int traId,int orgiId, int plaId,int catId, int carId) {
        TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao)FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");  
        TrabajadorDao traDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
        Trabajador tra  = new Trabajador();
        TrabajadorOrganigramaDetalle traOrgDet = new TrabajadorOrganigramaDetalle();
        try{
            traOrgDetDao.eliminarRegistroBD(traId,orgiId);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"trabajadorOrganigramaDetalle-eliminación",e);
            return WebResponse.crearWebResponseError("Error, el registro trabajadorOrganigramaDetalle no fue eliminado");
        }
        
        try{            
            traOrgDet.setTraId(traId);
            traOrgDet.setOrgiId(orgiId);
            traOrgDet.setPlaId(plaId);
            traOrgDet.setCatId(catId);
            traOrgDet.setCarId(carId);
            traOrgDetDao.insert(traOrgDet);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"trabajadorOrganigramaDetalle-inserción",e);
            return WebResponse.crearWebResponseError("Error, el registro trabajadorOrganigramaDetalle no fue actualizado");
        }
        
        try {
            tra = traDao.buscarPorId(traId);
            tra.setOrgiId(orgiId);
            traDao.update(tra);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Actualizar orgiId en Trabajador ",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("orgiId", traOrgDet.getOrgiId());
        oResponse.put("plaId", traOrgDet.getPlaId());
        oResponse.put("catId", traOrgDet.getCatId());
        oResponse.put("carId", traOrgDet.getCarId());
        return WebResponse.crearWebResponseExito("Registro trabajadorOrganigramaDetalle actualizado exitosamente",oResponse);
    } 
    
}
