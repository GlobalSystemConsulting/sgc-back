/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public interface DemeritoDao extends GenericDao<Demerito>{
    public List<Demerito> listarxFichaEscalafonaria(int ficEscId);
    public Demerito buscarPorId(Integer demId);
    public JSONObject contarxOrganizacion(int orgId);
    
}
