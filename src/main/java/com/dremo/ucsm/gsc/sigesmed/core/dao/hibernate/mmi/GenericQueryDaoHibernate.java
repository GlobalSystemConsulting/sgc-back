package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class GenericQueryDaoHibernate {

    protected List<Object[]> sqlQuery(String sqlquery) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object[]> genericList = new ArrayList<>();
        Transaction t = session.beginTransaction();
        Query query;
        try {
            query = session.createSQLQuery(sqlquery);
            genericList = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            throw new UnsupportedOperationException("sqlquery error " + e.getMessage());
        } finally {
            session.close();
        }
        return genericList;
    }
    
    protected void ExecuteSqlQuery(String sqlquery) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        Query query;
        try {
            query = session.createSQLQuery(sqlquery);
            query.executeUpdate();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            throw new UnsupportedOperationException("sqlquery error " + e.getMessage());
        } finally {
            session.close();
        }
    }
}
