package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ValoresActitudes;

/**
 * Created by Administrador on 31/10/2016.
 */
public interface ValorProgramacionAnualDao extends GenericDao<ValoresActitudes> {
    ValoresActitudes buscarValorPorId(int idVal);
}
