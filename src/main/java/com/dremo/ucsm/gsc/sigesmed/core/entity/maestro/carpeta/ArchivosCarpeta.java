package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

/**
 * Created by Administrador on 28/12/2016.
 */
public class ArchivosCarpeta implements java.io.Serializable{
    private Integer carDigId;
    private Integer secCarPedId;
    private Integer ord;
    private String secNom;
    private Integer conSecCarPedId;
    private String conNom;
    private Integer rutConCarId;
    private Integer tipUsu;
    private String pat;
    private String nomFil;

    public ArchivosCarpeta() {
    }

    public Integer getCarDigId() {
        return carDigId;
    }

    public Integer getSecCarPedId() {
        return secCarPedId;
    }

    public Integer getOrd() {
        return ord;
    }

    public String getSecNom() {
        return secNom;
    }

    public Integer getConSecCarPedId() {
        return conSecCarPedId;
    }

    public String getConNom() {
        return conNom;
    }

    public Integer getRutConCarId() {
        return rutConCarId;
    }

    public Integer getTipUsu() {
        return tipUsu;
    }

    public String getPat() {
        return pat;
    }

    public String getNomFil() {
        return nomFil;
    }

    public void setCarDigId(Integer carDigId) {
        this.carDigId = carDigId;
    }

    public void setSecCarPedId(Integer secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public void setSecNom(String secNom) {
        this.secNom = secNom;
    }

    public void setConSecCarPedId(Integer conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public void setConNom(String conNom) {
        this.conNom = conNom;
    }

    public void setRutConCarId(Integer rutConCarId) {
        this.rutConCarId = rutConCarId;
    }

    public void setTipUsu(Integer tipUsu) {
        this.tipUsu = tipUsu;
    }

    public void setPat(String pat) {
        this.pat = pat;
    }

    public void setNomFil(String nomFil) {
        this.nomFil = nomFil;
    }
}
