/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "demerito", schema="administrativo")

public class Demerito implements Serializable {

    @Id
    @Column(name = "dem_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_demerito", sequenceName="administrativo.demerito_dem_id_seq" )
    @GeneratedValue(generator="secuencia_demerito")
    private Integer demId;
    
    @Column(name = "ent_emi")
    private String entEmi;
    
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "sep")
    private Boolean sep;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_fin")
    @Temporal(TemporalType.DATE)
    private Date fecFin;
    
    @Column(name = "mot")
    private String mot;
    
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Column(name = "dep")
    private String dep;
        
    @Column(name = "car")
    private String car;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Demerito() {
    }

    public Demerito(Integer demId) {
        this.demId = demId;
    }
    
    public Demerito(Persona persona, String entEmi, String numDoc, Date fecDoc, Integer tipDocId, Boolean sep, Date fecIni, Date fecFin, String mot, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.entEmi = entEmi;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.sep = sep;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.mot = mot;
        this.tipDocId=tipDocId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getDemId() {
        return demId;
    }

    public void setDemId(Integer demId) {
        this.demId = demId;
    }

    public String getEntEmi() {
        return entEmi;
    }

    public void setEntEmi(String entEmi) {
        this.entEmi = entEmi;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public Boolean getSep() {
        return sep;
    }

    public void setSep(Boolean sep) {
        this.sep = sep;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }
    
    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (demId != null ? demId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demerito)) {
            return false;
        }
        Demerito other = (Demerito) object;
        if ((this.demId == null && other.demId != null) || (this.demId != null && !this.demId.equals(other.demId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Demerito{" + "demId=" + demId + ", entEmi=" + entEmi + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", sep=" + sep + ", fecIni=" + fecIni + ", fecFin=" + fecFin + ", mot=" + mot + ", tipDocId=" + tipDocId + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", persona=" + persona + '}';
    }

    
}
