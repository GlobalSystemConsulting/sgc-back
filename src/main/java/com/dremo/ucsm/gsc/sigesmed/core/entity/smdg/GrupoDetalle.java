/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "grupo_detalle")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "GrupoDetalle.findAll", query = "SELECT g FROM GrupoDetalle g"),
//    @NamedQuery(name = "GrupoDetalle.findByGdeId", query = "SELECT g FROM GrupoDetalle g WHERE g.gdeId = :gdeId"),
//    @NamedQuery(name = "GrupoDetalle.findByFecMod", query = "SELECT g FROM GrupoDetalle g WHERE g.fecMod = :fecMod"),
//    @NamedQuery(name = "GrupoDetalle.findByUsuMod", query = "SELECT g FROM GrupoDetalle g WHERE g.usuMod = :usuMod"),
//    @NamedQuery(name = "GrupoDetalle.findByEstReg", query = "SELECT g FROM GrupoDetalle g WHERE g.estReg = :estReg")})
public class GrupoDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gde_id")
    private Integer gdeId;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @JoinColumn(name = "tit_id", referencedColumnName = "tit_id")
    @ManyToOne
    private TipoItem titId;
    @JoinColumn(name = "tin_id", referencedColumnName = "tin_id")
    @ManyToOne
    private TipoIndicador tinId;
    @JoinColumn(name = "pgr_id", referencedColumnName = "pgr_id")
    @ManyToOne
    private PlantillaGrupo pgrId;

    public GrupoDetalle() {
    }

    public GrupoDetalle(Integer gdeId) {
        this.gdeId = gdeId;
    }

    public Integer getGdeId() {
        return gdeId;
    }

    public void setGdeId(Integer gdeId) {
        this.gdeId = gdeId;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public TipoItem getTitId() {
        return titId;
    }

    public void setTitId(TipoItem titId) {
        this.titId = titId;
    }

    public TipoIndicador getTinId() {
        return tinId;
    }

    public void setTinId(TipoIndicador tinId) {
        this.tinId = tinId;
    }

    public PlantillaGrupo getPgrId() {
        return pgrId;
    }

    public void setPgrId(PlantillaGrupo pgrId) {
        this.pgrId = pgrId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gdeId != null ? gdeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoDetalle)) {
            return false;
        }
        GrupoDetalle other = (GrupoDetalle) object;
        if ((this.gdeId == null && other.gdeId != null) || (this.gdeId != null && !this.gdeId.equals(other.gdeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.GrupoDetalle[ gdeId=" + gdeId + " ]";
    }
    
}
