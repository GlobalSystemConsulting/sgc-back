/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarUsuarioSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Usuario usuarioAct = null;
        List<UsuarioSession> nuevasSessiones = new ArrayList<UsuarioSession>();
        List<Trabajador> trabajadores = new ArrayList<Trabajador>();
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int usuarioID = requestData.getInt("usuarioID");
            
            int perId = requestData.getInt("perId");
            
            String nombre = requestData.getString("nombreUsuario");
            String password = requestData.getString("password");
            String estado = requestData.getString("estado");
            
            JSONArray arraySession = requestData.getJSONArray("sessiones");
            
            //usuarioAct = new Usuario(usuarioID, new Organizacion(organizacionID), new Rol(rolID), nombre, password, new Date(), new Date(), 1, estado.charAt(0));
            usuarioAct = new Usuario(usuarioID, nombre, password, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));            
            usuarioAct.setSessiones(new ArrayList<UsuarioSession>());
            
            for(int i=0;i<arraySession.length();i++ ){
                JSONObject o = arraySession.getJSONObject(i);
                
                int sessionID = o.optInt("sessionID");
                int rolID = o.getInt("rolID");
                int organizacionID = o.getInt("organizacionID");
                estado = o.getString("estado");
                
                int areaID = o.optInt("areaID");
                
                
                UsuarioSession session = new UsuarioSession(sessionID, new Organizacion(organizacionID), new Rol(rolID), usuarioAct, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
                
                //si tiene una area
                if(areaID>0)
                    session.setAreId(areaID);
                
                if(sessionID>0){                    
                    usuarioAct.getSessiones().add( session );
                }
                else{
                    nuevasSessiones.add( session );
                    Trabajador trabajador = new Trabajador();
                    trabajador.setPersona(new Persona(perId));
                    trabajador.setOrganizacion(session.getOrganizacion());
                    trabajador.setRol(session.getRol());
                    trabajador.setEstReg("A");
                    trabajador.setEstLab('1');
                    trabajadores.add(trabajador);
                }
                
            }
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarCambioAuditoria("Se modifico trabajador", "Nombre Usuario: "+nombre, wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////   
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("TrabajadorDao");
        FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("FichaEscalafonariaDao");
        try{
            usuarioDao.eliminarSessiones(usuarioAct.getUsuId(), wr.getIdUsuario());
            usuarioDao.update(usuarioAct);
            
            for(int i=0;i<nuevasSessiones.size();i++){
                usuarioDao.insertarSession(nuevasSessiones.get(i));
                trabajadorDao.insert(trabajadores.get(i));
                FichaEscalafonaria ficha = new FichaEscalafonaria();
                ficha.setTrabajador(trabajadores.get(i));
                ficha.setEstReg('A');
                fichaDao.insert(ficha);
            }
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Usuario del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Usuario del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Usuario del Sistema se actualizo correctamente");
        //Fin
    }
    
}
