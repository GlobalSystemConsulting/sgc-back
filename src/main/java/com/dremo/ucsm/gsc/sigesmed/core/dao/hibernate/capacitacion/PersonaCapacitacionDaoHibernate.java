package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class PersonaCapacitacionDaoHibernate extends GenericDaoHibernate<Persona> implements PersonaCapacitacionDao {

    private static final Logger logger = Logger.getLogger(PersonaCapacitacionDaoHibernate.class.getName());

    @Override
    public List<Persona> listarPersonasCapacitacion() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return session.createCriteria(Persona.class).list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarPersonasCapacitacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Persona buscarPorDni(String dni) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(Persona.class)
                    .add(Restrictions.eq("dni", dni));

            return (Persona) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorDni", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Persona buscarPorId(int codPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (Persona) session.get(Persona.class, codPer);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
