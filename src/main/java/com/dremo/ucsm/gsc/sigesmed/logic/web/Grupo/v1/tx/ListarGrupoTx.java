/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.GrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarGrupoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        boolean listarConUsuarios = false;
        int organizacionID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            organizacionID = requestData.getInt("organizacionID");
            listarConUsuarios = requestData.optBoolean("listar");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Grupos por organizacion", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Grupo> grupos = null;
        GrupoDao grupoDao = (GrupoDao)FactoryDao.buildDao("web.GrupoDao");
        try{
            if(listarConUsuarios)
                grupos = grupoDao.buscarConUsuariosPorOrganizacion(organizacionID);
            else
                grupos = grupoDao.buscarPorOrganizacion(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Grupos por organizacion \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Grupos por organizacion", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(Grupo grupo:grupos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("grupoID",grupo.getGruId() );
            oResponse.put("tipo",grupo.getTipGru());
            oResponse.put("nombre",grupo.getNom());
            oResponse.put("organizacionID",grupo.getOrgId());
            oResponse.put("fecha",grupo.getFecMod().toString());
            oResponse.put("estado",""+grupo.getEstReg());
            oResponse.put("i",i++);
            
            List<UsuarioSession> usuarios = grupo.getUsuarios();            
            if( listarConUsuarios && usuarios.size() > 0 ){
                JSONArray aUsuarios = new JSONArray();
                for( UsuarioSession session:usuarios ){
                    
                    JSONObject oSession = new JSONObject();
                    oSession.put("sessionID",session.getUsuSesId() );
                    oSession.put("usuarioID",session.getUsuId() );
                    oSession.put("rolID",session.getRolId() );
                    oSession.put("areaID",session.getAreId());
                    oSession.put("estado",""+session.getEstReg());

                    aUsuarios.put(oSession);
                }
                oResponse.put("usuarios",aUsuarios);
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

