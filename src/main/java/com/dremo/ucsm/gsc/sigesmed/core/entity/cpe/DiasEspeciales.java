package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="dias_especiales" ,schema="administrativo")
public class DiasEspeciales  implements java.io.Serializable {


    @Id
    @Column(name="dia_esp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_dias_especiales", sequenceName="administrativo.dias_especiales_dia_esp_id_seq" )
    @GeneratedValue(generator="secuencia_dias_especiales")
    private Integer diaEspId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false )
    private Organizacion organizacionId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dia_esp_fec")
    private Date fecha;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reg")
    private Date fechaRegistro;
    
    @Column(name="dia_esp_des")
    private String descripcion;
    
    @Column(name="est_reg")
    private String estReg;
    
    @Column(name="usu_reg")
    private Integer usuReg;

    public DiasEspeciales() {
    }

    public DiasEspeciales(Integer diaEspId) {
        this.diaEspId = diaEspId;
    }

    public DiasEspeciales(Organizacion organizacionId, Date fecha, Date fechaRegistro, String descripcion, String estReg,Integer usuReg) {
        this.organizacionId = organizacionId;
        this.fecha = fecha;
        this.fechaRegistro = fechaRegistro;
        this.descripcion = descripcion;
        this.estReg = estReg;
        this.usuReg=usuReg;
    }

    public Integer getDiaEspId() {
        return diaEspId;
    }

    public void setDiaEspId(Integer diaEspId) {
        this.diaEspId = diaEspId;
    }

    public Organizacion getOrganizacionId() {
        return organizacionId;
    }

    public void setOrganizacionId(Organizacion organizacionId) {
        this.organizacionId = organizacionId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getUsuReg() {
        return usuReg;
    }

    public void setUsuReg(Integer usuReg) {
        this.usuReg = usuReg;
    }

    
}


