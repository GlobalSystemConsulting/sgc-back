/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.sub_modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarSubModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        SubModuloSistema subModuloSistemaAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int moduloID = requestData.getInt("moduloID");
            int subModuloID = requestData.getInt("subModuloID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String icono = requestData.getString("icono");
            String estado = requestData.getString("estado");
            
            subModuloSistemaAct = new SubModuloSistema(subModuloID,new ModuloSistema(moduloID), codigo, nombre, descripcion, icono, new Date(), 1, estado.charAt(0), null);
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        SubModuloSistemaDao subModuloDao = (SubModuloSistemaDao)FactoryDao.buildDao("SubModuloSistemaDao");
        try{
            subModuloDao.update(subModuloSistemaAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar Sub Modulo del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Sub Modulo del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Sub Modulo Sistema se actualizo correctamente");
        //Fin
    }
    
}
