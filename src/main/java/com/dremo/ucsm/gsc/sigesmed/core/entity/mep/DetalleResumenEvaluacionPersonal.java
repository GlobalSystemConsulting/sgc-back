package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author geank
 */
@Entity
@Table(name="detalle_resume_evaluacion_personal",schema="pedagogico")
public class DetalleResumenEvaluacionPersonal implements Serializable {
    @Id
    @Column(name="det_res_eva_per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_resumen_sumas_escalas", sequenceName="pedagogico.detalle_resume_evaluacion_personal_det_res_eva_per_id_seq" )
    @GeneratedValue(generator="secuencia_resumen_sumas_escalas")
    private int detResEvaPerId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="det_eva_per_id")
    private ResumenEvaluacionPersonal resEvaPer;

    @Column(name = "sub_tot")
    private double subTot;

    @Column(name = "pun")
    private double pun;

    @Column(name = "tip", length = 1)
    private Character tip;

    @Column(name = "nom")
    private String nom;

    @Column(name = "id_con")
    private int idCon;

    public DetalleResumenEvaluacionPersonal() {
    }
    public DetalleResumenEvaluacionPersonal(double subTot, double pun, Character tip, String nom, int idCon) {
        this.subTot = subTot;
        this.pun = pun;
        this.tip = tip;
        this.nom = nom;
        this.idCon = idCon;
    }
    public DetalleResumenEvaluacionPersonal(double subTot, double pun, Character tip, String nom) {
        this.subTot = subTot;
        this.pun = pun;
        this.tip = tip;
        this.nom = nom;
    }

    public int getDetResEvaPerId() {
        return detResEvaPerId;
    }

    public void setDetResEvaPerId(int detResEvaPerId) {
        this.detResEvaPerId = detResEvaPerId;
    }

    public ResumenEvaluacionPersonal getResEvaPer() {
        return resEvaPer;
    }

    public void setResEvaPer(ResumenEvaluacionPersonal resEvaPer) {
        this.resEvaPer = resEvaPer;
    }

    public double getSubTot() {
        return subTot;
    }

    public void setSubTot(double subTot) {
        this.subTot = subTot;
    }

    public double getPun() {
        return pun;
    }

    public void setPun(double pun) {
        this.pun = pun;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getNom() {
        return nom;
    }

    public int getIdCon() {
        return idCon;
    }

    public void setIdCon(int idCon) {
        this.idCon = idCon;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"tip","nom","pun","subTot"},new String[]{"tip","nom","pun","subt"},this);
    }
}
