/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.service.base;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class BuiltJSON {
    
    public static JSONObject builtJSONObjectFromPojo(Object obj, String[] atributos,String[] etiquetas){

        JSONObject nuevo = null;

        Class miClase = obj.getClass();

        try {
            Object objResult = null;
            nuevo = new JSONObject();
            for(int i=0;i<atributos.length;i++){

                objResult = miClase.getMethod("get"+atributos[i]).invoke(obj);

                if(objResult instanceof Boolean)
                    nuevo.put(etiquetas[i], (Boolean)objResult );
                else
                    nuevo.put(etiquetas[i],objResult.toString());                

            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(BuiltJSON.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("ERROR al construir un BaseObject a partir de la clase : "+ miClase.getName());
        }

        //System.out.println( nuevo.toJSON() );

        return nuevo;        
    }
    
    //convertir de un BaseObject a una pojo
    //miClase : clase del pojo a convertir
    //atributos : nombres de los atributos del pojo
    //tipos : tipos de los atributos del pojo
    //etiquetas : nombres de las etiquetas del BaseObject
    public Object builtPojoFromBaseObject(Class miClase,JSONObject jobject ,String[] atributos, Class[] tipos, String[] etiquetas){
                
        Object miPojo = null;
        try {
            miPojo = miClase.newInstance();
        } catch (InstantiationException | IllegalAccessException  ex) {
            Logger.getLogger(BuiltJSON.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("ERROR al instancear el pojo : "+ miClase.getName());
        }
        try {            
            for(int i=0;i<atributos.length;i++){
                Class[] cArg = new Class[1];
                cArg[0] = tipos[i];                
                miClase.getMethod("set"+atributos[i], cArg ).invoke(miPojo , jobject.get( etiquetas[i] ) );
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(BuiltJSON.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("ERROR al construir un pojo de tipo: "+ miClase.getName());
        }
        
        return miPojo;
    }
}
