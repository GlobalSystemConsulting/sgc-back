/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarEstudioPostgradoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarEstudioPostgradoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        EstudioPostgrado estPos = null;
        
        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            Character tip = requestData.getString("tip").charAt(0);
            String numDoc = requestData.getString("numDoc");
            Integer tipdocId = requestData.getInt("tipDoc");
            Date fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            String pais = requestData.getString("pais");
            Date fecIniEst = sdi.parse(requestData.getString("fecIniEst").substring(0, 10));
            Date fecTerEst = sdi.parse(requestData.getString("fecTerEst").substring(0, 10));
            String ins = requestData.getString("ins");
            
            estPos = new EstudioPostgrado(new Persona(perId), tip, numDoc, fecDoc, tipdocId, fecIniEst, fecTerEst, ins, wr.getIdUsuario(), new Date(), 'A');
            estPos.setPais(pais);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo estudio de postgrado",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        EstudioPostgradoDao estPosDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
        try {
            estPosDao.insert(estPos);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego estudio postgs", "Detalle: "+perId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA///////////// 
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo estudio de postgrado",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estPosId", estPos.getEstPosId());
        oResponse.put("tip", estPos.getTip());
        oResponse.put("tipDes", "");
        oResponse.put("pais", estPos.getPais());
        oResponse.put("tipDocId", estPos.getTipDocId());
        oResponse.put("tipDocDes", "");
        oResponse.put("numDoc", estPos.getNumDoc());
        oResponse.put("fecDoc", sdo.format(estPos.getFecDoc()));
        oResponse.put("fecIniEst", sdo.format(estPos.getFecIniEst()));
        oResponse.put("fecTerEst", sdo.format(estPos.getFecTerEst()));
        oResponse.put("ins", estPos.getIns());
                
        return WebResponse.crearWebResponseExito("El registro del estudio de postgrado se realizo correctamente", oResponse);
        //Fin
    }
    
}
