package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 24/01/2017.
 */
@Entity
@Table(name = "registro_auxiliar", schema = "pedagogico")
public class RegistroAuxiliar implements java.io.Serializable {
    @Id
    @Column(name = "reg_aux_id",nullable = false, unique = true)
    @SequenceGenerator(name = "registro_auxiliar_reg_aux_id_seq",sequenceName = "pedagogico.registro_auxiliar_reg_aux_id_seq")
    @GeneratedValue(generator = "registro_auxiliar_reg_aux_id_seq")
    private int regAuxId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_apr_id")
    private IndicadorAprendizaje indicador;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private Docente docente;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_pla_est_id")
    private PeriodosPlanEstudios periodo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id")
    private GradoIEEstudiante gradoEstudiante;
    
    @Column(name = "not_ind", length = 4,nullable = false)
    private String notInd;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public RegistroAuxiliar() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public RegistroAuxiliar(String notInd) {
        this.notInd = notInd;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getRegAuxId() {
        return regAuxId;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }

    public void setIndicador(IndicadorAprendizaje indicador) {
        this.indicador = indicador;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public PeriodosPlanEstudios getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodosPlanEstudios periodo) {
        this.periodo = periodo;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public GradoIEEstudiante getGradoEstudiante() {
        return gradoEstudiante;
    }

    public void setGradoEstudiante(GradoIEEstudiante gradoEstudiante) {
        this.gradoEstudiante = gradoEstudiante;
    }

    public String getNotInd() {
        return notInd;
    }

    public void setNotInd(String notInd) {
        this.notInd = notInd;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
