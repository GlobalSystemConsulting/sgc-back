/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reconocimientos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarReconocimientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarReconocimientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer recId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            recId = requestData.getInt("recId");  
            System.out.println("DELETE: "+recId);
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarReconocimiento",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        ReconocimientoDao capacitacionDao = (ReconocimientoDao)FactoryDao.buildDao("se.ReconocimientoDao");
        try{
            capacitacionDao.delete(new Reconocimiento(recId));
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino reconocimiento", "REC_ID: "+recId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar el reconocimiento\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el reconocimiento", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El reconocimiento se elimino correctamente");
    }
    
}
