/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Calendario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class ActividadCalendarioDaoHibernate extends GenericDaoHibernate<ActividadCalendario> implements ActividadCalendarioDao{
    
    @Override
    public void crearActividad(ActividadCalendario actividad,int destinatarioID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //creando el actividad
            session.persist(actividad);
            
            Calendario bandeja = new Calendario(0,actividad.getActCalId(),destinatarioID);
            //enviando el actividad al destinatario
            session.persist(bandeja);
            
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo crear la actividad del usuario\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo crear la actividad del usuario\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void crearActividad(ActividadCalendario actividad,List<Integer> destinatarios){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            
            //creando el actividad
            session.persist(actividad);
            
            //creando los actividades a los destinatarios
            for(Integer destinatarioID: destinatarios){                
                Calendario bandeja = new Calendario(0,actividad.getActCalId(),destinatarioID);
                session.persist(bandeja);
            }                
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo crear la actividad a los destinatarios\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo crear la actividad a los destinatarios\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<Calendario> listarCalendarioForaneo(int usuarioID){
        List<Calendario> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT c FROM Calendario c JOIN FETCH c.actividad a WHERE c.usuSesId=:p1 and a.estReg='A' ORDER BY a.horIni";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los actividades foraneas del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las actividades foraneas del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<ActividadCalendario> listarCalendarioPropio(int usuarioID){
        List<ActividadCalendario> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT a FROM ActividadCalendario a WHERE a.usuId=:p1 and a.estReg='A' ORDER BY a.horIni";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las actividades propias del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las actividades propias del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
}
