/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Yemi
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador> implements TrabajadorDao {

    @Override
    public List<Trabajador> listarPorOrganizacion(int orgId, int carId) {
        List<Trabajador> trabajadores = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT t FROM com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador AS t"
                    + " join fetch t.persona p"
                    + " join fetch t.organizacion o"
                    + " join fetch t.trabajadorCargo c"
                    + " WHERE o.orgId=:organizacionId  AND c.crgTraIde=:cargoId AND t.estReg = 'A'";

            Query query = session.createQuery(hql);
            query.setParameter("organizacionId", orgId);
            query.setParameter("cargoId", carId);
            trabajadores = query.list();
            t.commit();
        } catch (Exception e) {
            System.out.println("No se pudo listar los trabajadores\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los trabajadores\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajadores;
    }

    @Override
    public Trabajador buscarPorId(Integer traId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trabajador col = (Trabajador) session.get(Trabajador.class, traId);
        session.close();
        return col;
    }

    public Trabajador buscarDatosPersonales(int traId) {
        Trabajador trabajador = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT t FROM com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador AS t"
                    + " join fetch t.persona p"
                    + " WHERE t.traId=:trabajadorId AND t.estReg = 'A'";

            Query query = session.createQuery(hql);
            query.setParameter("trabajadorId", traId);
            query.setMaxResults(1);
            trabajador = (Trabajador) query.uniqueResult();
            t.commit();
        } catch (Exception e) {
            System.out.println("No se pudo listar los datos personales del trabajador\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos personales del trabajador\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajador;
    }

}
