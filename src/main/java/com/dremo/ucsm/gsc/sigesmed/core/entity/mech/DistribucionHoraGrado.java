package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="distribucion_hora_grado" ,schema="institucional" )
public class DistribucionHoraGrado  implements java.io.Serializable {

    @Id
    @Column(name="dis_hor_gra_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_distribucionhoragrado", sequenceName="institucional.distribucion_hora_grado_dis_hor_gra_id_seq" )
    @GeneratedValue(generator="secuencia_distribucionhoragrado")
    private int disHorGraId;
    @Column(name="hor_asi")
    private int horAsi;    
    @Column(name="hor_tut")
    private int horTut;
    
    @Column(name="pla_est_id")
    private int plaEstId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_est_id",updatable = false,insertable = false)
    private PlanEstudios planEstudios;
    
    @Column(name="pla_mag_id")
    private int plaMagId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_mag_id",updatable = false,insertable = false)
    private PlazaMagisterial plazaMagisterial; 
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;
    
    @Column(name="sec_id")
    private char secId;
    
    @OneToMany(mappedBy="distribucionGrado",cascade=CascadeType.PERSIST)
    private List<DistribucionHoraArea> distribucionAreas;    

    public DistribucionHoraGrado() {
    }
    public DistribucionHoraGrado(int disHorGraId) {
        this.disHorGraId = disHorGraId;
    }
    public DistribucionHoraGrado(int disHorGraId,int horAsi,int plaEstId,int plaMagId, int graId,char secId) {
       this.disHorGraId = disHorGraId;
       
       this.horAsi = horAsi;
       
       this.plaEstId = plaEstId;
       this.plaMagId = plaMagId;
       this.graId = graId;
       this.secId = secId;
       this.horTut = 0;
    }
   
     
    public int getDisHorGraId() {
        return this.disHorGraId;
    }    
    public void setDisHorGraId(int disHorGraId) {
        this.disHorGraId = disHorGraId;
    }
    
    public int getHorAsi() {
        return this.horAsi;
    }
    public void setHorAsi(int horAsi) {
        this.horAsi = horAsi;
    }
    
    public int getHorTut() {
        return this.horTut;
    }
    public void setHorTut(int horTut) {
        this.horTut = horTut;
    }
    
    public int getPlaEstId() {
        return this.plaEstId;
    }    
    public void setPlaEstId(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    
    public PlanEstudios getPlanEstudios() {
        return this.planEstudios;
    }
    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }
    
    public int getPlaMagId() {
        return this.plaMagId;
    }    
    public void setPlaMagId(int plaMagId) {
        this.plaMagId = plaMagId;
    }
    
    public PlazaMagisterial getPlazaMagisterial() {
        return this.plazaMagisterial;
    }
    public void setPlazaMagisterial(PlazaMagisterial plazaMagisterial) {
        this.plazaMagisterial = plazaMagisterial;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    public char getSecId() {
        return this.secId;
    }    
    public void setSecId(char secId) {
        this.secId = secId;
    }
    
    public List<DistribucionHoraArea> getDistribucionAreas() {
        return this.distribucionAreas;
    }
    public void setDistribucionAreas(List<DistribucionHoraArea> distribucionAreas) {
        this.distribucionAreas = distribucionAreas;
    }
}


