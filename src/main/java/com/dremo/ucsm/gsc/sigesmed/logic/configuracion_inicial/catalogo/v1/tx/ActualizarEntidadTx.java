/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.EntidadObjetoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class ActualizarEntidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        //CatalogoTabla catalogoTabla = null;
        
        EntidadObjetoDao entidadObjetoDao = (EntidadObjetoDao)FactoryDao.buildDao("EntidadObjetoDao");
        Object obj = null ;
        String nombreclase = null;
        
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int EntidadID = requestData.getInt("id");
            
            nombreclase = requestData.getString("nombreclase");
            String nombre= requestData.getString("Nombre");
            String descripcion = requestData.getString("Descripcion");
            String estado = requestData.getString("estado");
            
            try{
                obj = Class.forName("com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreclase).newInstance();
           
                obj.getClass().getMethod("setId",int.class).invoke(obj,EntidadID);
                obj.getClass().getMethod("setNom",String.class).invoke(obj,nombre);
                obj.getClass().getMethod("setDes",String.class).invoke(obj,descripcion);
                obj.getClass().getMethod("setFecMod",Date.class).invoke(obj,new Date());
                obj.getClass().getMethod("setUsuMod",Integer.class).invoke(obj,wr.getIdUsuario());
                obj.getClass().getMethod("setEstReg",char.class).invoke(obj,estado.charAt(0));
            }catch(Exception e){
                System.out.println("No se pudo Editar registrar Set \n"+e);
             return WebResponse.crearWebResponseError("No se pudo registrar, Error de SETS", e.getMessage() );
            }  
           
        }catch(Exception e){
             System.out.println("No se pudo actualizar, datos incorrectos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            
            
            entidadObjetoDao.update(obj);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Registro para la entidad \n"+nombreclase+" "+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Registro", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Catalogo se actualizo correctamente");
        //Fin
    }
    
}
