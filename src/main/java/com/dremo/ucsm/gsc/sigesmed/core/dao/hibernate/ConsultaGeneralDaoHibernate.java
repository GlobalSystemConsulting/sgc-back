/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;


import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ConsultaGeneralDaoHibernate extends GenericDaoHibernate<MetadataConsultaGeneral> implements ConsultaGeneralDao{

    private static final Logger logger = Logger.getLogger(ConsultaGeneralDaoHibernate.class.getName());
    
    @Override
    public MetadataConsultaGeneral buscarPorID(int id_met){
        Session session = HibernateUtil.getSessionFactory().openSession();
        MetadataConsultaGeneral m = (MetadataConsultaGeneral)session.get(MetadataConsultaGeneral.class, id_met);
        session.close();
        return m;
    }
    @Override
    public List<MetadataConsultaGeneral> listarTodaMetadata() {
        List<MetadataConsultaGeneral> metadataCatalogo= null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT mc FROM MetadataConsultaGeneral as mc ";
            Query query = session.createQuery(hql);            
            metadataCatalogo = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return metadataCatalogo;
    }
    
    //@Override
    public JSONArray listarxCatalogo() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            //SELECT table_name,column_name,data_type FROM information_schema.columns WHERE table_schema = 'administrativo' AND table_name = 'legajo_personal'
            String queryStr = "SELECT entity_name,atrib_name,alias_name,data_type \n" +
                            "  FROM consulta_frecuente_metadata \n";

            
                            //+ table;
            JSONArray resultado = new JSONArray();
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            transaction.commit();
            for (Object[] res:respuesta){
                JSONObject object = new JSONObject();
                object.put("entity_name", res[0]);
                object.put("atrib_name", res[1]);
                object.put("alias_name", res[2]);
                object.put("data_type", res[3]);
                resultado.put(object);
            }
            return resultado;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se pudo obtener la metadata del esquema");
            throw ex;
        } finally {
            session.close();
        } 
    }
    //@Override
    public List<String> listarTablasCatalogo() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            //SELECT table_name,column_name,data_type FROM information_schema.columns WHERE table_schema = 'administrativo' AND table_name = 'legajo_personal'
            String queryStr = "SELECT DISTINCT c.table_name FROM pg_catalog.pg_statio_all_tables as st \n" +
                              "inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid) inner join information_schema.columns c on (pgd.objsubid=c.ordinal_position and  c.table_schema=st.schemaname and c.table_name=st.relname) \n" +
                              "WHERE table_schema = 'administrativo' OR table_schema = 'configuracion_inicial' OR table_schema = 'institucional' OR table_schema = 'pedagogico' OR table_schema = 'public' \n";

            
            List<String> respuesta =  session.createSQLQuery(queryStr).list();
            transaction.commit();
            return respuesta;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se pudo obtener DISTINCT de tablas de catalogo");
            throw ex;
        } finally {
            session.close();
        } 
    }
    //@Override
    public JSONArray listarxMetaDataConstraints() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            //SELECT table_name,column_name,data_type FROM information_schema.columns WHERE table_schema = 'administrativo' AND table_name = 'legajo_personal'
            String queryStr = "SELECT DISTINCT table_schema, table_name,column_name \n"
                    + "FROM information_schema.key_column_usage \n";
                    //+ "WHERE table_name='ascenso' OR table_name='capacitacion' OR table_name='ficha_escalafonaria' OR table_name='trabajador' OR table_name='persona' OR table_name='parientes' \n";

            List<String> catalogo = listarTablasCatalogo();
            String consultaWhere=" WHERE ";
            for (int i=0;i<catalogo.size();i++) {
                if(i!=catalogo.size()-1){
                    consultaWhere+=" table_name='";
                    consultaWhere+=catalogo.get(i);
                    consultaWhere+="'";
                    consultaWhere+=" OR ";
                }else{
                    consultaWhere+=" table_name='";
                    consultaWhere+=catalogo.get(i);
                    consultaWhere+="'";
                }
                
                //System.out.print("ELEMENTO CATALOGO: ");System.out.println(catalogo.get(i));
            }
            queryStr+=consultaWhere; queryStr+=" \n";
            System.out.print("QUERY LISTAR METADATA 2: ");System.out.println(queryStr);
            
            JSONArray resultado = new JSONArray();
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            transaction.commit();
            for (Object[] res:respuesta){
                JSONObject object = new JSONObject();
                object.put("table_schema", res[0]);
                object.put("table_name", res[1]);
                object.put("column_name", res[2]);
                resultado.put(object);
            }
            return resultado;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se pudo obtener la metadata del esquema");
            throw ex;
        } finally {
            session.close();
        } 
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////LIST ALL//////////////////////////////////////////////////////////
    //@Override
    public JSONArray listAllSchema() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            //SELECT table_name,column_name,data_type FROM information_schema.columns WHERE table_schema = 'administrativo' AND table_name = 'legajo_personal'
            String queryStr = "SELECT c.table_schema,c.table_name,c.column_name,pgd.description,data_type \n"
                    + "FROM pg_catalog.pg_statio_all_tables as st \n"
                    + "inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid) \n"
                    + "inner join information_schema.columns c on (pgd.objsubid=c.ordinal_position \n"
                    + "and  c.table_schema=st.schemaname and c.table_name=st.relname) \n"
                    + "WHERE table_schema = 'administrativo' OR table_schema = 'configuracion_inicial' OR table_schema = 'institucional' OR table_schema = 'pedagogico' OR table_schema = 'public' \n"
                    + "ORDER BY table_name \n";
                    //+ "WHERE table_name='ascenso' OR table_name='capacitacion' OR table_name='ficha_escalafonaria' OR table_name='trabajador' OR table_name='persona' OR table_name='parientes' \n";
            System.out.print("QUERY LIST ALL SCHEMA: ");System.out.println(queryStr);
            
            JSONArray resultado = new JSONArray();
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            transaction.commit();
            for (Object[] res:respuesta){
                JSONObject object = new JSONObject();
                object.put("table_schema", res[0]);
                object.put("table_name", res[1]);
                object.put("column_name", res[2]);
                object.put("description", res[3]);
                object.put("data_type", res[4]);
                resultado.put(object);
            }
            return resultado;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se pudo obtener la metadata del esquema");
            throw ex;
        } finally {
            session.close();
        } 
    }
    /////////////////////////////////////////LIST ALL//////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    ///////////////////////////////////////LISTAR X CONSULTA DIVERSA/////////////////////////////////////////
    //////////////////////////////////////AQUI ARMAAMOS LA SENTENCIA SQL CORRECTA////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //@Override
    public List<String[]> listarxConsultaGeneral(String consulta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        //System.out.println(consulta);
        ArrayList <String[]> respuestaConsulta = new ArrayList<String[]>();
        try {
            
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(consulta).list();
            transaction.commit();
            for (Object[] result : respuesta) {
                String[] oS = new String[result.length];
                for(int i = 0 ; i < result.length; i++){
                    if(result[i]!=null){
                        String item_data = result[i].toString();
                        oS[i]=item_data;
                    }else{
                        String item_data = "-";
                        oS[i]=item_data;
                    }
                }
                respuestaConsulta.add(oS);
            }
            return respuestaConsulta;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se puede listarxConsultaGeneral " + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        } 
    }
    
}
