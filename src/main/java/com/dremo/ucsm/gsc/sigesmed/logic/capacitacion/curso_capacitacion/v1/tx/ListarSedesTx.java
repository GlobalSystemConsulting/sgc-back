package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListarSedesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarSedesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarSedes(data.getInt("cod"));
                break;

            case 1:
                response = listarSedesBanPre(data.getInt("codCap"));
                break;
        }

        return response;
    }

    private WebResponse listarSedes(int cod) {
        try {
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            List<SedeCapacitacion> sedes = sedeCapacitacionDao.buscarSedesPorCapacitacion(cod);
            JSONArray jsonSedes = new JSONArray();

            for (SedeCapacitacion sede : sedes) {
                JSONObject jsonSede = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"sedCapId", "pro", "dis", "loc"},
                        new String[]{"cod", "pro", "dis", "loc"},
                        sede
                ));
                JSONObject jsonCro = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"fecIni", "fecFin"},
                        new String[]{"ini", "fin"},
                        sede.getCronograma()
                ));
                JSONObject merged = new JSONObject(jsonSede, JSONObject.getNames(jsonSede));
                for (String key : JSONObject.getNames(jsonCro)) {
                    merged.put(key, jsonCro.get(key));
                }
                jsonSedes.put(merged);
            }
            return WebResponse.crearWebResponseError("Error al listar las sedes", WebResponse.OK_RESPONSE).setData(jsonSedes);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarSedes", e);
            return WebResponse.crearWebResponseError("Error al listar las sedes", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarSedesBanPre(int codCap) {
        try {
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            List<SedeCapacitacion> places = sedeCapacitacionDao.buscarSedesPorCapacitacion(codCap);
            JSONArray array = new JSONArray();

            for (SedeCapacitacion sede : places) {
                JSONObject object = new JSONObject();
                object.put("cod", sede.getSedCapId());
                object.put("nom", sede.getPro() + " - " + sede.getDis() + " - " + sede.getDir());

                array.put(object);
            }

            return WebResponse.crearWebResponseError("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarSedesBanPre", e);
            return WebResponse.crearWebResponseError("Error al listar las sedes", WebResponse.BAD_RESPONSE);
        }
    }    
}
