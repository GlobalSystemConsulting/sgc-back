package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
@Entity
@Table(name = "competencia", schema = "pedagogico")
public class CompetenciaAprendizaje implements java.io.Serializable, Comparable<CompetenciaAprendizaje>{
    @javax.persistence.Id
    @Column(name = "com_id", nullable = false, unique = true)
    @SequenceGenerator(name = "competencia_com_id_sequence",sequenceName = "pedagogico.competencia_com_id_seq")
    @GeneratedValue(generator = "competencia_com_id_sequence")
    private int comId;

    @Type(type="com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CurriculaType")
    @Column(name = "dcn", length = 60)
    private TipoCurriculaEnum dcn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id", nullable = true)
    private AreaCurricular area;

    @Column(name = "nom", length = 500, unique = true, nullable = false)
    private String nom;

    @Column(name = "des")
    private String des;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "com",fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    List<CompetenciaCapacidad> capacidades = new ArrayList<>();

    public CompetenciaAprendizaje() {
    }

    public CompetenciaAprendizaje(String nom) {
        this.nom = nom;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public CompetenciaAprendizaje(String nom, TipoCurriculaEnum dcn, String des) {
        this.nom = nom;
        this.dcn = dcn;
        this.des = des;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public CompetenciaAprendizaje(String nom, String des) {
        this.nom = nom;
        this.des = des;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public TipoCurriculaEnum getDcn() {
        return dcn;
    }

    public void setDcn(TipoCurriculaEnum dcn) {
        this.dcn = dcn;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<CompetenciaCapacidad> getCapacidades() {
        return capacidades;
    }

    public void setCapacidades(List<CompetenciaCapacidad> capacidades) {
        this.capacidades = capacidades;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompetenciaAprendizaje)) return false;

        CompetenciaAprendizaje that = (CompetenciaAprendizaje) o;

        if (comId != that.comId) return false;
        return nom.equals(that.nom);

    }

    @Override
    public int hashCode() {
        int result = comId;
        result = 31 * result + nom.hashCode();
        return result;
    }

    @Override
    public int compareTo(CompetenciaAprendizaje o) {
        if (this == o) return 0;
        if (!(o instanceof CompetenciaAprendizaje)) return -1;
        if (comId != o.comId) return -1;
        return (nom.equals(o.nom)) ? 0 : -1;
    }

    @Override
    public String toString() {
        return "CompetenciaAprendizaje{" + "comId=" + comId + ", nom=" + nom + ", estReg=" + estReg + '}';
    }
    
    
}
