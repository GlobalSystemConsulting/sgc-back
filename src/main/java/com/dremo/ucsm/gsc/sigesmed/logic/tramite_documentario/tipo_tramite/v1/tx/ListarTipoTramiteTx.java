/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.tipo_tramite.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarTipoTramiteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        try{
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos TipoTramites", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TipoTramite> tramites = null;
        TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
        try{
            tramites =tipoTramiteDao.buscarConTipoOrganizacion();
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar tipos de tramites ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoTramite tramite:tramites ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoTramiteID",tramite.getTipTraId());
            oResponse.put("codigo",tramite.getCod());
            oResponse.put("nombre",tramite.getNom());
            oResponse.put("descripcion",tramite.getDes());
            oResponse.put("duracion",tramite.getDur());
            oResponse.put("costo",tramite.getCos());            
            oResponse.put("tipo",tramite.getTip());
            oResponse.put("tupa",tramite.getEsTup());
            
            if(tramite.getEsTup()){
                oResponse.put("tipoOrganizacionID",tramite.getTipoOrganizacion().getTipOrgId());
                oResponse.put("tipoOrganizacion",tramite.getTipoOrganizacion().getNom());
            }
            else{
                oResponse.put("tipoOrganizacionID",tramite.getOrganizacion().getOrgId());
                oResponse.put("tipoOrganizacion",tramite.getOrganizacion().getNom());
            }
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

