/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ListarLegajosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarLegajosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<LegajoPersonal> legajos = null;
        LegajoPersonalDao legajoPersonalDao = (LegajoPersonalDao)FactoryDao.buildDao("se.LegajoPersonalDao");
        
        try{
            legajos = legajoPersonalDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar legajos",e);
            System.out.println("No se pudo listar los legajos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los legajos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */

        JSONArray miArray = new JSONArray();
        for(LegajoPersonal legajo:legajos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("legId", legajo.getLegPerId());
            
            String route = ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + Legajo.LEGAJO_PATH + File.separator;
            Path path = Paths.get(route + legajo.getNom());
            if (Files.exists(path)){
                oResponse.put("existeArchivo", true);
                oResponse.put("mensaje", "El archivo existe");
            } else {
                oResponse.put("existeArchivo", false);
                oResponse.put("mensaje", "El archivo no existe");
            }
            oResponse.put("url", Sigesmed.UBI_ARCHIVOS + File.separator  + legajo.getUrl() + File.separator );
            oResponse.put("nomDoc", legajo.getNom());
            oResponse.put("des", legajo.getDes());
            oResponse.put("fecReg", legajo.getFecIng());
            oResponse.put("catLeg", legajo.getCatLeg());
            oResponse.put("subCat", legajo.getSubCat());
            oResponse.put("subCatDes", "");
            oResponse.put("codAspOrg", legajo.getCodAspOri());

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los legajos fueron listados exitosamente", miArray);
    }
    
    
}
