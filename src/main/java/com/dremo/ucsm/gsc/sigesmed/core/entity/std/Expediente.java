package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="expediente" ,schema="administrativo")
public class Expediente  implements java.io.Serializable {

    @Id
    @Column(name="exp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_expediente", sequenceName="administrativo.expediente_exp_id_seq" )
    @GeneratedValue(generator="secuencia_expediente")
    private int expId;
    @Column(name="cod", nullable=false, length=8)
    private String codigo;
    
    @Column(name="tip_exp")
    private boolean tipo;
    
    @Column(name="num_fol")
    private int folios;
    
    @Column(name="asu", length=256)
    private String asunto;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ini", length=29,updatable=false)
    private Date fecIni;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_fin", length=29,updatable=false)
    private Date fecFin;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ent", length=29,updatable=false)
    private Date fecEnt;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;
    
    @Column(name="org_id")
    private int orgId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", insertable=false,updatable=false)
    private Organizacion organizacion;
        
    @Column(name="tip_tra_id", insertable=false,updatable=false)
    private int tipoTramiteId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_tra_id")
    private TipoTramite tipoTramite;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pri_exp_id")
    private PrioridadExpediente prioridad;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="per_id")
    private Persona persona;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="emp_id")
    private Empresa empresa;
    
    @OneToMany(mappedBy="expediente",cascade=CascadeType.PERSIST)
    private List<DocumentoExpediente> documentos;
    
    @OneToMany(mappedBy="expediente",cascade=CascadeType.PERSIST)
    private List<HistorialExpediente> historial;

    public Expediente() {
    }

    public Expediente(int expId) {
        this.expId = expId;
    }
    public Expediente(int expId, String cod) {
        this.expId = expId;
        this.codigo = cod;
    }
    public Expediente(int expId, String cod,boolean tip,int folios,String asu, Date fecMod, Integer usuMod, char estReg, int tipoTramite,int prioridad,int organizacion,int persona) {
       this.expId = expId;
       this.codigo = cod;
       this.tipo = tip;
       this.folios = folios; 
       this.asunto = asu;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.orgId = organizacion;
       
       this.tipoTramite = new TipoTramite(tipoTramite);
       this.prioridad = new PrioridadExpediente(prioridad);
       this.persona = new Persona(persona);
    }
    
    public Expediente(int expId, String cod,boolean tip,int folios,String asu, Date fecMod, Integer usuMod, char estReg, int tipoTramite,int prioridad,int organizacion,int persona,Date fecIni) {
       this.expId = expId;
       this.codigo = cod;
       this.tipo = tip;
       this.folios = folios;
       this.asunto = asu;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.orgId = organizacion;
       
       this.tipoTramite = new TipoTramite(tipoTramite);
       this.prioridad = new PrioridadExpediente(prioridad);
       this.persona = new Persona(persona);
       
       this.fecIni = fecIni;
    }
    
    public Expediente(int expId, String cod,boolean tip,int folios,String asu, Date fecMod, Integer usuMod, char estReg, int tipoTramite,int prioridad,int organizacion,Date fecIni,int empresa) {
       this.expId = expId;
       this.codigo = cod;
       this.tipo = tip;
       this.folios = folios;
       this.asunto = asu;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.orgId = organizacion;
       
       this.tipoTramite = new TipoTramite(tipoTramite);
       this.prioridad = new PrioridadExpediente(prioridad);
       this.empresa = new Empresa(empresa);
       
       this.fecIni = fecIni;
    }
   
    public int getExpId() {
        return this.expId;
    }
    public void setExpId(int expId) {
        this.expId = expId;
    }

    
    public String getCodigo() {
        return this.codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getFolios() {
        return this.folios;
    }
    public void setFolios(int folios) {
        this.folios = folios;
    }
    
    public boolean getTipo() {
        return this.tipo;
    }
    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }
    
    public String getAsunto() {
        return this.asunto;
    }
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    
    public Date getFecIni() {
        return this.fecIni;
    }
    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }
    public Date getFecFin() {
        return this.fecFin;
    }
    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }
    public Date getFecEnt() {
        return this.fecEnt;
    }
    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getOrgId() {
        return this.orgId;
    }
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion getOrganizacion() {
        return this.organizacion;
    }    
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }    
    
    public int getTipoTramiteId() {
        return this.tipoTramiteId;
    }
    public void setTipoTramiteId(int tipoTramiteId) {
        this.tipoTramiteId = tipoTramiteId;
    }
    public TipoTramite getTipoTramite() {
        return this.tipoTramite;
    }    
    public void setTipoTramite(TipoTramite tipoTramite) {
        this.tipoTramite = tipoTramite;
    }
    
    public PrioridadExpediente getPrioridad() {
        return this.prioridad;
    }    
    public void setPrioridad(PrioridadExpediente prioridad) {
        this.prioridad = prioridad;
    }
    
    public Persona getPersona() {
        return this.persona;
    }    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<DocumentoExpediente> getDocumentos() {
        return this.documentos;
    }    
    public void setDocumentos(List<DocumentoExpediente> documentos) {
        this.documentos = documentos;
    }

    public List<HistorialExpediente> getHistorial() {
        return this.historial;
    }
    public void setHistorial(List<HistorialExpediente> historial) {
        this.historial = historial;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    
}


