/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.font.FontProgramFactory;
//import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.color.Color;
//import com.itextpdf.kernel.color.DeviceCmyk;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.Property;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
/*import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;*/
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
//import com.itextpdf.io.image.ImageType;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.imageio.ImageTypeSpecifier;

/**
 *
 * @author Administrador
 */
public class MyEventHandler implements IEventHandler{
    private PdfFont helvetica = null;
    private String titulo_de_Encabezado = "ESCALAFON UNSA";
    private String pie_de_pagina="";
    private Image imagenCabecera = null;
    private boolean useWatermark = false;
    private String useWatermarkText = "";
    protected PdfExtGState gState;
    
    public  MyEventHandler() throws Exception {        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);            
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png"));//
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
    }
    public  MyEventHandler(String titulo,String pie_pagina) throws Exception {        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);            
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png"));//
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        pie_de_pagina=pie_pagina;
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
    }
    ////My evet handler for using watermark
    public  MyEventHandler(boolean useWatermark, String useWatermarkText, String pie_pagina) throws Exception {        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);            
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png"));//
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
        this.useWatermark = useWatermark;
        this.useWatermarkText = useWatermarkText;
        this.pie_de_pagina=pie_pagina;
    }
    
    public  MyEventHandler(String titulo_de_Encabezado) throws Exception {
        this.titulo_de_Encabezado = titulo_de_Encabezado;        
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);  
        imagenCabecera = new Image(ImageDataFactory.create(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png"));
//        imagenCabecera = new Image(ImageDataFactory.create("src/main/webapp/recursos/img/minedu.png"));
        gState = new PdfExtGState();//.setFillOpacity(0.2f);
    }
    
    public void handleEvent(Event event) {
        
        PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
        int pageNumber = pdfDoc.getPageNumber(page);
        
        Rectangle pageSize = page.getPageSize();
        PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
        
        //Add watermark
        if(useWatermark){
            try {
                PdfCanvas watermark = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
                PdfFont font = PdfFontFactory.createFont(FontProgramFactory.createFont(FontConstants.HELVETICA));
                Paragraph p = new Paragraph(useWatermarkText).setFont(font).setFontSize(80);
                //add Opacity to watermark
                watermark.saveState();
                PdfExtGState gs1 = new PdfExtGState();
                gs1.setFillOpacity(0.1f);
                watermark.setExtGState(gs1);
                new Canvas(watermark, pdfDoc, pdfDoc.getDefaultPageSize()).showTextAligned(p, 450, 350, 1, TextAlignment.CENTER, VerticalAlignment.TOP, 0);
                watermark.restoreState();
            } catch (IOException ex) {
                Logger.getLogger(MyEventHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        String fecha = formato.format(new Date());
        //Add header and footer
        new Canvas(pdfCanvas, pdfDoc, page.getPageSize()).setFontSize(9).setFont(helvetica)
        .showTextAligned("ESCALAFON UNSA", pageSize.getWidth()/2, pageSize.getTop() - 20, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0)
        .showTextAligned(fecha, pageSize.getWidth()-30, pageSize.getTop() - 20, TextAlignment.RIGHT, VerticalAlignment.MIDDLE, 0)
        .showTextAligned(String.valueOf(pageNumber), pageSize.getWidth()-30, 20, TextAlignment.RIGHT, VerticalAlignment.MIDDLE, 0)
        .showTextAligned(pie_de_pagina, 30, 30, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
        
        /*pdfCanvas.beginText()                
                .setFontAndSize(helvetica, 9)
                .moveText(pageSize.getWidth() / 2 - 60, pageSize.getTop() - 20)/////contiene la altura para el encabezado de cabecera de pagina                              
                .showText("ESCALAFON UNSA")
                //.moveText(60, -pageSize.getTop()+30) 
                //.showText(String.valueOf(pageNumber))                
                .endText();*/
//        pdfCanvas.beginVariableText()
//                .setFontAndSize(helvetica, 9)
//               
//                .moveText(pageSize.getWidth() / 2 -60, pageSize.getTop() - 20)/////contiene la altura para el encabezado de cabecera de pagina                              
//                .showText("ESCALAFON UNSA")
//                .moveText(-220, -pageSize.getTop()+70)
//                //.showText(pie_de_pagina+"                             "+String.valueOf(pageNumber))
//                .newlineShowText(pie_de_pagina).showText(pie_de_pagina)
//                .newlineText()
//                .showText("porque")
//                
//                        .endText();
//          
//                .moveText(0, -pageSize.getTop()+70)
//                .showText(pie_de_pagina)
//                .newlineShowText(String.valueOf(pageNumber))
//                .moveText(60, -pageSize.getTop()+50) 
//                .showText(String.valueOf(pageNumber))
                
//       pdfCanvas.newlineShowText(pie_de_pagina);
                        
        //Add Cab Image
        imagenCabecera.setFixedPosition(30, pageSize.getTop() - 40);
        
        pdfCanvas.saveState().setExtGState(gState);        
        Canvas canvas = new Canvas(pdfCanvas, pdfDoc, page.getPageSize());
            canvas.add(imagenCabecera.scaleAbsolute(100, 35));
            
        ////////////////////ADD WATERMARK///////////////////////////    
        
        ////////////////////ADD WATERMARK///////////////////////////
        
            
        pdfCanvas.restoreState();
        pdfCanvas.release();
        
        
        
        
                
        //Add watermark
        //Canvas canvas = new Canvas(pdfCanvas, pdfDoc, page.getPageSize());
        /*canvas.setProperty(Property.FONT_COLOR, Color.WHITE);
        canvas.setProperty(Property.FONT_SIZE, 60);
        canvas.setProperty(Property.FONT, helvetica);//HELVENCIA
        canvas.showTextAligned(new Paragraph("CONFIDENTIAL"),
            298, 421, pdfDoc.getPageNumber(page),
            TextAlignment.CENTER, VerticalAlignment.MIDDLE, 45);*/
 
        
    }
}
