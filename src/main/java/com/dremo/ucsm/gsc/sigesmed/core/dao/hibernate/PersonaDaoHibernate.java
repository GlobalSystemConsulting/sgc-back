/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class PersonaDaoHibernate extends GenericDaoHibernate<Persona> implements PersonaDao {
    
    
    @Override
    public Persona buscarPorDNI(String dni) {
        Persona objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT p FROM Persona p LEFT JOIN FETCH p.usuario uu WHERE p.dni =:dni";
        
            Query query = session.createQuery(hql);
            query.setParameter("dni", dni);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Persona)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Persona por DNI\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por DNI\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    
    @Override
    public Persona buscarPorID(int usuarioID) {
        Persona objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT p FROM Persona p WHERE p.perId =:id ";
        
            Query query = session.createQuery(hql);
            query.setParameter("id", usuarioID);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Persona)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Persona por id\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por id\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public Persona buscarPorCod(int cod) {
        Persona objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT p FROM Persona p WHERE p.perId =:code ";
        
            Query query = session.createQuery(hql);
            query.setParameter("code", cod);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Persona)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Persona por Codigo\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por Codigo\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public List<Persona> buscarPorNombre(String nombreUser) {
        List<Persona> listPers = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM Persona p WHERE p.nom LIKE :nom";
            Query query = session.createQuery(hql);
            query.setString("nom", '%'+nombreUser+'%');
            listPers= query.list();
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Persona por Codigo\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por Codigo\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return listPers;
    }

    @Override
    public List<Persona> buscarPorApellido(String apellidoPaterno, String apellidoMaterno) {
        List<Persona> listPers = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if((apellidoPaterno != null && !apellidoPaterno.equals(""))){
                if((apellidoMaterno != null && !apellidoMaterno.equals(""))){
                    hql = "SELECT p FROM Persona p WHERE LOWER(p.apePat) LIKE LOWER(:apep) AND LOWER(p.apeMat) LIKE LOWER(:apem)";
                    query = session.createQuery(hql);
                    query.setString("apep", '%'+apellidoPaterno);
                    query.setString("apem",'%'+apellidoMaterno);
                }else{
                    hql = "SELECT p FROM Persona p WHERE LOWER(p.apePat) LIKE LOWER(:apep)";
                    query = session.createQuery(hql);
                    query.setString("apep", '%'+apellidoPaterno);
                }
            }else if(apellidoMaterno != null && !apellidoMaterno.equals("")){
                hql = "SELECT p FROM Persona p WHERE LOWER(p.apeMat) LIKE LOWER(:apem)";
                query = session.createQuery(hql);
                query.setString("apem",'%'+apellidoMaterno);
            }
            listPers= query.list();
        }catch(Exception e){
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por Apellido \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return listPers;
    }

    @Override
    public Persona buscarPorOrganizacionConUsuario(String dni, int idOrg) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM Persona p INNER JOIN FETCH p.usuario pu INNER JOIN FETCH  pu.sessiones s INNER JOIN FETCH s.organizacion o WHERE p.dni =:dni AND o.orgId=:idOrg";
            //String hql = "SELECT p FROM Persona p INNER JOIN FETCH p.usuario pu WHERE p.dni =:dni";
            
            Query query = sess.createQuery(hql);
            
            query.setString("dni", dni);
            query.setInteger("idOrg", idOrg);
            
            query.setMaxResults(1);
            return (Persona)query.uniqueResult();
        }catch(Exception e){
            throw e;
        }finally {
            sess.close();
        }
        
    }

    @Override
    public List<Persona> buscarPorOrganizacionConUsuario(String apellidoPaterno, String apellidoMaterno, int idOrg) {
        List<Persona> listPers = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if((apellidoPaterno != null && !apellidoPaterno.equals(""))){
                if((apellidoMaterno != null && !apellidoMaterno.equals(""))){
                    hql = "SELECT p FROM Persona p INNER JOIN FETCH p.usuario pu INNER JOIN FETCH  pu.sessiones s INNER JOIN FETCH s.organizacion o WHERE LOWER(p.apePat) LIKE LOWER(:apep) AND LOWER(p.apeMat) LIKE LOWER(:apem) AND o.orgId=:idOrg";
                    //hql = "SELECT p FROM Persona p WHERE LOWER(p.apePat) LIKE LOWER(:apep) AND LOWER(p.apeMat) LIKE LOWER(:apem)";
                    query = session.createQuery(hql);
                    query.setString("apep", '%'+apellidoPaterno);
                    query.setString("apem",'%'+apellidoMaterno);
                }else{
                    hql = "SELECT p FROM Persona p INNER JOIN FETCH p.usuario pu INNER JOIN FETCH  pu.sessiones s INNER JOIN FETCH s.organizacion o WHERE LOWER(p.apePat) LIKE LOWER(:apep) AND o.orgId=:idOrg";
                    //hql = "SELECT p FROM Persona p WHERE LOWER(p.apePat) LIKE LOWER(:apep)";
                    query = session.createQuery(hql);
                    query.setString("apep", '%'+apellidoPaterno);
                }
            }else if(apellidoMaterno != null && !apellidoMaterno.equals("")){
                hql = "SELECT p FROM Persona p INNER JOIN FETCH p.usuario pu INNER JOIN FETCH  pu.sessiones s INNER JOIN FETCH s.organizacion o WHERE LOWER(p.apeMat) LIKE LOWER(:apem) AND o.orgId=:idOrg";
                //hql = "SELECT p FROM Persona p WHERE LOWER(p.apeMat) LIKE LOWER(:apem)";
                query = session.createQuery(hql);
                query.setString("apem",'%'+apellidoMaterno);
            }
            listPers= query.list();
        }catch(Exception e){
            throw new UnsupportedOperationException("No se pudo encontrar a la Persona por Apellido \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return listPers;
    }
}
