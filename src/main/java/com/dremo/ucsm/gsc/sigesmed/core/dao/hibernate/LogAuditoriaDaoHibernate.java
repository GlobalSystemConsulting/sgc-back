/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.LogAuditoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.LogAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class LogAuditoriaDaoHibernate extends GenericDaoHibernate<LogAuditoria> implements LogAuditoriaDao{
    @Override
    public List<Object[]> listarTodosRegistros() {
        List<Object[]> todoRegistros = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //String hql = "SELECT cc FROM LogAuditoria as cc ";
            String hql = "SELECT public.log_auditoria.log_aud_id,public.log_auditoria.tip_ope,public.log_auditoria.obs_ope,public.log_auditoria.con_ope,public.log_auditoria.fec_emi,public.log_auditoria.usu_id,public.log_auditoria.usu_emi,public.log_auditoria.ip_acc,\n"+
"pedagogico.persona.nom,pedagogico.persona.ape_pat,pedagogico.persona.ape_mat,pedagogico.persona.dni \n"+
"FROM public.log_auditoria,pedagogico.persona WHERE public.log_auditoria.usu_id = persona.per_id \n";
            //List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            Query query = session.createSQLQuery(hql);            
            todoRegistros = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Todos Registros de auditoria \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Todos Registros de auditoria \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return todoRegistros;
    }
    /*@Override
    public List<String[]> listarReporteAuditoria(String fec_ini, String fec_fin, String dni) {
        List<Object[]> todoRegistros = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //String hql = "SELECT cc FROM LogAuditoria as cc ";
            String hql = "SELECT public.log_auditoria.log_aud_id,public.log_auditoria.tip_ope,public.log_auditoria.obs_ope,public.log_auditoria.con_ope,public.log_auditoria.fec_emi,public.log_auditoria.usu_id,public.log_auditoria.usu_emi,public.log_auditoria.ip_acc,\n"+
"pedagogico.persona.nom,pedagogico.persona.ape_pat,pedagogico.persona.ape_mat,pedagogico.persona.dni \n"+
"FROM public.log_auditoria,pedagogico.persona WHERE public.log_auditoria.usu_id = persona.per_id \n";
            if(dni!= "" && fec_ini!="" && fec_fin!=""){
                //si los tres campos llenos
                hql += "WHERE public.log_auditoria.dni = ";
                hql += dni;
                hql += "AND public.log_auditoria.fec_emi > ";
                hql += fec_ini;
                hql += "AND public.log_auditoria.fec_emi < ";
                hql += fec_fin;
            }else if(fec_ini!="" && fec_fin!=""){
                //si oslo fecha
                hql += "WHERE public.log_auditoria.fec_emi > ";
                hql += fec_ini;
                hql += "AND public.log_auditoria.fec_emi < ";
                hql += fec_fin;
            }else if(dni!= ""){
                //si solo dni
                hql += "WHERE public.log_auditoria.dni = ";
                hql += dni;
            }
            //List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            Query query = session.createSQLQuery(hql);            
            todoRegistros = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Todos Registros de auditoria \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Todos Registros de auditoria \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return todoRegistros;
    }*/
    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<String[]> listarReporteAuditoria(String dni,String fec_ini, String fec_fin) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        //System.out.println(consulta);
        ArrayList <String[]> respuestaConsulta = new ArrayList<String[]>();
        try {
            //String string = "January 2, 2010";
            DateFormat df = new SimpleDateFormat("yyyy,MM,dd");
            //String fi = df.parse(fec_ini).toString();
            //String ff = df.parse(fec_fin).toString();
            /////////////////////////////////////////////////////////////////////
            String hql = "SELECT public.log_auditoria.log_aud_id,public.log_auditoria.tip_ope,public.log_auditoria.obs_ope,public.log_auditoria.con_ope,public.log_auditoria.fec_emi,public.log_auditoria.usu_id,\n"+
"pedagogico.persona.nom,pedagogico.persona.ape_pat,pedagogico.persona.ape_mat,pedagogico.persona.dni \n"+
"FROM public.log_auditoria,pedagogico.persona WHERE public.log_auditoria.usu_id = persona.per_id \n";
            if(!dni.equals("") && !fec_ini.equals("") && !fec_fin.equals("")){
                //si los tres campos llenos
                hql += " AND pedagogico.persona.dni = '";
                hql += dni;hql += "' ";
                hql += " AND public.log_auditoria.fec_emi >= '";
                hql += fec_ini;hql += "' ";
                hql += " AND public.log_auditoria.fec_emi <= '";
                hql += fec_fin;hql += "' ";
            }else if(!fec_ini.equals("") && !fec_fin.equals("")){
                //si oslo fecha
                hql += " AND public.log_auditoria.fec_emi >= '";
                hql += fec_ini;hql += "' ";
                hql += " AND public.log_auditoria.fec_emi <= '";
                hql += fec_fin;hql += "' ";
            }else if(!dni.equals("")){
                //si solo dni
                hql += " AND pedagogico.persona.dni = '";
                hql += dni;hql += "' ";
            }
            System.out.print("HQL******** ");
            System.out.println(hql);
            /////////////////////////////////////////////////////////////////////
            
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(hql).list();
            transaction.commit();
            for (Object[] result : respuesta) {
                String[] oS = new String[result.length];
                for(int i = 0 ; i < result.length; i++){
                    if(result[i]!=null){
                        String item_data = result[i].toString();
                        oS[i]=item_data;
                    }else{
                        String item_data = "-";
                        oS[i]=item_data;
                    }
                }
                respuestaConsulta.add(oS);
            }
            return respuestaConsulta;
        } catch (Exception ex){
            transaction.rollback();/////evitamos que se cierre la conexion al servidor PostGres si se realiza una mala consulta
            System.out.println("No se puede listarxConsultaGeneral " + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        } 
    }
}
