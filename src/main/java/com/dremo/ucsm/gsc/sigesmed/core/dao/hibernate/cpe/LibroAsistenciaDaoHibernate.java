    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import java.util.Date;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Carlos
 */
public class LibroAsistenciaDaoHibernate extends GenericDaoHibernate<ConfiguracionControl> implements LibroAsistenciaDao {

    @Override
    public List<ConfiguracionControl> listarConfiguracionesControl(Organizacion organizacion) {
        
    List<ConfiguracionControl> configuraciones = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT cc FROM ConfiguracionControl cc JOIN FETCH cc.trabajadorId tc JOIN FETCH tc.persona pp  WHERE   cc.conPerOrg =:p1  AND cc.estReg<>'E'   ORDER BY cc.conPerId DESC";
           
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion.getOrgId());
            configuraciones = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Configuraciones \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Configuraciones \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return configuraciones;
    }

    @Override
    public List<Trabajador> listarTrabajadores(Organizacion organizacion) {

        List<Trabajador> trabajadores = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tt FROM Trabajador tt JOIN FETCH tt.persona pp LEFT JOIN tt.traCar tc  WHERE   tt.organizacion =:p1  AND tt.estReg!='E'   ORDER BY tt.traId DESC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            trabajadores = query.list();
            
        } catch (Exception e) {
            System.out.println("No se pudo buscar los Trabajadores \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Trabajadores \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajadores;
    }

    @Override
    public Trabajador buscarTrabajadorPorDNI(String dni, Integer organizacion) {
        Trabajador trabajador = null;
        Organizacion org=new Organizacion(organizacion);
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tt FROM Trabajador tt JOIN FETCH tt.persona pp JOIN FETCH tt.traCar tc WHERE pp.dni=:p1  AND tt.organizacion =:p2  AND tt.estReg!='E'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", dni);
            query.setParameter("p2", org);
            trabajador = (Trabajador)query.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo buscar el  Trabajador \\n " );
            throw new UnsupportedOperationException("No se pudo buscar el Trabajador \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajador;
    }
    
    @Override
    public Trabajador buscarDocentePorDNI(String dni, Integer organizacion) {
        Trabajador trabajador = null;
        Organizacion org=new Organizacion(organizacion);
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tt FROM Trabajador tt JOIN FETCH tt.persona pp JOIN FETCH pp.docente dc JOIN FETCH tt.traCar tc WHERE pp.dni=:p1  AND tt.organizacion =:p2  AND tt.estReg!='E'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", dni);
            query.setParameter("p2", org);
            trabajador = (Trabajador)query.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo buscar el  Docente \\n " );
            throw new UnsupportedOperationException("No se pudo buscar el Docente \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajador;
    }

    @Override
    public ConfiguracionControl verificarLibroAsistenciaAbiertoByFecha(Integer organizacion, Date fecha) {
        ConfiguracionControl  config=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT cc FROM ConfiguracionControl cc WHERE  cc.conPerOrg=:p1 AND  cc.estReg='1' AND  cc.conPerFecIni<=:p2 AND cc.conPerFecFin>=:p2";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", organizacion);
            query.setParameter("p2", fecha);
            config = (ConfiguracionControl)query.uniqueResult();
            

        } catch (Exception e) {
            
            e.printStackTrace();
            System.out.println("No se pudo buscar el  Trabajador \\n " );
            throw new UnsupportedOperationException("No se pudo buscar el Trabajador \\n " + e.getMessage());
        } finally {
            session.close();
        }
    
        return config;
    }

    @Override
    public List<ConfiguracionControl> listarConfiguraciones(Organizacion organizacion) {
       
     List<ConfiguracionControl> configuraciones = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT cc FROM ConfiguracionControl cc   WHERE   cc.conPerOrg =:p1  AND cc.estReg<>'E'   ORDER BY cc.conPerId DESC";
           
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion.getOrgId());
            configuraciones = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Configuraciones \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Configuraciones \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return configuraciones;
    }

    @Override
    public Date getMinFechaConfiguraciones(Organizacion org) {
        Date fechaInicio = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT MIN(cc.conPerFecIni) FROM ConfiguracionControl cc  WHERE cc.conPerOrg =:p1 AND cc.estReg!='E'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", org.getOrgId());
            fechaInicio = (Date)query.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo conseguir la fecha \\n " );
            throw new UnsupportedOperationException("No se pudo conseguir la fecha \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fechaInicio;
    
    }

    @Override
    public List<Trabajador> listarTrabajadoresUsuario(Organizacion organizacion) {
        List<Trabajador> trabajadores = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tt FROM Trabajador tt,Usuario usu JOIN FETCH tt.persona pp LEFT JOIN tt.traCar tc  WHERE   tt.organizacion =:p1  AND tt.estReg!='E'   AND usu.usuId = pp.perId ORDER BY tt.traId DESC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            trabajadores = query.list();
            
        } catch (Exception e) {
            System.out.println("No se pudo buscar los Trabajadores \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Trabajadores \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajadores;
    }

    @Override
    public UsuarioSession getSessionByConfiguracionId(Integer rol, Integer id) {
      UsuarioSession  objeto=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ss FROM ConfiguracionControl cc JOIN  cc.trabajadorId tt JOIN  tt.persona pp JOIN  pp.usuario uu JOIN  uu.sessiones ss WHERE  cc.conPerId=:p1 AND cc.estReg<>'E' AND ss.rolId=:p2";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", id);
            query.setParameter("p2", rol);
            objeto = (UsuarioSession)query.uniqueResult();
            

        } catch (Exception e) {
            
            e.printStackTrace();
            System.out.println("No se pudo buscar la Configuracion \\n " );
            throw new UnsupportedOperationException("No se pudo buscar La Configuracion \\n " + e.getMessage());
        } finally {
            session.close();
        }
    
        return objeto; 
    }

    
}
