/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class FormacionEducativaDaoHibernate extends GenericDaoHibernate<FormacionEducativa> implements FormacionEducativaDao{

    @Override
    public List<FormacionEducativa> listarxFichaEscalafonaria(int perId) {
        List<FormacionEducativa> forEdu = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT forEdu from FormacionEducativa as forEdu "
                    + "join fetch forEdu.persona as fe "
                    + "WHERE fe.perId=" + perId + " AND forEdu.estReg='A'";
            Query query = session.createQuery(hql);
            forEdu = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las formaciones educativas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las formaciones educativas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return forEdu;
    }

    @Override
    public FormacionEducativa buscarForEduPorId(Integer forEduId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        FormacionEducativa f = (FormacionEducativa)session.get(FormacionEducativa.class, forEduId);
        session.close();
        return f;
    }
    
}
