/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.LogAuditoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.LogAuditoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class ListarRegistrosTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();  
        List<Object[]> registros = null;
        LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao)FactoryDao.buildDao("LogAuditoriaDao");
        try{
            registros = logAuditoriaDao.listarTodosRegistros();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los registros de auditoria  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los registros de auditoria ", e.getMessage() );
        }
        //EMPAQUETAMOS EN JSON///
        JSONArray miArray = new JSONArray();
        for(Object[] cRegistros:registros ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("log_aud_id", cRegistros[0]);
            oResponse.put("tip_ope", cRegistros[1]);
            oResponse.put("obs_ope", cRegistros[2]);
            oResponse.put("con_ope", cRegistros[3]);
            oResponse.put("fec_emi", cRegistros[4]);
            oResponse.put("usu_id", cRegistros[5]);
            oResponse.put("usu_emi", cRegistros[6]);
            oResponse.put("ip_acc", cRegistros[7]);
            oResponse.put("nom", cRegistros[8]);
            oResponse.put("ape_pat", cRegistros[9]);
            oResponse.put("ape_mat", cRegistros[10]);
            oResponse.put("dni", cRegistros[11]);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);
     
    }
}
