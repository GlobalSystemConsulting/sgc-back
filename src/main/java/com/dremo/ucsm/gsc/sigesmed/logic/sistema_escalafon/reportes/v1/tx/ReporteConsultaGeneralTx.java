/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.ReportXls;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author Administrador
 */
public class ReporteConsultaGeneralTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ReporteConsultaGeneralTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject response = new JSONObject();
        try {
            long idCer = 0;
            long idPer = wr.getIdUsuario();
            JSONObject data = (JSONObject) wr.getData();
            Character est_certificacion = 'W';//SIN ESTADO SOLO VALIDOS N,C,D
            if (data.has("id")) {
                int id_consulta_certificada = data.optInt("id");
                ConsultaCertificadaDao consultaCertificadaDao = (ConsultaCertificadaDao) FactoryDao.buildDao("ConsultaCertificadaDao");
                ConsultaCertificada consultaCertificada = consultaCertificadaDao.buscarPorID(id_consulta_certificada);
                est_certificacion = consultaCertificada.getEstReg();
                idCer = consultaCertificada.getIdUsuCer();
            }
            String titulo_reporte = data.optString("titulo");
            String observaciones_reporte = data.optString("observaciones_reporte");
            String consulta = data.optString("consulta");
            String idCertificacion = data.optString("idCertificacion");
            String nombreCertifi = "";
            JSONArray cabeceras = data.optJSONArray("cabeceras");
            List<String> items_cabeceras = new ArrayList<String>();
            for (int i = 0; i < cabeceras.length(); i++) {
                System.out.print("ELEMENTO: ");
                System.out.println(cabeceras.optJSONObject(i).optString("item_cabecera"));
                items_cabeceras.add(cabeceras.optJSONObject(i).optString("item_cabecera").toUpperCase());
            }

            Persona pe, perCer = null;
            PersonaDao peDao = (PersonaDao) FactoryDao.buildDao("di.PersonaDao");

            try {
                pe = peDao.buscarPersonaxId(idPer);
                if (est_certificacion == 'C') {
                    perCer = peDao.buscarPersonaxId(idCer);
                    nombreCertifi = perCer.getApePat() + " " + perCer.getApeMat() + ", " + perCer.getNom();
                }

            } catch (Exception e) {
                System.out.println("No se encontro a la persona con el DNI dado \n" + e);
                return WebResponse.crearWebResponseError("No se encontro a la persona con el DNI dado ", e.getMessage());
            }
            String nombreDeSolicita = pe.getApePat() + " " + pe.getApeMat() + ", " + pe.getNom();

            ///////////AUDITORIA//////////////
            AgregarOperacionAuditoria.agregarLecturaAuditoria("Se accedio a datos de perfiles escalafonarios", "Titulo Reporte: " + titulo_reporte, wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA//////////////
            try {
                ////////////faTOS USADOS//////////////////
                Boolean flag = true;
                List<String[]> consultas = new ArrayList<String[]>();
                ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao) FactoryDao.buildDao("ConsultaGeneralDao");
                try {
                    consultas = consultaGeneralDao.listarxConsultaGeneral(consulta);
                } catch (Exception e) {
                    System.out.println("No se pudo Crear Archivo de lista la consulta General  \n" + e);
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////recojimos datos ahora armar mi text que al retornarmitext.encodetoBase64 retorna String
                ////////////////////////////////////////////////////////////////////////////////////////////////////// 
                try {
                    //////////////////////////////////////Desempaquetamos la consulta/////////////////////////////////
                    ////////////////////////calculamos dimensiones de la consulta numero de columnas primero = numero cabeceras luego numero de filas
                    int number_rows = consultas.size();
                    int number_columns = items_cabeceras.size();//OBTENEMOS EL NUMERO COLUMNAS!! QUE NOS RETORNO LA CONSULTA
                    System.out.println("TAMAÑO DE CONSULTA LENGTH CONSULTA:");
                    System.out.println(number_rows);
                    System.out.println("NUMERO DE COLUMNAS DE CONSULTA:");
                    System.out.println(number_columns);
                    /* DATOS DE LA ORGANIZACION*/
                    String nombre_inst = titulo_reporte;

                    /*TITULO*/
                    String titulo = "RESUMEN DE CONSULTA GENERAL ";

                    /*CABECERA DE TABLA*/
                    String[] cab = items_cabeceras.toArray(new String[0]);//The toArray() method without passing any argument returns Object[]. So you have to pass an array as an argument, which will be filled with the data from the list, and returned. You can pass an empty array as well, but you can also pass an array with the desired size.


                    /*LLENAMOS LOS DATOS A LA CABECERA DEL REPORTE*/
                    Mitext mitext = null;
                    //Create Mitext with watermark
                    String marcaAgua = "CERTIFICADO";
                    String solicita="";
                    
                    if (idCertificacion.equals("555125") && est_certificacion == 'C') {
                        flag = true;
                        solicita="Solicitado por: " + nombreDeSolicita+"\nCertificado por: "+nombreCertifi;
                        mitext = new Mitext(true, titulo, solicita, true, marcaAgua);//Crea con marca de agua Certificado
                        //System.out.print("SI CERTIFICO**********:: ");System.out.println(est_certificacion);System.out.print("TITULO: ");System.out.println(titulo_reporte);              
                    } else {
                        solicita="Solicitado por: " + nombreDeSolicita;
                        mitext = new Mitext(true, titulo, solicita);//Crea sin marca de agua
                        //System.out.print("NOOOO CERTIFICO**********:: ");System.out.println(est_certificacion);System.out.print("TITULO: ");System.out.println(titulo_reporte);                
                    }
                    mitext.newLine(1);
                    mitext.agregarTitulo2("Titulo del reporte : " + nombre_inst);
                    mitext.newLine(-3);
                    //mitext.agregarParrafo("Titulo del reporte : " + nombre_inst);

                    /*CONSTRUIMOS EL OBJETO REPORTE*/
                    //float columnWidths_1 []= {1,1,2,1,2,2,1};
                    float columnWidths_1[] = new float[number_columns];
                    for (int nc = 0; nc < number_columns; nc++) {
                        columnWidths_1[nc] = 1;
                    }
                    GTabla tabla_1 = new GTabla(columnWidths_1);
                    tabla_1.setWidthPercent(100);
                    tabla_1.setRelativePosition(0, 20, 0, 0);
                    tabla_1.build(cab);

                    /*LLENAMOS LA DATA A LA TABLA*/
 /*GCell[] cell ={tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)
            ,tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)};*/
                    GCell[] cell = new GCell[number_columns];

                    /////////////////////////////////RECORREMOS ARREGLO DE ELEMENTOS OBTENIDOS EN LISTAR POR CONSULTA GENERAL///
                    /////////////////////////////////RECORREMOS ARREGLO DE ELEMENTOS OBTENIDOS EN LISTAR POR CONSULTA GENERAL///         
                    for (int nr = 0; nr < number_rows; nr++) {
                        for (int nc = 0; nc < number_columns; nc++) {
                            cell[nc] = tabla_1.createCellCenter(1, 1);
                        }
                        tabla_1.processLineCell(consultas.get(nr), cell);
                    }
                    mitext.agregarTabla(tabla_1);

                    /*PIE DE PAGINA*/
                    ArrayList<String>piePagina=new ArrayList();
                    mitext.newLine(5);
                    mitext.agregarNotaParrafo("OBSERVACIONES: " + observaciones_reporte);
                    piePagina.add("OBSERVACIONES: " + observaciones_reporte);
                    //mitext.agregarParrafo("NOTA : * El Presente Formato será utilizado para Reportes de Datos ESCALAFON");
                    piePagina.add("NOTA :* El Presente Formato será utilizado para Reportes de Datos ESCALAFON");
                    /*FECHA DE REPORTE*/
                    java.util.Date fecha = new Date();
                    String dat = new SimpleDateFormat("yyyy-MM-dd").format(fecha);
                    //mitext.agregarParrafo("** Reporte generado con Datos a la Fecha: " + dat);
                    piePagina.add("** Reporte generado con Datos a la Fecha: " + dat);
                    //mitext.agregarParrafo("Solicitado por: " + nombreDeSolicita);
                    piePagina.add("Solicitado por: " + nombreDeSolicita);
                    mitext.cerrarDocumento();
                    ///////////////////////////////7
                    String[] labels2 = cab;
                    float[] peso = columnWidths_1;
                    
                    ReportXls myReportXls = new ReportXls();
                    try {
                        //
                        //set header
                        myReportXls.setTitle1("ESCALAFON UNSA");
                        myReportXls.setTitle2("Titulo del reporte : " + nombre_inst);

                        //set title1
                        //myReportXls.setTitle1("SISTEMA DE ESCALAFON");
                        //set title2
                        //myReportXls.setTitle2("REPORTE RETRASOS");
                        //Show image
                        //myReportXls.setImageTop(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png");

                        //add fecha
                        myReportXls.setDate();

                        //create subtitles
                        //System.out.println(">>>>>>>>: "+cabecera.toString());
                        //myReportXls.setSubtitles(cabecera);
                        //add directorio
                        int filas = myReportXls.llenarData(labels2, consultas);
                        //agregando tabla
                        myReportXls.piePagina(piePagina,filas);
                        //close book
                        myReportXls.closeReport();

                    } catch (Exception e) {
                        System.out.println("No se pudo generar reporte en xls \n" + e);
                    }

                    response.append("file", mitext.encodeToBase64());
                    response.append("reporteXls", myReportXls.encodeToBase64());

                } catch (Exception e) {
                    System.out.println(e);
                    return null;

                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "crearReporte", e);
                return WebResponse.crearWebResponseError("No se puede crear el reporte");
            }
        } catch (Exception ex) {
            Logger.getLogger(ReporteConsultaGeneralTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        return WebResponse.crearWebResponseExito("El reporte se realizo", response);
    }

    

}
