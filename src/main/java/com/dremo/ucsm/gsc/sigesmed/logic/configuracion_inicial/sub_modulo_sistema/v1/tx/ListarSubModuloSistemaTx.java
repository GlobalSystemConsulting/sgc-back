/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.sub_modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarSubModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        boolean listarConFunciones = false;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            if( requestData!= null && requestData.length()> 0 ){
                listarConFunciones = requestData.getBoolean("listar");
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los SubModulos del sistema", e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<SubModuloSistema> subModulos = null;
        
        SubModuloSistemaDao subModuloDao = (SubModuloSistemaDao)FactoryDao.buildDao("SubModuloSistemaDao");
        try{
            if(listarConFunciones)
                subModulos = subModuloDao.buscarConModuloYFunciones();
            else
                subModulos = subModuloDao.buscarConModulo();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Sub Modulos del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Sub Modulos del Sistema", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(SubModuloSistema subModulo:subModulos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("subModuloID",subModulo.getSubModSisId() );
            oResponse.put("moduloID",subModulo.getModuloSistema().getModSisId() );
            oResponse.put("codigo",subModulo.getCod());
            oResponse.put("nombre",subModulo.getNom());
            oResponse.put("descripcion",subModulo.getDes());
            oResponse.put("icono",subModulo.getIco());
            oResponse.put("fecha",subModulo.getFecMod().toString());
            oResponse.put("estado",""+subModulo.getEstReg());
            
            List<FuncionSistema> funciones = subModulo.getFuncionSistemas();            
            if( listarConFunciones && funciones.size() > 0 ){
                JSONArray aFunciones = new JSONArray();
                for( FuncionSistema f:funciones ){
                    JSONObject oFuncion = new JSONObject();
                    oFuncion.put("nombre",f.getNom() );
                    oFuncion.put("clave",f.getClaNav());
                    oFuncion.put("estado",""+f.getEstReg() );
                    aFunciones.put(oFuncion);
                }
                oResponse.put("funciones",aFunciones);
            } 
            
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}


