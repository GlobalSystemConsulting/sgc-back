package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="periodo" ,schema="institucional" )
public class Periodo  implements java.io.Serializable {

    @Id
    @Column(name="per_id", unique=true, nullable=false)
    private char perId;
    @Column(name="fac_per")
    private int facPer;
    @Column(name="nom",length=64)
    private String nom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    

    public Periodo() {
    }
    public Periodo(char perId) {
        this.perId = perId;
    }
    public Periodo(char perId,int facPer, String nom, Date fecMod, int usuMod, char estReg) {
       this.perId = perId;
       this.facPer = facPer;
       this.nom = nom;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public char getPerId() {
        return this.perId;
    }    
    public void setPerId(char perId) {
        this.perId = perId;
    }
    
    public int getFacPer() {
        return this.facPer;
    }
    public void setFacPer(int facPer) {
        this.facPer = facPer;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
}


