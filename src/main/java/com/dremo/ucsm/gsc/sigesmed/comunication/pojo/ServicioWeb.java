/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.comunication.pojo;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author User
 */
public class ServicioWeb {

    private final static String Servicio="/rideaoffice/apijson?dni=";
    private String Ip;
    private String URL;

    public String getIp() throws IOException {
        return leerFichero();        
    }

    public void setIp(String Ip) throws IOException {
        escribirFichero(Ip);
    }

    public String getURL() throws IOException {
        return getIp()+Servicio;
    }

    public static String getServicio() {
        return Servicio;
    }
   
    private String leerFichero() throws FileNotFoundException, IOException{
        String cadena;
        String ruta=(ServicioREST.PATH_SIGESMED+"/archivos/dirRIDEA.txt").replace('\\','/');
        File archivo=new File(ruta);
        BufferedWriter bw;
        System.out.println("POR FAVOR "+archivo.getPath());        
        if(archivo.exists()){              
              try (BufferedReader b = new BufferedReader(new FileReader(ruta))) {
                while((cadena = b.readLine())!=null) {
                    System.out.println(cadena);
                    this.Ip=cadena;
                }
                b.close();
            }
        } else {
              archivo.createNewFile();
              bw = new BufferedWriter(new FileWriter(archivo));
              bw.write("http://10.100.100.18:8091");
              this.Ip="http://10.100.100.18:8091";
              bw.close();
        }
        
        return this.Ip;
    }
    
    private void escribirFichero( String ipruta) throws IOException{
        String ruta= (ServicioREST.PATH_SIGESMED+"/archivos/dirRIDEA.txt").replace('\\','/');
        BufferedWriter bw= new BufferedWriter( new FileWriter(ruta));
        System.out.println(ipruta);
        bw.write(ipruta);      
        bw.close();
    
    }
    
}
 