/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarCargosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarCargosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer carId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           carId = requestData.getInt("carId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarCargo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        CargoDao cargoDao = (CargoDao)FactoryDao.buildDao("se.CargoDao");
        try{
            cargoDao.delete(new Cargo(carId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar el cargo\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el cargo", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El cargo se elimino correctamente");
    }
    
}
