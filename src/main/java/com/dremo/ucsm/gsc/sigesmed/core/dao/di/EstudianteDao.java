/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Estudiante;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface EstudianteDao extends GenericDao<Estudiante> {
    public String buscarUltimoCodigo();
    public List<Estudiante> datosEstudiante();
    public List listarEstudiantes(String s, int orgId);
    //public List<Area> buscarConOrganizacionYAreaPadre();
    //public List<Area> buscarPorOrganizacion(int organizacionID);    
}
