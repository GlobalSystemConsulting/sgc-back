/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface FacultadDao extends GenericDao<Facultad>{
    public List<Facultad> listarxFacultad ();
    public Facultad buscarPorId(Integer facId);
}
