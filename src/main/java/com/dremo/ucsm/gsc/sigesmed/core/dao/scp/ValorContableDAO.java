/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Administrador
 */
public interface ValorContableDAO extends GenericDao<ValorContable>{
    
    public List<ValorContable> listarCuentas();
    public ValorContable obtenerValorBien(int id_bien);
}
