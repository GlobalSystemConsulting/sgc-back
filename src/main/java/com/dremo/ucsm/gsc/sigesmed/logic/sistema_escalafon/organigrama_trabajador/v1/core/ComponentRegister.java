/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_trabajador.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_trabajador.v1.tx.*;

/**
 *
 * @author felipe
 */
public class ComponentRegister implements IComponentRegister {
    
     @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("organigrama_trabajador");
        seComponent.setVersion(1);
        
        seComponent.addTransactionPOST("agregarTraOrgDet", InsertarOrganigramaToTrabajadorTx.class);
        seComponent.addTransactionPOST("actualizarTraOrgDet", ActualizarOrganigramaToTrabajadorTx.class);
        seComponent.addTransactionPUT("actTraOrgDetByCase", ActualizarTraOrgDetByCasosTx.class);
        return seComponent;
    }
    
}