/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoJornadaLaboralDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoJornadaLaboral;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarJornadasTx implements ITransaction{
private static final Logger logger = Logger.getLogger(ListarJornadasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        List<TipoJornadaLaboral> jornadas = null;
        TipoJornadaLaboralDao jornadaDao = (TipoJornadaLaboralDao)FactoryDao.buildDao("se.TipoJornadaLaboralDao");
        
        try{
            jornadas = jornadaDao.listarAll();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar jornadas",e);
            System.out.println("No se pudo listar las jornadas.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas jornadas.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoJornadaLaboral j: jornadas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id", j.getId());
            oResponse.put("nom", j.getNom());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las jornadas laborales fueron listadas exitosamente", miArray);
    }
    
}
