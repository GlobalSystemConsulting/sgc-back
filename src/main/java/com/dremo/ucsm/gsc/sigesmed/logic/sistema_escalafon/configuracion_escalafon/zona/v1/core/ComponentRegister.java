/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx.ActualizarZonasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx.AgregarZonasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx.EliminarZonasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx.ListarZonasTx;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
       WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("zonaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarZonas", ListarZonasTx.class);
        seComponent.addTransactionPUT("actualizarZonas", ActualizarZonasTx.class);
        seComponent.addTransactionPOST("agregarZonas", AgregarZonasTx.class);
        seComponent.addTransactionDELETE("eliminarZonas", EliminarZonasTx.class);
        return seComponent;
    }
    
}
