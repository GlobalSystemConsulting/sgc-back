package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrador on 02/12/2016.
 */
@Entity
@Table(name = "seccion_sequencia_didactica_sesion", schema = "pedagogico")
public class SeccionSequenciaDidacticaSesion implements java.io.Serializable {
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "sec_sec_did", nullable = true)
        protected int secSeqDid;
        @Column(name = "ses_sec_did_id", nullable = true)
        protected int sesSeqDidId;

        public Id() {
        }

        public Id(int secSeqDid, int sesSeqDidId) {
            this.secSeqDid = secSeqDid;
            this.sesSeqDidId = sesSeqDidId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (secSeqDid != id.secSeqDid) return false;
            return sesSeqDidId == id.sesSeqDidId;

        }

        @Override
        public int hashCode() {
            int result = secSeqDid;
            result = 31 * result + sesSeqDidId;
            return result;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "secSeqDid",column = @Column(name = "sec_sec_did", nullable = false)),
            @AttributeOverride(name = "sesSeqDidId",column = @Column(name = "ses_sec_did_id", nullable = false))
    })
    private Id id = new Id();
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_sec_did", insertable = false, updatable = false)
    private SeccionSequenciaDidactica seccionSequencia;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ses_sec_did_id", insertable = false, updatable = false)
    private SesionSequenciaDidactica sequenciaDidactica;

    @OneToMany(mappedBy = "seccionSequenciaDidactica", fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private List<ActividadSeccionSequenciaDidactica> actividades;
    @Column(name = "dur", nullable = false)
    private int dur;

    public SeccionSequenciaDidacticaSesion() {
    }

    public SeccionSequenciaDidacticaSesion(SeccionSequenciaDidactica seccionSequencia, SesionSequenciaDidactica sequenciaDidactica, int dur) {
        this.seccionSequencia = seccionSequencia;
        this.sequenciaDidactica = sequenciaDidactica;

        this.id.secSeqDid = seccionSequencia.getSecSeqDid();
        this.id.sesSeqDidId = sequenciaDidactica.getSesSeqDidId();

        this.dur = dur;
    }

    public SeccionSequenciaDidactica getSeccionSequencia() {
        return seccionSequencia;
    }



    public SesionSequenciaDidactica getSequenciaDidactica() {
        return sequenciaDidactica;
    }


    public int getDur() {
        return dur;
    }

    public void setDur(int dur) {
        this.dur = dur;
    }

    public List<ActividadSeccionSequenciaDidactica> getActividades() {
        return actividades;
    }
}
