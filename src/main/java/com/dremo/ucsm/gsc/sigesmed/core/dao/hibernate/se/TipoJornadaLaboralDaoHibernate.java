/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoJornadaLaboralDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoJornadaLaboral;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class TipoJornadaLaboralDaoHibernate extends GenericDaoHibernate<TipoJornadaLaboral> implements TipoJornadaLaboralDao{

    @Override
    public List<TipoJornadaLaboral> listarAll() {
        List<TipoJornadaLaboral> jornadas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from TipoJornadaLaboral as d "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            jornadas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los datos Organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos Organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return jornadas;
    }

    @Override
    public TipoJornadaLaboral buscarPorId(Integer jorLabId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoJornadaLaboral des = (TipoJornadaLaboral)session.get(TipoJornadaLaboral.class, jorLabId);
        session.close();
        return des;
    }
    
}
