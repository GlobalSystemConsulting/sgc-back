package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ComentarioTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoComentarioTema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarComentariosTemaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ListarComentariosTemaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            
            ComentarioTemaCapacitacionDao comentarioTemaCapacitacionDao = (ComentarioTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.ComentarioTemaCapacitacionDao");
            List<ComentarioTemaCapacitacion> comentarios = comentarioTemaCapacitacionDao.listarComentarios(data.getInt("temCod"));
            JSONArray array = new JSONArray();
            
            for(ComentarioTemaCapacitacion comment : comentarios) {
                JSONObject objectComment = changeCommentToObject(comment, data.getString("url"));
                List<ComentarioTemaCapacitacion> respuestas = comentarioTemaCapacitacionDao.listarRespuestas(comment.getComTemCapId());
                JSONArray arrayAnswers = new JSONArray();
                
                for(ComentarioTemaCapacitacion answer : respuestas)
                    arrayAnswers.put(changeCommentToObject(answer, data.getString("url")));
                
                objectComment.put("res", arrayAnswers);
                array.put(objectComment);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarComentarios", e);
            return WebResponse.crearWebResponseError("Error al listar los comentarios", WebResponse.BAD_RESPONSE);
        }
    }
    
    private JSONObject changeCommentToObject(ComentarioTemaCapacitacion comment, String url) {
        JSONObject object = new JSONObject();
                
        object.put("com", comment.getComTemCapId());
        object.put("tem", comment.getTemCurCapId());
        object.put("sed", comment.getSedCapId());

        Persona person = comment.getPersona();

        object.put("doc", person.getNom() + " " + person.getApePat() + " " + person.getApeMat());
        object.put("usu", person.getPerId());
        object.put("tex", comment.getCom());
        object.put("est", comment.getEstReg());

        JSONArray arrayAttachment = new JSONArray();

        for(AdjuntoComentarioTema attachment: comment.getAdjuntos()) {
            JSONObject objectAttachment = new JSONObject();

            objectAttachment.put("cod", attachment.getAdjComTemId());

            String nomFile = attachment.getNom();
            nomFile = nomFile.substring(nomFile.lastIndexOf("_adj_") + 6);
            nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
            nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));

            objectAttachment.put("nom", nomFile);                    
            objectAttachment.put("url", url + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_C_Address + attachment.getNom());

            arrayAttachment.put(objectAttachment);
        }

        object.put("adj", arrayAttachment);
        
        return object;
    }
}
