/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class CapacitacionDaoHibernate extends GenericDaoHibernate<Capacitacion> implements CapacitacionDao{

    @Override
    public List<Capacitacion> listarxFichaEscalafonaria(int perId) {
        List<Capacitacion> capacitaciones = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT capacitacion from Capacitacion as capacitacion "
                    + "join fetch capacitacion.persona as fe "
                    + "WHERE fe.perId=" + perId + " AND capacitacion.estReg='A'";
            Query query = session.createQuery(hql);
            capacitaciones = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las capacitaciones\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las capacitaciones\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return capacitaciones;
    }

    @Override
    public Capacitacion buscarPorId(Integer capId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Capacitacion cap = (Capacitacion)session.get(Capacitacion.class, capId);
        session.close();
        return cap;
    }
    
}
