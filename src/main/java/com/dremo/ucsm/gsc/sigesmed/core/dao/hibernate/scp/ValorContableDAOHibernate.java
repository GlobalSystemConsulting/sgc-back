/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;

/**
 *
 * @author Administrador
 */
public class ValorContableDAOHibernate extends GenericDaoHibernate<ValorContable> implements ValorContableDAO{

    @Override
    public List<ValorContable> listarCuentas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ValorContable obtenerValorBien(int id_bien) {
        
        
        ValorContable vc = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT vc FROM ValorContable vc WHERE vc.cod_bie=:p1 ";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",id_bien);
            vc = (ValorContable)(query.uniqueResult());
 
       }
        catch(Exception e){
            System.out.println("No se pudo Mostrar el Valor Contable del Bien \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Valor Contable del Bien \\n "+ e.getMessage());

        }
        finally{
            session.close();
        }
        return vc;  
        
    }
    
}
