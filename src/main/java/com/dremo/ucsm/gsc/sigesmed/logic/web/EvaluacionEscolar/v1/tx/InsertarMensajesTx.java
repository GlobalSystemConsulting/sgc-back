/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeCalificacion;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class InsertarMensajesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        List<MensajeCalificacion> mensajes = new ArrayList<MensajeCalificacion>();
        int evaluacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray listaMensajes = requestData.getJSONArray("mensajes");
            evaluacionID = requestData.getInt("evaluacionID");
            
            EvaluacionEscolar evaluacion = new EvaluacionEscolar(evaluacionID);
            
            //leendo los mensajes
            if(listaMensajes.length() > 0){
                for(int i = 0; i < listaMensajes.length();i++){
                    JSONObject bo = listaMensajes.getJSONObject(i);                    

                    String mensaje = bo.getString("mensaje");
                    int puntos = bo.getInt("puntos");                  

                    mensajes.add( new MensajeCalificacion(i+1,evaluacion,mensaje,puntos ));
                }
            }         
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar las mensajes, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        EvaluacionEscolarDao evalucionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            evalucionDao.eliminarMensajes(evaluacionID);
            evalucionDao.insertarMensajes(mensajes);
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar las mensajes", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        JSONArray aPre = new JSONArray();
        
        for(MensajeCalificacion p: mensajes){
            aPre.put(p.getMenCalId());
        }
        
        oResponse.put("mensajes",aPre);
        return WebResponse.crearWebResponseExito("El registro de las mensajes se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
