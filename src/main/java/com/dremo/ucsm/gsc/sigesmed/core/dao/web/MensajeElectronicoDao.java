/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensajeModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import java.util.List;

/**
 *
 * @author abel
 */
public interface MensajeElectronicoDao extends GenericDao<MensajeElectronico>{
    
    public void enviarMensaje(MensajeElectronico mensaje,int destinatarioID);
    public void enviarMensaje(MensajeElectronico mensaje,List<Integer> destinatarios);
    
    public List<BandejaMensaje> listarMensajesPorUsuario(int usuarioID);
    public List<MensajeElectronico> listarMensajesEnviadosPorUsuario(int usuarioID);
    
    public List<BandejaMensajeModel> listarMensajesNuevosNoVistosPorUsuario(int usuarioID);
    
    public List<MensajeDocumento> verDocumentosMensaje(int mensajeID);
    
    public void marcarVistoMensaje(int bandejaMensajeID);
    public void verMensaje(int bandejaMensajeID);
}
