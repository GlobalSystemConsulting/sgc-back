/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import javax.persistence.*;
import java.util.Date;

                                                                                                                  @Entity
@Table(name = "cargo_de_comision", schema = "pedagogico")
//@org.hibernate.annotations.Immutable
public class CargoDeComision implements java.io.Serializable{
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "com_id", nullable = false)
        protected int comId;

        @Column(name = "car_com_id", nullable = false)
        protected int carComId;

        public Id(){}
        public Id(int comId, int carComId){
            this.comId = comId;
            this.carComId = carComId;
        }
        @Override
        public boolean equals(Object o){
            if(o != null && o instanceof Id){
                Id id = (Id) o;
                return this.comId == id.comId
                        && this.carComId == id.carComId;
            }
            return false;
        }
        @Override
        public int hashCode(){
            return this.comId*this.carComId + 1;
        }
    }
    @EmbeddedId
    @AttributeOverrides( {
            @AttributeOverride(name="comId", column=@Column(name="com_id", nullable=false) ),
            @AttributeOverride(name="carComId", column=@Column(name="car_com_id", nullable=false)) })
    private Id id = new Id();

    @Column(name = "des_car")
    private  String desCar;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_id",insertable = false, updatable = false)
    private Comision comision;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_com_id",insertable = false, updatable = false)
    private CargoComision cargoComision;

    protected CargoDeComision(){}
    public CargoDeComision(Comision comision, CargoComision cargoComision, String desCar) {
        this.comision = comision;
        this.cargoComision = cargoComision;
        this.desCar = desCar;

        this.id.carComId = cargoComision.getCarComId();
        this.id.comId = comision.getComId();

        this.comision.getCargoDeComisiones().add(this);
        this.fecMod = new Date();
        this.estReg = 'A';
        //this.cargoComision.getCargosDeComision().add(this);
    }

    public String getDesCar() {
        return desCar;
    }

    public void setDesCar(String desCar) {
        this.desCar = desCar;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Comision getComision() {
        return comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

    public CargoComision getCargoComision() {
        return cargoComision;
    }

    public void setCargoComision(CargoComision cargoComision) {
        this.cargoComision = cargoComision;
    }
}