/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ActualizarTipoOrganigramaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarTipoOrganigramaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer facId = requestData.getInt("tipId");
            String codFac = requestData.optString("codTipOrga");
            String nomFac = requestData.optString("nomTipOrga");
            String desTip= requestData.optString("desTipOrga");
            Integer datId = requestData.getInt("datId");
            DatosOrganigramaDao datosOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");
            DatosOrganigrama datosOrganigrama = datosOrganigramaDao.buscarXId(datId);
            
            return actualizarCargos( facId, codFac, nomFac,desTip ,datosOrganigrama);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCargos(Integer facId,String codFac, String nomFac,String des, DatosOrganigrama organismo) {
        try{
            TipoOrganigramaDao tipoOrganigramaDao = (TipoOrganigramaDao)FactoryDao.buildDao("se.TipoOrganigramaDao");        
            TipoOrganigrama tipoOrganigrama = tipoOrganigramaDao.buscarXId(facId);

            tipoOrganigrama.setCodTipOrgi(codFac);
            tipoOrganigrama.setNomTipOrgi(nomFac);
            tipoOrganigrama.setDesTipOrgi(des);
            tipoOrganigrama.setDatosOrganigramas(organismo);
            tipoOrganigramaDao.update(tipoOrganigrama);
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipId", tipoOrganigrama.getTipOrgiId());
            oResponse.put("datId", tipoOrganigrama.getDatosOrganigramas().getNomDatOrgi()+" "+tipoOrganigrama.getDatosOrganigramas().getAnioDatOrgi());
            oResponse.put("codTipOrga", tipoOrganigrama.getCodTipOrgi());
            oResponse.put("nomTipOrga", tipoOrganigrama.getNomTipOrgi());
            oResponse.put("desTipOrga", tipoOrganigrama.getDesTipOrgi());
            return WebResponse.crearWebResponseExito(" La Tipo Organigrama actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, La Tipo Organigrama no fue actualizado");
        }
    }
}
