/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.service;

import com.dremo.ucsm.gsc.sigesmed.core.security.TokenAuthentication;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;

import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Administrador
 */
@Provider
public class FiltroAutenticacionREST implements ContainerResponseFilter{

    @Override
    public void filter(ContainerRequestContext requestContext,  ContainerResponseContext response) throws IOException {
        
        //System.out.println(" URI : "+ requestContext.getUriInfo().getPath()+ "  tipo " + requestContext.getMethod());
         if (requestContext.getMethod().equals("OPTIONS")) {
            response.setStatus(200);
            response.getHeaders().add("Access-Control-Expose-Headers", "Authorization, autorizacion");
            response.getHeaders().add("Access-Control-Allow-Origin", "*");
            response.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, autorizacion");
            response.getHeaders().add("Access-Control-Allow-Credentials", "true");
            response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            response.getHeaders().add("Access-Control-Max-Age", "1209600");
        }
        else {
            response.getHeaders().add("Access-Control-Expose-Headers", "Authorization, autorizacion");
            response.getHeaders().add("Access-Control-Allow-Origin", "*");
            response.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, autorizacion");
            response.getHeaders().add("Access-Control-Allow-Credentials", "true");
            response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            response.getHeaders().add("Access-Control-Max-Age", "1209600");
        }
        
        //si no solicitamos logiarnos verificacmos la seguridad de nuestro servicio
//        if( !requestContext.getUriInfo().getPath().contentEquals("login") ){        
//            String autorizacion = requestContext.getHeaders().getFirst("autorizacion");
//            
//            //System.out.println(autorizacion);
//            
//            //si no hay token
//            if( autorizacion==null || autorizacion.contentEquals("") ){
//                System.out.println("NO HAY TOKEN");
//                requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
//                return;
//            }
//            //token no autorizado
//            if( !(new TokenAuthentication()).isValidAuthentication(autorizacion) ){
//                System.out.println("TOKEN INVALIDO");
//                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
//                return;
//            }
//            
//            
//        }
    }
}