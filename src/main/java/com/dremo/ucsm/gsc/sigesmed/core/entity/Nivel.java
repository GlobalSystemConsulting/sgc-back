/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 *
 * @author Administrador
 */
@Entity(name="com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Nivel")
@Table(name = "nivel")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Nivel.findAll", query = "SELECT n FROM Nivel n"),
//    @NamedQuery(name = "Nivel.findByNivId", query = "SELECT n FROM Nivel n WHERE n.nivId = :nivId"),
//    @NamedQuery(name = "Nivel.findByAbr", query = "SELECT n FROM Nivel n WHERE n.abr = :abr"),
//    @NamedQuery(name = "Nivel.findByNom", query = "SELECT n FROM Nivel n WHERE n.nom = :nom"),
//    @NamedQuery(name = "Nivel.findByDes", query = "SELECT n FROM Nivel n WHERE n.des = :des"),
//    @NamedQuery(name = "Nivel.findByFecMod", query = "SELECT n FROM Nivel n WHERE n.fecMod = :fecMod"),
//    @NamedQuery(name = "Nivel.findByUsuMod", query = "SELECT n FROM Nivel n WHERE n.usuMod = :usuMod"),
//    @NamedQuery(name = "Nivel.findByEstReg", query = "SELECT n FROM Nivel n WHERE n.estReg = :estReg")})
public class Nivel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "niv_id")
    private Integer nivId;
    @Column(name = "abr")
    private String abr;
    @Column(name = "nom")
    private String nom;
    @Column(name = "des")
    private String des;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private Character estReg;    
    @OneToMany(mappedBy = "nivel")
    private List<Organizacion> organizacion;

    public Nivel() {
    }

    public Nivel(Integer nivId) {
        this.nivId = nivId;
    }

    public Integer getNivId() {
        return nivId;
    }

    public void setNivId(Integer nivId) {
        this.nivId = nivId;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    @XmlTransient
    public List<Organizacion> getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacionCollection(List<Organizacion> organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nivId != null ? nivId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nivel)) {
            return false;
        }
        Nivel other = (Nivel) object;
        if ((this.nivId == null && other.nivId != null) || (this.nivId != null && !this.nivId.equals(other.nivId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.Nivel[ nivId=" + nivId + " ]";
    }
    
}
