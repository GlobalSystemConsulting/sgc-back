package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tecnicas_curso_capacitacion", schema = "pedagogico")
public class TecnicaCursoCapacitacion implements Serializable {
    @Id
    @Column(name = "tec_cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_tecnicas_curso_capacitacion", sequenceName="pedagogico.tecnicas_curso_capacitacion_tec_cur_cap_id_seq")
    @GeneratedValue(generator = "secuencia_tecnicas_curso_capacitacion")
    private int tecCurCapId;
    
    @Column(name = "nom",nullable = false,length = 20)
    private String nom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "met_cur_cap_id")
    private MetodologiaCursoCapacitacion metodologiaCursoCapacitacion;

    public TecnicaCursoCapacitacion() {}
    
    public TecnicaCursoCapacitacion(String nom) {
        this.nom = nom;
    }

    public int getTecCurCapId() {
        return tecCurCapId;
    }

    public void setTecCurCapId(int tecCurCapId) {
        this.tecCurCapId = tecCurCapId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public MetodologiaCursoCapacitacion getMetodologiaCursoCapacitacion() {
        return metodologiaCursoCapacitacion;
    }

    public void setMetodologiaCursoCapacitacion(MetodologiaCursoCapacitacion metodologiaCursoCapacitacion) {
        this.metodologiaCursoCapacitacion = metodologiaCursoCapacitacion;
    }
}
