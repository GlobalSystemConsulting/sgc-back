/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.PasosAyudaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.PasosAyuda;

/**
 *
 * @author Administrador
 */
public class PasosAyudaDaoHibernate extends GenericDaoHibernate<PasosAyuda> implements PasosAyudaDao{
    
}
