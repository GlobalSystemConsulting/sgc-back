/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.se.TipoPariente")
@Table(name = "tipo_pariente", schema="public")

public class TipoPariente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tpa_id")
    private int tpaId;
    
    @Column(name = "tpa_des")
    private String tpaDes;

    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private char estReg;
    
    @Column(name = "des")
    private String des;
    
    public TipoPariente() {
    }

    public TipoPariente(int tpaId) {
        this.tpaId = tpaId;
    }

    public TipoPariente(String tpaDes) {        
        this.tpaDes = tpaDes;
    }

    public int getTpaId() {
        return tpaId;
    }

    public void setTpaId(int tpaId) {
        this.tpaId = tpaId;
    }

    public String getTpaDes() {
        return tpaDes;
    }

    public void setTpaDes(String tpaDes) {
        this.tpaDes = tpaDes;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    //INICIO -usado para el catalogo
    
    public int getId(){
        return this.tpaId;
    }
    public void setId(int rolId){
        this.tpaId=rolId;
    }
    
    public String getNom() {
        return this.tpaDes;
    }
    public void setNom(String nom) {
        this.tpaDes = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsm.sigesmed.core.entity.se.TipoPariente[ tpaId=" + tpaId + " ]";
    }
    
}
