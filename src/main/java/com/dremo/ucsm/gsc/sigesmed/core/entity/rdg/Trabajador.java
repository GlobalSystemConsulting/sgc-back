/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 *
 * @author ucsm
 */

@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.rdg.Trabajador")
@Table(name = "trabajador")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Trabajador.findAll", query = "SELECT t FROM Trabajador t"),
//    @NamedQuery(name = "Trabajador.findByTraId", query = "SELECT t FROM Trabajador t WHERE t.traId = :traId"),
//    @NamedQuery(name = "Trabajador.findByTieServ", query = "SELECT t FROM Trabajador t WHERE t.tieServ = :tieServ"),
//    @NamedQuery(name = "Trabajador.findByFecIng", query = "SELECT t FROM Trabajador t WHERE t.fecIng = :fecIng"),
//    @NamedQuery(name = "Trabajador.findBySal", query = "SELECT t FROM Trabajador t WHERE t.sal = :sal"),
//    @NamedQuery(name = "Trabajador.findByTraCar", query = "SELECT t FROM Trabajador t WHERE t.traCar = :traCar"),
//    @NamedQuery(name = "Trabajador.findByFecMod", query = "SELECT t FROM Trabajador t WHERE t.fecMod = :fecMod"),
//    @NamedQuery(name = "Trabajador.findByUsuMod", query = "SELECT t FROM Trabajador t WHERE t.usuMod = :usuMod"),
//    @NamedQuery(name = "Trabajador.findByEstReg", query = "SELECT t FROM Trabajador t WHERE t.estReg = :estReg"),
//    @NamedQuery(name = "Trabajador.findByPerId", query = "SELECT t FROM Trabajador t WHERE t.perId = :perId")})
public class Trabajador implements Serializable {
    @JoinColumn(name = "tra_car", referencedColumnName = "crg_tra_ide")
    @ManyToOne
    private TrabajadorCargo traCar;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tra_id")
    private Integer traId;
    @Column(name = "tie_serv")
    private Short tieServ;
    @Column(name = "fec_ing")
    @Temporal(TemporalType.DATE)
    private Date fecIng;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sal")
    private BigDecimal sal;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "per_id")
    private long perId;
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne
    private Organizacion orgId;

    public Trabajador() {
    }

    public Trabajador(Integer traId) {
        this.traId = traId;
    }

    public Trabajador(Integer traId, long perId) {
        this.traId = traId;
        this.perId = perId;
    }

    public Integer getTraId() {
        return traId;
    }

    public void setTraId(Integer traId) {
        this.traId = traId;
    }

    public Short getTieServ() {
        return tieServ;
    }

    public void setTieServ(Short tieServ) {
        this.tieServ = tieServ;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public BigDecimal getSal() {
        return sal;
    }

    public void setSal(BigDecimal sal) {
        this.sal = sal;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    public Organizacion getOrgId() {
        return orgId;
    }

    public void setOrgId(Organizacion orgId) {
        this.orgId = orgId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (traId != null ? traId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trabajador)) {
            return false;
        }
        Trabajador other = (Trabajador) object;
        if ((this.traId == null && other.traId != null) || (this.traId != null && !this.traId.equals(other.traId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador[ traId=" + traId + " ]";
    }

    public TrabajadorCargo getTraCar() {
        return traCar;
    }

    public void setTraCar(TrabajadorCargo traCar) {
        this.traCar = traCar;
    }
    
}
