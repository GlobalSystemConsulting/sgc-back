/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import javax.persistence.CascadeType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.di.Parientes")
@Table(name = "parientes", schema="public")
@IdClass(ParientesPK.class)
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Parientes.findAll", query = "SELECT p FROM Parientes p"),
//    @NamedQuery(name = "Parientes.findByParId", query = "SELECT p FROM Parientes p WHERE p.parientePK.parId = :parId"),
//    @NamedQuery(name = "Parientes.findByPerId", query = "SELECT p FROM Parientes p WHERE p.parientePK.perId = :perId"),
//    @NamedQuery(name = "Parientes.findByTpaId", query = "SELECT p FROM Parientes p WHERE p.tpaId = :tpaId")})

public class Parientes implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "par_id")
    private long parId;
    
    @Id
    @Column(name = "per_id")
    private long perId;
    
    @JoinColumn(name = "tpa_id", referencedColumnName = "tpa_id")    
    @ManyToOne(fetch=FetchType.LAZY)    
    private TipoPariente parentesco;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)    
    @JoinColumn(name = "per_id", insertable = false, updatable = false)    
    private Persona persona;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)    
    @JoinColumn(name = "par_id", insertable = false, updatable = false)
    private Persona pariente;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";

    public Parientes() {
    }
    
    public Parientes(long parId, long perId) {
        this.parId = parId;
        this.perId = perId;     
    }

    public Parientes(long parId, long perId, TipoPariente parentesco) {
        this.parId = parId;
        this.perId = perId;
        this.parentesco = parentesco;        
    }
    
    public Parientes(long parId, long perId, TipoPariente parentesco, Persona persona, Persona pariente, Date fecMod, Integer usuMod) {
        this.parId = parId;
        this.perId = perId;
        this.parentesco = parentesco;
        this.persona = persona;
        this.pariente = pariente;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
    }

    public Persona getPariente() {
        return pariente;
    }

    public void setPariente(Persona pariente) {
        this.pariente = pariente;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TipoPariente getParentesco() {
        return parentesco;
    }

    public void setParentesco(TipoPariente parentesco) {
        this.parentesco = parentesco;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    public long getParId() {
        return parId;
    }

    public void setParId(long parId) {
        this.parId = parId;
    }
        
}

