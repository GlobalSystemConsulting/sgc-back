/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author Alex
 */
public class BuscarPersonaTrabajadorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String dni = "";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dni = requestData.getString("dni");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("El dni ingresado es incorrecto" );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Persona persona = null;
        FichaEscalafonaria fichaEscalafonaria = new FichaEscalafonaria();
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("se.PersonaDao");
        int caso=0;
        /*3 Casos: 
        *   1.- Tiene: persona, trabajador, ficha
        *   2.- Tiene: persona
        *   0.- No tiene: persona, trabajador, ficha
        */
        try{
           fichaEscalafonaria = fichaEscalafonariaDao.buscarPorDNI(dni);
           if(fichaEscalafonaria!=null){
               persona = fichaEscalafonaria.getTrabajador().getPersona();
               caso=1;
           }else{
               persona = personaDao.buscarPorDNI(dni);
               if(persona!=null)
                   caso=2;
           }
        }catch(Exception e){
            System.out.println("\n"+e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage() );
        }
        
        if(caso==1){
            
            JSONObject oRes = new JSONObject();
            JSONObject oPersona = new JSONObject();
            
            String apePat = persona.getApePat();
            String apeMat = persona.getApeMat();
            String nom = persona.getNom();
            Character sex = persona.getSex();
            Integer estCivId = persona.getEstCivId()==null?0:persona.getEstCivId();
            String pas=persona.getPas();
            Boolean estAseEss=persona.getBoolSalud();
            String autEss = persona.getAutEss();
            String fij = persona.getFij();
            String num1 = persona.getNum1();
            String num2 = persona.getNum2();
            String email = persona.getEmail();
            Integer nacId = persona.getNacionalidad()==null?0:persona.getNacionalidad().getNacId();
            String depNac = persona.getDepNac();
            String proNac = persona.getProNac();
            String disNac = persona.getDisNac();
            Date fecNac = persona.getFecNac();
            
            String sisPen = persona.getSisPen();
            String tipAfp = persona.getTipAfp();
            String codCuspp = persona.getCodCuspp();
            Date fecIngAfp = persona.getFecIngAfp();
            Date fecTraAfp = persona.getFecTraAfp();
            Boolean perDis = persona.getPerDis();
            String regCon = persona.getRegCon();
            Integer idiomId = persona.getIdiomas()==null?0:persona.getIdiomas().getIdiId();
            String licCond = persona.getLicCond();
            String bonCaf = persona.getBonCaf();
            String foto=fichaEscalafonaria.getTrabajador().getPersona().getFoto();
            
            oPersona.put("perId", persona.getPerId());
            oPersona.put("perCod", persona.getPerCod());
            oPersona.put("apePat", apePat==null?"":apePat);
            oPersona.put("apeMat",  apeMat==null?"":apeMat);
            oPersona.put("nom", nom==null?"":nom);
            oPersona.put("sex", sex==null?"":sex);
            oPersona.put("estCivId", estCivId);
            //edad
            oPersona.put("dni", dni);
            oPersona.put("pas", pas==null?"":pas);
            oPersona.put("estAseEss", estAseEss==null?"":estAseEss);
            oPersona.put("autEss", autEss==null?"":autEss);
            oPersona.put("fij", fij==null?"":fij);
            oPersona.put("num1", num1==null?"":num1);
            oPersona.put("num2", num2==null?"":num2);
            oPersona.put("email", email==null?"":email);
            oPersona.put("nacId", nacId);
            oPersona.put("depNac", depNac==null?"":depNac);
            oPersona.put("proNac", proNac==null?"":proNac);
            oPersona.put("disNac", disNac==null?"":disNac);
            oPersona.put("fecNac", fecNac==null?"":fecNac);
            oPersona.put("sisPen", sisPen==null?"":sisPen);
            oPersona.put("tipAfp", tipAfp==null?"":tipAfp);
            oPersona.put("codCuspp", codCuspp==null?"":codCuspp);
            oPersona.put("fecIngAfp", fecIngAfp==null?"":fecIngAfp);
            oPersona.put("fecTraAfp", fecTraAfp==null?"":fecTraAfp);
            oPersona.put("perDis", perDis==null?"":perDis);
            oPersona.put("regCon", regCon==null?"":regCon);
            oPersona.put("idiomId", idiomId);
            oPersona.put("licCond", licCond==null?"":licCond);
            oPersona.put("bonCaf", bonCaf==null?"":bonCaf);
            oPersona.put("foto",foto==null?"":foto);
            oPersona.put("caso",caso);
            
            oRes.put("persona", oPersona);
            
            return WebResponse.crearWebResponseExito("El trabajador ya se encuentra registrado", oRes);
        } else {
            if (caso == 2) {
                JSONObject oRes = new JSONObject();
                JSONObject oPersona = new JSONObject();

                String apePat = persona.getApePat();
                String apeMat = persona.getApeMat();
                String nom = persona.getNom();
                Character sex = persona.getSex();
                Date fecNac = persona.getFecNac();
                String fij = persona.getFij();
                String num1 = persona.getNum1();
                String num2 = persona.getNum2();
                String email = persona.getEmail();
                
                oPersona.put("perId", persona.getPerId());
                oPersona.put("perCod", persona.getPerCod());
                oPersona.put("apePat", apePat == null ? "" : apePat);
                oPersona.put("apeMat", apeMat == null ? "" : apeMat);
                oPersona.put("nom", nom == null ? "" : nom);
                oPersona.put("sex", sex == null ? "" : sex);
                oPersona.put("fecNac", fecNac==null?"":fecNac);
                oPersona.put("fij", fij==null?"":fij);
                oPersona.put("num1", num1==null?"":num1);
                oPersona.put("num2", num2==null?"":num2);
                oPersona.put("email", email==null?"":email);
                 oPersona.put("caso",caso);
            
                oRes.put("persona", oPersona);
            
                return WebResponse.crearWebResponseExito("Se encontró a la persona, pero no es trabajador", oRes);
            } else {
                return WebResponse.crearWebResponseError("El trabajador no esta registrado en el sistema");
            }
        }
    }
    
}
