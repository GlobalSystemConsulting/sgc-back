package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CriterioCertificacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.DesarrolloTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CriterioCertificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class CalcularCriteriosTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(CalcularCriteriosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            int codCap = data.getInt("codCap");
            int sedCod = data.getInt("sedCod");
            
            CriterioCertificacionDao criterioCertificacionDao = (CriterioCertificacionDao) FactoryDao.buildDao("capacitacion.CriterioCertificacionDao");
            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            DesarrolloTemaCapacitacionDao desarrolloTemaCapacitacionDao = (DesarrolloTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.DesarrolloTemaCapacitacionDao");
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            
            List<CriterioCertificacion> criterios = criterioCertificacionDao.buscarPorCapacitacion(codCap);
            List<ParticipanteCapacitacion> participantes = participanteCapacitacionDao.listarPorSede(sedCod);
            
            int totEvaCon = desarrolloTemaCapacitacionDao.totalEvaluaciones(sedCod);
            int totTar = desarrolloTemaCapacitacionDao.totalTareas(sedCod);
            int totEvaPar = evaluacionCursoCapacitacionDao.totalEvaluaciones(sedCod);
            int totAsi = asistenciaParticipanteDao.totalAsistencias(sedCod);

            
            JSONArray arrayCri = new JSONArray(EntityUtil.listToJSONString(
                new String[]{"nom", "tipCri"}, new String[]{"nom", "tip"},
                criterios
            ));
            
            JSONArray array = new JSONArray();
            
            for(ParticipanteCapacitacion participante: participantes) {
                JSONObject object = new JSONObject();
                
                Persona persona = participante.getPersona();
                object.put("nom", persona.getApePat() + " " + persona.getApeMat() + " " + persona.getNom());
                object.put("cod", persona.getPerId());
                
                JSONArray criterias = new JSONArray();
                
                //boolean withGrade = false;
                boolean state = true;
                //double genPro = 0;
                //int genNum = 0;
                
                for(CriterioCertificacion criterio: criterios) {                   
                    switch(criterio.getTipCri()) {
                        case 'C': //Evaluaciones Continuas
                            List<Double> evaCon = evaluacionDesarrolloDao.obtenerParaCertificacion(sedCod, persona.getPerId(), criterio.getNotMin(), 'E');
                            JSONObject objectEvaCon = new JSONObject();
                            objectEvaCon.put("tot", criterio.getVal());
                            double val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectEvaCon.put("tip", true);
                                    double per = 100*evaCon.size()/totEvaCon;
                                    objectEvaCon.put("per", per);
                                    val = per;
                                    
                                    if(per >= criterio.getVal()) {
                                        objectEvaCon.put("est", true);
                                        /*double not = promedio(evaCon);
                                        objectEvaCon.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectEvaCon.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectEvaCon.put("tip", false);
                                    objectEvaCon.put("num", evaCon.size());
                                    val = 100*evaCon.size()/criterio.getVal();
                                    
                                    if(evaCon.size() >= criterio.getVal()) {
                                        objectEvaCon.put("est", true);
                                        /*double not = promedio(evaCon)*20/evaCon.size();
                                        objectEvaCon.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectEvaCon.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectEvaCon.put("val", val);
                            objectEvaCon.put("sty", estilo(val));
                            criterias.put(objectEvaCon);
                            break;
                        
                        case 'T': //Tardanzas (Asistencias)
                            int cantTar = asistenciaParticipanteDao.obtenerParaCertificacion(sedCod, persona.getPerId(), 'T');
                            JSONObject objectTard = new JSONObject();
                            objectTard.put("tot", criterio.getVal());
                            val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectTard.put("tip", true);
                                    double per = 100*cantTar/totAsi;
                                    objectTard.put("per", per);
                                    val = per;
                                    
                                    if(per <= criterio.getVal())
                                        objectTard.put("est", true);
                                    else {
                                        objectTard.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectTard.put("tip", false);
                                    objectTard.put("num", cantTar);
                                    val = 100*cantTar/criterio.getVal();
                                    
                                    if(cantTar <= criterio.getVal())
                                        objectTard.put("est", true);
                                    else {
                                        objectTard.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectTard.put("val", val);
                            objectTard.put("sty", estilo(val));
                            criterias.put(objectTard);
                            break;
                        
                        case 'F': //Faltas (Asistencias)
                            int cantFal = asistenciaParticipanteDao.obtenerParaCertificacion(sedCod, persona.getPerId(), 'F');
                            int cantJus = asistenciaParticipanteDao.obtenerParaCertificacion(sedCod, persona.getPerId(), 'J');
                            JSONObject objectFal = new JSONObject();
                            objectFal.put("tot", criterio.getVal());
                            objectFal.put("fal", cantFal);
                            objectFal.put("jus", cantJus);
                            val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectFal.put("tip", true);
                                    double per = 100*(cantFal + cantJus)/totAsi;
                                    objectFal.put("per", per);
                                    val = per;
                                    
                                    if(per <= criterio.getVal())
                                        objectFal.put("est", true);
                                    else {
                                        objectFal.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectFal.put("tip", false);
                                    val = 100*(cantFal + cantJus)/criterio.getVal();
                                    
                                    if((cantFal + cantJus) <= criterio.getVal())
                                        objectFal.put("est", true);
                                    else {
                                        objectFal.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectFal.put("val", val);
                            objectFal.put("sty", estilo(val));
                            criterias.put(objectFal);
                            break;
                        
                        case 'A': //Asistencias
                            int cantAsi = asistenciaParticipanteDao.obtenerParaCertificacion(sedCod, persona.getPerId(), 'A');
                            JSONObject objectAsi = new JSONObject();
                            objectAsi.put("tot", criterio.getVal());
                            val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectAsi.put("tip", true);
                                    double per = 100*cantAsi/totAsi;
                                    objectAsi.put("per", per);
                                    val = per;
                                    
                                    if(per >= criterio.getVal())
                                        objectAsi.put("est", true);
                                    else {
                                        objectAsi.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectAsi.put("tip", false);
                                    objectAsi.put("num", cantAsi);
                                    val = 100*cantAsi/criterio.getVal();
                                    
                                    if(cantAsi >= criterio.getVal())
                                        objectAsi.put("est", true);
                                    else {
                                        objectAsi.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectAsi.put("val", val);
                            objectAsi.put("sty", estilo(val));
                            criterias.put(objectAsi);
                            break;
                        
                        case 'P': //Evaluaciones Parciales
                            List<Double> evaPar = evaluacionParticipanteCapacitacionDao.obtenerParaCertificacion(sedCod, persona.getPerId(), criterio.getNotMin());
                            JSONObject objectEvaPar = new JSONObject();
                            objectEvaPar.put("tot", criterio.getVal());
                            val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectEvaPar.put("tip", true);
                                    double per = 100*evaPar.size()/totEvaPar;
                                    objectEvaPar.put("per", per);
                                    val = per;
                                    
                                    if(per >= criterio.getVal()) {
                                        objectEvaPar.put("est", true);
                                        /*double not = promedio(evaPar);
                                        objectEvaPar.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectEvaPar.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectEvaPar.put("tip", false);
                                    objectEvaPar.put("num", evaPar.size());
                                    val = 100*evaPar.size()/criterio.getVal();
                                    
                                    if(evaPar.size() >= criterio.getVal()) {
                                        objectEvaPar.put("est", true);
                                        /*double not = promedio(evaPar);
                                        objectEvaPar.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectEvaPar.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectEvaPar.put("val", val);
                            objectEvaPar.put("sty", estilo(val));
                            criterias.put(objectEvaPar);
                            break;
                        
                        case 'H': //Tareas
                            List<Double> tar = evaluacionDesarrolloDao.obtenerParaCertificacion(sedCod, persona.getPerId(), criterio.getNotMin(), 'T');
                            JSONObject objectTar = new JSONObject();
                            objectTar.put("tot", criterio.getVal());
                            val = 0;
                            
                            switch(criterio.getTipEva()) {
                                case 'P': //Porcentual
                                    objectTar.put("tip", true);
                                    double per = 100*tar.size()/totTar;
                                    objectTar.put("per", per);
                                    val = per;
                                    
                                    if(per >= criterio.getVal()) {
                                        objectTar.put("est", true);
                                        /*double not = promedio(tar);
                                        objectTar.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectTar.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                                    
                                case 'N': //Nominal
                                    objectTar.put("tip", false);
                                    objectTar.put("num", tar.size());
                                    val = 100*tar.size()/criterio.getVal();
                                    
                                    if(tar.size() >= criterio.getVal()) {
                                        objectTar.put("est", true);
                                        /*double not = promedio(tar);
                                        objectTar.put("pro", not);
                                        genPro += not;
                                        genNum++;
                                        withGrade = true;*/
                                    } else {
                                        objectTar.put("est", false);
                                        state = false;
                                    }
                                    
                                    break;
                            }
                            
                            objectTar.put("val", val);
                            objectTar.put("sty", estilo(val));
                            criterias.put(objectTar);
                            break;
                    }
                }
                
                //object.put("genNot", withGrade);
                //object.put("notGen", genPro/genNum);
                object.put("genEst", state);
                object.put("cri", criterias);
                array.put(object);
            }
            
            JSONObject result = new JSONObject();
            result.put("crit", arrayCri);
            result.put("part", array);
            
        return WebResponse.crearWebResponseExito("Exito al calcular los criterios", WebResponse.OK_RESPONSE).setData(result);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "calcularCriterios", e);
            return WebResponse.crearWebResponseError("Error al calcular los criterios", WebResponse.BAD_RESPONSE);
        }
    }
    
    private Double promedio(List<Double> notas) {
        double result = 0;
        
        for(double grade: notas) {
            result += grade;
        }
        
        return result/notas.size();
    }
    
    private String estilo(double value) {
        String style = "";
        
        if (value < 25)
            style = "danger";
        else if (value < 50)
            style = "warning";
        else if (value < 75)
            style = "info";
        else
            style = "success";
        
        return style;
    }
}
