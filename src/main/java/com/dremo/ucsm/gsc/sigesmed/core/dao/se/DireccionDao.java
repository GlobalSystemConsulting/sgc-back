/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface DireccionDao extends GenericDao<Direccion>{
    public Direccion buscarPorId(Integer dirId);
    public List<Direccion> listarxPersona(int perId);

}
