/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import java.util.List;
import javax.persistence.*;
/**
 *
 * @author Administrador
 */
@Entity
@Table(name="ayuda" ,schema="pedagogico")
public class Ayuda implements java.io.Serializable{
    @Id
    @Column(name="ayu_id", unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_ayu", sequenceName="pedagogico.ayuda_ayu_id_seq")
    @GeneratedValue(generator="secuencia_ayu")
    private int ayudaId;
    
    @Column(name="ayu_tip", nullable=false, length=1)
    private char ayuTip;
   
    @Column(name="est_reg", nullable=false, length=1)
    private char estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fun_sis_id")
    private FuncionSistema funcionSistemaId;
    
    
    @OneToMany(mappedBy="ayuda", cascade=CascadeType.PERSIST)
    private List<PasosAyuda> pasosAyuda;
    
    public Ayuda(){}
    
    public Ayuda(int ayuId){
        this.ayudaId=ayuId;
    }

    public Ayuda(int ayudaId, int funcionSistemId, char ayuTip){
        this.ayudaId=ayudaId;
        this.funcionSistemaId=new FuncionSistema(funcionSistemId);
        this.ayuTip=ayuTip;
    }
     public Ayuda(int ayudaId, int funcionSistemId, char ayuTip,char estReg){
        this.ayudaId=ayudaId;
        this.funcionSistemaId=new FuncionSistema(funcionSistemId);
        this.ayuTip=ayuTip;
        this.estReg=estReg;
    }
    public Ayuda(int ayudaId, int funcionSistemaId) {
        this.ayudaId = ayudaId;
        this.funcionSistemaId = new FuncionSistema(funcionSistemaId);
    }
    public Ayuda(int ayudaId, int funcionSistemaId, List<PasosAyuda> pasos) {
        this.ayudaId = ayudaId;
        this.funcionSistemaId = new FuncionSistema(funcionSistemaId);
        this.pasosAyuda=pasos;
    }
    
    public int getAyudaId() {
        return ayudaId;
    }

    public void setAyudaId(int ayudaId) {
        this.ayudaId = ayudaId;
    }

    public FuncionSistema getFuncionSistemaId() {
        return funcionSistemaId;
    }

    public void setFuncionSistemaId(FuncionSistema funcionSistemaId) {
        this.funcionSistemaId = funcionSistemaId;
    }

    public List<PasosAyuda> getPasosAyuda() {
        return pasosAyuda;
    }

    public void setPasosAyuda(List<PasosAyuda> pasosAyuda) {
        this.pasosAyuda = pasosAyuda;
    }

    public char getAyuTip() {
        return ayuTip;
    }

    public void setAyuTip(char ayuTip) {
        this.ayuTip = ayuTip;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "Ayuda{" + "ayudaId=" + ayudaId + ", ayuTip=" + ayuTip + ", estReg=" + estReg + ", funcionSistemaId=" + funcionSistemaId + ", pasosAyuda=" + pasosAyuda + '}';
    }
   
}
