/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarCargosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarCargosTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Cargo cargo = null;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            //Integer carId = requestData.getInt("carId");
            String codCar = requestData.optString("codCar");
            //Character estReg = requestData.optString("estReg").charAt(0);
            String nomCar = requestData.optString("nomCar");
            //Date fecMod = (Date)requestData.getString("fecMod");
            //Integer usuMod = requestData.getInt("usuMod");
            cargo = new Cargo();
            cargo.setCodCar(codCar);
            cargo.setNomCar(nomCar);
            cargo.setEstReg('A');
            cargo.setFecMod(new Date());
            cargo.setUsuMod(wr.getIdUsuario());
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo Cargo",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        CargoDao cargoDao = (CargoDao) FactoryDao.buildDao("se.CargoDao");
        try {
            cargoDao.insert(cargo);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva organizacion",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("carId", cargo.getCarId());
        oResponse.put("codCar", cargo.getCodCar());
        oResponse.put("nomCar", cargo.getNomCar());
                
        return WebResponse.crearWebResponseExito("El registro del cargo se realizo correctamente", oResponse);

    }
    
}