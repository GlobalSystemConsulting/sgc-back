/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoAreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarTipoAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TipoArea> tipoAreas = null;
        TipoAreaDao areaDao = (TipoAreaDao)FactoryDao.buildDao("TipoAreaDao");
        try{
            tipoAreas = areaDao.buscarTodos(TipoArea.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las TipoAreas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos de Areas", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoArea area:tipoAreas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoAreaID",area.getTipAreId() );
            oResponse.put("nombre",area.getNom());
            oResponse.put("alias",area.getAli());
            oResponse.put("fecha",area.getFecMod().toString());
            oResponse.put("estado",""+area.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

