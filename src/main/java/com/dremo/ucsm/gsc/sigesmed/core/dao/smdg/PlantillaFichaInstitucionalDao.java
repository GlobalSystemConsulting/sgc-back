/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface PlantillaFichaInstitucionalDao extends GenericDao<PlantillaFichaInstitucional>{
    public String buscarUltimoCodigo();
    public List<Object[]> listarPlantillas();
    public PlantillaFichaInstitucional obtenerPlantilla(int plaId);
    public PlantillaFichaInstitucional getPlantilla(int plaId);
}
