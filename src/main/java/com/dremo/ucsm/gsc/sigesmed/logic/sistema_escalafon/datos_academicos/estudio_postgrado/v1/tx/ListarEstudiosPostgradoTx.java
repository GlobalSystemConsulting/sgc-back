/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarEstudiosPostgradoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarEstudiosPostgradoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<EstudioPostgrado> estPos = null;
        EstudioPostgradoDao estPosDao = (EstudioPostgradoDao)FactoryDao.buildDao("se.EstudioPostgradoDao");
        
        try{
            estPos = estPosDao.listarxFichaEscalafonaria(perId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar estudios de postgrado",e);
            System.out.println("No se pudo listar los estudios de postgrado\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los estudios de postgrado", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(EstudioPostgrado ep:estPos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("estPosId", ep.getEstPosId());
            oResponse.put("tip", ep.getTip());
            oResponse.put("tipDes", "");
            oResponse.put("numDoc", ep.getNumDoc());
            oResponse.put("pais", ep.getPais());
            oResponse.put("fecDoc", ep.getFecDoc());
            oResponse.put("tipDocId", ep.getTipDocId());
            oResponse.put("tipDoc", new TipoDoc(ep.getTipDocId()).getNom());
            oResponse.put("fecIniEst", ep.getFecIniEst());
            oResponse.put("fecTerEst", ep.getFecTerEst());
            oResponse.put("ins", ep.getIns());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los estudios de postgrado fueron listados exitosamente", miArray);
    }
    
}
