package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="estado_expediente" ,schema="administrativo")
public class EstadoExpediente  implements java.io.Serializable  {

    @Id
    @Column(name="est_exp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_estadoexpediente", sequenceName="administrativo.estado_expediente_est_exp_id_seq" )
    @GeneratedValue(generator="secuencia_estadoexpediente")
    private int estExpId;
    @Column(name="nom", nullable=false, length=32)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;

    public EstadoExpediente() {
    }
    public EstadoExpediente(int estExpId) {
        this.estExpId = estExpId;
    }
    public EstadoExpediente(int estExpId, String nom, String des, Date fecMod, Integer usuMod, char estReg) {
       this.estExpId = estExpId;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
    public int getEstExpId() {
        return this.estExpId;
    }
    public void setEstExpId(int estExpId) {
        this.estExpId = estExpId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}


