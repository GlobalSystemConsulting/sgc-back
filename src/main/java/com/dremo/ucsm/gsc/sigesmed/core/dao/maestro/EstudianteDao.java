package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;

/**
 * Created by Administrador on 17/01/2017.
 */
public interface EstudianteDao extends GenericDao<Estudiante>{
    GradoIEEstudiante buscarGradoIEEstudiante(int idGradoIE);
}
