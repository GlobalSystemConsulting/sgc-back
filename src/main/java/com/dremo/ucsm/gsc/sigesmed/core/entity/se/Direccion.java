/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "direccion", schema="administrativo")
public class Direccion implements Serializable {

    @Id
    @Column(name = "dir_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_direccion", sequenceName="administrativo.direccion_dir_id_seq" )
    @GeneratedValue(generator = "secuencia_direccion")
    private Integer dirId;

    @Column(name = "tip")
    private Character tip;
    
    @Column(name = "nom", length=200)
    private String nom;
    
    @Column(name = "des_ref", length=256)
    private String desRef;
    
    @Column(name = "nom_zon", length=20)
    private String nomZon;
    
    @Column(name = "dep", length=20)
    private String dep;
    
    @Column(name = "pro", length=20)
    private String pro;
    
    @Column(name = "dis", length=20)
    private String dis;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Direccion() {
    }

    public Direccion(Integer dirId) {
        this.dirId = dirId;
    }
    
    public Direccion(Persona persona){
        this.persona = persona;
    }
    
    public Direccion(Persona persona, Character tip, String nom, String dep, String pro, String dis, String nomZon, String desRef, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.tip = tip;
        this.nom = nom;
        this.dep = dep;
        this.pro = pro;
        this.dis = dis;
        this.nomZon = nomZon;
        this.desRef = desRef;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public Direccion(Persona persona, Character tip, String nom, String dep, String pro, String dis, String nomZon, String desRef, Character estReg) {
        this.persona = persona;
        this.tip = tip;
        this.nom = nom;
        this.dep = dep;
        this.pro = pro;
        this.dis = dis;
        this.nomZon = nomZon;
        this.desRef = desRef;
        this.estReg = estReg;
    }

    public Integer getDirId() {
        return dirId;
    }

    public void setDirId(Integer dirId) {
        this.dirId = dirId;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDesRef() {
        return desRef;
    }

    public void setDesRef(String desRef) {
        this.desRef = desRef;
    }

    public String getNomZon() {
        return nomZon;
    }

    public void setNomZon(String nomZon) {
        this.nomZon = nomZon;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dirId != null ? dirId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.dirId == null && other.dirId != null) || (this.dirId != null && !this.dirId.equals(other.dirId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Direccion{" + "dirId=" + dirId + ", tip=" + tip + ", nom=" + nom + ", desRef=" + desRef + ", nomZon=" + nomZon + ", dep=" + dep + ", pro=" + pro + ", dis=" + dis + ", usuMod=" + usuMod + ", estReg=" + estReg + ", fecMod=" + fecMod + '}';
    }

}
