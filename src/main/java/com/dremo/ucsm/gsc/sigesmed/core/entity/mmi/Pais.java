package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Pais generated by hbm2java
 */
@Entity
@Table(name="pais"
    ,schema="pedagogico"
)
public class Pais  implements java.io.Serializable {


     private int paiId;
     private String paiNom;
     private Integer usuMod;
     private Date fecMod;
     private Character estReg;
     private Set<DatosNacimiento> datosNacimientos = new HashSet<DatosNacimiento>(0);

    public Pais() {
    }

	
    public Pais(int paiId) {
        this.paiId = paiId;
    }
    public Pais(int paiId, String paiNom, Integer usuMod, Date fecMod, Character estReg, Set<DatosNacimiento> datosNacimientos) {
       this.paiId = paiId;
       this.paiNom = paiNom;
       this.usuMod = usuMod;
       this.fecMod = fecMod;
       this.estReg = estReg;
       this.datosNacimientos = datosNacimientos;
    }
   
     @Id 

    
    @Column(name="pai_id", unique=true, nullable=false)
    public int getPaiId() {
        return this.paiId;
    }
    
    public void setPaiId(int paiId) {
        this.paiId = paiId;
    }

    
    @Column(name="pai_nom", length=60)
    public String getPaiNom() {
        return this.paiNom;
    }
    
    public void setPaiNom(String paiNom) {
        this.paiNom = paiNom;
    }

    
    @Column(name="usu_mod")
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    @Column(name="est_reg", length=1)
    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

@OneToMany(mappedBy="pais")
    public Set<DatosNacimiento> getDatosNacimientos() {
        return this.datosNacimientos;
    }
    
    public void setDatosNacimientos(Set<DatosNacimiento> datosNacimientos) {
        this.datosNacimientos = datosNacimientos;
    }




}


