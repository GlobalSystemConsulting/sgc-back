/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.PrioridadExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.PrioridadExpediente;
import java.util.Date;

/**
 *
 * @author abel
 */
public class ActualizarPrioridadExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PrioridadExpediente prioridadExpedienteAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int prioridadExpedienteID = requestData.getInt("prioridadExpedienteID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            prioridadExpedienteAct = new PrioridadExpediente(prioridadExpedienteID, nombre, descripcion, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la prioridad de expediente, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PrioridadExpedienteDao prioridadDao = (PrioridadExpedienteDao)FactoryDao.buildDao("std.PrioridadExpedienteDao");
        try{
            prioridadDao.update(prioridadExpedienteAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar la prioridad de expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la prioridad de expediente", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Prioridad de Expediente se actualizo correctamente");
        //Fin
    }
    
}
