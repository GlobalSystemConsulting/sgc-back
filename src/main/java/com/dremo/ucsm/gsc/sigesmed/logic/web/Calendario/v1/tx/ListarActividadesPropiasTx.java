/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Calendario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarActividadesPropiasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
              
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<ActividadCalendario> actividadesPropias = null;
        List<Calendario> actividadesForaneas = null;
        ActividadCalendarioDao actividadDao = (ActividadCalendarioDao)FactoryDao.buildDao("web.ActividadCalendarioDao");
        try{
            actividadesPropias = actividadDao.listarCalendarioPropio(wr.getIdUsuario());
            actividadesForaneas = actividadDao.listarCalendarioForaneo(wr.getIdUsuario());
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las actividades del usuario"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los actividades del usuario", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miActPro = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;
        for(ActividadCalendario actividad:actividadesPropias ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("actividadID",actividad.getActCalId() );
            oResponse.put("titulo",actividad.getTit());
            oResponse.put("descripcion",actividad.getDes());
            oResponse.put("tipo",""+actividad.getTipo());
            
            if(actividad.getHorIni().getHours()==0 && actividad.getHorIni().getMinutes()==0 && actividad.getHorIni().getSeconds()==0){
                oResponse.put("horaInicio",sf2.format(actividad.getHorIni()) );
                oResponse.put("horaI",false);
            }
            else{
                oResponse.put("horaInicio",sf.format(actividad.getHorIni()) );
                oResponse.put("horaI",true);
            }
            
            if(actividad.getHorFin() != null ){
                if(actividad.getHorFin().getHours()==0 && actividad.getHorFin().getMinutes()==0 && actividad.getHorFin().getSeconds()==0){
                    oResponse.put("horaFin",sf2.format(actividad.getHorFin()) );
                    oResponse.put("horaF",false);
                }
                else{
                    oResponse.put("horaFin",sf.format(actividad.getHorFin()) );
                    oResponse.put("horaF",true);
                }
            }
            
            oResponse.put("i",i++);
            
            miActPro.put(oResponse);
        }
        
        JSONArray miActFor = new JSONArray();
        i = 0;
        for(Calendario actividad:actividadesForaneas ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("calendarioID",actividad.getCalId() );
            oResponse.put("actividadID",actividad.getActCalId() );
            oResponse.put("titulo",actividad.getActividad().getTit());
            oResponse.put("descripcion",actividad.getActividad().getDes());
            oResponse.put("tipo",""+actividad.getActividad().getTipo());
            oResponse.put("horaInicio",sf.format(actividad.getActividad().getHorIni()) );
            if(actividad.getActividad().getHorFin() != null )
                oResponse.put("horaFin",sf.format(actividad.getActividad().getHorFin()) );
            
            oResponse.put("i",i++);
            
            miActFor.put(oResponse);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("actividadesPropias", miActPro);
        oResponse.put("actividadesForaneas", miActFor);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los actividades programadas del usuario",oResponse);
        //Fin
    }
    
}


