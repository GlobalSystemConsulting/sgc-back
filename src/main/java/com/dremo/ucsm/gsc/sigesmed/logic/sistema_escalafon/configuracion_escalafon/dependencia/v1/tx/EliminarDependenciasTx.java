/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarDependenciasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarDependenciasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer depId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           depId = requestData.getInt("depId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarDependencia",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        DependenciaDao dependenciaDao = (DependenciaDao)FactoryDao.buildDao("se.DependenciaDao");
        try{
            dependenciaDao.delete(new Dependencia(depId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la dependencia\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la dependencia", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La dependencia se elimino correctamente");
    }
    
}
