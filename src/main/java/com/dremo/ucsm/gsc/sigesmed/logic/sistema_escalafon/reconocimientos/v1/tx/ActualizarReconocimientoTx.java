/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reconocimientos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarReconocimientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarReconocimientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer recId = requestData.getInt("recId");
            Character mot = requestData.getString("mot").charAt(0);
            String desMot = requestData.getString("desMot");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("")?null:sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            String entEmi = requestData.getString("entEmi");
            Integer tipdocId = requestData.getInt("tipDocId");
            
            return actualizarReconocimiento(recId, mot, desMot, numDoc, fecDoc, tipdocId, entEmi);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar reconocimiento",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarReconocimiento(Integer recId, Character mot, String desMot, String numDoc, Date fecDoc, Integer tipDocId, String entEmi) {
        try{
            ReconocimientoDao reconocimientoDao = (ReconocimientoDao)FactoryDao.buildDao("se.ReconocimientoDao");        
            Reconocimiento reconocimiento = reconocimientoDao.buscarPorId(recId);

            reconocimiento.setMot(mot);
            reconocimiento.setDesMot(desMot);
            reconocimiento.setNumDoc(numDoc);
            reconocimiento.setFecDoc(fecDoc);
            reconocimiento.setTipDocId(tipDocId);
            reconocimiento.setEntEmi(entEmi);
            
            reconocimientoDao.update(reconocimiento);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("recId", reconocimiento.getRecId());
            oResponse.put("mot", reconocimiento.getMot());
            oResponse.put("desMot", reconocimiento.getDesMot()==null?"":reconocimiento.getDesMot());
            oResponse.put("numDoc", reconocimiento.getNumDoc()==null?"":reconocimiento.getNumDoc());
            oResponse.put("fecDoc", reconocimiento.getFecDoc()==null?"":sdo.format(reconocimiento.getFecDoc()));
            oResponse.put("tipDocId", reconocimiento.getTipDocId()==null?0:reconocimiento.getTipDocId());
            oResponse.put("entEmi", reconocimiento.getEntEmi()==null?"":reconocimiento.getEntEmi());
            
            return WebResponse.crearWebResponseExito("Reconocimiento actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarReconocimiento",e);
            return WebResponse.crearWebResponseError("Error, el reconocimiento no fue actualizado");
        }
    } 
    
}
