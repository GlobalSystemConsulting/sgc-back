/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abel
 */
public class PersonaDaoHibernate extends GenericDaoHibernate<Persona> implements PersonaDao {

    private static final Logger logger = Logger.getLogger(PersonaDaoHibernate.class.getName());
    
    @Override
    public Persona buscarPersonaPorId(Integer perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Persona p = (Persona)session.get(Persona.class, perId);
        session.close();
        return p;
    }

    @Override
    public Persona buscarPorDNI(String perDNI) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(Persona.class)
                    .add(Restrictions.eq("dni", perDNI));

            return (Persona) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorDni", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
