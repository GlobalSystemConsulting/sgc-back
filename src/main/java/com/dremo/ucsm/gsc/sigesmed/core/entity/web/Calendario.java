package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="pedagogico.calendario" )
public class Calendario  implements java.io.Serializable {

    @Id 
    @Column(name="cal_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_calendario", sequenceName="pedagogio.calendario_cal_id_seq" )
    @GeneratedValue(generator="secuencia_calendario")
    private int calId;
    
    @Column(name="act_cal_id")
    private int actCalId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="act_cal_id",updatable = false,insertable = false)
    private ActividadCalendario actividad;
    
    @Column(name="usu_ses_id")
    private int usuSesId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_ses_id",insertable = false,updatable = false)
    private UsuarioSessionPersona session;

    public Calendario() {
    }
    public Calendario(int calId) {
        this.calId = calId;
    }

	
    public Calendario(int calId, int actCalId, int usuSesId) {
        this.calId = calId;
        this.actCalId = actCalId;
        this.usuSesId = usuSesId;
    }
     
    public int getCalId() {
        return this.calId;
    }
    
    public void setcalId(int calId) {
        this.calId = calId;
    }
    
    public int getActCalId() {
        return this.actCalId;
    }
    public void setActCalId(int actCalId) {
        this.actCalId = actCalId;
    }
    public ActividadCalendario getActividad() {
        return this.actividad;
    }
    public void setActividad(ActividadCalendario actividad) {
        this.actividad = actividad;
    }
    public int getUsuSesId() {
        return this.usuSesId;
    }    
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }
    
    public UsuarioSessionPersona getSession() {
        return this.session;
    }    
    public void setSession(UsuarioSessionPersona session) {
        this.session = session;
    }
}


