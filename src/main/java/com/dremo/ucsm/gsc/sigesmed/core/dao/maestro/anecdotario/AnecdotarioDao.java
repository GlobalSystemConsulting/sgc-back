package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.AccionesCorrectivas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;

import java.util.List;

/**
 * Created by Administrador on 19/12/2016.
 */
public interface AnecdotarioDao extends GenericDao<Anecdotario>{

    Anecdotario buscarAnecdota(int idAnecdota);
    Anecdotario buscarAnecdotaConAcciones(int idAnecdota);
    Estudiante buscarEstudiante(int idEstu);
    List<Grado> listarGradosDocente(int org,int doc);
    List<Grado> listarGradosDocente(int org,int idPlan,int doc);
    com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado buscarGrado(int idGrado);
    List<Estudiante> listarEstudiantes(int idOrg,int idGrad,Character idSecc);
    List<Anecdotario> listarAnecdotasEstudiante(int idEstudiante,int idDocente);
    List<AccionesCorrectivas> listarAccionesAnecdotario(int idAnecdota);
    void eliminarAccionesCorrectivas(int idAnec);
    Matricula buscarMatriculaActual(int idEst,int idOrg);
}
