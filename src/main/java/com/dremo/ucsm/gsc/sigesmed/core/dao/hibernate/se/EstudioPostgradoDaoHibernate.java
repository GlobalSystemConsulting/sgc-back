/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class EstudioPostgradoDaoHibernate extends GenericDaoHibernate<EstudioPostgrado> implements EstudioPostgradoDao{

    @Override
    public List<EstudioPostgrado> listarxFichaEscalafonaria(int perId) {
        List<EstudioPostgrado> estPos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT estPos from EstudioPostgrado as estPos "
                    + "join fetch estPos.persona as fe "
                    + "WHERE fe.perId=" + perId + " AND estPos.estReg='A'";
            Query query = session.createQuery(hql);
            estPos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los estudios de postgrado \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los estudios de postgrado \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return estPos;
    }

    @Override
    public EstudioPostgrado buscarPorId(Integer estPosId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudioPostgrado estPos = (EstudioPostgrado)session.get(EstudioPostgrado.class, estPosId);
        session.close();
        return estPos;
    }
    
}
