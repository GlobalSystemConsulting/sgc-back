/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarPublicacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarPublicacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer pubId = requestData.getInt("pubId");
            String nomEdi = requestData.getString("nomEdi");
            String tipPub = requestData.getString("tipPub");
            String titPub = requestData.getString("titPub");
            String graPar = requestData.getString("graPar");
            String lug = requestData.getString("lug");
            Date fecPub = requestData.getString("fecPub").equals("")?null:sdi.parse(requestData.getString("fecPub").substring(0, 10));
            String numReg = requestData.getString("numReg");       
            
            return actualizarPublicacion(pubId, nomEdi, tipPub, titPub, graPar, lug, fecPub, numReg);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar publicacion",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarPublicacion(Integer pubId, String nomEdi, String tipPub, String titPub, 
            String graPar, String lug, Date fecPub, String numReg) {
        try{
            PublicacionDao publicacionDao = (PublicacionDao)FactoryDao.buildDao("se.PublicacionDao");        
            Publicacion publicacion = publicacionDao.buscarPorId(pubId);

            publicacion.setNomEdi(nomEdi);
            publicacion.setTipPub(tipPub);
            publicacion.setTitPub(titPub);
            publicacion.setGraPar(graPar);
            publicacion.setLug(lug);
            publicacion.setFecPub(fecPub);
            publicacion.setNumReg(numReg);
            
            publicacionDao.update(publicacion);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("pubId", publicacion.getPubId());
            oResponse.put("nomEdi", publicacion.getNomEdi()==null?"":publicacion.getNomEdi());
            oResponse.put("tipPub", publicacion.getTipPub()==null?"":publicacion.getTipPub());
            oResponse.put("titPub", publicacion.getTitPub()==null?"":publicacion.getTitPub());
            oResponse.put("graPar", publicacion.getGraPar()==null?"":publicacion.getGraPar());
            oResponse.put("lug", publicacion.getLug()==null?"":publicacion.getLug());
            oResponse.put("fecPub", publicacion.getFecPub()==null?"":sdo.format(publicacion.getFecPub()));
            oResponse.put("numReg", publicacion.getNumReg()==null?"":publicacion.getNumReg());
            return WebResponse.crearWebResponseExito("Publicacion actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarPublicacion",e);
            return WebResponse.crearWebResponseError("Error, la publicacion no fue actualizada");
        }
    } 
    
}
