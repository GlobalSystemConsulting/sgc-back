package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "detalle_permisos_banco", schema = "pedagogico")
public class DetallePermisosBanco implements java.io.Serializable{
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "ban_lec_id",nullable = false)
        protected int bacLecId;
        @Column(name = "per_id",nullable = false)
        protected int perId;

        public Id() {
        }

        public Id(int bacLecId, int perId) {
            this.bacLecId = bacLecId;
            this.perId = perId;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (bacLecId != id.bacLecId) return false;
            return perId == id.perId;

        }

        @Override
        public int hashCode() {
            int result = bacLecId;
            result = 31 * result + perId;
            return result;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "bacLecId",column = @Column(name = "ban_lec_id", nullable = false)),
            @AttributeOverride(name = "perId",column = @Column(name = "per_id", nullable = false))
    })
    private Id id = new Id();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ban_lec_id", insertable = false, updatable = false)
    private BancoLectura banco;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id", insertable = false, updatable = false)
    private Persona persona;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_usu", insertable = false, updatable = false)
    private Organizacion org;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_asign")
    private Date fecAsig;

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public DetallePermisosBanco() {
    }

    public DetallePermisosBanco(BancoLectura banco, Persona persona, Date fecAsig) {
        this.banco = banco;
        this.persona = persona;
        this.id.bacLecId = banco.getBanLecId();
        this.id.perId = persona.getPerId();
        this.fecAsig = fecAsig;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public BancoLectura getBanco() {
        return banco;
    }

    public void setBanco(BancoLectura banco) {
        this.banco = banco;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Organizacion getOrg() {
        return org;
    }

    public void setOrg(Organizacion org) {
        this.org = org;
    }

    public Date getFecAsig() {
        return fecAsig;
    }

    public void setFecAsig(Date fecAsig) {
        this.fecAsig = fecAsig;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
