    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 *
 * @author Carlos
 */
public class AsistenciaDaoHibernate extends GenericDaoHibernate<RegistroAsistencia> implements AsistenciaDao {

    @Override
    public List<HorarioDet> listarHorario(Trabajador trabajador, DiaSemana dia) {
      List<HorarioDet> lista=null;
      Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  hd FROM Trabajador tt INNER JOIN  tt.horaCabId hc INNER JOIN hc.horarioDetalle hd  WHERE hd.estReg<>'E' AND hd.diaSemId =:p2  AND tt.traId=:p1 ORDER BY hd.fecIngreso ASC, hd.fecSalida ASC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", trabajador.getTraId());
            query.setParameter("p2", dia);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
    
      return lista;
    
    }
    
    @Override
    public List<HorarioDetalle> listarHorarioMech(Integer docente, Character dia) {
      List<HorarioDetalle> lista=null;
      Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  hd FROM HorarioDetalle hd JOIN FETCH hd.plazaMagisterial pm WHERE  pm.docenteId=:p1 AND hd.diaId=:p2 ORDER BY hd.horFin ASC ,hd.horIni ASC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", docente);
            query.setParameter("p2", dia);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
    
      return lista;
    
    }
    

    @Override
    public RegistroAsistencia buscarRegistroAsistenciaByFecha(Date fecha,Trabajador trabajador) {
        RegistroAsistencia registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ra FROM RegistroAsistencia ra  WHERE ra.trabajadorId=:p2 AND ";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", fecha);
            query.setParameter("p2", trabajador);
            registro = (RegistroAsistencia)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;

    }

    @Override
    public List<RegistroAsistencia> buscarRegistroAsistenciaJustificadosByFecha(Date fechaInicio,Date fechaFin,Trabajador trabajador) {
        List<RegistroAsistencia> registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ra FROM RegistroAsistencia ra INNER JOIN ra.justificacion  WHERE ra.trabajadorId=:p3 AND ra.horaIngreso>=:p1 AND ra.horaSalida >=:p1 AND ra.horaIngreso<=:p2 AND ra.horaSalida <=:p2 ORDER BY ra.horaIngreso ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", trabajador);
            registro = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Asistencias Justificadas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Asistencias Justificadas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;

    }
    
    @Override
    public List<RegistroAsistencia> listarRegistroAsistenciaByFecha(Trabajador trabajador, Date fecha) {
        List<RegistroAsistencia> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ra FROM RegistroAsistencia ra LEFT JOIN FETCH ra.justificacion jj WHERE ra.trabajadorId=:p1 AND ra.estReg<>'E' AND (ra.horaIngreso>=:p2 OR ra.horaSalida>=:p2) AND (ra.horaIngreso<=:p3 OR ra.horaSalida<=:p3) AND (jj IS NULL OR jj.estReg<>'E') ORDER BY ra.regAsiId ASC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", trabajador);
            query.setParameter("p2", fecha);
            query.setParameter("p3", DateUtil.removeTime(DateUtil.addDays(fecha, 1)));
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los registros \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los registros \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return lista;
    }
    
    @Override
    public List<AsistenciaAula> listarAsistenciaAulaByFecha(Docente docenteID, Date fecha) {
        List<AsistenciaAula> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ra FROM AsistenciaAula ra  WHERE ra.docente=:p1 AND ra.estReg<>'E' AND (ra.horaIngreso>=:p2 OR ra.horaSalida>=:p2)  ORDER BY ra.asistenciaAulaId ASC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", docenteID);
            query.setParameter("p2", fecha);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las asistencias \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las asistencias \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return lista;
    }

    @Override
    public Boolean registrarHoraSalida(Integer idRegistroAsistencia, Date hora) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  RegistroAsistencia ra SET  ra.horaSalida=:p2 WHERE  ra.regAsiId =:p1 AND ra.estReg<>'E'";
            Query query = session.createQuery(hql);
            query.setParameter("p2", hora);
            query.setParameter("p1", idRegistroAsistencia);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se Libero el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se libero el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    
    }

    @Override
    public List<Trabajador> listarAsistenciaTrabajadorByFecha(Date fechaInicio,Date fechaFin, Organizacion org) {
        List<Trabajador> lista=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE   tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt,RegistroAsistencia asi2  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE  Q tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
            
            //Query query=session.createSQLQuery("SELECT {tt.*},{pp.*},{hc.*},{hd.*},{ds.*},{tc.*},{ra.*},{ina.*} FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=tt.hor_cab_id AND hc.est_reg<>'E' INNER JOIN administrativo.horario_det hd ON hd.hor_cab_id=hc.hor_cab_id AND hd.est_reg<>'E' INNER JOIN dia_semana ds ON ds.dia_sem_id=hd.dia_sem_id LEFT JOIN trabajador_cargo tc ON crg_tra_ide=tt.tra_car AND tc.est_reg<>'E' LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) LEFT JOIN administrativo.inasistencia ina ON ina.tra_id=tt.tra_id AND ina.est_reg<>'E' AND ina.ina_fec>=:p1 AND ina.ina_fec<:p2  WHERE tt.org_id=:p3 AND tt.est_reg<>'E'").
            Query query=session.createSQLQuery("SELECT {tt.*},{pp.*},{hc.*},{tc.*},{ra.*},{ina.*},{ja.*},{ji.*} FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=tt.hor_cab_id AND hc.est_reg<>'E'  LEFT JOIN trabajador_cargo tc ON crg_tra_ide=tt.tra_car AND tc.est_reg<>'E' LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) LEFT JOIN administrativo.inasistencia ina ON ina.tra_id=tt.tra_id AND ina.est_reg<>'E' AND ina.ina_fec>=:p1 AND ina.ina_fec<:p2  LEFT JOIN administrativo.justificacion_inasistencia ji ON ji.ina_id=ina.ina_id AND  ji.est_reg<>'E' LEFT JOIN administrativo.justificacion ja ON ja.reg_asi_id=ra.reg_asi_id AND ra.est_reg<>'E'  WHERE tt.org_id=:p3 AND tt.est_reg<>'E'").
            addEntity("tt",Trabajador.class).
                    addJoin("pp", "tt.persona").
                    addJoin("hc", "tt.horaCabId").
//                    addJoin("hd", "hc.horarioDetalle").
//                    addJoin("ds", "hd.diaSemId").
                    addJoin("tc","tt.traCar").
                    addJoin("ra", "tt.asistencias").
                    addJoin("ja", "ra.justificacion").
                    addJoin("ina", "tt.inasistencias").
                    addJoin("ji", "ina.justificacion").
                    addEntity("tt",Trabajador.class).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                    
//            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", org.getOrgId());
            
            
            
            lista = query.list();
            t.commit();
        } catch (Exception e) {
             t.rollback();
            System.out.println("No se pudo buscar el trabajador \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el trabajador  \\n " + e.getMessage());
        } finally {
            session.close();
        }  
      return lista;

    }

    @Override
    public Inasistencia buscarInasistencia(Trabajador trabajador, Date fecha) {

        Inasistencia inasistencia = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ina FROM Inasistencia ina  LEFT JOIN FETCH ina.justificacion jj   WHERE ina.estReg<>'E'  AND ina.trabajadorId=:p1 AND ina.inaFecha=:p2 AND (jj IS NULL OR jj.estReg<>'E')";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", trabajador);
            query.setParameter("p2", fecha);
            inasistencia = (Inasistencia)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los registros \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los registros \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return inasistencia;

    }

    @Override
    public List<Trabajador> listarAsistenciaAllTrabajadorByFecha(Date fechaInicio,Date fechaFin,Date hoy) {
        List<Trabajador> lista=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE   tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt,RegistroAsistencia asi2  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE  Q tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
            
            Query query=session.createSQLQuery("SELECT {tt.*},{hc.*},{hd.*},{ds.*},{ra.*} ,{og.*},{des.*}  FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=tt.hor_cab_id AND hc.est_reg<>'E' INNER JOIN administrativo.horario_det hd ON hd.hor_cab_id=hc.hor_cab_id AND hd.est_reg<>'E' INNER JOIN dia_semana ds ON ds.dia_sem_id=hd.dia_sem_id LEFT JOIN trabajador_cargo tc ON crg_tra_ide=tt.tra_car AND tc.est_reg<>'E' LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) INNER JOIN organizacion og ON og.org_id=tt.org_id AND og.est_reg<>'E' LEFT JOIN administrativo.dias_especiales des ON des.org_id=og.org_id  AND des.dia_esp_fec=:p3 AND des.est_reg='1'   WHERE  tt.est_reg<>'E'").
            addEntity("tt",Trabajador.class).
                   
                    addJoin("hc", "tt.horaCabId").
                    addJoin("hd", "hc.horarioDetalle").
                    addJoin("ds", "hd.diaSemId").
                    addJoin("ra", "tt.asistencias").
                    addJoin("og", "tt.organizacion").
                    addJoin("des", "og.diasEspeciales").
                    addEntity("tt",Trabajador.class).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                    
//            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", hoy);
            
            
            
            
            lista = query.list();
            t.commit();
        } catch (Exception e) {
             t.rollback();
            System.out.println("No se pudo buscar el trabajador \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el trabajador  \\n " + e.getMessage());
        } finally {
            session.close();
        }
      return lista;

    }
    
    @Override
    public List<Trabajador> listarAsistenciaAllTrabajadorByFechaAndOrganizacion(Integer organizacion,Date fechaInicio,Date fechaFin,Date hoy) {
        List<Trabajador> lista=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE   tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt,RegistroAsistencia asi2  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE  Q tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
            
            Query query=session.createSQLQuery("SELECT {tt.*},{hc.*},{hd.*},{ds.*},{ra.*} ,{og.*},{des.*}  FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=tt.hor_cab_id AND hc.est_reg<>'E' INNER JOIN administrativo.horario_det hd ON hd.hor_cab_id=hc.hor_cab_id AND hd.est_reg<>'E' INNER JOIN dia_semana ds ON ds.dia_sem_id=hd.dia_sem_id LEFT JOIN trabajador_cargo tc ON crg_tra_ide=tt.tra_car AND tc.est_reg<>'E' LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) INNER JOIN organizacion og ON og.org_id=tt.org_id AND og.est_reg<>'E' LEFT JOIN administrativo.dias_especiales des ON des.org_id=og.org_id  AND des.dia_esp_fec=:p3 AND des.est_reg='1'   WHERE  tt.est_reg<>'E' AND tt.org_id=:p4").
            addEntity("tt",Trabajador.class).
                   
                    addJoin("hc", "tt.horaCabId").
                    addJoin("hd", "hc.horarioDetalle").
                    addJoin("ds", "hd.diaSemId").
                    addJoin("ra", "tt.asistencias").
                    addJoin("og", "tt.organizacion").
                    addJoin("des", "og.diasEspeciales").
                    addEntity("tt",Trabajador.class).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                    
//            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", hoy);
            query.setParameter("p4", organizacion);
            
            
            
            
            lista = query.list();
            t.commit();
        } catch (Exception e) {
             t.rollback();
            System.out.println("No se pudo buscar el trabajador \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el trabajador  \\n " + e.getMessage());
        } finally {
            session.close();
        }
      return lista;

    }

    @Override
    public List<RegistroAsistencia> listarAsistenciasPosiblesToAdicional(Trabajador trabajador, Date fecha,DiaSemana dia) {
        List<RegistroAsistencia> lista = null;
        Date finDia=DateUtil.addDays(fecha, 1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  ra FROM RegistroAsistencia ra JOIN FETCH ra.horario hc JOIN FETCH hc.horarioDetalle hd WHERE ra.estReg<>'E' AND ra.estAsi<>'3' AND ra.nroDocumento IS NULL AND ra.trabajadorId=:p1 AND ra.horaSalida>:p2   AND ra.horaSalida<:p3 AND hd.diaSemId=:p4";

            Query query = session.createQuery(hql);
            query.setParameter("p1", trabajador);
            query.setParameter("p2", fecha);
            query.setParameter("p3", finDia);
            query.setParameter("p4", dia);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Asistencias \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Asistencias \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;

    }

    @Override
    public Boolean registrarHoraAdicional(Integer idRa, String descripcion, String nroDoc, String doc, Integer minAdicionales) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  RegistroAsistencia ra SET  ra.descripcionAdicional=:p2 , ra.nroDocumento=:p3 , ra.documento=:p4 ,ra.minutosAdicionales=:p5 WHERE  ra.regAsiId =:p1 AND ra.estReg<>'E'";
            Query query = session.createQuery(hql);
            
            query.setParameter("p1", idRa);
            query.setParameter("p2", descripcion);
            query.setParameter("p3", nroDoc);
            query.setParameter("p4", doc);
            query.setParameter("p5", minAdicionales);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se registro horas adicionales \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se registro horas adicionales \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    
    
    }

    @Override
    public List<Trabajador> listarConsolidadoAsistenciaTrabajadorByFecha(Date fechaInicio, Date fechaFin, Organizacion org) {
        
        List<Trabajador> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE   tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
            // String hql = "SELECT DISTINCT tt FROM Trabajador tt,RegistroAsistencia asi2  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE  Q tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";

            //Query query=session.createSQLQuery("SELECT {tt.*},{pp.*},{hc.*},{hd.*},{ds.*},{tc.*},{ra.*},{ina.*} FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=tt.hor_cab_id AND hc.est_reg<>'E' INNER JOIN administrativo.horario_det hd ON hd.hor_cab_id=hc.hor_cab_id AND hd.est_reg<>'E' INNER JOIN dia_semana ds ON ds.dia_sem_id=hd.dia_sem_id LEFT JOIN trabajador_cargo tc ON crg_tra_ide=tt.tra_car AND tc.est_reg<>'E' LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) LEFT JOIN administrativo.inasistencia ina ON ina.tra_id=tt.tra_id AND ina.est_reg<>'E' AND ina.ina_fec>=:p1 AND ina.ina_fec<:p2  WHERE tt.org_id=:p3 AND tt.est_reg<>'E'").
//            Query query = session.createSQLQuery("SELECT {tt.*},{pp.*},{hc.*},{hd.*},{ds.*},{ra.*},{ina.*},{ja.*},{ji.*} FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id    LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2) LEFT JOIN administrativo.horario_cab hc ON hc.hor_cab_id=ra.reg_hor_cab  LEFT JOIN administrativo.horario_det hd ON hd.hor_cab_id=hc.hor_cab_id AND hd.est_reg<>'E' LEFT JOIN dia_semana ds ON ds.dia_sem_id=hd.dia_sem_id  LEFT JOIN administrativo.inasistencia ina ON ina.tra_id=tt.tra_id AND ina.est_reg<>'E' AND ina.ina_fec>=:p1 AND ina.ina_fec<:p2  LEFT JOIN administrativo.justificacion_inasistencia ji ON ji.ina_id=ina.ina_id AND  ji.est_reg<>'E' LEFT JOIN administrativo.justificacion ja ON ja.reg_asi_id=ra.reg_asi_id AND ra.est_reg<>'E'  WHERE tt.org_id=:p3 AND tt.est_reg<>'E' ORDER BY pp.ape_pat,pp.ape_mat").
            Query query = session.createSQLQuery("SELECT {tt.*},{pp.*},{ra.*},{ina.*},{ja.*},{ji.*} FROM trabajador tt INNER  JOIN pedagogico.persona pp ON pp.per_id=tt.per_id    LEFT JOIN administrativo.registro_asistencia ra ON ra.tra_id=tt.tra_id AND ra.est_reg<>'E' AND (ra.hor_ing>=:p1 OR ra.hor_sal>=:p1) AND (ra.hor_sal<=:p2 OR ra.hor_ing<=:p2)   LEFT JOIN administrativo.inasistencia ina ON ina.tra_id=tt.tra_id AND ina.est_reg<>'E' AND ina.ina_fec>=:p1 AND ina.ina_fec<:p2  LEFT JOIN administrativo.justificacion_inasistencia ji ON ji.ina_id=ina.ina_id AND  ji.est_reg<>'E' LEFT JOIN administrativo.justificacion ja ON ja.reg_asi_id=ra.reg_asi_id AND ra.est_reg<>'E'  WHERE tt.org_id=:p3 AND tt.est_reg<>'E' ORDER BY pp.ape_pat,pp.ape_mat").
                    addEntity("tt", Trabajador.class).
                    addJoin("pp", "tt.persona").
                    addJoin("ra", "tt.asistencias").
//                    addJoin("hc", "ra.horario").
//                    addJoin("hd", "hc.horarioDetalle").
//                    addJoin("ds", "hd.diaSemId").
                    addJoin("ja", "ra.justificacion").
                    addJoin("ina", "tt.inasistencias").
                    addJoin("ji", "ina.justificacion").
                    addEntity("tt", Trabajador.class).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

//            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", org.getOrgId());

            lista = query.list();
            t.commit();
        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo buscar el trabajador \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el trabajador  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return lista;

    }

    @Override
    public List<Trabajador> listarAsistenciaByFecha(Date fechaInicio, Date fechaFin, Organizacion org, String dni) {
       
        List<Trabajador> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT tt FROM Trabajador tt LEFT JOIN FETCH tt.asistencias ra LEFT JOIN tt.traCar JOIN FETCH tt.persona pp WHERE tt.organizacion=:p3 AND pp.dni=:p4 AND (ra.horaIngreso>=:p1 OR ra.horaSalida>=:p1) AND (ra.horaIngreso<=:p2 OR ra.horaSalida<=:p2) ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", fechaInicio);
            query.setParameter("p2", fechaFin);
            query.setParameter("p3", org);
            query.setParameter("p4", dni);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Asistencias \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Asistencias \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;
    }

    @Override
    public Boolean actualizarAsistenciaToFalta(RegistroAsistencia asistencia) {

        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  RegistroAsistencia ra SET  ra.estAsi='3' WHERE  AND ra.regAsiId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", asistencia.getRegAsiId());
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se Libero el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se libero el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;

    }

    @Override
    public List<Docente> listarHorarioMechDocentes(Integer organizacion,Date fechaInicio,Date fechaFin) {
        List<Docente> lista=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE   tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
           // String hql = "SELECT DISTINCT tt FROM Trabajador tt,RegistroAsistencia asi2  JOIN FETCH  tt.persona pp LEFT JOIN FETCH  tt.horaCabId hc LEFT JOIN FETCH  tt.traCar tc LEFT JOIN FETCH  tt.asistencias asi  WHERE  Q tt.organizacion=:p3 AND  (hc IS NULL OR (hc.estReg<>'E' )) AND (tc IS NULL OR (tc.estReg<>'E')) AND (asi is null or ((asi.horaIngreso>=:p1 OR asi.horaSalida>=:p1) AND (asi.horaIngreso<=:p2 OR asi.horaSalida<=:p2) AND asi.estReg<>'E'))  AND tt.estReg<>'E' ORDER BY asi.horaIngreso,asi.horaSalida ";
            
            Query query=session.createSQLQuery("SELECT {dc.*},{pm.*},{hd.*} FROM pedagogico.docente dc INNER  JOIN pedagogico.persona pp ON pp.per_id=dc.doc_id INNER JOIN public.trabajador tt ON tt.per_id=pp.per_id AND tt.est_reg<>'E' AND tt.org_id=:p1 LEFT JOIN institucional.plaza_magisterial pm ON pm.doc_id=dc.doc_id  AND pm.org_id=:p1 LEFT JOIN institucional.horario_detalle  hd ON hd.pla_mag_id=pm.pla_mag_id  WHERE hd.hor_ini>=:p2 AND hd.hor_fin<=:p3").
            addEntity("pm",PlazaMagisterial.class).
                   
                    addJoin("hd", "pm.horarios").
                    addJoin("hd", "hc.horarioDetalle").
                    addJoin("ds", "hd.diaSemId").
                    addJoin("ra", "tt.asistencias").
                    addJoin("og", "tt.organizacion").
                    addJoin("des", "og.diasEspeciales").
                    addEntity("tt",Trabajador.class).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                    
//            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            query.setParameter("p2", fechaInicio);
            query.setParameter("p3", fechaFin);
            
            
            
            
            lista = query.list();
            t.commit();
        } catch (Exception e) {
             t.rollback();
            System.out.println("No se pudo buscar el trabajador \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el trabajador  \\n " + e.getMessage());
        } finally {
            session.close();
        }
      return lista;

    }

    @Override
    public List<Inasistencia> listarInasistencias(Persona persona,Organizacion organizacion, Date inicio, Date fin) {
        List<Inasistencia> lista = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  ra FROM Inasistencia ra JOIN FETCH ra.trabajadorId tt WHERE tt.persona=:p1 AND tt.organizacion=:p2 AND tt.estReg<>'E' AND ra.estReg<>'E' AND ra.inaFecha>=:p3 AND ra.inaFecha<=:p4";

            Query query = session.createQuery(hql);
            query.setParameter("p1", persona);
            query.setParameter("p2", organizacion);
            query.setParameter("p3", inicio);
            query.setParameter("p4", fin);
            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Inasistencias \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Inasistencias \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;
    
    }
}
