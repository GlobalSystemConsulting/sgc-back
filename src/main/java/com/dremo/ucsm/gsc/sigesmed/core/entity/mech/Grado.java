package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="grado" )
public class Grado  implements java.io.Serializable {

    @Id
    @Column(name="gra_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_grado", sequenceName="grado_gra_id_seq" )
    @GeneratedValue(generator="secuencia_grado")
    private int graId;
    @Column(name="abr",length=4)
    private String abr;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="dis_cur_id")
    private int disCurId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="dis_cur_id",updatable = false,insertable = false)
    private DiseñoCurricular diseñoCurricular;
    
    @Column(name="cic_id")
    private int cicEduId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cic_id",updatable = false,insertable = false)
    private CicloEducativo ciclo;
    
    @Column(name="niv_id")
    private int nivId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="niv_id",updatable = false,insertable = false)
    private Nivel nivel;
    
    @JoinTable(name = "grado_area", joinColumns = {
        @JoinColumn(name = "gra_id", referencedColumnName = "gra_id")}, inverseJoinColumns = {
        @JoinColumn(name = "are_cur_id", referencedColumnName = "are_cur_id")})
    @ManyToMany
    protected List<AreaCurricular> areas;

    public Grado() {
    }
    public Grado(int graId) {
        this.graId = graId;
    }
    public Grado(int graId,String abr, String nom, String des,int disCurId,int nivId,int cicEduId, Date fecMod, int usuMod, char estReg) {
       this.graId = graId;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       this.disCurId = disCurId;
       this.nivId = nivId;
       this.cicEduId = cicEduId;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public String getAbr() {
        return this.abr;
    }
    public void setAbr(String abr) {
        this.abr = abr;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getDisCurId() {
        return this.disCurId;
    }    
    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }
    
    public DiseñoCurricular getDiseñoCurricular() {
        return this.diseñoCurricular;
    }
    public void setDiseñoCurricular(DiseñoCurricular diseñoCurricular) {
        this.diseñoCurricular = diseñoCurricular;
    }
    
    public int getCicEduId() {
        return this.cicEduId;
    }    
    public void setCicEduId(int cicEduId) {
        this.cicEduId = cicEduId;
    }
    
    public CicloEducativo getCiclo() {
        return this.ciclo;
    }
    public void setCiclo(CicloEducativo ciclo) {
        this.ciclo = ciclo;
    }
    
    public int getNivId() {
        return this.nivId;
    }    
    public void setNivId(int nivId) {
        this.nivId = nivId;
    }
    
    public Nivel getNivel() {
        return this.nivel;
    }
    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }
    
    public List<AreaCurricular> getAreas() {
        return this.areas;
    }
    public void setAreas(List<AreaCurricular> areas) {
        this.areas = areas;
    }
}


