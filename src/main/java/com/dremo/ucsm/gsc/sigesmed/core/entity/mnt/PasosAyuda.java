/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="pasos_ayuda", schema="pedagogico")
public class PasosAyuda implements java.io.Serializable{
    @Id
    @Column(name="pas_ayu_id", unique=true, nullable=false)
    private int pasAyuId;
    
    @Column(name="pas_ayu_des", nullable=false, length=150)
    private String pasDesAyuda;
    
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ayu_id", nullable=false)
    private Ayuda ayuda;
    
    @Column(name="est_reg",nullable=false, length=1)
    private char estReg;
    
    public PasosAyuda(){}

    public PasosAyuda(int pasAyuId) {
        this.pasAyuId = pasAyuId;
    }

    public PasosAyuda(int pasAyuId, String pasDesAyuda) {
        this.pasAyuId = pasAyuId;
        this.pasDesAyuda = pasDesAyuda;
    }

    public PasosAyuda(int pasAyuId, String pasDesAyuda, Ayuda ayuda) {
        this.pasAyuId = pasAyuId;
        this.pasDesAyuda = pasDesAyuda;
        this.ayuda = ayuda;
    }
    public PasosAyuda(int pasAyuId, String pasDesAyuda, Ayuda ayuda, char estReg) {
        this.pasAyuId = pasAyuId;
        this.pasDesAyuda = pasDesAyuda;
        this.ayuda = ayuda;
        this.estReg=estReg;
    }

    public int getPasAyuId() {
        return pasAyuId;
    }

    public void setPasAyuId(int pasAyuId) {
        this.pasAyuId = pasAyuId;
    }

    public String getPasDesAyuda() {
        return pasDesAyuda;
    }

    public void setPasDesAyuda(String pasDesAyuda) {
        this.pasDesAyuda = pasDesAyuda;
    }

    public Ayuda getAyuda() {
        return ayuda;
    }

    public void setAyuda(Ayuda ayuda) {
        this.ayuda = ayuda;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    
}
