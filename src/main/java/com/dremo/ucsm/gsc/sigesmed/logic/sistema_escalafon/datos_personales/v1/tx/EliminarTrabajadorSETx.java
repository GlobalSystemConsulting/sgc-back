/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class EliminarTrabajadorSETx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarTrabajadorSETx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer traId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            traId = requestData.getInt("traId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarTrabajador",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
        try{
            trabajadorDao.delete(new Trabajador(traId));
             ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino trabajador", "ID: "+traId, wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar el trabajador\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el trabajador", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El trabajador se elimino correctamente");
    }
    
}
