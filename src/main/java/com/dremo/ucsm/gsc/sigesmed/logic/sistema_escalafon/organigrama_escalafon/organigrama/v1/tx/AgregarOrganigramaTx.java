/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class AgregarOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        Organigrama organigrama=null;
        TipoOrganigrama tipoOrganigrama = null;
        DatosOrganigrama datosOrganigrama=null;
        Ubicacion ubicacion=null;
        Integer orgIdPad=0;
        Integer zonaOrg=0;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            //Integer datId = requestData.getInt("datId");  
            Integer tipId = requestData.getInt("tipId");
                    orgIdPad = requestData.getInt("orgIdPad");
            String codOrg = requestData.optString("codOrg");
            String abrOrg = requestData.optString("abrOrg");
            String nomOrg = requestData.optString("nomOrg");
            String datOrg = requestData.optString("datOrg");
                   zonaOrg=requestData.getInt("ubiId");
            
            OrganigramaDao organigramaDao=(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
            UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
            Organigrama organigramaPadre=new Organigrama();
            organigrama=new Organigrama();
            if(orgIdPad!=0){
               organigramaPadre = organigramaDao.buscarXId(orgIdPad);
               System.out.println("Papa organigrama:"+ organigramaPadre.getOrgiId());
               organigrama.setOrgiPadId(organigramaPadre.getOrgiId());
               //System.out.println("Papa organigrama2:"+ organigrama.getOrganigramaPadre().getOrgiId());
            }
            if(zonaOrg!=0){
                ubicacion=ubicacionDao.buscarPorId(zonaOrg);
                organigrama.setZona(ubicacion);
            }
            TipoOrganigramaDao tipoOrganigramaDao =(TipoOrganigramaDao)FactoryDao.buildDao("se.TipoOrganigramaDao");
            tipoOrganigrama = tipoOrganigramaDao.buscarXId(tipId);
            //DatosOrganigramaDao datosOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");
            //datosOrganigrama = datosOrganigramaDao.buscarXId(datId);
            
            //
            organigrama.setCodOrgi(codOrg);
            organigrama.setAbrOrgi(abrOrg);
            organigrama.setNomOrgi(nomOrg);
            organigrama.setCamOrgi(datOrg);
            organigrama.setEstReg('A');
            organigrama.setFecMod(new Date());
            organigrama.setUsuMod(wr.getIdUsuario());
            organigrama.setTipoOrganigrama(tipoOrganigrama);
            
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo  Organigrama",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
       OrganigramaDao organigramaDao=(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        try {
            organigramaDao.insert(organigrama);
            //System.out.println("FREDYGO:"+organigrama.getOrganigramaPadre().getOrgiId());
            //organigrama.getOrganigramaPadre().getOrgiId();
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva  Organigrama",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("orgId", organigrama.getOrgiId());
        oResponse.put("tipId", organigrama.getTipoOrganigrama().getNomTipOrgi());
        if(orgIdPad==0)
            oResponse.put("orgIdPad","");
        else{
            oResponse.put("orgIdPad", organigrama.getOrgiPadId());}
        if(zonaOrg==0)
            oResponse.put("ubiId","");
        else
            oResponse.put("ubiId",organigrama.getZona().getUbiId());
        oResponse.put("codOrg", organigrama.getCodOrgi());
        oResponse.put("abrOrg", organigrama.getAbrOrgi());
        oResponse.put("nomOrg", organigrama.getNomOrgi());
        oResponse.put("datOrg", organigrama.getCamOrgi());
                
        return WebResponse.crearWebResponseExito("El registro Organigrama se realizo correctamente", oResponse);

    }
    
}

