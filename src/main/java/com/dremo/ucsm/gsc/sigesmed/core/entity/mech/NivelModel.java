/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

/**
 *
 * @author abel
 */
public class NivelModel {
    public int ID;
    public int jornadaID;
    public char periodoID;
    public char turnoID;
    public int nivelID;
    public int diseñoCurID;
    
    public String descripcion;
    public String jornada;
    public String nivel;
    
    
    public NivelModel(int ID,String descripcion,int nivelID,int jornadaID,String jornada,char turnoID,char periodoID,int diseñoCurID){
        this.ID = ID;
        this.descripcion = descripcion;
        this.nivelID = nivelID;
        //this.nivel = nivel;
        this.jornadaID = jornadaID;
        this.jornada = jornada;
        this.turnoID = turnoID;
        this.periodoID = periodoID;
        this.diseñoCurID = diseñoCurID;
    }
}
