/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="asistencia_estudiante", schema="pedagogico")
@DynamicUpdate(value = true)
public class AsistenciaEstudiante implements java.io.Serializable{
    @Id
    @Column(name="asi_est_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_asistencia_estudiante", sequenceName="pedagogico.asistencia_estudiante_asi_est_id_seq" )
    @GeneratedValue(generator="secuencia_asistencia_estudiante")
    private int asiEstId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mat_id")
    private Matricula matricula;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    private Usuario usuario;

    @OneToOne(mappedBy = "asistencia",fetch = FetchType.LAZY)
    private JustificacionInasistencia justificacion;
    @Column(name = "est_asi")
    private Integer estAsi;
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_asi")
    private Date fecAsi;
    @Column(name = "tip_asi")
    private Integer tipAsi;
    @Column(name = "asi_are")
    private Boolean asiAre;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public AsistenciaEstudiante() {
        this.fecMod = new Date();
        this.estReg= 'A';
    }

    public AsistenciaEstudiante(Integer estAsi, Date fecAsi, Integer tipAsi, Boolean asiAre) {
        this.estAsi = estAsi;
        this.fecAsi = fecAsi;
        this.tipAsi = tipAsi;
        this.asiAre = asiAre;
        this.fecMod = new Date();
        this.estReg= 'A';
    }

    public int getAsiEstId() {
        return asiEstId;
    }

    public void setAsiEstId(int asiEstId) {
        this.asiEstId = asiEstId;
    }

    public Matricula getMatricula() {
        return matricula;
    }

    public void setMatricula(Matricula matricula) {
        this.matricula = matricula;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getEstAsi() {
        return estAsi;
    }

    public void setEstAsi(Integer estAsi) {
        this.estAsi = estAsi;
    }

    public Date getFecAsi() {
        return fecAsi;
    }

    public void setFecAsi(Date fecAsi) {
        this.fecAsi = fecAsi;
    }

    public Integer getTipAsi() {
        return tipAsi;
    }

    public void setTipAsi(Integer tipAsi) {
        this.tipAsi = tipAsi;
    }

    public Boolean getAsiAre() {
        return asiAre;
    }

    public void setAsiAre(Boolean asiAre) {
        this.asiAre = asiAre;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public JustificacionInasistencia getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(JustificacionInasistencia justificacion) {
        this.justificacion = justificacion;
    }
}
