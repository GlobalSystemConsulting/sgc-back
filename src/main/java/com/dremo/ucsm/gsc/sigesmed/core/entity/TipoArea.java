package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tipo_area" ,schema="administrativo")
public class TipoArea  implements java.io.Serializable {

    @Id
    @Column(name="tip_are_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_tipoarea", sequenceName="administrativo.tipo_area_tip_are_id_seq" )
    @GeneratedValue(generator="secuencia_tipoarea")
    private int tipAreId;
    @Column(name="nom", nullable=false, length=64)
    private String nom;
    @Column(name="ali", length=32)
    private String ali;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;

    public TipoArea() {
    }
    public TipoArea(int tipAreId) {
        this.tipAreId = tipAreId;
    }
    public TipoArea(int tipAreId, String nom, String ali, Date fecMod, Integer usuMod, char estReg) {
       this.tipAreId = tipAreId;
       this.nom = nom;
       this.ali = ali;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
    public int getTipAreId() {
        return this.tipAreId;
    }
    public void setTipAreId(int tipAreId) {
        this.tipAreId = tipAreId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getAli() {
        return this.ali;
    }
    public void setAli(String ali) {
        this.ali = ali;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}


