/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.GetDatosAuditoriaUsuarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.ListarRegistrosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.ReporteAuditoriaTx;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("auditoria");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarRegistros", ListarRegistrosTx.class);
        seComponent.addTransactionPUT("getDatosAuditoriaUsuario", GetDatosAuditoriaUsuarioTx.class);
        seComponent.addTransactionPOST("reporteAuditoria", ReporteAuditoriaTx.class);
	//seComponent.addTransactionPOST("agregarDatosAuditoria", AgregarDatosAuditoriaTx.class);
        //seComponent.addTransactionPUT("actualizarDatosPersonales", ActualizarDatosPersonalesTx.class);
        //seComponent.addTransactionDELETE("eliminarTrabajador", EliminarTrabajadorSETx.class);
        return seComponent;
    }
    
}
