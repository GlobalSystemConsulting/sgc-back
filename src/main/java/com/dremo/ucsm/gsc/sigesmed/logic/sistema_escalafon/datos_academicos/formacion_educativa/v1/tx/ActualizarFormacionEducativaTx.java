/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarFormacionEducativaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarFormacionEducativaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {

            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

            JSONObject requestData = (JSONObject) wr.getData();

            Integer forEduId = requestData.getInt("forEduId");

            Integer tipForId = requestData.getInt("tipForId");
            Integer nivAcaId = requestData.getInt("nivAcaId");
            Integer tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("") ? null : sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            String esp = requestData.getString("esp");
            Boolean estCon = requestData.getBoolean("estCon");
            Date fecIni = requestData.getString("fecIni").equals("") ? null : sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = estCon ? (requestData.getString("fecTer").equals("") ? null : sdi.parse(requestData.getString("fecTer").substring(0, 10))) : null;
            String cenEst = requestData.getString("cenEst");
            Integer paisId = requestData.getInt("paisId");

            return actualizarForEdu(forEduId, tipForId, nivAcaId, tipDocId, numDoc, fecDoc, esp, estCon, fecIni, fecTer, cenEst, paisId);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Actualizar formacion educativa", e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }
    }

    private WebResponse actualizarForEdu(Integer forEduId, Integer tipForId, Integer nivAcaId, Integer tipDocId, String numDoc, Date fecDoc, String esp, Boolean estCon, Date fecIni, Date fecTer, String cenEst, Integer paisId) {
        try {
            FormacionEducativaDao forEduDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
            FormacionEducativa forEdu = forEduDao.buscarForEduPorId(forEduId);

            forEdu.setTipForId(tipForId);
            forEdu.setNivAcaId(nivAcaId);
            forEdu.setTipDocId(tipDocId);
            forEdu.setNumDoc(numDoc);
            forEdu.setFecDoc(fecDoc);
            forEdu.setEspAca(esp);
            forEdu.setEstCon(estCon);
            forEdu.setFecIniFor(fecIni);
            forEdu.setFecTerFor(fecTer);
            forEdu.setCenEst(cenEst);
            forEdu.setNacId(paisId);

            forEduDao.update(forEdu);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");

            oResponse.put("forEduId", forEdu.getForEduId());
            oResponse.put("tipForId", forEdu.getTipForId() == null ? 0 : forEdu.getTipForId());
            oResponse.put("tipFor", "");
            oResponse.put("nivAcaId", forEdu.getNivAcaId() == null ? 0 : forEdu.getNivAcaId());
            oResponse.put("nivAca", "");
            oResponse.put("tipDocId", forEdu.getTipDocId() == null ? 0 : forEdu.getTipDocId());
            oResponse.put("tipDoc", "");
            oResponse.put("numDoc", forEdu.getNumDoc() == null ? "" : forEdu.getNumDoc());
            oResponse.put("fecDoc", forEdu.getFecDoc() == null ? "" : sdo.format(forEdu.getFecDoc()));
            oResponse.put("esp", forEdu.getEspAca() == null ? "" : forEdu.getEspAca());
            oResponse.put("estCon", forEdu.getEstCon() == null ? "" : forEdu.getEstCon());
            oResponse.put("estConDes", "");
            oResponse.put("fecIni", forEdu.getFecIniFor() == null ? "" : sdo.format(forEdu.getFecIniFor()));
            oResponse.put("fecTer", forEdu.getEstCon() ?(forEdu.getFecTerFor()==null?"":sdo.format(forEdu.getFecTerFor())):"");
            oResponse.put("cenEst", forEdu.getCenEst() == null ? "" : forEdu.getCenEst());
            oResponse.put("paisId", forEdu.getNacId() == null ? 0 : forEdu.getNacId());
            oResponse.put("pais", "");

            return WebResponse.crearWebResponseExito("Formacion Educativa actualizada exitosamente", oResponse);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "actualizarFormacionEducativa", e);
            return WebResponse.crearWebResponseError("Error, La formacion educativa no fue actualizada");
        }
    }

}
