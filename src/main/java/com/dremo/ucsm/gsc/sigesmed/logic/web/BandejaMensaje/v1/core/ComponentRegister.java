/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.ActualizarBandejaMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.EnviarMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.ListarDocumentosDeMensajeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.ListarMensajesEnviadosPorUsuarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.ListarMensajesNuevosPorUsuarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.ListarMensajesPorUsuarioTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrando el Nombre del componente
        component.setName("mensaje");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("enviarMensaje", EnviarMensajeTx.class);
        component.addTransactionGET("listarMensajes", ListarMensajesPorUsuarioTx.class);
        component.addTransactionGET("listarMensajesEnviados", ListarMensajesEnviadosPorUsuarioTx.class);
        component.addTransactionGET("listarMensajesNuevos", ListarMensajesNuevosPorUsuarioTx.class);
        component.addTransactionGET("listarDocumentos", ListarDocumentosDeMensajeTx.class);
        component.addTransactionPUT("actualizarBandejaMensaje", ActualizarBandejaMensajeTx.class);
        
        return component;
    }
}


