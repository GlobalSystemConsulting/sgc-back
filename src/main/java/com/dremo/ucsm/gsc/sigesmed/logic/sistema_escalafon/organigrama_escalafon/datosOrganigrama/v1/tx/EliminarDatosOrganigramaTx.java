/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class EliminarDatosOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarDatosOrganigramaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer datId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            datId = requestData.getInt("datId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminar Datos Organigrama",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        DatosOrganigramaDao datOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");
        try{
            datOrganigramaDao.delete(new DatosOrganigrama(datId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar Datos Organigrama\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar Datos Organigrama", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("Datos Organigrama se elimino correctamente");
    }
    
}

