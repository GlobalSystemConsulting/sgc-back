package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Persona generated by hbm2java
 */
@Entity
@Table(name = "persona", schema = "pedagogico", uniqueConstraints = @UniqueConstraint(columnNames = "dni")
)
public class PersonaMMI_datosGenerales implements java.io.Serializable {

    private long perId;
    private Lengua lenguaByLenSeg;
    private Lengua lenguaByLenMat;
    private String apeMat;
    private String apePat;
    private String nom;
    private Date fecNac;
    private String dni;
    private String perDir;
    private Character sex;
    private String estCiv;
    private Integer usuMod;
    private Date fecMod;
    private Character estReg;
    private String depNac;
    private String proNac;
    private String disNac;

    public PersonaMMI_datosGenerales() {
    }

    public PersonaMMI_datosGenerales(long perId, String apeMat, String apePat, String nom, Date fecNac, String dni) {
        this.perId = perId;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
    }

    public PersonaMMI_datosGenerales(long perId, Lengua lenguaByLenSeg, Lengua lenguaByLenMat,
            String apeMat, String apePat, String nom, Date fecNac, String dni,
            String perDir, Character sex, String estCiv, Integer usuMod, Date fecMod, Character estReg,
            String depNac, String proNac, String disNac) {
        this.perId = perId;
        this.lenguaByLenSeg = lenguaByLenSeg;
        this.lenguaByLenMat = lenguaByLenMat;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
        this.perDir = perDir;
        this.sex = sex;
        this.estCiv = estCiv;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.depNac = depNac;
        this.proNac = proNac;
        this.disNac = disNac;
    }

    @Id
    @Column(name = "per_id", unique = true, nullable = false)
    public long getPerId() {
        return this.perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "len_seg")
    public Lengua getLenguaByLenSeg() {
        return this.lenguaByLenSeg;
    }

    public void setLenguaByLenSeg(Lengua lenguaByLenSeg) {
        this.lenguaByLenSeg = lenguaByLenSeg;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "len_mat")
    public Lengua getLenguaByLenMat() {
        return this.lenguaByLenMat;
    }

    public void setLenguaByLenMat(Lengua lenguaByLenMat) {
        this.lenguaByLenMat = lenguaByLenMat;
    }


    @Column(name = "ape_mat", nullable = false, length = 60)
    public String getApeMat() {
        return this.apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    @Column(name = "ape_pat", nullable = false, length = 60)
    public String getApePat() {
        return this.apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    @Column(name = "nom", nullable = false, length = 60)
    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_nac", nullable = false, length = 13)
    public Date getFecNac() {
        return this.fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    @Column(name = "dni", unique = true, nullable = false, length = 8)
    public String getDni() {
        return this.dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Column(name = "per_dir")
    public String getPerDir() {
        return this.perDir;
    }

    public void setPerDir(String perDir) {
        this.perDir = perDir;
    }

    @Column(name = "sex", length = 1)
    public Character getSex() {
        return this.sex;
    }

    public void setSex(Character sex) {
        this.sex = sex;
    }

    @Column(name = "est_civ", length = 2)
    public String getEstCiv() {
        return this.estCiv;
    }

    public void setEstCiv(String estCiv) {
        this.estCiv = estCiv;
    }

    @Column(name = "usu_mod")
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    @Column(name = "est_reg", length = 1)
    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Column(name = "dep_nac", length = 2)
    public String getDepNac() {
        return this.depNac;
    }

    public void setDepNac(String depNac) {
        this.depNac = depNac;
    }

    @Column(name = "pro_nac", length = 2)
    public String getProNac() {
        return this.proNac;
    }

    public void setProNac(String proNac) {
        this.proNac = proNac;
    }

    @Column(name = "dis_nac", length = 2)
    public String getDisNac() {
        return this.disNac;
    }

    public void setDisNac(String disNac) {
        this.disNac = disNac;
    }
}
