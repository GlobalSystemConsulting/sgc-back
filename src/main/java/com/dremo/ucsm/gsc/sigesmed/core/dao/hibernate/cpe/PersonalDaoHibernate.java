    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Carlos
 */
public class PersonalDaoHibernate extends GenericDaoHibernate<Trabajador> implements PersonalDao {

    @Override
    public List<TrabajadorCargo> listarCargoTrabajadorByTipo(String tipoTrabajador) {

        List<TrabajadorCargo> cargos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tc FROM TrabajadorCargo tc  WHERE   tc.traTip =:p1 AND tc.estReg<>'E'    ORDER BY tc.crgTraIde DESC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoTrabajador);
            cargos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo listar los Cargos \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los Cargos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return cargos;

    }
    
    public List<TrabajadorCargo> listarCargoTrabajador() {

        List<TrabajadorCargo> cargos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tc FROM TrabajadorCargo tc  WHERE   tc.estReg<>'E'    ORDER BY tc.crgTraIde DESC";

            Query query = session.createQuery(hql);
           
            cargos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo listar los Cargos \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los Cargos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return cargos;

    }

    @Override
    public Persona buscarPorDNI(String dni) {
        Persona persona = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT pp FROM Persona pp  WHERE   pp.dni =:p1 ";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", dni);
            persona = (Persona)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo encontrar la Persona \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la Persona \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return persona;
    }

    @Override
    public List<Organizacion> listarOrganizacionesActivasDelTrabajador(String dni, List<Integer> orgId) {
        List<Organizacion> organizaciones = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT o FROM Organizacion o  INNER JOIN o.tipoOrganizacion ot INNER JOIN  o.trabajadores tt INNER JOIN  tt.persona pp WHERE  pp.dni=:p1  AND ot.tipOrgId IN (:p2)";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", dni);
            query.setParameterList("p2", orgId);
            organizaciones = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo listar las Organizaciones \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las Organizaciones \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return organizaciones;
    
    }
    
    @Override
    public Persona buscarPersonaxDNI(String dni){
        Persona p = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT p FROM Persona p WHERE p.dni=:p1";            
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", dni);
            //query.setMaxResults(1);
            p = (Persona)query.uniqueResult();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            //System.out.println("No se encontro la persona \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se encontro la persona \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }        
        return p;
    }

    @Override
    public Trabajador getTrabajadorByPersonaOrganizacion(Persona persona, Organizacion org) {
       Trabajador trabajador = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT tr FROM Trabajador tr  WHERE   tr.persona=:p1 AND tr.organizacion=:p2 AND tr.estReg<>'E'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", persona);
            query.setParameter("p2", org);
            trabajador = (Trabajador)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo encontrar la Persona \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la Persona \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return trabajador;
    
    }
}
