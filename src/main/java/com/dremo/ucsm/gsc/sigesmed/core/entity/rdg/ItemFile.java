/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "item_file",schema="institucional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemFile.findAll", query = "SELECT i FROM ItemFile i"),
    @NamedQuery(name = "ItemFile.findByIteIde", query = "SELECT i FROM ItemFile i WHERE i.iteIde = :iteIde"),
    @NamedQuery(name = "ItemFile.findByIteAltIde", query = "SELECT i FROM ItemFile i WHERE i.iteAltIde = :iteAltIde"),
    @NamedQuery(name = "ItemFile.findByIteNom", query = "SELECT i FROM ItemFile i WHERE i.iteNom = :iteNom"),
    @NamedQuery(name = "ItemFile.findByIteTam", query = "SELECT i FROM ItemFile i WHERE i.iteTam = :iteTam"),
    @NamedQuery(name = "ItemFile.findByIteVer", query = "SELECT i FROM ItemFile i WHERE i.iteVer = :iteVer"),
    @NamedQuery(name = "ItemFile.findByIteFecCre", query = "SELECT i FROM ItemFile i WHERE i.iteFecCre = :iteFecCre"),
    @NamedQuery(name = "ItemFile.findByIteUrlDes", query = "SELECT i FROM ItemFile i WHERE i.iteUrlDes = :iteUrlDes"),
    @NamedQuery(name = "ItemFile.findByIteOrgIde", query = "SELECT i FROM ItemFile i WHERE i.iteOrgIde = :iteOrgIde"),
    @NamedQuery(name = "ItemFile.findByFecMod", query = "SELECT i FROM ItemFile i WHERE i.fecMod = :fecMod"),
    @NamedQuery(name = "ItemFile.findByIteUsuIde", query = "SELECT i FROM ItemFile i WHERE i.iteUsuIde = :iteUsuIde"),
    @NamedQuery(name = "ItemFile.findByEstReg", query = "SELECT i FROM ItemFile i WHERE i.estReg = :estReg"),
    @NamedQuery(name = "ItemFile.findByIteCodCat", query = "SELECT i FROM ItemFile i WHERE i.iteCodCat = :iteCodCat")})
public class ItemFile implements Serializable {
    @OneToMany(mappedBy = "ifdIteIde")
    private List<ItemFileDetalle> itemFileDetalleList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ite_ide")
    private Integer iteIde;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ite_alt_ide")
    private String iteAltIde;
    @Size(max = 100)
    @Column(name = "ite_nom")
    private String iteNom;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ite_tam")
    private Double iteTam;
    @Column(name = "ite_ver")
    private Short iteVer;
    @Column(name = "ite_fec_cre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iteFecCre;
    @Size(max = 400)
    @Column(name = "ite_url_des")
    private String iteUrlDes;
    
    //@Column(name = "ite_org_ide")
    //private Integer iteOrgIde;
    @OneToOne(fetch=FetchType.LAZY) 
    @JoinColumn(name="ite_org_ide")    
    private Organizacion iteOrgIde;
    
    
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "ite_usu_ide")
    private Integer iteUsuIde;
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    @Size(max = 1)
    @Column(name = "ite_cod_cat")
    private String iteCodCat;
    
    
//    @JoinColumn(name = "ite_tif_ide", referencedColumnName = "tif_ide")
//    @ManyToOne        
//    private TipoItemFile iteTifIde;
//    @JoinColumn(name = "ite_cod_pro", referencedColumnName = "prt_ide")
//    @ManyToOne
    
    @JoinColumn(name = "tes_ide", referencedColumnName = "tes_ide")
    @ManyToOne        
    private TipoEspecializado tesIde;

    
    
    
    @JoinColumn(name = "ite_pla_ide", referencedColumnName = "plz_ide")
    @ManyToOne
    private Plazo itePlaIde;
    
    
    @OneToMany(mappedBy = "itePadIde")
    private List<ItemFile> itemFileList;
    @JoinColumn(name = "ite_pad_ide", referencedColumnName = "ite_ide")    
    @ManyToOne
    private ItemFile itePadIde;
    
    

    @Column(name="ite_mod")
    private char iteMod;
    
    @Column (name="ite_prot")
    private String iteProt;
    
    public ItemFile() {
    }

    public char getIteMod() {
        return iteMod;
    }

    public void setIteMod(char iteMod) {
        this.iteMod = iteMod;
    }

    public ItemFile(Integer iteIde) {
        this.iteIde = iteIde;
    }

    public String getIteProt() {
        return iteProt;
    }

    public void setIteProt(String iteProt) {
        this.iteProt = iteProt;
    }

    public ItemFile(Integer iteIde, String iteAltIde) {
        this.iteIde = iteIde;
        this.iteAltIde = iteAltIde;
    }
    
    //Archivo con padre
    public ItemFile(String iteNom, String iteCodCat,TipoEspecializado tesIde,Double iteTam,Date iteFecCre,String iteUrlDes,Integer iteUsuIde,String estReg, ItemFile itePadIde,Organizacion iteOrg) {
        this.iteOrgIde = iteOrg;
        this.tesIde = tesIde;
        this.iteNom = iteNom;
        this.iteTam = iteTam;
      
        this.iteFecCre = iteFecCre;
        this.iteUrlDes = iteUrlDes;
        this.iteUsuIde = iteUsuIde;
        this.estReg = estReg;
        this.iteCodCat = iteCodCat;
        this.itePadIde = itePadIde;
    }
     public ItemFile(String iteNom, Short iteVer,String iteCodCat,TipoEspecializado tesIde,Double iteTam,Date iteFecCre,String iteUrlDes,Integer iteUsuIde,String estReg, ItemFile itePadIde,Organizacion iteOrg) {
        this.iteOrgIde = iteOrg;
        this.iteVer=iteVer;
        this.tesIde = tesIde;
        this.iteNom = iteNom;
        this.iteTam = iteTam;
      
        this.iteFecCre = iteFecCre;
        this.iteUrlDes = iteUrlDes;
        this.iteUsuIde = iteUsuIde;
        this.estReg = estReg;
        this.iteCodCat = iteCodCat;
        this.itePadIde = itePadIde;
    }

    public ItemFile(List<ItemFileDetalle> itemFileDetalleList, Integer iteIde, String iteAltIde, String iteNom, Double iteTam, Short iteVer, Date iteFecCre, String iteUrlDes, Organizacion iteOrgIde, Date fecMod, Integer iteUsuIde, String estReg, String iteCodCat, TipoEspecializado tesIde, Plazo itePlaIde, List<ItemFile> itemFileList, ItemFile itePadIde, char iteMod) {
        this.itemFileDetalleList = itemFileDetalleList;
        this.iteIde = iteIde;
        this.iteAltIde = iteAltIde;
        this.iteNom = iteNom;
        this.iteTam = iteTam;
        this.iteVer = iteVer;
        this.iteFecCre = iteFecCre;
        this.iteUrlDes = iteUrlDes;
        this.iteOrgIde = iteOrgIde;
        this.fecMod = fecMod;
        this.iteUsuIde = iteUsuIde;
        this.estReg = estReg;
        this.iteCodCat = iteCodCat;
        
        this.tesIde = tesIde;
      
        
        this.itePlaIde = itePlaIde;
        this.itemFileList = itemFileList;
        this.itePadIde = itePadIde;
        this.iteMod = iteMod;
    }
    
    
    //Directorio con Padre
    public ItemFile(String iteNom, Short iteVer,String iteCodCat,Double iteTam,Date iteFecCre,String iteUrlDes,Integer iteUsuIde,String estReg, ItemFile itePadIde) {
        this.iteNom = iteNom;
        this.iteTam = iteTam;
        this.iteVer = iteVer;
        this.iteFecCre = iteFecCre;
        this.iteUrlDes = iteUrlDes;
        this.iteUsuIde = iteUsuIde;
        this.estReg = estReg;
        this.iteCodCat = iteCodCat;
        this.itePadIde = itePadIde;
    }
    
    //Directorio sin Padre

    public ItemFile(String iteNom, Short iteVer,String iteCodCat,Double iteTam,Date iteFecCre,String iteUrlDes,Integer iteUsuIde,String estReg) {
        this.iteNom = iteNom;
        this.iteTam = iteTam;
        this.iteVer = iteVer;
        this.iteFecCre = iteFecCre;
        this.iteUrlDes = iteUrlDes;
        this.iteUsuIde = iteUsuIde;
        this.estReg = estReg;
        this.iteCodCat = iteCodCat;
    }

    public Integer getIteIde() {
        return iteIde;
    }

    public void setIteIde(Integer iteIde) {
        this.iteIde = iteIde;
    }

    public String getIteAltIde() {
        return iteAltIde;
    }

    public void setIteAltIde(String iteAltIde) {
        this.iteAltIde = iteAltIde;
    }

    public String getIteNom() {
        return iteNom;
    }

    public void setIteNom(String iteNom) {
        this.iteNom = iteNom;
    }

    public Double getIteTam() {
        return iteTam;
    }

    public void setIteTam(Double iteTam) {
        this.iteTam = iteTam;
    }

    public Short getIteVer() {
        return iteVer;
    }

    public void setIteVer(Short iteVer) {
        this.iteVer = iteVer;
    }

    public Date getIteFecCre() {
        return iteFecCre;
    }

    public void setIteFecCre(Date iteFecCre) {
        this.iteFecCre = iteFecCre;
    }

    public String getIteUrlDes() {
        return iteUrlDes;
    }

    public void setIteUrlDes(String iteUrlDes) {
        this.iteUrlDes = iteUrlDes;
    }

    public Organizacion getIteOrgIde() {
        return iteOrgIde;
    }

    public void setIteOrgIde(Organizacion iteOrgIde) {
        this.iteOrgIde = iteOrgIde;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getIteUsuIde() {
        return iteUsuIde;
    }

    public void setIteUsuIde(Integer iteUsuIde) {
        this.iteUsuIde = iteUsuIde;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getIteCodCat() {
        return iteCodCat;
    }

    public void setIteCodCat(String iteCodCat) {
        this.iteCodCat = iteCodCat;
    }

    public TipoEspecializado getTesIde() {
        return tesIde;
    }

    public void setTeside(TipoEspecializado tesIde) {
        this.tesIde = tesIde;
    }

  

    public Plazo getItePlaIde() {
        return itePlaIde;
    }

    public void setItePlaIde(Plazo itePlaIde) {
        this.itePlaIde = itePlaIde;
    }

    @XmlTransient
    public List<ItemFile> getItemFileList() {
        return itemFileList;
    }

    public void setItemFileList(List<ItemFile> itemFileList) {
        this.itemFileList = itemFileList;
    }

    public ItemFile getItePadIde() {
        return itePadIde;
    }

    public void setItePadIde(ItemFile itePadIde) {
        this.itePadIde = itePadIde;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iteIde != null ? iteIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemFile)) {
            return false;
        }
        ItemFile other = (ItemFile) object;
        if ((this.iteIde == null && other.iteIde != null) || (this.iteIde != null && !this.iteIde.equals(other.iteIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemFile{" + "itemFileDetalleList=" + itemFileDetalleList + ", iteIde=" + iteIde + ", iteAltIde=" + iteAltIde + ", iteNom=" + iteNom + ", iteTam=" + iteTam + ", iteVer=" + iteVer + ", iteFecCre=" + iteFecCre + ", iteUrlDes=" + iteUrlDes + ", iteOrgIde=" + iteOrgIde + ", fecMod=" + fecMod + ", iteUsuIde=" + iteUsuIde + ", estReg=" + estReg + ", iteCodCat=" + iteCodCat + ", tesIde=" + tesIde + ", itePlaIde=" + itePlaIde + ", itemFileList=" + itemFileList + ", itePadIde=" + itePadIde + ", iteMod=" + iteMod + '}';
    }

    

    @XmlTransient
    public List<ItemFileDetalle> getItemFileDetalleList() {
        return itemFileDetalleList;
    }

    public void setItemFileDetalleList(List<ItemFileDetalle> itemFileDetalleList) {
        this.itemFileDetalleList = itemFileDetalleList;
    }
    
}
