/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador.TiempoExtendido;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarDatosCentroLaboralTrabajadorTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarDatosCentroLaboralTrabajadorTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject) wr.getData();
        int perId = requestData.getInt("perId");
        int traId = requestData.getInt("traId");

        TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
        TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao) FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        
        Trabajador t = new Trabajador();
        TrabajadorOrganigramaDetalle traOrgDet = new TrabajadorOrganigramaDetalle();
        Organigrama organigrama = new Organigrama();
        Cargo cargo = new Cargo();
        Categoria categoria = new Categoria();
        Ubicacion ubicacion = new Ubicacion();
        
        try {
            t = trabajadorDao.mostrarDatosTrabajador(traId,perId);
        } catch (Exception e) {
            System.out.println("No se pudo listar los datos del trabajador.Error: " + e);
            return WebResponse.crearWebResponseError("No se pudo los datos del trabajador ", e.getMessage());
        }
        
           
        JSONObject oResponse = new JSONObject();                

        oResponse.put("traId", t.getTraId());

        if (t.getRol() != null) {
            oResponse.put("rolId", t.getRol().getRolId());
            oResponse.put("rolNom", t.getRol().getNom());
            oResponse.put("rolAbr", t.getRol().getAbr());
        } else {
            oResponse.put("rolId", -1);
            oResponse.put("rolNom", "");
        }

        oResponse.put("estLab", t.getEstLab());

        if (t.getSal() != null) {
            oResponse.put("sal", t.getSal());
        } else {
            oResponse.put("sal", 0);
        }

        if (t.getFecIng() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            oResponse.put("fecIng", sdf.format((t.getFecIng())));
            oResponse.put("fecIngEst", true);
        }else {
            oResponse.put("fecIngEst", false);
        }

        if (t.getAniosOut() != null) {
            oResponse.put("aniosOut", t.getAniosOut());
        }
        else{
            oResponse.put("aniosOut", 0);
        }

        //Datos de persona
        oResponse.put("perId", t.getPersona().getPerId());
        oResponse.put("perDni", t.getPersona().getDni());
        System.out.println("trabajadorOrgiId " + t.getOrgiId());
        if (t.getOrgiId()!= null && t.getOrgiId()>0){ 
            oResponse.put("orgiId", t.getOrgiId());
            traOrgDet = traOrgDetDao.buscarPorId(t.getTraId(), t.getOrgiId());
            oResponse.put("tipOrgId", traOrgDet.getOrganigrama().getTipoOrganigrama().getTipOrgiId());
            
            organigrama = organigramaDao.obtenerDetalle(traOrgDet.getOrgiId());
            oResponse.put("datOrgId", organigrama.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
            oResponse.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
            oResponse.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());

            oResponse.put("nomOrgiN3", organigrama.getNomOrgi());
            oResponse.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
            oResponse.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
            
            if (traOrgDet.getPlaId() != null)
                oResponse.put("plaId",traOrgDet.getPlaId());
            else
                oResponse.put("plaId",0);

            if (traOrgDet.getCatId() != null && traOrgDet.getCatId()>0){
                categoria = categoriaDao.obtenerDetalle(traOrgDet.getCatId());
                oResponse.put("catId", traOrgDet.getCatId());
                oResponse.put("nomCat", categoria.getNomCat());
                oResponse.put("nomPla", categoria.getPlanilla().getNomPla());
            }else{
                oResponse.put("catId",0);
                oResponse.put("nomCat", "");
                oResponse.put("nomPla", "");
            }
            if (traOrgDet.getCarId() != null && traOrgDet.getCarId()>0){
                cargo = cargoDao.buscarPorId(traOrgDet.getCarId());
                oResponse.put("carId",traOrgDet.getCarId());
                oResponse.put("nomCar", cargo.getNomCar());
            }else{
                oResponse.put("carId",0);
                oResponse.put("nomCar", "");
            }

            if (traOrgDet.getUbiId() != null && traOrgDet.getUbiId()>0){
                ubicacion = ubicacionDao.buscarPorId(traOrgDet.getUbiId());
                oResponse.put("ubiId",traOrgDet.getUbiId());
                oResponse.put("nomUbi",ubicacion.getNom());
            }else{
                oResponse.put("ubiId",0);
                oResponse.put("nomUbi","");
            }
            if(t.getTiemserest()!=null & t.getTiemserforedu()!=null)
            {
                int dias1 = t.getTiemserest();
                int an1 = dias1 / 365;
                int me1 = (dias1 - (an1 * 365)) / 30;
                int di1 = dias1 - (an1 * 365) - (me1 * 30);
                int dias2 = t.getTiemserforedu();
                int an2 = dias2 / 365;
                int me2 = (dias2 - (an2 * 365)) / 30;
                int di2 = dias2 - (an2 * 365) - (me2 * 30);

                oResponse.put("an1", an1);
                oResponse.put("me1", me1);
                oResponse.put("di1", di1);
                oResponse.put("an2", an2);
                oResponse.put("me2", me2);
                oResponse.put("di2", di2);
            }else{
                 oResponse.put("an1", 0);
                oResponse.put("me1", 0);
                oResponse.put("di1", 0);
                oResponse.put("an2", 0);
                oResponse.put("me2", 0);
                oResponse.put("di2", 0);
            }
           
            
        }else{
            oResponse.put("orgiId", 0);
            oResponse.put("plaId",0);
            oResponse.put("catId",0);
            oResponse.put("nomCat", "");
            oResponse.put("nomPla", "");
            oResponse.put("carId",0);
            oResponse.put("nomCar", "");
            oResponse.put("datOrgId", 0);
            oResponse.put("nomOrgi", "");
            oResponse.put("nomDatOrgi", "");
            oResponse.put("anioDatOrgi", 0);
            oResponse.put("nomOrgiN3", "");
            oResponse.put("nomOrgiN2", "");
            oResponse.put("nomOrgiN1", "");
            oResponse.put("ubiId", 0);
            oResponse.put("nomUbi","");
            
            if(t.getTiemserest()!=null & t.getTiemserforedu()!=null)
            {
                int dias1 = t.getTiemserest();
                int an1 = dias1 / 365;
                int me1 = (dias1 - (an1 * 365)) / 30;
                int di1 = dias1 - (an1 * 365) - (me1 * 30);
                int dias2 = t.getTiemserforedu();
                int an2 = dias2 / 365;
                int me2 = (dias2 - (an2 * 365)) / 30;
                int di2 = dias2 - (an2 * 365) - (me2 * 30);

                oResponse.put("an1", an1);
                oResponse.put("me1", me1);
                oResponse.put("di1", di1);
                oResponse.put("an2", an2);
                oResponse.put("me2", me2);
                oResponse.put("di2", di2);
            
            }else{
                oResponse.put("an1", 0);
                oResponse.put("me1", 0);
                oResponse.put("di1", 0);
                oResponse.put("an2", 0);
                oResponse.put("me2", 0);
                oResponse.put("di2", 0);
            }
            
            
        }
        
        JSONArray oArray = new JSONArray();
        oArray.put(oResponse);
        return WebResponse.crearWebResponseExito("Se listo correctamente", oArray);
    }
}
