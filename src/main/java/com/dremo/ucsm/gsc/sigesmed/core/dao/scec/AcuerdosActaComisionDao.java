package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AcuerdosActaComision;

import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface AcuerdosActaComisionDao extends GenericDao<AcuerdosActaComision> {
    List<AcuerdosActaComision> buscarAcuerdosPorActa(int idActa);
    AcuerdosActaComision buscarAcuerdosPorId(int idAcu);
}
