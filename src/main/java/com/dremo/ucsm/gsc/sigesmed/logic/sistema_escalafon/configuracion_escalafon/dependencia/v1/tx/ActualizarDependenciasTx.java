/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarDependenciasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarDependenciasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer depId = requestData.getInt("depId");
            String codDep = requestData.optString("codDep");
            String nomDep = requestData.optString("nomDep");
            Integer facId = requestData.getInt("facId");
            FacultadDao organismoDao = (FacultadDao)FactoryDao.buildDao("se.FacultadDao");
            Facultad facultad = organismoDao.buscarPorId(facId);
            
            return actualizarCargos( depId, codDep, nomDep, facultad);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCargos(Integer depId, String codDep, String nomDep, Facultad facultad) {
        try{
            DependenciaDao dependenciaDao = (DependenciaDao)FactoryDao.buildDao("se.DependenciaDao");        
            Dependencia dependencia = dependenciaDao.buscarPorId(depId);

            dependencia.setCodDep(codDep);
            dependencia.setNomDep(nomDep);
            dependencia.setFacultad(facultad);
            
            dependenciaDao.update(dependencia);
            JSONObject oResponse = new JSONObject();
            oResponse.put("depId", dependencia.getDepId());
            oResponse.put("codDep", dependencia.getCodDep());
            oResponse.put("nomDep", dependencia.getNomDep());
            return WebResponse.crearWebResponseExito(" La Dependencia actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, la Dependencia no fue actualizado");
        }
    }
}
