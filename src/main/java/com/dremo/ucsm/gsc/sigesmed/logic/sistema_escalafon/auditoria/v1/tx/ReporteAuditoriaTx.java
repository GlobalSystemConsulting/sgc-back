/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.LogAuditoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.ReporteFichaEscalafonariaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class ReporteAuditoriaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ReporteFichaEscalafonariaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        long dato= wr.getIdUsuario();
        JSONObject data = (JSONObject) wr.getData();
        String titulo_reporte =  data.optString("Reporte de Auditoria");
        String observaciones_reporte = data.optString("Observaciones Reporte de Auditoria");
        String dni =  "";//data.optString("dni");
        if(data.optString("dni").length()>0){
            dni = data.optString("dni");
        }else{dni = "";}
        String fec_ini = "";//data.optString("fec_ini");
        if(data.optString("fec_ini").length()>0){
            fec_ini = data.optString("fec_ini");
        }else{fec_ini = "";}
        String fec_fin = "";//data.optString("fec_fin");
        if(data.optString("fec_fin").length()>0){
            fec_fin = data.optString("fec_fin");
        }else{fec_fin = "";}
        
        Persona pe = null;
        PersonaDao peDao = (PersonaDao)FactoryDao.buildDao("di.PersonaDao");
        
        try{
            pe = peDao.buscarPersonaxId(dato);
            
        
        }catch(Exception e){
            System.out.println("No se encontro a la persona con el DNI dado \n"+e);
            return WebResponse.crearWebResponseError("No se encontro a la persona con el DNI dado ", e.getMessage() );
        }
        String nombreDeSolicita=pe.getApePat()+" "+pe.getApeMat()+", "+pe.getNom();
        
        //JSONArray cabeceras= data.optJSONArray("cabeceras");
        List<String> items_cabeceras = new ArrayList<String>();

            items_cabeceras.add("Numero Registro");
            items_cabeceras.add("Tipo de operacion");
            items_cabeceras.add("Observacion de operacion");
            items_cabeceras.add("Contenido de operacion");
            items_cabeceras.add("Fecha de operacion");
            items_cabeceras.add("Id de usuario");
            items_cabeceras.add("Nombre Usuario");
            items_cabeceras.add("Apellido Paterno");
            items_cabeceras.add("Apellido Materno");
            items_cabeceras.add("DNI");
            
        AgregarOperacionAuditoria.agregarLecturaAuditoria("Descarga de log ", "DNI para el log:"+dni , wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
        return crearReporte(dni,fec_ini,fec_fin,items_cabeceras,nombreDeSolicita);
    }

    private WebResponse crearReporte(String dni,String fec_ini,String fec_fin,List<String> cabeceras, String nombredesolicitador) {
        try{
            String data = crearArchivo(dni,fec_ini,fec_fin,cabeceras,nombredesolicitador);
            return WebResponse.crearWebResponseExito("", new JSONObject().put("file",data));
        }catch (Exception e){
            logger.log(Level.SEVERE,"crearReporte",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String  crearArchivo(String dni,String fec_ini,String fec_fin,List<String> cabeceras,String nombredesolicitador) throws Exception{
        ////////////faTOS USADOS//////////////////
        List<String[]> consulta = new ArrayList<String[]>();
        LogAuditoriaDao logAuditoriaDao = (LogAuditoriaDao)FactoryDao.buildDao("LogAuditoriaDao");
        try{
            consulta = logAuditoriaDao.listarReporteAuditoria(dni,fec_ini,fec_fin);
        }catch(Exception e){
            System.out.println("No se pudo Crear Archivo de lista la consulta General  \n"+e);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////recojimos datos ahora armar mi text que al retornarmitext.encodetoBase64 retorna String
        ////////////////////////////////////////////////////////////////////////////////////////////////////// 
        try{
        //////////////////////////////////////Desempaquetamos la consulta/////////////////////////////////
        ////////////////////////calculamos dimensiones de la consulta numero de columnas primero = numero cabeceras luego numero de filas
            int number_rows = consulta.size();
            int number_columns = cabeceras.size();//OBTENEMOS EL NUMERO COLUMNAS!! QUE NOS RETORNO LA CONSULTA
            System.out.println("FECHASSS:"+ fec_ini +"-"+fec_fin);
            System.out.println("TAMAÃ‘O DE CONSULTA LENGTH CONSULTA:");System.out.println(number_rows);
            System.out.println("NUMERO DE COLUMNAS DE CONSULTA:");System.out.println(number_columns);
        /* DATOS DE LA ORGANIZACION*/
        String nombre_inst = "Reporte de Auditoria";

        /*TITULO*/  
        String titulo = "RESUMEN DE CONSULTA GENERAL ";
        
        
        /*CABECERA DE TABLA*/
        String[] cab = cabeceras.toArray(new String[0]);//The toArray() method without passing any argument returns Object[]. So you have to pass an array as an argument, which will be filled with the data from the list, and returned. You can pass an empty array as well, but you can also pass an array with the desired size.


        /*LLENAMOS LOS DATOS A LA CABECERA DEL REPORTE*/
           Mitext m = null; 
           m = new Mitext(true,titulo, "Realizado por: "+nombredesolicitador);
           m.agregarParrafo("Titulo del reporte : " +nombre_inst);
           
           
        /*CONSTRUIMOS EL OBJETO REPORTE*/
            //float columnWidths_1 []= {1,1,2,1,2,2,1};
            float columnWidths_1 []= new float[number_columns];
            for(int nc=0 ; nc<number_columns ; nc++){
                    columnWidths_1[nc]=1;
                }
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.setRelativePosition(0,20,0,0);
            tabla_1.build(cab);
          
       
        /*LLENAMOS LA DATA A LA TABLA*/  
             /*GCell[] cell ={tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)
            ,tabla_1.createCellCenter(1,1),tabla_1.createCellCenter(1,1)};*/
            GCell[] cell = new GCell[number_columns];
            
             
  /////////////////////////////////RECORREMOS ARREGLO DE ELEMENTOS OBTENIDOS EN LISTAR POR CONSULTA GENERAL///
            
  /////////////////////////////////RECORREMOS ARREGLO DE ELEMENTOS OBTENIDOS EN LISTAR POR CONSULTA GENERAL///         
            for(int nr=0 ; nr<number_rows ; nr++){
                for(int nc=0 ; nc<number_columns ; nc++){
                    cell[nc]=tabla_1.createCellCenter(1,1);
                }
                tabla_1.processLineCell(consulta.get(nr),cell);  
            }
           m.agregarTabla(tabla_1);
           
                /*PIE DE PAGINA*/
                        m.newLine(4);
                        m.agregarParrafo("FIN DE REPORTE DE AUDITORIA ");
                        //m.newLine(7);
                            /*FECHA DE REPORTE*/
                            java.util.Date fecha = new Date();
                            String dat = new SimpleDateFormat("yyyy-MM-dd").format(fecha);
                        m.agregarNotaParrafo("** Reporte generado con Datos a la Fecha: "+dat);
                        //m.agregarParrafo("Solicitado por:" +nombredesolicitador);
                        
                        
           
           
            m.cerrarDocumento();
            
            return m.encodeToBase64();
            
            
           
        }
        catch(Exception e){
              System.out.println(e);
              return null;
            
        }     
  
    }
    

}
