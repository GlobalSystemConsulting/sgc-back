/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "zona", schema="administrativo")

public class Zona implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "zon_id")
    private Integer zonId;
    
    @Size(max = 2147483647)
    @Column(name = "cod_zon")
    private String codZon;
    
    @Size(max = 2147483647)
    @Column(name = "nom_zon")
    private String nomZon;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "zona")
    List<Trabajador> trabajadores;

    public Zona() {
    }

    public Zona(Integer zonId) {
        this.zonId = zonId;
    }

    public Integer getZonId() {
        return zonId;
    }

    public void setZonId(Integer zonId) {
        this.zonId = zonId;
    }

    public String getCodZon() {
        return codZon;
    }

    public void setCodZon(String codZon) {
        this.codZon = codZon;
    }

    public String getNomZon() {
        return nomZon;
    }

    public void setNomZon(String nomZon) {
        this.nomZon = nomZon;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zonId != null ? zonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zona)) {
            return false;
        }
        Zona other = (Zona) object;
        if ((this.zonId == null && other.zonId != null) || (this.zonId != null && !this.zonId.equals(other.zonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Zona{" + "zonId=" + zonId + ", codZon=" + codZon + ", nomZon=" + nomZon + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }


    
}
