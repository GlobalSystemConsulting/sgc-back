package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="registro_asistencia" ,schema="administrativo")
public class RegistroAsistencia  implements java.io.Serializable {


    @Id
    @Column(name="reg_asi_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_registro_asistencia", sequenceName="administrativo.registro_asistencia_reg_asi_id_seq" )
    @GeneratedValue(generator="secuencia_registro_asistencia")
    private Integer regAsiId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_ing")
    private Date horaIngreso;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_sal")
    private Date horaSalida;
    
    @Column(name="est_reg")
    private String estReg;
    
    @Column(name="reg_asi_est")
    private String estAsi;
    
    @Column(name="reg_min_tar")
    private Integer minTardanza;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tra_id", nullable=false )
    private Trabajador trabajadorId;    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="reg_hor_cab", nullable=true )
    private HorarioCab horario;
    
    @OneToOne(fetch=FetchType.EAGER, mappedBy="jusRegAsiId")
    private Justificacion justificacion;
    
    @Column(name="reg_des_adi")
    private String descripcionAdicional;
    
    @Column(name="reg_nro_doc_adi")
    private String nroDocumento;
    
    @Column(name="reg_doc_adi")
    private String documento;
    
    @Column(name="reg_min_adi")
    private Integer minutosAdicionales;
    
    
    
    public RegistroAsistencia() {
    }

    public RegistroAsistencia(Integer regAsiId) {
        this.regAsiId = regAsiId;
    }

    
    public RegistroAsistencia(Date horaIngreso, Date horaSalida, String estReg, String estAsi, Trabajador trabajadorId,HorarioCab horario) {
        this.horaIngreso = horaIngreso;
        this.horaSalida = horaSalida;
        this.estReg = estReg;
        this.estAsi = estAsi;
        this.trabajadorId = trabajadorId;
        this.horario = horario;
    }
    
    public RegistroAsistencia(Date horaIngreso, Date horaSalida, String estReg, String estAsi, Trabajador trabajadorId,HorarioCab horario,Integer minTardanza) {
        this.horaIngreso = horaIngreso;
        this.horaSalida = horaSalida;
        this.estReg = estReg;
        this.estAsi = estAsi;
        this.trabajadorId = trabajadorId;
        this.horario = horario;
        this.minTardanza=minTardanza;
    }
    
    
    
    
    public Integer getRegAsiId() {
        return regAsiId;
    }

    public void setRegAsiId(Integer regAsiId) {
        this.regAsiId = regAsiId;
    }

    public Date getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(Date horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getEstAsi() {
        return estAsi;
    }

    public void setEstAsi(String estAsi) {
        this.estAsi = estAsi;
    }

    public Trabajador getTrabajadorId() {
        return trabajadorId;
    }

    public void setTrabajadorId(Trabajador trabajadorId) {
        this.trabajadorId = trabajadorId;
    }

    public Justificacion getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(Justificacion justificacion) {
        this.justificacion = justificacion;
    }

    public HorarioCab getHorario() {
        return horario;
    }

    public void setHorario(HorarioCab horario) {
        this.horario = horario;
    }

    public String getDescripcionAdicional() {
        return descripcionAdicional;
    }

    public void setDescripcionAdicional(String descripcionAdicional) {
        this.descripcionAdicional = descripcionAdicional;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Integer getMinutosAdicionales() {
        return minutosAdicionales;
    }

    public void setMinutosAdicionales(Integer minutosAdicionales) {
        this.minutosAdicionales = minutosAdicionales;
    }

    public Integer getMinTardanza() {
        return minTardanza;
    }

    public void setMinTardanza(Integer minTardanza) {
        this.minTardanza = minTardanza;
    }

    
    

}


