/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class AgregarTipoOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarTipoOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        TipoOrganigrama tipoOrganigrama = null;
        DatosOrganigrama datosOrganigrama=null;
        
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String codTip = requestData.optString("codTipOrga");
            String nomTip = requestData.optString("nomTipOrga");
            String desTip = requestData.optString("desTipOrga");  
            Integer datId = requestData.getInt("datId");
            
            
            tipoOrganigrama = new TipoOrganigrama();
            DatosOrganigramaDao datosOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");
            datosOrganigrama = datosOrganigramaDao.buscarXId(datId);
            
            tipoOrganigrama.setCodTipOrgi(codTip);
            tipoOrganigrama.setNomTipOrgi(nomTip);
            tipoOrganigrama.setDesTipOrgi(desTip);
            tipoOrganigrama.setEstReg('A');
            tipoOrganigrama.setFecMod(new Date());
            tipoOrganigrama.setUsuMod(wr.getIdUsuario());
            tipoOrganigrama.setDatosOrganigramas(datosOrganigrama);
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo Tipo Organigrama",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        TipoOrganigramaDao tipoOrganigramaDao = (TipoOrganigramaDao) FactoryDao.buildDao("se.TipoOrganigramaDao");
        try {
            tipoOrganigramaDao.insert(tipoOrganigrama);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva Tipo Organigrama",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("tipId", tipoOrganigrama.getTipOrgiId());
        oResponse.put("datId", datosOrganigrama.getNomDatOrgi()+" "+datosOrganigrama.getAnioDatOrgi());
        oResponse.put("codTipOrga", tipoOrganigrama.getCodTipOrgi());
        oResponse.put("nomTipOrga", tipoOrganigrama.getNomTipOrgi());
        oResponse.put("desTipOrga", tipoOrganigrama.getDesTipOrgi());
                
        return WebResponse.crearWebResponseExito("El registro de la Tipo Organigrama se realizo correctamente", oResponse);

    }
    
}
