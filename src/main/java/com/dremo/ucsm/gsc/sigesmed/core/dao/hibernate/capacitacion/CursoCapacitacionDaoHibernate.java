package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx.HelpTraining;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.type.IntegerType;
import org.json.JSONArray;
import org.json.JSONObject;

public class CursoCapacitacionDaoHibernate extends GenericDaoHibernate<CursoCapacitacion> implements CursoCapacitacionDao {

    private static final Logger logger = Logger.getLogger(CursoCapacitacionDaoHibernate.class.getName());

    @Override
    public List<CursoCapacitacion> listarCapacitaciones() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(CursoCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .setFetchMode("sedesCapacitacion", FetchMode.JOIN)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionPorOrganizacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<CursoCapacitacion> listarCapacitacionPorOrganizacion(int idOrg) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(CursoCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .createCriteria("organizacion")
                    .add(Restrictions.eq("orgId", idOrg));

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionPorOrganizacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<CursoCapacitacion> listarCapacitacionPorOrganizacionYAno(int idOrg, int ano) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query;
            query = session.createCriteria(CursoCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .createCriteria("organizacion")
                    .add(Restrictions.eq("orgId", idOrg))
                    .add(Restrictions.sqlRestriction("(SELECT extract(year from fec_ini)) = ? ", ano, IntegerType.INSTANCE));

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionPorOrganizacionYAno", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public CursoCapacitacion buscarPorId(int idCur) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(CursoCapacitacion.class)
                    .setFetchMode("organizacion", FetchMode.JOIN)
                    .setFetchMode("organizacionAut", FetchMode.JOIN)
                    .add(Restrictions.eq("curCapId", idCur));

            return (CursoCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Organizacion> listarOrganizaciones() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(Organizacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .add(Restrictions.ne("cod", "*"));

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionPorOrganizacion", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<CursoCapacitacion> listarCapacitacionPorTipo(String tipo) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(CursoCapacitacion.class)
                    .add(Restrictions.eq("tip", tipo))
                    .add(Restrictions.eq("est", 'A'))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionPorTipo", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public JSONArray listarCapacitacionesCapacitador(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String queryStr = "SELECT curso_capacitacion.nom AS nomCur, curso_capacitacion.cur_cap_id, sedes_capacitacion.sed_cap_id, curso_capacitacion.est, "
                    + "curso_capacitacion.tip, organizacion.nom AS nomOrg, curso_capacitacion.num_par, sedes_capacitacion.pro, sedes_capacitacion.dis,cronograma_sedes_capacitacion.fec_ini, cronograma_sedes_capacitacion.fec_fin "
                    + "FROM pedagogico.sedes_capacitacion, pedagogico.capacitador_curso_capacitacion, pedagogico.curso_capacitacion, public.organizacion, pedagogico.cronograma_sedes_capacitacion  "
                    + "WHERE "
                    + "sedes_capacitacion.sed_cap_id = capacitador_curso_capacitacion.sed_cap_id AND sedes_capacitacion.cur_cap_id = curso_capacitacion.cur_cap_id AND sedes_capacitacion.sed_cap_id = cronograma_sedes_capacitacion.sed_cap_id AND "
                    + "curso_capacitacion.org_id_org = organizacion.org_id AND capacitador_curso_capacitacion.per_id = " + usuCod;

            List<Object[]> cursos = session.createSQLQuery(queryStr).list();
            JSONArray curArr = new JSONArray();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date ini, fin;
            
            for (Object[] curso : cursos) {
                JSONObject curObj = new JSONObject();
                curObj.put("curTip", curso[4].toString());
                if (curso[4].toString().equals("Online")) {
                    curObj.put("curNom", curso[0].toString());
                } else {
                    curObj.put("curNom", curso[0].toString() + " SEDE: " + curso[7].toString() + " - " + curso[8].toString());
                }

                curObj.put("curCod", curso[1]);
                curObj.put("curEst", curso[3]);
                curObj.put("curPar", curso[6]);
                curObj.put("curIni", curso[9]);
                curObj.put("curFin", curso[10]);
                
                try {                    
                    ini = HelpTraining.getStartOfDay(format.parse(curso[9].toString()));
                    curObj.put("sedIni", format.format(ini));
                    fin = HelpTraining.getEndOfDay(format.parse(curso[10].toString()));
                    curObj.put("sedFin", format.format(fin));
                    
                    if (ini.before(new Date()))
                        if (fin.before(new Date()))
                            curObj.put("sedEst", "F");
                        else
                            curObj.put("sedEst", "I");
                    else
                        curObj.put("sedEst", "A");
                } catch (ParseException e) {
                    logger.log(Level.SEVERE, "listarCapacitacionesCapacitador", e);    
                }
                
                curObj.put("sedCod", curso[2]);
                curObj.put("orgNom", curso[5]);
                curArr.put(curObj);
            }

            return curArr;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionesCapacitador", e);    
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public JSONArray listarCapacitacionesPersona(int usuCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String queryStr = "SELECT curso_capacitacion.nom, cronograma_sedes_capacitacion.fec_ini, cronograma_sedes_capacitacion.fec_fin, cronograma_sedes_capacitacion.sed_cap_id, "
                            + "curso_capacitacion.cur_cap_id, sedes_capacitacion.pro, sedes_capacitacion.dis, curso_capacitacion.est_reg, curso_capacitacion.tip " 
                            + "FROM pedagogico.curso_capacitacion, pedagogico.sedes_capacitacion, pedagogico.cronograma_sedes_capacitacion, pedagogico.participantes_capacitacion, public.usuario  "
                            + "WHERE "
                            + "sedes_capacitacion.cur_cap_id = curso_capacitacion.cur_cap_id AND sedes_capacitacion.sed_cap_id = cronograma_sedes_capacitacion.sed_cap_id AND "
                            + "participantes_capacitacion.sed_cap_id = sedes_capacitacion.sed_cap_id AND usuario.usu_id = participantes_capacitacion.per_id AND participantes_capacitacion.est_reg = 'A' AND "
                            + "usuario.usu_id = " + usuCod;

            List<Object[]> cursos = session.createSQLQuery(queryStr).list();
            JSONArray curArr = new JSONArray();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date ini, fin;
            
            for (Object[] curso : cursos) {
                JSONObject curObj = new JSONObject();
                
                curObj.put("curTip", curso[8].toString());
                
                if (curso[8].toString().equals("Online")) {
                    curObj.put("curNom", curso[0].toString());
                } else {
                    curObj.put("curNom", curso[0].toString() + " SEDE: " + curso[5].toString() + " - " + curso[6].toString());
                }

                curObj.put("curCod", curso[4]);
                curObj.put("curEst", curso[7]);
                curObj.put("curIni", curso[1]);
                curObj.put("curFin", curso[2]);
                
                try {
                    ini = HelpTraining.getStartOfDay(format.parse(curso[1].toString()));
                    curObj.put("sedIni", format.format(ini));
                    fin = HelpTraining.getEndOfDay(format.parse(curso[2].toString()));
                    curObj.put("sedFin", format.format(fin));
                    
                    if (ini.before(new Date()))
                        if (fin.before(new Date()))
                            curObj.put("sedEst", "F");
                        else
                            curObj.put("sedEst", "I");
                    else
                        curObj.put("sedEst", "A");
                } catch (ParseException e) {
                    logger.log(Level.SEVERE, "listarCapacitacionesDocente", e);    
                }
                
                curObj.put("sedCod", curso[3]);
                curArr.put(curObj);
            }

            return curArr;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCapacitacionesDocente", e);    
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<CursoCapacitacion> listarReportes() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(CursoCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarReportes", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<AsistenciaParticipante> asistenciaDocente(int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT a FROM AsistenciaParticipante a "
                    + "JOIN a.horario k "
                    + "JOIN a.persona p "
                    + "WHERE p.perId = :perId AND a.fec <= :fecReg AND k.turFin < :turFin AND a.estReg = :estReg";
              

            Query query = session.createQuery(hql);
            query.setParameter("perId", perId);
            query.setParameter("fecReg", new Date());
            query.setParameter("estReg", 'G');
            query.setParameter("turFin", new Date());

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "asistenciaDocente", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<SedeCapacitacion> listarDisponibles(int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT DISTINCT s FROM SedeCapacitacion s "
                    + "JOIN FETCH s.cursoCapacitacion r "
                    + "JOIN FETCH r.organizacion "
                    + "JOIN FETCH r.organizacionAut "
                    + "JOIN FETCH s.horarios "
                    + "JOIN FETCH s.cronograma k "
                    + "LEFT JOIN FETCH s.capacitadoresCursoCapacitacion o "
                    + "LEFT JOIN FETCH o.per "
                    + "WHERE s NOT IN "
                        + "(SELECT c.sedCap FROM ParticipanteCapacitacion c "
                        + "JOIN c.persona p "
                        + "WHERE p.perId = :perId) "
                    + "AND k.fecIni > :fecIni";

            Query query = session.createQuery(hql);
            query.setParameter("perId", perId);
            query.setParameter("fecIni", new Date());
            
            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarDisponibles", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
