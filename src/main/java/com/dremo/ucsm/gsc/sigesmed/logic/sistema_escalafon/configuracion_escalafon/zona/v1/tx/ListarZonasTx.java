/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ZonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarZonasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarZonasTx.class.getName());
  
    @Override
        public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Ubicacion> zonas = null;
        UbicacionDao zonaDao = (UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        
        try{
            zonas = zonaDao.listarxZona();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar zonas",e);
            System.out.println("No se pudo listar las zonas.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas zonas.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Ubicacion z: zonas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("zonaId", z.getUbiId());
            oResponse.put("codZon", z.getUbiCod());
            oResponse.put("nomZon", z.getUbiNom());
            oResponse.put("fecMod", z.getFecMod());
            oResponse.put("UsuMod", z.getUsuMod());
            oResponse.put("estReg", z.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las zonas fueron listadas exitosamente", miArray);
    }
    
}
