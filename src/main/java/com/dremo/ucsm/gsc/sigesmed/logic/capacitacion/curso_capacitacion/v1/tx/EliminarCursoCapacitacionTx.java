package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 04/10/2016.
 */
public class EliminarCursoCapacitacionTx implements ITransaction{
    private static  final Logger logger = Logger.getLogger(EliminarCursoCapacitacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject curso = (JSONObject) wr.getData();
        return eliminarCurso(curso.getInt("cod"));
    }
    public WebResponse eliminarCurso(int idCurso){
        try{
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion curso = capacitacionDao.buscarPorId(idCurso);
            capacitacionDao.delete(curso);
            return WebResponse.crearWebResponseExito("La capacitación se elimino correctamente", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCurso",e);
            return WebResponse.crearWebResponseError("Error al eliminar la capacitación", WebResponse.BAD_RESPONSE);
        }
    }
}
