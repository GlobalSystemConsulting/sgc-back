/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.tipo_organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarTipoOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoOrganizacion actualizarTipoOrganizacion = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoOrganizacionID = requestData.getInt("tipoOrganizacionID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            JSONArray roles = requestData.getJSONArray("roles");
            
            actualizarTipoOrganizacion = new TipoOrganizacion(tipoOrganizacionID, codigo, nombre, descripcion, new Date(), 1, estado.charAt(0), null);
            
            actualizarTipoOrganizacion.setRoles(new ArrayList<Rol>());
            for(int i = 0; i < roles.length();i++){
                JSONObject bo = roles.getJSONObject(i);
                actualizarTipoOrganizacion.getRoles().add( new Rol( bo.getInt("rolID") ) );
            }
            //public TipoOrganizacion(int tipOrgId, String cod, String nom, String des, Date fecMod, int usuMod, char estReg, null)
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoOrganizacionDao tipoOrgDao = (TipoOrganizacionDao)FactoryDao.buildDao("TipoOrganizacionDao");
        try{
            
            tipoOrgDao.eliminarRoles(actualizarTipoOrganizacion.getTipOrgId());
            tipoOrgDao.update(actualizarTipoOrganizacion);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar Tipo de Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Tipo de Organizacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo de Organizacion se actualizo correctamente");
        //Fin
    }
    
}
