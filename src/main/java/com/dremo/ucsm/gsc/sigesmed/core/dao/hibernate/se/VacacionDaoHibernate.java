/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class VacacionDaoHibernate extends GenericDaoHibernate<Vacacion> implements VacacionDao{

    @Override
    public List<Vacacion> listarxFichaEscalafonaria(int ficEscId) {
        List<Vacacion> vacaciones = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT vac from Vacacion as vac "
                    + "join fetch vac.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND vac.estReg='A'";
            Query query = session.createQuery(hql);
            vacaciones = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las vacaciones\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las vacaciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return vacaciones;
    }

    @Override
    public Vacacion buscarPorId(Integer vacId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Vacacion vac = (Vacacion)session.get(Vacacion.class, vacId);
        session.close();
        return vac;
    }

    @Override
    public List<Vacacion> listarSGxTrabajador(int traId) {
        List<Vacacion> vacaciones = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT vac FROM Vacacion AS vac "
                    + "JOIN FETCH vac.fichaEscalafonaria AS fe "
                    + "JOIN FETCH fe.trabajador AS tr "
                    + "WHERE tr.traId=:workerId "
                    + "AND vac.estGoc=false "
                    + "AND vac.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("workerId", traId);
            vacaciones = query.list();
            t.commit();
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las vacaciones\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las vacaciones \\n "+ e.getMessage());            
        }finally{
            session.close();
        }
        return vacaciones;
    }
    
}
