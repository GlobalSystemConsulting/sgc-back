/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="ajuste_pagina_web" ,schema="pedagogico")
public class AjustePaginaWeb implements java.io.Serializable{
    @Id
    @Column(name="ajp_ide", unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_ajp", sequenceName="pedagogico.ajuste_pagina_web_ajp_ide_seq")
    @GeneratedValue(generator="secuencia_ajp")
    private int ajpId;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_id")
    private Usuario usuario;
   
    @Column(name="ajp_plw_izq", nullable=false, length=1)
    private String ajusteIzq;
    
    @Column(name="ajp_plw_der", nullable=false, length=1)
    private String ajusteDer;
    
    @Column(name="ajp_col", nullable=false)
    private int ajusteColor;
    
    @Column(name="est_reg", length=1)
    private char estReg;

    public AjustePaginaWeb() {
    }

    public AjustePaginaWeb(int ajpId, Usuario usuario, String ajusteIzq, String ajusteDer, int ajusteColor, char estReg) {
        this.ajpId = ajpId;
        this.usuario = usuario;
        this.ajusteIzq = ajusteIzq;
        this.ajusteDer = ajusteDer;
        this.ajusteColor = ajusteColor;
        this.estReg = estReg;
    }

    public int getAjpId() {
        return ajpId;
    }

    public void setAjpId(int ajpId) {
        this.ajpId = ajpId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getAjusteIzq() {
        return ajusteIzq;
    }

    public void setAjusteIzq(String ajusteIzq) {
        this.ajusteIzq = ajusteIzq;
    }

    public String getAjusteDer() {
        return ajusteDer;
    }

    public void setAjusteDer(String ajusteDer) {
        this.ajusteDer = ajusteDer;
    }

    public int getAjusteColor() {
        return ajusteColor;
    }

    public void setAjusteColor(int ajusteColor) {
        this.ajusteColor = ajusteColor;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "AjustePaginaWeb{" + "ajpId=" + ajpId + ", usuario=" + usuario + ", ajusteIzq=" + ajusteIzq + ", ajusteDer=" + ajusteDer + ", ajusteColor=" + ajusteColor + ", estReg=" + estReg + '}';
    }
    
    
   
}
