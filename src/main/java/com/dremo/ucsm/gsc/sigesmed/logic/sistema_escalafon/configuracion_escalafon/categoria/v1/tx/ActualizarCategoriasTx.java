/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarCategoriasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarCategoriasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer plaCat=0;
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer catId = requestData.getInt("catId");
            String abrCat = requestData.optString("abrCat");
                    plaCat= requestData.getInt("plaCat");
            //Character estReg = requestData.optString("estReg").charAt(0);
            //Date fecMod = (Date)requestData.getString("fecMod");
            ////////historialCambioCategorias
            String nomCat = requestData.optString("nomCat");
            ////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");

            return actualizarCategorias(catId,abrCat,nomCat,plaCat);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCategorias(Integer catId,String abrCat,String nomCat, Integer plaCat) {
        
        try{
            CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");        
            Categoria categoria = categoriaDao.buscarPorId(catId);
            
            Planilla planilla=null;
            if(plaCat!=0){
                PlanillaDao planillaDao = (PlanillaDao) FactoryDao.buildDao("se.PlanillaDao");
                planilla=planillaDao.buscarPorId(plaCat);
                categoria.setPlanilla(planilla);            
            }
            
            categoria.setAbrCat(abrCat);
            categoria.setNomCat(nomCat);
                    
            categoriaDao.update(categoria);
            JSONObject oResponse = new JSONObject();
            oResponse.put("catId", categoria.getCatId());
            if(plaCat==0){
                oResponse.put("plaCat","");
                oResponse.put("plaNom","");
            }
            else{   
                oResponse.put("plaCat",categoria.getPlanilla().getPlaId());
                oResponse.put("plaNom", categoria.getPlanilla().getNomPla());
            }
            oResponse.put("abrCat", categoria.getAbrCat());
            oResponse.put("nomCat", categoria.getNomCat());
            return WebResponse.crearWebResponseExito(" Categoria actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, la  Categoria no fue actualizada");
        }
    }
}
