/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import java.util.List;


public interface LicenciaDao extends GenericDao<Licencia>{
    List<Licencia> listarxFichaEscalafonaria(int ficEscId);
    Licencia buscarPorId(Integer licId);
    List<Licencia> listarSGxTrabajador(int traId);
}
 