package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ObjetivoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PerfilCapacitadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ObjetivoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PerfilCapacitador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;

public class RegistrarCursoCapacitacionTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(RegistrarCursoCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();

            OrganizacionCapacitacionDao organizacionDao = (OrganizacionCapacitacionDao) FactoryDao.buildDao("capacitacion.OrganizacionCapacitacionDao");
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            CursoCapacitacionDao cursoCapacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            ObjetivoCapacitacionDao objetivoCapacitacionDao = (ObjetivoCapacitacionDao) FactoryDao.buildDao("capacitacion.ObjetivoCapacitacionDao");
            PerfilCapacitadorDao perfilCapacitadorDao = (PerfilCapacitadorDao) FactoryDao.buildDao("capacitacion.PerfilCapacitadorDao");
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");
            
            Organizacion organizacion = organizacionDao.buscarPorId(data.getInt("organizacion"));
            Organizacion organizacionAut = organizacionDao.buscarPorId(data.getInt("organizacionAut"));
            
            CursoCapacitacion course = new CursoCapacitacion(organizacion,   
                organizacionAut, data.getString("nom").toUpperCase(), 
                data.getString("tip"),  
                data.getInt("usuMod")
            );

            cursoCapacitacionDao.insert(course);
            
            JSONArray sedes = data.getJSONArray("sed");
            Date fecIniCap = null;
            Date fecFinCap = null;
            
            for(int i = 0;i < sedes.length();i++) {
                JSONObject sede = sedes.getJSONObject(i);

                SedeCapacitacion place = new SedeCapacitacion(course, sede.getString("pro"),
                    sede.getString("dis"), sede.getString("loc"), sede.getString("dir"), sede.getString("ref")
                );
                
                place.setCursoCapacitacion(course);
                
                JSONObject cronograma = sede.getJSONObject("cro");
                
                Date fecIni = HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(cronograma.getString("fecIni")).getTime());
                Date fecFin = HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(cronograma.getString("fecFin")).getTime());
                
                CronogramaSedeCapacitacion schedule = new CronogramaSedeCapacitacion(fecIni, fecFin,
                    cronograma.getString("dniRes"), cronograma.getString("nomRes")
                );
                
                schedule.setSedeCapacitacion(place);
                place.setCronograma(schedule);
                sedeCapacitacionDao.insert(place);
                
                JSONArray horarios = sede.getJSONObject("hor").getJSONArray("shifts");

                for(int j = 0;j < horarios.length();j++) {
                    JSONObject horario = horarios.getJSONObject(j);
                    
                    Date turIni = HelpTraining.getStartOfTime(DatatypeConverter.parseDateTime(horario.getString("turIni")).getTime());
                    Date turFin = HelpTraining.getStartOfTime(DatatypeConverter.parseDateTime(horario.getString("turFin")).getTime());
                    
                    HorarioSedeCapacitacion shift = new HorarioSedeCapacitacion(place, horario.getInt("dia"), turIni, turFin);
                    place.getHorarios().add(shift);
                    horarioSedeCapacitacionDao.insert(shift);
                }
                
                if (fecIniCap == null || fecIniCap.after(fecIni)) {
                    fecIniCap = fecIni;
                }

                if (fecFinCap == null || fecFinCap.before(fecFin)) {
                    fecFinCap = fecFin;
                }
            }
            
            course.setFecIni(fecIniCap);
            course.setFecFin(fecFinCap);
            
            if (course.getFecIni().before(new Date()))
                if (course.getFecFin().before(new Date()))
                    course.setEst('F');
                else
                    course.setEst('I');
            
            cursoCapacitacionDao.update(course);
            
            if(data.getJSONObject("obj").getBoolean("est")) {
                JSONObject objetivo = data.getJSONObject("obj").getJSONObject("obj");
                Set<ObjetivoCapacitacion> goals = new HashSet<>();                
                JSONArray objetivos = objetivo.getJSONArray("array");
                
                if(objetivo.getString("modo").equals("Busqueda")) {
                    for(int i = 0;i < objetivos.length();i++)
                        goals.add(objetivoCapacitacionDao.buscarPorId(objetivos.getInt(i)));
                } else {
                    for(int i = 0;i < objetivos.length();i++) {
                        ObjetivoCapacitacion goal = new ObjetivoCapacitacion(objetivos.getJSONObject(i).getString("tip").charAt(0),
                            objetivos.getJSONObject(i).getString("des"), "Fuente"
                        );

                        objetivoCapacitacionDao.insert(goal);
                        goals.add(goal);
                    }
                }
                
                course.setObjetivosCapacitacion(goals);
                cursoCapacitacionDao.update(course);
            }
            
            if(data.getJSONObject("per").getBoolean("est")) {
                JSONObject perfil = data.getJSONObject("per").getJSONObject("obj");
                Set<PerfilCapacitador> profiles = new HashSet<>();
                JSONArray perfiles = perfil.getJSONArray("array");
                
                if(perfil.getString("modo").equals("Busqueda")) {
                    for(int i = 0;i < perfiles.length();i++)
                        profiles.add(perfilCapacitadorDao.buscarPorId(perfiles.getInt(i)));
                } else {
                    for(int i = 0;i < perfiles.length();i++) {
                        PerfilCapacitador profile = new PerfilCapacitador(perfiles.getJSONObject(i).getString("tip").charAt(0),
                            perfiles.getJSONObject(i).getString("des"), "Fuente"
                        );

                        perfilCapacitadorDao.insert(profile);
                        profiles.add(profile);
                    }
                }
                
                course.setPerfilesCapacitacion(profiles);
                cursoCapacitacionDao.update(course);
            }
            
            
            JSONObject object = new JSONObject();
            object.put("cod", course.getCurCapId());
            object.put("ini", course.getFecFin().getTime());
            object.put("fin", course.getFecFin().getTime());
            object.put("est", course.getEst());
            object.put("tip", course.getTip());
            object.put("org", organizacion.getNom());
            
            /*return WebResponse.crearWebResponseExito("El curso de capacitación fue creado correctamente", WebResponse.OK_RESPONSE).setData(
                    new JSONObject(EntityUtil.objectToJSONString(
                                    new String[]{"curCapId", "fecIni", "fecFin", "est", "tip"},
                                    new String[]{"cod", "ini", "fin", "est", "tip"},
                                    course
                            )));*/
            return WebResponse.crearWebResponseExito("El curso de capacitación fue creado correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarCurso",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el curso de capacitación",WebResponse.BAD_RESPONSE);
        }
    }
}
