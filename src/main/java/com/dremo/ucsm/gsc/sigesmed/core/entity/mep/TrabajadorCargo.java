package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import javax.persistence.*;

/**
 * Created by Administrador on 12/10/2016.
 */
@Entity(name = "mep.TrabajadorCargo")
@Table(name = "trabajador_cargo")
public class TrabajadorCargo implements java.io.Serializable{
    @Id
    @Column(name="crg_tra_ide", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_trabajador_cargo_mep", sequenceName="trabajador_cargo_crg_tra_ide_seq" )
    @GeneratedValue(generator="secuencia_trabajador_cargo_mep")

    private int traCarId;

    @Column(name = "crg_tra_ali", length = 5)
    private String carAli;

    @Column(name = "crg_tra_des", length = 400)
    private String carDes;

    @Column(name = "crg_tra_nom", nullable = false)
    private String carNom;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @Column(name = "crg_tra_tip", length = 3)
    private String carTip;

    public TrabajadorCargo() {
    }

    public TrabajadorCargo(String carNom) {
        this.carNom = carNom;
    }

    public TrabajadorCargo(String carNom, String carDes, String carAli, String carTip) {
        this.carNom = carNom;
        this.carDes = carDes;
        this.carAli = carAli;
        this.carTip = carTip;
    }

    public int getTraCarId() {
        return traCarId;
    }

    public void setTraCarId(int traCarId) {
        this.traCarId = traCarId;
    }

    public String getCarAli() {
        return carAli;
    }

    public void setCarAli(String carAli) {
        this.carAli = carAli;
    }

    public String getCarDes() {
        return carDes;
    }

    public void setCarDes(String carDes) {
        this.carDes = carDes;
    }

    public String getCarNom() {
        return carNom;
    }

    public void setCarNom(String carNom) {
        this.carNom = carNom;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getCarTip() {
        return carTip;
    }

    public void setCarTip(String carTip) {
        this.carTip = carTip;
    }
}
