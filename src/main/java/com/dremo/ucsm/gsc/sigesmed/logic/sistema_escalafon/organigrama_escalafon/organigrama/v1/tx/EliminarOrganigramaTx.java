/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class EliminarOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarOrganigramaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer orgId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           orgId = requestData.getInt("orgId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminar  Organigrama",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        OrganigramaDao organigramaDao = (OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        try{
            organigramaDao.delete(new Organigrama(orgId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Organizacion Padre\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Organigram", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("Tipo Organigram se elimino correctamente");
    }
    
}

