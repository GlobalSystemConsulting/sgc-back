/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.EntidadObjetoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class EliminarEntidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int registroId = 0;
        EntidadObjetoDao entidadObjetoDao = (EntidadObjetoDao)FactoryDao.buildDao("EntidadObjetoDao");
        Object obj = null ;
        String nombreclase = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            registroId= requestData.getInt("regsitroID");
            nombreclase = requestData.getString("nombreclase");
            
             try{
                obj = Class.forName("com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreclase).newInstance();
            
            //catalogoTabla = new CatalogoTabla(0, nombretabla, nombreclase, new Date(),wr.getIdUsuario(), estado.charAt(0));
           
                obj.getClass().getMethod("setId",int.class).invoke(obj,registroId);
              
            }catch(Exception e){
                System.out.println("No se pudo registrar Set \n"+e);
             return WebResponse.crearWebResponseError("No se pudo registrar, Error de SETS", e.getMessage() );
            }
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        try{
            entidadObjetoDao.delete(obj);
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Registro de la entidad \n"+nombreclase+"  "+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Registro ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Registro se elimino correctamente");
        //Fin
    }
}
