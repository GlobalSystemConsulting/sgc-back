package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;

import java.util.List;

/**
 * Created by Administrador on 29/11/2016.
 */
public interface SesionAprendizajeDao extends GenericDao<SesionAprendizaje> {
    List<SesionAprendizaje> listarSesionesUnidad(int idUidad);
    List<SesionAprendizaje> listarSesionesConDatos(int idUidad);
    SesionAprendizaje buscarSesionById(int idSesion);
    IndicadoresSesionAprendizaje registrarIndicadorSesion(IndicadoresSesionAprendizaje indicador);
    List<IndicadorAprendizaje> listarIndicadoresSesion(int idSession);
    List<IndicadorAprendizaje> listarIndicadoresSeleccionados(int sesId,int capId);
    List<SeccionSequenciaDidactica> listarSeccionesSequenciaDidactica();
    List<SesionSequenciaDidactica> listarSecuenciasDidacticas(int idSesion);
    SeccionSequenciaDidacticaSesion getSeccioneSequencia(int idSecuencia,int idSeccion);
    SeccionSequenciaDidactica getSeccionSequenciaDidactica(int id);
    SesionSequenciaDidactica registrarSequenciaDidactica(SesionSequenciaDidactica seq);
    SeccionSequenciaDidacticaSesion registrarSeccionDeSequencia(SeccionSequenciaDidacticaSesion sesSeq);
    SesionSequenciaDidactica buscarSequenciaDidactica(int idSec);
    void registrarActividadSequencia(ActividadSeccionSequenciaDidactica actividad);
    void eliminarIndicadoresSesionTodos(int idSes);
    void eliminarSecuenciaDidactica(SesionSequenciaDidactica secuencia);
    List<IndicadoresSesionAprendizaje> buscarIndicadoresUnidadAprendizaje(int idUnidad,int idComp,int idCap,int idInd);

    List<TareaSesionAprendizaje> listarTareasSesion(int idSesion);
    TareaSesionAprendizaje buscarTareaSesion(int idTarea);
    void registrarTareaSesion(TareaSesionAprendizaje tarea);
    void eliminarTareaSesion(int idTarea);
    void editarTareaSesion(TareaSesionAprendizaje tarea);
    SesionAprendizaje buscarSesionSiguiente(int idSesion,int idUnidad);
}
