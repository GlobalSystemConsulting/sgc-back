/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ListarDatosOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarDatosOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<DatosOrganigrama> datosOrganigrama = null;
        DatosOrganigramaDao datosOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");
        
        try{
            datosOrganigrama = datosOrganigramaDao.listarXDatosOrganigrama();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar Datos Organigrama",e);
            System.out.println("No se pudo listar Datos organigrama.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas  Datos organigrama.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(DatosOrganigrama p: datosOrganigrama ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("datId", p.getDatOrgiId());
            oResponse.put("codDat", p.getCodDatOrgi());
            oResponse.put("nomDat", p.getNomDatOrgi());
            oResponse.put("desDat", p.getDesDatOrgi());
            oResponse.put("anioDat", p.getAnioDatOrgi());
            oResponse.put("fecMod",p.getFecMod());
            oResponse.put("UsuMod",p.getUsuMod());
            oResponse.put("estReg",p.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los Datos Organigrama fueron listadas exitosamente", miArray);
    }
    
}
