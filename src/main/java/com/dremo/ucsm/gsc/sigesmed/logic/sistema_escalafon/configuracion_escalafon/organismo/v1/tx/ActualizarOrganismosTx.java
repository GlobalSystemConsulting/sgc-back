/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarOrganismosTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarOrganismosTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer orgaId = requestData.getInt("orgaId");
            String codOrga = requestData.optString("codOrga");
            String nomOrga = requestData.optString("nomOrga");

            return actualizarCargos( orgaId, codOrga, nomOrga);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCargos(Integer orgaId,String codOrga, String nomOrga) {
        try{
            OrganismoDao organismoDao = (OrganismoDao)FactoryDao.buildDao("se.OrganismoDao");        
            Organismo organismo = organismoDao.buscarPorId(orgaId);

            organismo.setCodOrga(codOrga);
            organismo.setNomOrga(nomOrga);
            
            organismoDao.update(organismo);
            JSONObject oResponse = new JSONObject();
            oResponse.put("orgaId", organismo.getOrgaId());
            oResponse.put("codOrga", organismo.getCodOrga());
            oResponse.put("nomOrga", organismo.getNomOrga());
            return WebResponse.crearWebResponseExito(" El Organismo actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, el Organismo no fue actualizado");
        }
    }
}