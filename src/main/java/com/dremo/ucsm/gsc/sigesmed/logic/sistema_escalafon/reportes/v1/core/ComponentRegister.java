/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.*;
/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("reportes");
        seComponent.setVersion(1);
        
        
        seComponent.addTransactionGET("getChildrenGroupByWorker", GetChildrenGroupByWorkerTx.class);
        seComponent.addTransactionGET("getChildrenGroupByWorker", GetChildrenGroupByWorkerTx.class);
        seComponent.addTransactionGET("contarMeritosPorOrganizacion", ContarMeritosPorOrganizacionTx.class);
        seComponent.addTransactionGET("contarDemeritosPorOrganizacion", ContarDemeritosPorOrganizacionTx.class);
        seComponent.addTransactionGET("listarConsultaGeneral", ListarConsultaGeneralTx.class);////////
        seComponent.addTransactionGET("listarTodoEsquema", ListarTodoEsquemaTx.class);////////
        //seComponent.addTransactionGET("listarConsultasFrecuentes", ListarConsultasFrecuentesTx.class);////////
        seComponent.addTransactionGET("listarCatalogoConsultaGeneral", ListarCatalogoConsultaGeneralTx.class);/////////
        seComponent.addTransactionGET("listarCatalogoConsultaGeneral2", ListarCatalogoConsultaGeneral2Tx.class);/////////
        seComponent.addTransactionGET("listarSolicitudesCertificacion", ListarSolicitudesCertificacionTx.class);/////////
        seComponent.addTransactionGET("listarSolicitudesCertificacionxUsuario", ListarSolicitudesCertificacionxUsuarioTx.class);//////
        seComponent.addTransactionGET("listarConsultasFrecuentesxUsuario", ListarConsultasFrecuentesxUsuarioTx.class);//////
        seComponent.addTransactionPOST("reporteFichaEscalafonaria", ReporteFichaEscalafonariaTx.class);
        seComponent.addTransactionPOST("reportePersonalizado", ReportePersonalizadoTx.class);
        //seComponent.addTransactionPOST("modelos12ReporteFichaEscalafonaria", Modelos12ReporteFichaEscalafonariaTx.class); //////modelo reporte
        seComponent.addTransactionPOST("reporteEstadistico", ReporteEstadisticoTx.class);
        seComponent.addTransactionPOST("reporteConsultaGeneral", ReporteConsultaGeneralTx.class);//////
        seComponent.addTransactionPOST("agregarConsultaFrecuente", AgregarConsultaFrecuenteTx.class);//////
        seComponent.addTransactionPOST("agregarConsultaCertificada", AgregarConsultaCertificadaTx.class);//////
        seComponent.addTransactionPOST("agregarAtributoMetadata", AgregarAtributoMetadataTx.class);//////
        seComponent.addTransactionPUT("actualizarAtributoMetadata", ActualizarAtributoMetadataTx.class);//////
        seComponent.addTransactionPUT("actualizarFechaCertificacionConsulta", ActualizarFechaCertificacionConsultaTx.class);//////
        seComponent.addTransactionPUT("actualizarEstadoConsulta", ActualizarEstadoConsultaTx.class);//////
        seComponent.addTransactionPUT("actualizarEstadoConsultaCertificarNuevamente", ActualizarEstadoConsultaCertificarNuevamenteTx.class);//////
        seComponent.addTransactionDELETE("eliminarAtributo", EliminarAtributoTx.class);////////
        seComponent.addTransactionPOST("selecReporteFicha", SeleccionReporteFichaEscalafonariaTx.class);/////////
        
        return seComponent;
    }
    
}
