/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoDocDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoEstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoEstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author alex
 */
public class ListarTiposEstComTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ListarTiposEstComTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        List<TipoEstudioComplementario> documentos = null;
        TipoEstudioComplementarioDao tipoEstComDao = (TipoEstudioComplementarioDao)FactoryDao.buildDao("se.TipoEstudioComplementarioDao");
        
        try{
            documentos = tipoEstComDao.listarAll();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar los tipos de estudio complementario",e);
            System.out.println("No se pudo listar los tipos de estudio complementario. Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listar los tipos de estudio complementario. Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoEstudioComplementario d: documentos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id", d.getId());
            oResponse.put("nom", d.getNom());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los tipos de estudio complementario fueron listados exitosamente", miArray);
    }
}
