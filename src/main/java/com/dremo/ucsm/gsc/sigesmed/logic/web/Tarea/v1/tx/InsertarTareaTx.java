/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        TareaEscolar nuevaTarea = null;
        FileJsonObject miF = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.optString("descripcion");
            int numeroDoc = requestData.getInt("numeroDoc");
            
            int planID = requestData.getInt("planID");
            int gradoID = requestData.getInt("gradoID");
            char seccionID = requestData.getString("seccionID").charAt(0);
            int areaID = requestData.getInt("areaID");            
            
            int docenteID = requestData.getInt("docenteID");            
            
            String adjunto = requestData.optString("adjunto");
            JSONObject jsonArchivo = requestData.optJSONObject("archivo");
            if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                //nombre del documento adjunto tarea -> planID_gradoID_areaID_tar_tareaID
                miF = new FileJsonObject( jsonArchivo ,planID+"_"+gradoID+"_"+areaID+"_tar_");
                adjunto = miF.getName();
            }
            
            nuevaTarea = new TareaEscolar(0,nombre,descripcion,null,null,numeroDoc,0,0,adjunto,Tarea.ESTADO_NUEVO,planID,gradoID,seccionID,areaID,new Date(),docenteID,'A');
            
                      
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tareaDao.insert(nuevaTarea);            
            
            //si ya se registro la tarea
            //verificamos si hay algun archivo adjunto
            if(miF!=null){
                nuevaTarea.setDocAdj( miF.concatName(""+nuevaTarea.getTarEscId()) );
                BuildFile.buildFromBase64(Tarea.TAREA_PATH, nuevaTarea.getDocAdj(), miF.getData());
                tareaDao.cambiarNombreAdjunto(nuevaTarea.getTarEscId(), nuevaTarea.getDocAdj() );
            }
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tareaID",nuevaTarea.getTarEscId());
        oResponse.put("adjunto",nuevaTarea.getDocAdj());
        oResponse.put("estado",""+nuevaTarea.getEstado());
        return WebResponse.crearWebResponseExito("El registro de la tarea se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
