
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.tipo_organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarTipoOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
            
        boolean listarConRoles = false;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            if( requestData!= null && requestData.length()> 0 ){
                listarConRoles = requestData.getBoolean("listar");
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Roles", e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TipoOrganizacion> tipoOrganizaciones = null;
        TipoOrganizacionDao tipoOrgDao = (TipoOrganizacionDao)FactoryDao.buildDao("TipoOrganizacionDao");
        try{
            if(listarConRoles)
                tipoOrganizaciones = tipoOrgDao.listarConRoles();
            else
                tipoOrganizaciones = tipoOrgDao.buscarTodosOrdenados(TipoOrganizacion.class,"ORDER BY o.tipOrgId");
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Tipo de Organizaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipo de Organizaciones", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoOrganizacion tipoOrganizacion:tipoOrganizaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoOrganizacionID",tipoOrganizacion.getTipOrgId() );
            oResponse.put("codigo",tipoOrganizacion.getCod());
            oResponse.put("nombre",tipoOrganizacion.getNom());
            oResponse.put("descripcion",tipoOrganizacion.getDes());
            oResponse.put("fecha",tipoOrganizacion.getFecMod().toString());
            oResponse.put("estado",""+tipoOrganizacion.getEstReg());
            
            List<Rol> roles = tipoOrganizacion.getRoles();            
            if( listarConRoles && roles.size() > 0 ){
                JSONArray aRoles = new JSONArray();
                for( Rol r:roles ){
                    JSONObject oRol = new JSONObject();
                    oRol.put("rolID",r.getRolId() );
                    oRol.put("nombre",r.getNom() );
                    oRol.put("descripcion",r.getDes() );
                    oRol.put("estado",""+r.getEstReg() );
                    aRoles.put(oRol);
                }
                oResponse.put("roles",aRoles);
            } 
            
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}
