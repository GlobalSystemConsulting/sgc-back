package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity(name = "EstudianteMaestro")
@Table(name = "estudiante", schema = "pedagogico")
public class Estudiante implements java.io.Serializable {
    @Id
    @GeneratedValue(generator = "maestroEstudianteKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "maestroEstudianteKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property",value = "persona"
            )
    )
    private int per_id;
    @OneToOne(fetch = FetchType.EAGER,optional = false)
    @PrimaryKeyJoinColumn
    private Persona persona;

    @Column(name="cod_est",length = 10,nullable = false,unique = true)
    private String codEst;
    @Column(name = "num_her",length = 2)
    private String numHer;
    @Column(name = "lug",length = 2)
    private String lug;
    @Column(name = "tip_par",length = 2)
    private String tipPar;
    @Column(name = "rel",length = 2)
    private String rel;
    @Column(name = "tip_dis",length = 2)
    private String tipDis;
    @Column(name = "obs",length = 256)
    private String obs;

    @OneToMany(mappedBy = "estudiante",fetch = FetchType.LAZY)
    private List<Matricula> matriculas = new ArrayList<>();

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public Estudiante() {
    }

    public Estudiante(String codEst, String numHer, String lug, String tipPar, String rel, String tipDis, String obs) {
        this.codEst = codEst;
        this.numHer = numHer;
        this.lug = lug;
        this.tipPar = tipPar;
        this.rel = rel;
        this.tipDis = tipDis;
        this.obs = obs;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getCodEst() {
        return codEst;
    }

    public void setCodEst(String codEst) {
        this.codEst = codEst;
    }

    public String getNumHer() {
        return numHer;
    }

    public void setNumHer(String numHer) {
        this.numHer = numHer;
    }

    public String getLug() {
        return lug;
    }

    public void setLug(String lug) {
        this.lug = lug;
    }

    public String getTipPar() {
        return tipPar;
    }

    public void setTipPar(String tipPar) {
        this.tipPar = tipPar;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getTipDis() {
        return tipDis;
    }

    public void setTipDis(String tipDis) {
        this.tipDis = tipDis;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public List<Matricula> getMatriculas() {
        return matriculas;
    }

    public void setMatriculas(List<Matricula> matriculas) {
        this.matriculas = matriculas;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
