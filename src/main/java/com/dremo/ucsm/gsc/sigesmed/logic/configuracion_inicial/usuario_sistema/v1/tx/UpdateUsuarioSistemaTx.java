
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class UpdateUsuarioSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        Usuario usuarioAct = null;
        UsuarioSession session = null;
        Trabajador trabajador = null;
        Trabajador trabajadorExistente = null;
        FichaEscalafonaria ficha = null;
        int opcionAct = 0;
        int orgId = 0;
        int rolId = 0;
        int tipoUsuario = -1;
        int perId = 0;
        
        List<UsuarioSession> nuevasSessiones = new ArrayList<UsuarioSession>(); 
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        UsuarioSessionDao usuarioSessionDao = (UsuarioSessionDao)FactoryDao.buildDao("UsuarioSessionDao");
        TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("TrabajadorDao");
        FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("FichaEscalafonariaDao");
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            System.out.println("Entrada: " + requestData);
            //Opcion
            opcionAct = requestData.getInt("opcion");            
            tipoUsuario = requestData.getInt("tipoUsuario");
            //Datos de usuario
            int usuarioID = requestData.getJSONObject("usuario").getInt("usuarioID");
            perId = requestData.getJSONObject("usuario").getInt("perId");
            String nombre = requestData.getJSONObject("usuario").getString("nombreUsuario");
            String password = requestData.getJSONObject("usuario").getString("password");
            String estado = requestData.getJSONObject("usuario").getString("estado");
            JSONArray arraySession = requestData.getJSONObject("usuario").getJSONArray("sessiones");
            
            usuarioAct = new Usuario(usuarioID, nombre, password, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));   
            
            if (opcionAct==0){
                usuarioAct.setSessiones(new ArrayList<UsuarioSession>());

                for(int i=0;i<arraySession.length();i++ ){
                    JSONObject o = arraySession.getJSONObject(i);

                    int sessionID = o.optInt("sessionID");
                    int rolID = o.getInt("rolID");
                    int organizacionID = o.getInt("organizacionID");
                    estado = o.getString("estado");
                    int areaID = o.optInt("areaID");

                    session = new UsuarioSession(sessionID, new Organizacion(organizacionID), new Rol(rolID), usuarioAct, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));

                    //si tiene una area
                    if(areaID>0)
                        session.setArea(new Area(areaID));

                    if(sessionID>0){                    
                        usuarioAct.getSessiones().add(session);
                    }else{
                         nuevasSessiones.add(session);
                    }
                    try{
                       trabajadorExistente = trabajadorDao.buscarTrabajadorxPerIdyRolId(perId,rolID);

                    }catch(Exception e){
                        System.out.println("No hay trabajador existente "+e);
                        
                    }
                    
                    
                    if(trabajadorExistente != null){
                        trabajadorExistente.setRol(new Rol(rolID));
                        trabajadorDao.update(trabajadorExistente);
                    }
                }

                usuarioDao.eliminarSessiones(usuarioAct.getUsuId(), wr.getIdUsuario());
                usuarioDao.update(usuarioAct);

                for(int i=0;i<nuevasSessiones.size();i++){
                    usuarioDao.insertarSession(nuevasSessiones.get(i));
                }
            }
            
            if(opcionAct == 1){
                orgId = requestData.getInt("orgId");
                rolId = requestData.getInt("rolId");
                
                session = new UsuarioSession(0, new Organizacion(orgId), new Rol(rolId), usuarioAct, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
                //usuarioSessionDao.insert(session);
                usuarioAct.setSessiones(new ArrayList<UsuarioSession>());
                usuarioAct.getSessiones().add(session);
                usuarioDao.update(usuarioAct);
                
                if(tipoUsuario == 1){
                    trabajador = new Trabajador();
                    trabajador.setPersona(new Persona(perId));
                    trabajador.setOrganizacion(new Organizacion(orgId));
                    trabajador.setRol(new Rol(rolId));
                    trabajador.setEstReg("A");
                    trabajador.setEstLab('1');
                    trabajadorDao.insert(trabajador);

                    ficha = new FichaEscalafonaria();
                    ficha.setTrabajador(trabajador);
                    ficha.setEstReg('A');
                    fichaDao.insert(ficha);
                }
            }
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarCambioAuditoria("Se modifico trabajador", "Nombre Usuario: "+nombre, wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////   
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }
        
        JSONArray miArray = new JSONArray();
        JSONObject usuarioJSON = new JSONObject();
        usuarioJSON.put("usuarioID",usuarioAct.getUsuId());
        usuarioJSON.put("nombreUsuario",usuarioAct.getNom());            
        usuarioJSON.put("password",usuarioAct.getPas());
        usuarioJSON.put("estado",""+usuarioAct.getEstReg());
        usuarioJSON.put("perId", perId);
        usuarioJSON.put("DNI", "");
        usuarioJSON.put("nombres", "");
        usuarioJSON.put("nombre", "");
        usuarioJSON.put("paterno", "");
        usuarioJSON.put("materno", "");
        usuarioJSON.put("email", "");
        usuarioJSON.put("numero1", "");
        usuarioJSON.put("numero2", "");
            
        //datos de sessiones
        JSONArray aSessiones = new JSONArray();
        for( UsuarioSession ses: usuarioAct.getSessiones() ){
            JSONObject oSession = new JSONObject();
            oSession.put("rolID",ses.getRol().getRolId() );
            oSession.put("rol",ses.getRol().getNom() );
            oSession.put("sessionID",ses.getUsuSesId() );
            oSession.put("areaID",ses.getAreId());
            oSession.put("organizacionID",ses.getOrganizacion().getOrgId() );
            oSession.put("organizacion",ses.getOrganizacion().getNom() );                
            oSession.put("estado",""+ses.getEstReg());

            aSessiones.put(oSession);
        }
        
        usuarioJSON.put("sessiones", aSessiones);
        
                    
        JSONObject respuesta = new JSONObject();
        respuesta.put("usuarioAct",usuarioJSON);
        
        if(opcionAct==1 && tipoUsuario==1)
            respuesta.put("ficEscId", ficha.getFicEscId());
        else
            respuesta.put("ficEscId", -1);
        
        return WebResponse.crearWebResponseExito("El Usuario del Sistema se actualizo correctamente", respuesta);
    }
    
}
