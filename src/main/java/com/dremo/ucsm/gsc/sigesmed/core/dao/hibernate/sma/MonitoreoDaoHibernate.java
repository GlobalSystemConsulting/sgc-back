/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Monitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class MonitoreoDaoHibernate extends GenericDaoHibernate<Monitoreo> implements MonitoreoDao{

    @Override
    public List<Monitoreo> listarMonitoreosxUgel(int ugelId) {
        List<Monitoreo> monitoreos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT mon from Monitoreo as mon "
                    + "join fetch mon.organizacion as org "
                    + "join fetch org.organizacionPadre as orgPad "
                    + "WHERE orgPad.orgId=" + ugelId;
            Query query = session.createQuery(hql);
            monitoreos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los monitoreos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los monitoreos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return monitoreos;
    }
    
    @Override
    public List<MonitoreoDetalle> listarMonitoreoDetalle (int monId){
        List<MonitoreoDetalle> monitoreosDetalle = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        try{
            String hql = "SELECT monDet from MonitoreoDetalle as monDet "
                    + "join fetch monDet.mniId as mon "
                    + "WHERE mon.mniId=" + monId;
            Query query = session.createQuery(hql);
            monitoreosDetalle = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo el detalle de los monitoreos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar el detalle de los monitoreos \\n "+ e.getMessage());            
        }
        
        finally{
            session.close();
        }
        return monitoreosDetalle;
    }

    @Override
    public Monitoreo buscarPorId(Integer monId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Monitoreo a = (Monitoreo)session.get(Monitoreo.class, monId);
        session.close();
        return a;
    }

}
