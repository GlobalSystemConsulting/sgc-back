package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class PreguntaEvaluacionCapacitacionDaoHibernate extends GenericDaoHibernate<PreguntaEvaluacionCapacitacion> implements PreguntaEvaluacionCapacitacionDao {

    private static final Logger logger = Logger.getLogger(PreguntaEvaluacionCapacitacionDaoHibernate.class.getName());

    @Override
    public PreguntaEvaluacionCapacitacion buscarPorId(int codPre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (PreguntaEvaluacionCapacitacion) session.get(PreguntaEvaluacionCapacitacion.class, codPre);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<PreguntaEvaluacionCapacitacion> buscarPorEvaluacion(int codEva) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(PreguntaEvaluacionCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .add(Restrictions.eq("evaCurCapId", codEva))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorEvaluacion", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public boolean existenPreguntas(int codEva) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try {
            Criteria query = session.createCriteria(PreguntaEvaluacionCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .add(Restrictions.eq("evaCurCapId", codEva))
                    .setProjection(Projections.rowCount());
            
            return (((Long) query.uniqueResult()).intValue() > 0);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "existenPreguntas", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
