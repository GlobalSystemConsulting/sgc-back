package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="naturaleza_plaza" ,schema="institucional" )
public class NaturalezaPlaza  implements java.io.Serializable {

    @Id
    @Column(name="nat_pla_id", unique=true, nullable=false)
    private char natPlaId;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des",length=256)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    

    public NaturalezaPlaza() {
    }
    public NaturalezaPlaza(char natPlaId) {
        this.natPlaId = natPlaId;
    }
    public NaturalezaPlaza(char natPlaId, String nom, String des, Date fecMod, int usuMod, char estReg) {
       this.natPlaId = natPlaId;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public char getNatPlaId() {
        return this.natPlaId;
    }    
    public void setNatPlaId(char natPlaId) {
        this.natPlaId = natPlaId;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
}


