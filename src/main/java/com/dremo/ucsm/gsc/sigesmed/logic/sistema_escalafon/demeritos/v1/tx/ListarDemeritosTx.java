/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarDemeritosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarDemeritosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<Demerito> demeritos = null;
        DemeritoDao demeritoDao = (DemeritoDao)FactoryDao.buildDao("se.DemeritoDao");
        
        try{
            demeritos = demeritoDao.listarxFichaEscalafonaria(perId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar demeritos",e);
            System.out.println("No se pudo listar los demeritos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los demeritos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Demerito d:demeritos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("demId", d.getDemId());
            oResponse.put("entEmi", d.getEntEmi()==null?"":d.getEntEmi());
            oResponse.put("numDoc", d.getNumDoc()==null?"":d.getNumDoc());
            oResponse.put("fecDoc", d.getFecDoc()==null?"":sdi.format(d.getFecDoc()));
            oResponse.put("tipDocId", d.getTipDocId()==null?0:d.getTipDocId());
            oResponse.put("sep", d.getSep()==null?false:d.getSep());
            oResponse.put("fecIni", d.getFecIni()==null?"":sdi.format(d.getFecIni()));
            oResponse.put("fecFin", d.getFecFin()==null?"":sdi.format(d.getFecFin()));
            oResponse.put("mot", d.getMot()==null?"":d.getMot());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los demeritos fueron listados exitosamente", miArray);
    }
    
}
