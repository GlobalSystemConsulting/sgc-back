/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.NivelAcademicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.NivelAcademico;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author alex
 */
public class NivelAcademicoDaoHibernate extends GenericDaoHibernate<NivelAcademico> implements NivelAcademicoDao{
    @Override
    public List<NivelAcademico> listarAll() {
        List<NivelAcademico> nivAca = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT na from NivelAcademico as na "
                    + "WHERE na.estReg='A'";
            Query query = session.createQuery(hql);
            nivAca = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los niveles academicos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los niveles academicos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return nivAca;
    }
    
    @Override
    public List<NivelAcademico> listarxTipoFormacion(Integer nivAcaPad){
        List<NivelAcademico> nivAca = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT na from NivelAcademico as na "
                    + " WHERE na.nivAcaPad=" + nivAcaPad
                    + " AND na.estReg='A'";
            Query query = session.createQuery(hql);
            nivAca = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los niveles academicos por TipoFormacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los niveles academicos por TipoFormacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return nivAca;
    }

    @Override
    public NivelAcademico buscarPorId(Integer nivAcaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        NivelAcademico des = (NivelAcademico)session.get(NivelAcademico.class, nivAcaId);
        session.close();
        return des;
    }
}
