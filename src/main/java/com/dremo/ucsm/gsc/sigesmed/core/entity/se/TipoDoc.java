/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "tipo_doc", schema="administrativo")
public class TipoDoc implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tip_doc_id")
    private int tipDocId;
    
    @Size(max = 2147483647)
    @Column(name = "tip_doc_nom")
    private String tipDocNom;
    
    @Column(name = "est_reg")
    private char estReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "des")
    private String des;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "tipoDoc")
    List<FichaEscalafonaria> fichaEscalafonaria;
   

    public TipoDoc() {
    }

    public TipoDoc(int tipDocId) {
        this.tipDocId = tipDocId;
    }

    public int getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(int tipDocId) {
        this.tipDocId = tipDocId;
    }

    public String getTipDocNom() {
        return tipDocNom;
    }

    public void setTipDocNom(String tipDocNom) {
        this.tipDocNom = tipDocNom;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    //INICIO -usado para el catalogo
    public int getId(){
        return this.tipDocId;
    }
    public void setId(int Id){
        this.tipDocId=Id;
    }
    
    public String getNom() {
        return this.tipDocNom;
    }
    public void setNom(String nom) {
        this.tipDocNom = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo

    public List<FichaEscalafonaria> getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(List<FichaEscalafonaria> fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public String toString() {
        return "TipoDoc{" + "tipDocId=" + tipDocId + ", tipDocNom=" + tipDocNom + ", estReg=" + estReg + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }

    
}
