/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RequisitoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RutaTramite;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import org.json.JSONObject;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class ActualizarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoTramite tipoTramite = null;
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int id = requestData.getInt("tipoTramiteID");
            String nombre = requestData.getString("nombre");
            String codigo = requestData.getString("codigo");
            String descripcion = requestData.getString("descripcion");
            int duracion = requestData.getInt("duracion");
            double costo = requestData.getDouble("costo");
            boolean tipo = requestData.getBoolean("tipo");
            boolean tupa = requestData.getBoolean("tupa");
            int tipoOrganizacion = requestData.getInt("tipoOrganizacionID");
            
            tipoTramite = new TipoTramite(id, codigo, nombre, descripcion, duracion, new BigDecimal(costo), tipo, tupa, new Date(), wr.getIdUsuario(), 'A', tipoOrganizacion);
            
            JSONArray listaRequisitos = requestData.optJSONArray("requisitos");
            JSONArray listaRutas = requestData.optJSONArray("rutas");
            
            //leendo los requisitos            
            if( listaRequisitos !=null && listaRequisitos.length()> 0){
                tipoTramite.setRequisitoTramites( new ArrayList<RequisitoTramite>());
                for( int i = 0 ; i < listaRequisitos.length(); i++){
                    JSONObject bo = listaRequisitos.getJSONObject(i);

                    String nombreArchivo = bo.getString("nombreArchivo");
                    //Integer requisitoId = bo.getInt("requisitoID");
                    String rutaDescripcion = bo.getString("descripcion");
                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject archivo = bo.optJSONObject("archivo");
                    if( archivo!=null && archivo.length() > 0 ){
                        FileJsonObject miF = new FileJsonObject( archivo );
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    tipoTramite.getRequisitoTramites().add( new RequisitoTramite(i+1, tipoTramite,rutaDescripcion,nombreArchivo,new Date(),wr.getIdUsuario(),'A') );
                }
            }
            //leendo las rutas
            if(listaRutas!=null && listaRutas.length() > 0){
                tipoTramite.setRutaTramites( new ArrayList<RutaTramite>() );
                for(int i = 0 ; i < listaRutas.length(); i++){
                    JSONObject bo = listaRutas.getJSONObject(i);

                    int areaOriID = bo.getInt("areaOriID");
                    int areaDesID = bo.getInt("areaDesID");
                    String areaDescripcion = bo.getString("descripcion");

                    tipoTramite.getRutaTramites().add( new RutaTramite(i+1,tipoTramite,areaDescripcion, areaOriID,areaDesID, new Date(), wr.getIdUsuario(),'A'  ) );
                }
            }
            
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
        try{
            tipoTramiteDao.eliminarRequisitos(tipoTramite);
            tipoTramiteDao.eliminarRutas(tipoTramite);
            tipoTramiteDao.update(tipoTramite);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el tipo de tramite ", e.getMessage() );
        }
        
        //si ya se registro el tipo de tramite 
        //ahora creamos los archivos que se desean subir
        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64("tramite", archivo.getName(), archivo.getData());
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo TipoTramite se actualizo correctamente");
        //Fin
    }
    
}
