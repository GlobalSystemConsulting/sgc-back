/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.me;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.me.Estudiante;
import java.util.List;

/**
 *
 * @author abel
 */
public interface EstudianteDao extends GenericDao<Estudiante>{
    
    
    public List<Object[]> buscarEstudiantesIdsPorGradoYSeccion(int organizacionID,int gradoID,char seccionID);
}
