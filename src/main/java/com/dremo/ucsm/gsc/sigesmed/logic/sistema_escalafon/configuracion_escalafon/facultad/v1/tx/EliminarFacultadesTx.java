/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarFacultadesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarFacultadesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer facId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           facId = requestData.getInt("facId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarFacultad",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        FacultadDao facultadDao = (FacultadDao)FactoryDao.buildDao("se.FacultadDao");
        try{
            facultadDao.delete(new Facultad(facId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la facultad\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la facultad", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La facultad se elimino correctamente");
    }
    
}
