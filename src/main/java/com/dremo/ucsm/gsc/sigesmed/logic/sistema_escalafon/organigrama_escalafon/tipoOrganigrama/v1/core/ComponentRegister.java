/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx.ActualizarTipoOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx.AgregarTipoOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx.EliminarTipoOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx.ListarTipoOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx.ListarTipoOrganismoxOrganigramaTx;
/**
 *
 * @author forev
 */
public class ComponentRegister implements IComponentRegister {
    
     @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("tiposOrganigramaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarTiposOrganigrama", ListarTipoOrganigramaTx.class);
        seComponent.addTransactionPUT("actualizarTiposOrganigrama", ActualizarTipoOrganigramaTx.class);
        seComponent.addTransactionPOST("agregarTiposOrganigrama", AgregarTipoOrganigramaTx.class);
        seComponent.addTransactionDELETE("eliminarTiposOrganigrama", EliminarTipoOrganigramaTx.class);
        
        seComponent.addTransactionGET("listarTiposOrganismosxOrgi", ListarTipoOrganismoxOrganigramaTx.class);
        return seComponent;
    }
    
}

