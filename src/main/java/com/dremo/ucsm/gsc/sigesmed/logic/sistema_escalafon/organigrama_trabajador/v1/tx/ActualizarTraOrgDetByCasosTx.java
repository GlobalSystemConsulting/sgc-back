/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author felipe
 */
public class ActualizarTraOrgDetByCasosTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarTraOrgDetByCasosTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        TrabajadorOrganigramaDetalle traOrgDet = new TrabajadorOrganigramaDetalle();
        Trabajador tra  = null;
        int traId = 0;
        int orgiOld = 0;
        int orgiNew = 0;
        int opcion = 0;
        int plaId = 0;
        int catId = 0;
        int carId = 0;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            opcion = requestData.getInt("opcion");
            traId = requestData.getJSONObject("ascenso").getInt("traId");
            orgiOld = requestData.getJSONObject("ascenso").getInt("orgiIdOld");
            orgiNew = requestData.getJSONObject("ascenso").getInt("orgiIdNew");
            plaId = requestData.getJSONObject("ascenso").getInt("plaId");
            catId = requestData.getJSONObject("ascenso").getInt("catId");
            carId = requestData.getJSONObject("ascenso").getInt("carId");
            if(orgiNew  > 0){
                TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao)FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");

                switch(opcion){
                    case 1: traOrgDetDao.eliminarRegistroBD(traId,orgiOld);
                            traOrgDet.setOrgiId(orgiNew);
                            traOrgDet.setTraId(traId);
                            traOrgDet.setPlaId(plaId);
                            traOrgDet.setCatId(catId);
                            traOrgDet.setCarId(carId);
                            traOrgDetDao.insert(traOrgDet);
                            break;
                    case 2: 
                            traOrgDet = traOrgDetDao.buscarPorId(traId, orgiOld);
                            traOrgDet.setPlaId(plaId);
                            traOrgDet.setCatId(catId);
                            traOrgDet.setCarId(carId);
                            traOrgDetDao.update(traOrgDet);
                            break;
                    case 3: 
                            traOrgDet = traOrgDetDao.buscarPorId(traId, orgiOld);
                            traOrgDet.setPlaId(plaId);
                            traOrgDet.setCatId(catId);
                            traOrgDet.setCarId(carId);
                            traOrgDetDao.update(traOrgDet);
                            break;
                }
                
            }           
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Actualizar relacion trabajador organigrama ",e);
            System.out.println(e);
        }
        
        try {
            if(orgiNew > 0){
                TrabajadorDao traDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
                tra = traDao.buscarPorId(traId);
                tra.setOrgiId(orgiNew);
                traDao.update(tra);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Actualizar orgiId en Trabajador ",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("orgiId", traOrgDet.getOrgiId());
        oResponse.put("plaId", traOrgDet.getPlaId());
        oResponse.put("catId", traOrgDet.getCatId());
        oResponse.put("carId", traOrgDet.getCarId());
        
        if(orgiNew>0){    
            return WebResponse.crearWebResponseExito("La actualizacion del ascenso se realizo correctamente",oResponse);
        }else{
            return WebResponse.crearWebResponseExito("El trabajador no tiene un organigrama asignado",oResponse);
        }
    }
    
}
