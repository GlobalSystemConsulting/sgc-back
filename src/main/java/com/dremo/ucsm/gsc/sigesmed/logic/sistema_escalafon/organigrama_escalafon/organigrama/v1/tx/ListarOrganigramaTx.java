/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ListarOrganigramaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Organigrama> organigrama = null;
        OrganigramaDao organigramaDao = (OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        
        try{
            organigrama = organigramaDao.listarXOrganigrama();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar  Organigrama",e);
            System.out.println("No se pudo listar  Organigrama.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas Organigrama.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Organigrama c: organigrama ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipId", c.getTipoOrganigrama().getTipOrgiId());
            oResponse.put("tipNom",c.getTipoOrganigrama().getNomTipOrgi());
            oResponse.put("datNom", c.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi()+" "+ c.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
            oResponse.put("datId", c.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
            if(c.getOrganigramaPadre()==null){
                oResponse.put("orgIdPad", "");
                oResponse.put("orgNomPad","");}
            else{
                oResponse.put("orgIdPad", c.getOrganigramaPadre().getOrgiId());
                oResponse.put("orgNomPad",c.getOrganigramaPadre().getNomOrgi());}
            if(c.getZona()==null){
                oResponse.put("ubiId", "");
            }
            else{
                oResponse.put("ubiId", c.getZona().getUbiId());
            }
            oResponse.put("codOrg", c.getCodOrgi());
            oResponse.put("abrOrg", c.getAbrOrgi()); 
            oResponse.put("nomOrg", c.getNomOrgi());
            oResponse.put("datOrg", c.getCamOrgi());        
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            oResponse.put("orgId",c.getOrgiId());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito(" Organigrama fueron listadas exitosamente", miArray);
    }
}
