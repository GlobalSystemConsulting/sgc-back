
package com.dremo.ucsm.gsc.sigesmed.core.dao;


import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import java.util.List;

/**
 *
 * @author abel
 */
public interface ConsultaCertificadaDao extends GenericDao<ConsultaCertificada>{
    
    public ConsultaCertificada buscarPorID(int con_cer_id);
    public List<ConsultaCertificada> listarTodasConsultas();
    public List<ConsultaCertificada> listarConsultasxUsuario(int usuId);
    public List<ConsultaCertificada> listarConsultasCertificadasxUsuario(int usuId);
    public boolean verificarPassword(int usuId, String pass);
    //public boolean almacenarxConsultasFrecuentes(String atributosUsados,String catalogo,String logica,String filtros,String titulo,String observaciones);
    //public List<ConsultaCertificada> listarxConsultasFrecuentes();
    /*public Persona buscarPorCod(int cod);
    public List<Persona> buscarPorNombre(String nombreUser);
    List<Persona> buscarPorApellido(String apellidoPaterno, String apellidoMaterno);
    Persona buscarPorOrganizacionConUsuario(String dni,int idOrg);
    List<Persona> buscarPorOrganizacionConUsuario(String apellidoPaterno, String apellidoMaterno,int idOrg);
*/}
