/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterJoinTable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Administrador
 */
@Entity
@FilterDef(name="filtroplantilla")
@Table(name = "plantilla_ficha_institucional", schema = "institucional")
public class PlantillaFichaInstitucional implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pfi_ins_id")
    private Integer pfiInsId;
    @Column(name = "pfi_cod")
    private String pfiCod;
    @Column(name = "pfi_nom")
    private String pfiNom;
    @Column(name = "pfi_fec")
    @Temporal(TemporalType.DATE)
    private Date pfiFec;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;    
    @Column(name = "est_reg")    
    private String estReg  = "A";
    @JoinColumn(name = "pti_id", referencedColumnName = "pti_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private PlantillaTipo tipo;
//    @OneToMany(mappedBy = "pfiId")
//    private List<FichaEvaluacionDocumentos> fichas;
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="pfi_id")    
//    @LazyCollection(LazyCollectionOption.FALSE)
    @Filter(name="filtroplantilla", condition=" est_reg = 'A'")
    private List<PlantillaGrupo> grupos;

    public PlantillaFichaInstitucional() {
    }

    public PlantillaFichaInstitucional(Integer pfiInsId) {
        this.pfiInsId = pfiInsId;
    }

    public PlantillaFichaInstitucional(String pfiCod, String pfiNom, Date pfiFec, PlantillaTipo tipo) {        
        this.pfiCod = pfiCod;
        this.pfiNom = pfiNom;
        this.pfiFec = pfiFec;
        this.tipo = tipo;
    }
    
    public Integer getPfiInsId() {
        return pfiInsId;
    }

    public void setPfiInsId(Integer pfiInsId) {
        this.pfiInsId = pfiInsId;
    }

    public String getPfiCod() {
        return pfiCod;
    }

    public void setPfiCod(String pfiCod) {
        this.pfiCod = pfiCod;
    }

    public Date getPfiFec() {
        return pfiFec;
    }

    public void setPfiFec(Date pfiFec) {
        this.pfiFec = pfiFec;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public PlantillaTipo getTipo() {
        return tipo;
    }

    public void setTipo(PlantillaTipo tipo) {
        this.tipo = tipo;
    }

    public String getPfiNom() {
        return pfiNom;
    }

    public void setPfiNom(String pfiNom) {
        this.pfiNom = pfiNom;
    }
    
//    @XmlTransient
//    public List<FichaEvaluacionDocumentos> getFichaEvaluacionDocumentosList() {
//        return fichas;
//    }
//
//    public void setFichaEvaluacionDocumentosList(List<FichaEvaluacionDocumentos> fichas) {
//        this.fichas = fichas;
//    }

    @XmlTransient
    public List<PlantillaGrupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<PlantillaGrupo> grupos) {
        this.grupos = grupos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pfiInsId != null ? pfiInsId.hashCode() : 0);
        return hash;
    }

//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof PlantillaFichaInstitucional)) {
//            return false;
//        }
//        PlantillaFichaInstitucional other = (PlantillaFichaInstitucional) object;
//        if ((this.pfiInsId == null && other.pfiInsId != null) || (this.pfiInsId != null && !this.pfiInsId.equals(other.pfiInsId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.PlantillaFichaInstitucional[ pfiInsId=" + pfiInsId + " ]";
//    }
    
}
