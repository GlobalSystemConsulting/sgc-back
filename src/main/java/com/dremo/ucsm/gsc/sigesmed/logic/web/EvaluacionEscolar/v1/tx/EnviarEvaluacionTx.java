/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public class EnviarEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        EvaluacionEscolar evaluacion = new EvaluacionEscolar();
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            organizacionID =  requestData.getInt("organizacionID" );
            
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            evaluacion.setEvaEscId( requestData.getInt("evaluacionID") );
            evaluacion.setGraId( requestData.getInt("gradoID") );
            evaluacion.setSecId( requestData.getString("seccionID").charAt(0) );
            evaluacion.setFecIni( sf.parse(requestData.getString("fechaInicio")) );
            evaluacion.setFecFin( sf.parse(requestData.getString("fechaFin")) );
            
            evaluacion.setEstado(Evaluacion.ESTADO_ENVIADO);
            evaluacion.setFecEnv(new Date());
            
            //verificando que la fecha de envio sea menor que la fecha de entrega de la evaluacion
            if( evaluacion.getFecEnv().compareTo( evaluacion.getFecIni() ) >=0 ){
                System.out.println("la fecha de inicio de la evalaucion es menor que la fecha de envio");
                return WebResponse.crearWebResponseError("error con la fecha de entrega", "la fecha de inicio de la evalaucion es menor que la fecha de envio" );
            }
            if( evaluacion.getFecIni().compareTo( evaluacion.getFecFin() ) >=0 ){
                System.out.println("la fecha de Fin de la evalaucion es menor que la fecha de Inicio");
                return WebResponse.crearWebResponseError("error con la fecha de entrega", "la fecha de inicio de la evalaucion es menor que la fecha de envio" );
            }
        
        }catch(Exception e){
            System.out.println("No se pudo enviar las evaluacion, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar las evaluacion, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        EvaluacionEscolarDao evaluacionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        EstudianteDao estDao = (EstudianteDao)FactoryDao.buildDao("me.EstudianteDao");
        try{
            
            List<Object[]> alumnos = estDao.buscarEstudiantesIdsPorGradoYSeccion(organizacionID, evaluacion.getGraId(), evaluacion.getSecId());
            
            //si hay alumnos se envia la tarea
            if(alumnos==null || alumnos.size()==0){
                System.out.println("No hay alumnos para enviar la evaluacion");
                return WebResponse.crearWebResponseError("No hay alumnos para enviar la evaluacion" );
            }
            List<BandejaEvaluacion> bandejas = new ArrayList<BandejaEvaluacion>();
            List<Integer> ids = new ArrayList<Integer>();
            for(Object[] alumno : alumnos){                
                bandejas.add( new BandejaEvaluacion(0,null,null,0,Evaluacion.ESTADO_NUEVO,evaluacion.getEvaEscId(),((BigInteger)alumno[0]).intValue() ));
                ids.add((Integer)alumno[1]);
            }
            
            evaluacionDao.enviarEvaluacion(evaluacion,bandejas);
            AlertManager.sendAll(Alertas.WEB_NUEVA_EVALUACION, ids);
        }catch(Exception e){
            System.out.println("No se pudo enviar la evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar la evaluacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+evaluacion.getEstado());
        return WebResponse.crearWebResponseExito("Se envio la evaluacion correctamente",oResponse);
        //Fin
    }    
    
    
}
