/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarMensajesEnviadosPorUsuarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
              
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<MensajeElectronico> mensajes = null;
        MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
        try{
            mensajes = mensajeDao.listarMensajesEnviadosPorUsuario(wr.getIdUsuario());
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes enviados por el usuario"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los mensajes enviados por el usuario", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int i = 0;
        for(MensajeElectronico mensaje:mensajes ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("mensajeID",mensaje.getMenEleId() );
            oResponse.put("asunto",mensaje.getAsu());
            oResponse.put("descripcion",mensaje.getDes());
            oResponse.put("grupos",mensaje.getLisGru());
            oResponse.put("usuarios",mensaje.getLisUsu());
            oResponse.put("remitente",mensaje.getUsuSesId());
            oResponse.put("fechaEnvio",sf.format(mensaje.getFecEnv()) );
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los mensajes enviados por el usuario",miArray);        
        //Fin
    }
    
}


