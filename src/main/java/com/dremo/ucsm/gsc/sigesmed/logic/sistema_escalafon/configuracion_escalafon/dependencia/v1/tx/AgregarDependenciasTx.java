/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarDependenciasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarDependenciasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Dependencia dependencia = null;
        
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String codDep = requestData.optString("codDep");
            String nomDep = requestData.optString("nomDep");
            Integer facId = requestData.getInt("facId");
            dependencia = new Dependencia();
            FacultadDao facultadDao = (FacultadDao)FactoryDao.buildDao("se.FacultadDao");
            Facultad facultad = facultadDao.buscarPorId(facId);
            
            dependencia.setCodDep(codDep);
            dependencia.setNomDep(nomDep);
            dependencia.setEstReg('A');
            dependencia.setFecMod(new Date());
            dependencia.setUsuMod(wr.getIdUsuario());
            dependencia.setFacultad(facultad);
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo Dependencia",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        DependenciaDao dependenciaDao = (DependenciaDao) FactoryDao.buildDao("se.DependenciaDao");
        try {
            dependenciaDao.insert(dependencia);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva dependencia",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("depId", dependencia.getDepId());
            oResponse.put("codDep", dependencia.getCodDep());
            oResponse.put("nomDep", dependencia.getNomDep());
                
        return WebResponse.crearWebResponseExito("El registro de la dependencia se realizo correctamente", oResponse);

    }
    
}