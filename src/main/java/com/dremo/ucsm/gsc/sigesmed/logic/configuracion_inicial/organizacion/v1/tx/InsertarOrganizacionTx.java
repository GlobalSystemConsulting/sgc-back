/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class InsertarOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Organizacion nuevoOrganizacion = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoOrganizacionID = requestData.getInt("tipoOrganizacionID");
            int organizacionPadreID = requestData.getInt("organizacionPadreID");            
            String codigo = requestData.getString("codigo");            
            String nombre = requestData.getString("nombre");
            String alias = requestData.getString("alias");
            String descripcion = requestData.getString("descripcion");
            String direccion = requestData.getString("direccion");
            String estado = requestData.getString("estado");
            
            String ubigeo = requestData.getString("ubigeo");
            String organizacionGestion = requestData.getString("organizacionGestion");
            String organizacionCaracteristica = requestData.getString("organizacionCaracteristica");
            String organizacionPrograma = requestData.getString("organizacionPrograma");
            String organizacionForma = requestData.getString("organizacionForma");
            String organizacionVariante = requestData.getString("organizacionVariante");
            String organizacionModalidad = requestData.getString("organizacionModalidad");
            
            if(organizacionPadreID == 0)
                nuevoOrganizacion = new Organizacion(0,new TipoOrganizacion(tipoOrganizacionID), codigo, nombre, alias, descripcion,direccion, new Date(), 1, estado.charAt(0));
            else
                nuevoOrganizacion = new Organizacion(0,new TipoOrganizacion(tipoOrganizacionID), new Organizacion(organizacionPadreID), codigo, nombre, alias, descripcion,direccion, new Date(), 1, estado.charAt(0),null);
        
            nuevoOrganizacion.setUbiCod(ubigeo);
            nuevoOrganizacion.setOrgGes(organizacionGestion);
            nuevoOrganizacion.setOrgCar(organizacionCaracteristica);
            nuevoOrganizacion.setOrgPro(organizacionPrograma);
            nuevoOrganizacion.setOrgFor(organizacionForma);
            nuevoOrganizacion.setOrgVar(organizacionVariante);
            nuevoOrganizacion.setOrgMod(organizacionModalidad);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            moduloDao.insert(nuevoOrganizacion);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar la Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Organizacion", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("organizacionID",nuevoOrganizacion.getOrgId());
        oResponse.put("fecha",nuevoOrganizacion.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la Organizacion se realizo correctamente", oResponse);
        //Fin
    }
    
}
