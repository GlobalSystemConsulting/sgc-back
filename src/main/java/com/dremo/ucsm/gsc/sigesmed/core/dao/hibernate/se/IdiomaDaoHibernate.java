/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.IdiomaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Idioma;

/**
 *
 * @author STADIMCJ7
 */
public class IdiomaDaoHibernate extends GenericDaoHibernate<Idioma> implements IdiomaDao{
    
}
