/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="bajas_det", schema="administrativo")
public class BajasDetalle {
    
    
    @Id
    @Column(name="bajas_det_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.bajas_bajas_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int bajas_det_id;
    
    @Column(name="cod_bie")
    private int cod_bie;
    
    @Column(name="bajas_id")
    private int bajas_id;

    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private BienesMuebles bm;

    public void setBm(BienesMuebles bm) {
        this.bm = bm;
    }

    public BienesMuebles getBm() {
        return bm;
    }

    public void setBajas_det_id(int bajas_det_id) {
        this.bajas_det_id = bajas_det_id;
    }

    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public void setBajas_id(int bajas_id) {
        this.bajas_id = bajas_id;
    }

    public int getBajas_det_id() {
        return bajas_det_id;
    }

    public int getCod_bie() {
        return cod_bie;
    }

    public int getBajas_id() {
        return bajas_id;
    }

    public BajasDetalle() {
    }

    public BajasDetalle(int bajas_det_id, int cod_bie, int bajas_id) {
        this.bajas_det_id = bajas_det_id;
        this.cod_bie = cod_bie;
        this.bajas_id = bajas_id;
    }
    
    
   

}
