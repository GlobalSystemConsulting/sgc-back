/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Grupo;
import java.util.List;

/**
 *
 * @author abel
 */
public interface GrupoDao extends GenericDao<Grupo>{
    
    public List<Grupo> buscarPorOrganizacion(int organizacionID);
    public List<Grupo> buscarConUsuariosPorOrganizacion(int organizacionID);    
    public void eliminarUsuarios(int grupoID);
}
