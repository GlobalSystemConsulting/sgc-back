
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class EliminarAtributoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarAtributoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer traId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            traId = requestData.getInt("id_met");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarAtributo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao)FactoryDao.buildDao("ConsultaGeneralDao");
        try{
            consultaGeneralDao.deleteAbsolute(new MetadataConsultaGeneral(traId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el trabajador\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el trabajador", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El trabajador se elimino correctamente");
    }
    
}
