package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.EnumUserType;

/**
 * Created by Administrador on 13/10/2016.
 */
public class CurriculaType extends EnumUserType<TipoCurriculaEnum> {

    public CurriculaType() {
        super(TipoCurriculaEnum.class);
    }
    @Override
    public Class<TipoCurriculaEnum> returnedClass() {
        return TipoCurriculaEnum.class;
    }
}
