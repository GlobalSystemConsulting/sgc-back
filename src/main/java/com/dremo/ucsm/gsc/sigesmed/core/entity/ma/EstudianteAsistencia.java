package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import java.util.Date;

/**
 * Created by Administrador on 11/01/2017.
 */
public class EstudianteAsistencia implements java.io.Serializable {
    private Integer id;
    private String dni;
    private Integer perId;
    private String apePat;
    private String apeMat;
    private String nom;
    private Date fecAsi;
    private Integer tipAsi;
    private Integer estAsi;
    private Boolean asiAre;
    private Integer asiId;
    private String desJus;
    private String docJus;


    public EstudianteAsistencia() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecAsi() {
        return fecAsi;
    }

    public void setFecAsi(Date fecAsi) {
        this.fecAsi = fecAsi;
    }

    public Integer getTipAsi() {
        return tipAsi;
    }

    public void setTipAsi(Integer tipAsi) {
        this.tipAsi = tipAsi;
    }

    public Integer getEstAsi() {
        return estAsi;
    }

    public void setEstAsi(Integer estAsi) {
        this.estAsi = estAsi;
    }

    public Boolean getAsiArea() {
        return asiAre;
    }

    public Boolean getAsiAre() {
        return asiAre;
    }

    public void setAsiAre(Boolean asiAre) {
        this.asiAre = asiAre;
    }

    public Integer getAsiId() {
        return asiId;
    }

    public void setAsiId(Integer asiId) {
        this.asiId = asiId;
    }

    public void setAsiArea(Boolean asiArea) {
        this.asiAre = asiArea;
    }

    public String getDesJus() {
        return desJus;
    }

    public void setDesJus(String desJus) {
        this.desJus = desJus;
    }

    public String getDocJus() {
        return docJus;
    }

    public void setDocJus(String docJus) {
        this.docJus = docJus;
    }
}
