/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarDatosUsuarioEmisorTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarDatosUsuarioEmisorTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        
        int usuId = requestData.getInt("usuId");
                
        Trabajador trabajador = new Trabajador();
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
        
        try{
            trabajador = trabajadorDao.buscarPorUsuarioId(usuId);

        }catch(Exception e){
            System.out.println("No se encontro datos del usuario emisor\n"+e);
            logger.log(Level.SEVERE,"Buscar datos de ususario emisor",e);
            return WebResponse.crearWebResponseError("No se encontro datos del usuario emisor", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONObject data = new JSONObject();       
        data.put("perNom", trabajador.getPersona().getNom());
        data.put("perApePat", trabajador.getPersona().getApePat());
        data.put("perApeMat", trabajador.getPersona().getApeMat());
        return WebResponse.crearWebResponseExito("Se listo correctamente", data);
    }
    
}
