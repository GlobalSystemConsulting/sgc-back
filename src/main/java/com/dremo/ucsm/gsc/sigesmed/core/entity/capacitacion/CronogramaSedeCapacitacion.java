package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cronograma_sedes_capacitacion",schema = "pedagogico")
public class CronogramaSedeCapacitacion implements java.io.Serializable {
    @Id
    @Column(name = "sed_cap_id", unique = true, nullable = false)
    @GeneratedValue(generator = "cronogramaSedeKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "cronogramaSedeKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property",value = "sedeCapacitacion"
            )
    )
    private int sed_cap_id;

    @OneToOne(fetch = FetchType.LAZY,optional = false)
    @PrimaryKeyJoinColumn
    private SedeCapacitacion sedeCapacitacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini",nullable = false)
    private Date fecIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin",nullable = false)
    private Date fecFin;

    @Column(name="num_par")
    private int numPar;

    @Column(name="dni_res", length = 8)
    private String dniRes;

    @Column(name="nom_res")
    private String nomRes;
    
    public CronogramaSedeCapacitacion() {}
    
    public CronogramaSedeCapacitacion(Date fecIni, Date fecFin, String dniRes, String nomRes) {
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.dniRes = dniRes;
        this.nomRes = nomRes;
    }

    public int getSed_cap_id() {
        return sed_cap_id;
    }

    public void setSed_cap_id(int sed_cap_id) {
        this.sed_cap_id = sed_cap_id;
    }

    public SedeCapacitacion getSedeCapacitacion() {
        return sedeCapacitacion;
    }

    public void setSedeCapacitacion(SedeCapacitacion sedeCapacitacion) {
        this.sedeCapacitacion = sedeCapacitacion;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public int getNumPar() {
        return numPar;
    }

    public void setNumPar(int numPar) {
        this.numPar = numPar;
    }

    public String getDniRes() {
        return dniRes;
    }

    public void setDniRes(String dniRes) {
        this.dniRes = dniRes;
    }

    public String getNomRes() {
        return nomRes;
    }

    public void setNomRes(String nomRes) {
        this.nomRes = nomRes;
    }

    @Override
    public String toString() {
        return "CronogramaSedeCapacitacion{" + "sed_cap_id=" + sed_cap_id + ", fecIni=" + fecIni + ", fecFin=" + fecFin + ", numPar=" + numPar + ", dniRes=" + dniRes + ", nomRes=" + nomRes + '}';
    }
    
    
}
