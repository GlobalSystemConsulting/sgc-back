/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador> implements TrabajadorDao{

    @Override
    public Trabajador buscarPorPerId(Integer perId) {
        Trabajador trabajador = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador AS t "
                    + "join fetch t.persona as p "
                    + "WHERE p.perId= '" + perId + "'";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            trabajador = (Trabajador)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo  \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajador;
    }

    @Override
    public Trabajador buscarPorId(Integer traId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trabajador t = (Trabajador)session.get(Trabajador.class, traId);
        session.close();
        return t;
    }

    @Override
    public Trabajador buscarPorUsuarioId(int codUsr) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador t"
                    + " INNER JOIN FETCH t.persona p"
                    + " INNER JOIN p.usuario u"
                    + " WHERE u.usuId =:cod";
            Query query = session.createQuery(hql);
            query.setParameter("cod", codUsr);
            query.setMaxResults(1);
            return (Trabajador) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public Trabajador buscarTrabajador(int traId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador t"
                    /*+ " INNER JOIN FETCH t.planilla pla"*/
                    + " INNER JOIN FETCH t.categoria cat"
                    + " INNER JOIN FETCH t.zona zon"
                    + " INNER JOIN FETCH t.cargo car"
                    + " INNER JOIN FETCH t.organizacion org"
                    + " WHERE t.traId=:trabajadorId";
            Query query = session.createQuery(hql);
            query.setParameter("trabajadorId", traId);
            query.setMaxResults(1);
            return (Trabajador) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public List<Trabajador> buscarPersonaTrabajador(int perId){
        List<Trabajador> tra=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador as t"
                    /*+ " LEFT JOIN FETCH t.planilla pla"*/
                    + " LEFT JOIN FETCH t.categoria cat"
                    + " LEFT JOIN FETCH t.zona zon"
                    + " LEFT JOIN FETCH t.cargo car"
                    + " LEFT JOIN FETCH t.organizacion org"
                    + " LEFT JOIN FETCH t.rol rol"
                    + " LEFT JOIN FETCH t.dependencia dep"
                    + " LEFT JOIN FETCH dep.facultad fac"
                    + " LEFT JOIN FETCH fac.organismo o"
                    + " LEFT OUTER JOIN FETCH t.persona p"
                    + " WHERE p.perId=:personaId";
            
            Query query = session.createQuery(hql);
            query.setParameter("personaId", perId);
            tra=query.list();
            t.commit();
        }catch (Exception e){
            t.rollback();
            System.out.println("No se pudo listar\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar \\n "+ e.getMessage());            
        }finally {
            session.close();
        }
        return tra;
    }
    
    @Override
    public List<Trabajador> listarTrabajadoresxPersona(int perId){
        List<Trabajador> tra=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador as t"
                    + " LEFT JOIN FETCH t.organizacion org"
                    + " LEFT JOIN FETCH t.rol rol "
                    + " LEFT OUTER JOIN FETCH t.persona p"
                    + " WHERE p.perId=:personaId";
            
            Query query = session.createQuery(hql);
            query.setParameter("personaId", perId);
            tra=query.list();
            t.commit();
        }catch (Exception e){
            t.rollback();
            System.out.println("No se pudo listar los trabajadores\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los trabajadores\\n "+ e.getMessage());            
        }finally {
            session.close();
        }
        return tra;
    }

    @Override
    public Trabajador mostrarDatosTrabajador(int traId, int perId) {
        Trabajador tra=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador as t"
                    + " LEFT JOIN FETCH t.organizacion org"
                    + " LEFT JOIN FETCH t.rol rol "
                    + " LEFT OUTER JOIN FETCH t.persona p"
                    + " WHERE t.traId=:trabajadorId AND p.perId=:personaId";
            
            Query query = session.createQuery(hql);
            query.setParameter("trabajadorId", traId);
            query.setParameter("personaId", perId);
            query.setMaxResults(1);
            tra = (Trabajador)query.uniqueResult();
            t.commit();
        }catch (Exception e){
            t.rollback();
            System.out.println("No se pudo listar los trabajadores\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los trabajadores\\n "+ e.getMessage());            
        }finally {
            session.close();
        }
        return tra;
    }
}
