package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EnviarMensajeCapacitacionTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EnviarMensajeCapacitacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
            List<Integer> participantes = participanteCapacitacionDao.listarMensaje(data.getInt("codSed"));
            MensajeElectronico mensaje = new MensajeElectronico(0, "NUEVO CONTENIDO DISPONIBLE", data.getString("cont"), "", "", new Date(), data.getInt("usuAut"));
            mensajeDao.enviarMensaje(mensaje, participantes);
            
            return WebResponse.crearWebResponseExito("Los mensajes fueron enviados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarMensaje", e);
            return WebResponse.crearWebResponseError("No se pudo registrar los mensajes del desarrollo del tema", WebResponse.BAD_RESPONSE);
        }
    }    
}
