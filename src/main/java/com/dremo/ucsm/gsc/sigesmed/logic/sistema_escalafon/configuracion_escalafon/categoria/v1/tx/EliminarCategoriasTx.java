/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarCategoriasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarCategoriasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer catId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           catId = requestData.getInt("catId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarCategoria",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        try{
            categoriaDao.delete(new Categoria(catId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la categoria\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la categoria", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La categoria se elimino correctamente");
    }
    
}