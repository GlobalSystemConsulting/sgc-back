/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoEstudioComplementario;
import java.util.List;

/**
 *
 * @author alex
 */
public interface TipoEstudioComplementarioDao {
    public List<TipoEstudioComplementario> listarAll();
    public TipoEstudioComplementario buscarPorId(Integer tipEstComId);
}
