/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.servicioRidea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.comunication.pojo.ServicioWeb;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;
/**
 *
 * @author User
 */
public class VerServcioRIDEATx implements ITransaction {
    @Override
    public WebResponse execute(WebRequest wr) {
        
        ServicioWeb servicioWeb=new  ServicioWeb();
        String conection="";
               
      JSONObject oResponse = new JSONObject();
        try {
            String ipServicios=servicioWeb.getIp()+"/rideaoffice";
            conection=getConecctionStatus(ipServicios);
            oResponse.put("servicio", servicioWeb.getIp());
            oResponse.put("com", conection);
            
        } catch (IOException ex) {
            Logger.getLogger(VerServcioRIDEATx.class.getName()).log(Level.SEVERE, null, ex);
        }
           return WebResponse.crearWebResponseExito(" El Servicio actualizado exitosamente",oResponse);
    }
    private String getConecctionStatus( String ip){
    
        String conection =null;
        try {
            URL u =new URL(ip);
            URLConnection huc= u.openConnection();
            huc.connect();
            conection="Conexion exitosa establecida";
        } catch (Exception e) {
            conection="Conexion no realizada";
        }    
      return conection;   
    }
}
