/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="altas_det", schema="administrativo")
public class AltasDetalle {
    
    @Id
    @Column(name="alt_det_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.altas_det_alt_det_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int alt_det_id;
    
    @Column(name="cod_bie")
    private int cod_bie;
    
    @Column(name="altas_id")
    private int altas_id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private BienesMuebles bm;

    public void setBm(BienesMuebles bm) {
        this.bm = bm;
    }

    public BienesMuebles getBm() {
        return bm;
    }
    
    
    
    public void setAlt_det_id(int alt_det_id) {
        this.alt_det_id = alt_det_id;
    }

    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public void setAltas_id(int altas_id) {
        this.altas_id = altas_id;
    }

    public int getAlt_det_id() {
        return alt_det_id;
    }

    public int getCod_bie() {
        return cod_bie;
    }

    public int getAltas_id() {
        return altas_id;
    }

    public AltasDetalle() {
    }

    public AltasDetalle(int alt_det_id, int cod_bie, int altas_id) {
        this.alt_det_id = alt_det_id;
        this.cod_bie = cod_bie;
        this.altas_id = altas_id;
    }
    
    
    
}
