/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoDocDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.meritos_felicitaciones_reconocimientos;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.*;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.ReportDoc;
import com.dremo.ucsm.gsc.sigesmed.util.ReportXls;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author felipe
 */
public class ReportePersonalizadoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ReporteFichaEscalafonariaTx.class.getName());

    public String estadoGoce(boolean bool) {
        if (bool = true) {
            return "sin goce de haber";
        } else {
            return "con goce de haber";
        }
    }

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject response = new JSONObject();
        try {
            long idPer = wr.getIdUsuario();
            JSONObject data = (JSONObject) wr.getData();
            Integer traId = data.optInt("traId");
            String perDni = data.optString("perDni");
            JSONArray seleccionados = data.optJSONArray("seleccionados");
            //creas la variable para que reciba el objeto del frontend
            JSONObject reporteData = data.optJSONObject("reporteData");
            //System.out.println(">>>>: "+reporteData.toString());
            //return crearReporte(traId, perDni, tipoInforme, reporteData);

            System.out.println("Seleccionados: " + seleccionados);

            FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");

            TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
            
            TipoDocDao tipoDocDao = (TipoDocDao) FactoryDao.buildDao("se.TipoDocDao");
            
            //ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
            //FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
            //ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
            //EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
            //ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
            //PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
            //DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
            //ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
            //DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
            //EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
            //AscensoDao ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
            //VacacionDao vacacionDao = (VacacionDao) FactoryDao.buildDao("se.VacacionDao");
            //LicenciaDao licenciaDao = (LicenciaDao) FactoryDao.buildDao("se.LicenciaDao");
            //DependenciaDao dependenciaDao = (DependenciaDao) FactoryDao.buildDao("se.DependenciaDao");

            FichaEscalafonaria fichaEscalafonaria = null;
            fichaEscalafonaria = fichaEscalafonariaDao.buscarPorTraId(traId);
            Trabajador trabajador=trabajadorDao.buscarPorId(traId);
            //CASOS
            LicenciaDao licenciaDao = (LicenciaDao) FactoryDao.buildDao("se.LicenciaDao");
            List<Licencia> licenciasTodas = new ArrayList<Licencia>();
            licenciasTodas = licenciaDao.listarxFichaEscalafonaria(fichaEscalafonaria.getFicEscId());
            
            VacacionDao vacacionDao = (VacacionDao) FactoryDao.buildDao("se.VacacionDao");
            List<Vacacion> vacacionesTodas = new ArrayList<Vacacion>();
            vacacionesTodas = vacacionDao.listarxFichaEscalafonaria(fichaEscalafonaria.getFicEscId());
            
            DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
            List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
            desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(fichaEscalafonaria.getFicEscId());
            
            AscensoDao ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
            List<Ascenso> ascensos = new ArrayList<Ascenso>();
            ascensos = ascensoDao.listarxFichaEscalafonaria(fichaEscalafonaria.getFicEscId());
            
            CeseDao ceseDao = (CeseDao) FactoryDao.buildDao("se.CeseDao");
            List<Cese> ceses = new ArrayList<Cese>();
            ceses = ceseDao.listarxFichaEscalafonaria(fichaEscalafonaria.getFicEscId());
            
            //Trabajador cargo1 = null;
            //List<Trabajador> cargos = new ArrayList<Trabajador>();
            //List<Parientes> parientes = new ArrayList<Parientes>();
            //List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
            //List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
            //List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
            //List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
            //List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
            //List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
            //List<Exposicion> exposiciones = new ArrayList<Exposicion>();
            //List<Publicacion> publicaciones = new ArrayList<Publicacion>();
            /*List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
            List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
            //List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
            //List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
            List<Demerito> demeritos = new ArrayList<Demerito>();
            //List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
            List<Ascenso> ascensos = new ArrayList<Ascenso>();
            List<Vacacion> vacacionesTodas = new ArrayList<Vacacion>();
            //List<Vacacion> vacacionesSinGoce = new ArrayList<Vacacion>();
            List<Licencia> licenciasTodas = new ArrayList<Licencia>();
            //List<Licencia> licenciasSinGoce = new ArrayList<Licencia>();
            Dependencia claseDependencia = new Dependencia();*/

            //consultas a la base de datos para obtener los datos necesarios
            //datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
            //System.out.println("|||||||||||DATOSGENERALES" + datosGenerales.toString());
            //cargos = trabajadorDao.buscarPersonaTrabajador(datosGenerales.getTrabajador().getPersona().getPerId());
            //System.out.println("|||||||||||cargos" + cargos.toString());
            //cargo1 = trabajadorDao.buscarTrabajador(datosGenerales.getTrabajador().getTraId());

            //parientes = parientesDao.listarxTrabajador(traId);
            //formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            //colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            //estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            //exposiciones = exposicionDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            //publicaciones = publicacionDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            /*desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||desplazamientos" + desplazamientos.toString());
            reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||recono" + reconocimientos.toString());
            demeritos = demeritoDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||demetiros" + demeritos.toString());
            //estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            ascensos = ascensoDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||ascensos" + ascensos.toString());
            vacacionesTodas = vacacionDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||vacaciones" + vacacionesTodas.toString());
            //vacacionesSinGoce = vacacionDao.listarSGxTrabajador(traId);
            licenciasTodas = licenciaDao.listarxFichaEscalafonaria(datosGenerales.getFicEscId());
            System.out.println("|||||||||||licenciasTodas" + licenciasTodas.toString());
            //licenciasSinGoce = licenciaDao.listarSGxTrabajador(traId);*/
            
            Persona pe = null;
            PersonaDao peDao = (PersonaDao) FactoryDao.buildDao("di.PersonaDao");

            try {
                pe = peDao.buscarPersonaxId(idPer);
            } catch (Exception e) {
                System.out.println("No se encontro a la persona con el DNI dado \n" + e);
                return WebResponse.crearWebResponseError("No se encontro a la persona con el DNI dado ", e.getMessage());
            }
            String nombreDeSolicita = pe.getApePat() + " " + pe.getApeMat() + ", " + pe.getNom();
            
            //creación de archivo
            Mitext mitext = new Mitext(false,"SIGESMED","Realizado por: " + nombreDeSolicita);
            mitext.agregarTitulo("");

            PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

            float[] columnWidthsHead = {1, 3};
            Table head = new Table(columnWidthsHead);
            head.setWidthPercent(100);
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("INFORME N°: ").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph((reporteData.getString("numInf")+"/"+reporteData.getString("numRef"))).setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("A: ").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(reporteData.getString("lugDes")).setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("DE: ").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(reporteData.getString("lugPro")).setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("ASUNTO: ").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            head.addCell(new Cell(2, 1).setBorder(Border.NO_BORDER).add(new Paragraph(reporteData.getString("asu")).setFontSize(12).setTextAlignment(TextAlignment.LEFT)));

            ArrayList<String> cabecera = new ArrayList();
            ArrayList<String> cuerpo = new ArrayList();
            ArrayList<String> piePag = new ArrayList();
            
            cabecera.add("INFORME N°: ");
            cabecera.add(reporteData.getString("numInf")+"/"+reporteData.getString("numRef"));
            cabecera.add("A: ");
            cabecera.add(reporteData.getString("lugDes"));
            cabecera.add("DE: ");
            cabecera.add(reporteData.getString("lugPro"));
            cabecera.add("ASUNTO: ");
            cabecera.add(reporteData.getString("asu"));

            head.setFontSize(12);

            if ((Boolean) seleccionados.get(r_situacionLaboral) && trabajador != null) {
                
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                String fechaComoCadena = trabajador.getFecIng() == null ? "" : sdf.format(trabajador.getFecIng());
                //System.out.println("VVVVVV" + trabajador.toString());
                //Validacion del cargo
                String planilla = "";
                String categoria = "";
                String cargo = "";//opcional
                
                TrabajadorOrganigramaDetalle tod=null;
                TrabajadorOrganigramaDetalleDao todDao = (TrabajadorOrganigramaDetalleDao) FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");
                tod = todDao.buscarPorId(trabajador.getTraId(), trabajador.getOrgiId());
                
                if (tod != null) {
                    Planilla pl = todDao.buscarPlanilla(tod.getPlaId()==null?0:tod.getPlaId());
                    Categoria cat = todDao.buscarCategoria(tod.getCatId()==null?0:tod.getCatId());
                    Cargo car = todDao.buscarCargo(tod.getCarId()==null?0:tod.getCarId());
                    
                    if (pl != null) {
                        planilla = pl.getNomPla() == null ? "" : pl.getNomPla();
                    }
                    if (cat != null) {
                        categoria = cat.getNomCat() == null ? "" : " como " + cat.getNomCat();
                    }
                    if (car != null) {
                        cargo = car.getNomCar() == null ? "." : " del cargo " + car.getNomCar()+".";
                    }
                }
                ////
                String linea1 = "Con referencia al asunto cumplo con informar lo siguiente: ";
                String lineaCargos = "Que, "
                        + trabajador.getPersona().getApePat() + " "
                        + trabajador.getPersona().getApeMat() + " "
                        + trabajador.getPersona().getNom() + " "
                        + "labora en nuestra institución en calidad de "
                        + planilla
                        + categoria
                        + cargo;
                        //+ zona + ".";
                String lineaIngreso = "Ingreso a prestar servicios a partir del " + fechaComoCadena + ".";

                head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(linea1)));
                head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaCargos)).setTextAlignment(TextAlignment.JUSTIFIED));
                head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaIngreso)).setTextAlignment(TextAlignment.JUSTIFIED));
                cuerpo.add(linea1);
                cuerpo.add(lineaCargos);
                cuerpo.add(lineaIngreso);

            }
            //Ingresos y Salidas
            if ((Boolean) seleccionados.get(r_ingresos_salidas) && !desplazamientos.isEmpty()) {
                for (Desplazamiento d : desplazamientos) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = d.getFecDoc() == null ? "" : sdf.format(d.getFecDoc());
                    //tipo de documento
                    TipoDoc tipoDoc = tipoDocDao.buscarPorId(d.getTipDocId()==null?0:d.getTipDocId());
                    String tipDoc = (tipoDoc==null)?"":(tipoDoc.getNom()==null ? "" : tipoDoc.getNom());
                    //numero de resolución
                    String numRes = d.getNumDoc() == null ? "" : d.getNumDoc();
                    //entidad emisora responsable
                    String entEmiRes = d.getEntEmiRes() == null ? "" : d.getEntEmiRes();
                    //Ini
                    String fechaCadenaIni = d.getFecIni() == null ? "" : sdf.format(d.getFecIni());
                    ////////////////////////////////////////
                    String categoria = "";
                    String organigrama = "";
                    
                    Categoria cat = null;
                    CategoriaDao catDao = (CategoriaDao) FactoryDao.buildDao("se.CategoriaDao");
                    cat = catDao.buscarPorId(d.getCatActId());

                    if (cat != null) {
                        categoria = cat.getNomCat() == null ? "" : " como " + cat.getNomCat()+",";
                    }
                    
                    Organigrama org = null;
                    OrganigramaDao orgDao = (OrganigramaDao) FactoryDao.buildDao("se.OrganigramaDao");
                    org = orgDao.buscarXId(d.getOrgiActId());

                    if (org != null) {
                        organigrama = org.getNomOrgi() == null ? "" : " adscrito al " + org.getNomOrgi()+",";
                    }
                    
                    String lineaDes = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Aprobar el/la: " + mostrarTipoIngresoSalida(d.getTip())
                            + categoria
                            + organigrama
                            + " a partir del " + fechaCadenaIni+".";
                            //+ " y la entidad emisora correspondiente " + entEmiRes + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaDes)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaDes);
                }
            }

            //Licencias
            if ((Boolean) seleccionados.get(r_licencias)) {
                for (Licencia li : licenciasTodas) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    //fecha de inicio
                    String fechaCadenaIni = li.getFecIni() == null ? "" : sdf.format(li.getFecIni());
                    //fecha de termino
                    String fechaCadenaTer = li.getFecTer() == null ? "" : sdf.format(li.getFecTer());
                    //fecha de resolución
                    String fechaCadenaRes = li.getFecDoc() == null ? "" : sdf.format(li.getFecDoc());
                    //entidad emisora responsable
                    String entEmiRes = li.getEntEmiRes() == null ? "" : li.getEntEmiRes();
                    //tipo de documento
                    TipoDoc tipoDoc = tipoDocDao.buscarPorId(li.getTipDocId()==null?0:li.getTipDocId());
                    String tipDoc = tipoDoc==null?"":(tipoDoc.getNom()==null ? "" : tipoDoc.getNom());
                    //numero de resolución
                    String numRes = li.getNumDoc() == null ? "" : li.getNumDoc();
                    //motivo
                    String motivo = li.getMotLic() == null ? "" : li.getMotLic();
                    //estado de goce
                    Boolean estGoc = li.getEstGoc() == null ? true : li.getEstGoc();

                    String lineaLic = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Acceder a su solicitud y concederle licencia " + estadoGoce(estGoc)
                            + " desde " + fechaCadenaIni
                            + " hasta " + fechaCadenaTer
                            + " por motivos de " + motivo
                            //+ " donde su cargo actual es " + cargo
                            + " y la entidad emisora correspondiente " + entEmiRes + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaLic)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaLic);
                }
            }

            //Vacaciones
            if ((Boolean) seleccionados.get(r_vacaciones)) {
                for (Vacacion v : vacacionesTodas) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    //fecha de inicio
                    String fechaCadenaIni = v.getFecIni() == null ? "" : sdf.format(v.getFecIni());
                    //fecha de termino
                    String fechaCadenaTer = v.getFecTer() == null ? "" : sdf.format(v.getFecTer());
                    //fecha de resolución
                    String fechaCadenaRes = v.getFecDoc() == null ? "" : sdf.format(v.getFecDoc());
                    //entidad emisora responsable
                    String entEmiRes = v.getEntEmiRes() == null ? "" : v.getEntEmiRes();
                    //tipo de documento
                    TipoDoc tipoDoc = tipoDocDao.buscarPorId(v.getTipDocId()==null?0:v.getTipDocId());
                    String tipDoc = tipoDoc==null?"":(tipoDoc.getNom()==null ? "" : tipoDoc.getNom());
                    //numero de resolución
                    String numRes = v.getNumDoc() == null ? "" : v.getNumDoc();
                    //motivo
                    String motivo = v.getMotLic() == null ? "" : v.getMotLic();
                    //estado de goce
                    Boolean estGoc = v.getEstGoc() == null ? true : v.getEstGoc();

                    String lineaVac = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Acceder a su solicitud y concederle vacaciones " + estadoGoce(estGoc)
                            + " desde " + fechaCadenaIni
                            + " hasta " + fechaCadenaTer
                            + " por motivos de " + motivo
                            //+ " donde su cargo actual es " + cargo
                            + " y la entidad emisora correspondiente " + entEmiRes + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaVac)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaVac);
                }
            }

            //Rotaciones o Desplazamientos
            if ((Boolean) seleccionados.get(r_desplazamientos) && !ascensos.isEmpty()) {
                for (Ascenso a : ascensos) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = a.getFecDoc() == null ? "" : sdf.format(a.getFecDoc());
                    //tipo de documento
                    TipoDoc tipoDoc = tipoDocDao.buscarPorId(a.getTipDocId()==null?0:a.getTipDocId());
                    String tipDoc = tipoDoc==null?"":(tipoDoc.getNom()==null ? "" : tipoDoc.getNom());
                   
                    //numero de resolución
                    String numRes = a.getNumDoc() == null ? "" : a.getNumDoc();
                    
                    //fechaEfectiva
                    String fechaEfec = a.getFecEfe() == null ? "" : sdf.format(a.getFecEfe());
                    //NivelEscala
                    //String escala = a.getEsc() == null ? "?" : a.getEsc();
                    //entidad emisora responsable
                    String entEmiRes = a.getEntEmiRes() == null ? "" : a.getEntEmiRes();
                    //motivo
                    String motivo = a.getMot() == null ? "" : a.getMot();
                    /////////////////////////
                    String categoriaAnt = "";
                    String categoriaPos = "";
                    String organigramaAnt = "";
                    String organigramaPos = "";
                    
                    Categoria catAnt = null, catPos = null;
                    CategoriaDao catDao = (CategoriaDao) FactoryDao.buildDao("se.CategoriaDao");
                    
                    if(a.getTip().toString().equals("1"))//Rotacion
                    {
                        Organigrama orgAnt = null, orgPos = null;
                        OrganigramaDao orgDao = (OrganigramaDao) FactoryDao.buildDao("se.OrganigramaDao");
                        orgAnt = orgDao.buscarXId(a.getOrgiAntId());
                        orgPos = orgDao.buscarXId(a.getOrgiPostId());

                        if (orgAnt != null) {
                            organigramaAnt = orgAnt.getNomOrgi() == null ? "" : orgAnt.getNomOrgi();
                        }
                        if (orgPos != null) {
                            organigramaPos = orgPos.getNomOrgi() == null ? "" : orgPos.getNomOrgi();
                        }
                    }
                    else{//Cambio de Regimen o Nombramiento
                        catAnt = catDao.buscarPorId(a.getCatAntId());
                        catPos = catDao.buscarPorId(a.getCatPostId());

                        if (catAnt != null) {
                            categoriaAnt = catAnt.getNomCat() == null ? "" : catAnt.getNomCat();
                        }
                        if (catPos != null) {
                            categoriaPos = catPos.getNomCat() == null ? "" : catPos.getNomCat();
                        }
                    }

                    String lineaAse = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Aprobar la/el " + mostrarTipoDesplazamiento(a.getTip())
                            + (a.getTip().toString().equals("1")?" del "+organigramaAnt+" al "+organigramaPos:" como "+categoriaAnt+" al "+categoriaPos)
                            + " a partir del " + fechaEfec
                            + " por motivos de " + motivo +".";
                            //+ " y la entidad emisora correspondiente " + entEmiRes + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaAse)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaAse);
                }
            }
            //Interrupciones
            if ((Boolean) seleccionados.get(r_interrupciones) && !ascensos.isEmpty()) {
                for (Cese c : ceses) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = c.getFecDoc() == null ? "" : sdf.format(c.getFecDoc());
                    //fecha de inicio
                    String fechaCadenaIni = c.getFecIni() == null ? "" : sdf.format(c.getFecIni());
                    //fecha de termino
                    String fechaCadenaTer = c.getFecFin() == null ? "" : sdf.format(c.getFecFin());
                    //tipo de documento+
                    TipoDoc tipoDoc = tipoDocDao.buscarPorId(c.getTipDocId());//???
                    String tipDoc = tipoDoc==null?"":(tipoDoc.getNom()==null ? "" : tipoDoc.getNom());
                    //numero de resolución
                    String numRes = c.getNumDoc() == null ? "" : c.getNumDoc();
                    //motivo
                    String motivo = c.getMotCes() == null ? "" : c.getMotCes();
                    String entEmiRes = c.getEntEmiRes() == null ? "" : c.getEntEmiRes();

                    String lineaAse = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Aprobar la interrupción"
                            + " desde " + fechaCadenaIni
                            + " hasta " + fechaCadenaTer
                            + " por motivos de " + motivo
                            //+ " donde su cargo actual es " + cargo
                            + " y la entidad emisora correspondiente " + entEmiRes + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaAse)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaAse);
                }
            }
            //Ascensos
            /*if ((Boolean) seleccionados.get(r_ascensos) && !ascensos.isEmpty()) {
                for (Ascenso a : ascensos) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = a.getFecDoc() == null ? "" : sdf.format(a.getFecDoc());
                    //tipo de documento
                    String tipDoc = a.getTipDoc() == null ? "" : a.getTipDoc();
                    //numero de resolución
                    String numRes = a.getNumDoc() == null ? "" : a.getNumDoc();
                    //dependencia
                    String dependencia = "";
                    if (a.getDep() != null) {
                        claseDependencia = dependenciaDao.buscarPorId(Integer.parseInt(a.getDep()));
                        dependencia = claseDependencia.getNomDep();
                    }

                    //fechaEfectiva
                    String fechaEfec = a.getFecEfe() == null ? "" : sdf.format(a.getFecEfe());
                    //NivelEscala
                    String escala = a.getEsc() == null ? "?" : a.getEsc();

                    String lineaAse = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: Aprobar el cambio a " + escala
                            + " de " + dependencia
                            + " a partir del " + fechaEfec + ".";

                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaAse)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaAse);
                }
            }*/
            //Ceses
            /*
             for(Cese a:ascensos){
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
             
             //fecha de resolución
             String fechaCadenaRes = a.getFecDoc() == null ? "" : sdf.format(a.getFecDoc());
             //tipo de documento
             String tipDoc = a.getTipDoc() == null ? "" : a.getTipDoc();
             //numero de resolución
             String numRes = a.getNumDoc() == null ? "" : a.getNumDoc();
             //dependencia
             String dependencia = a.getDep() == null ? "" : a.getDep();
             //fechaEfectiva
             String fechaEfec = a.getFecEfe() == null ? "" : sdf.format(a.getFecEfe());
             //NivelEscala
             String escala = a.getEsc() == null ? "?" : a.getEsc();
			
             String lineaDes= "Por " +tipDoc
             + " N° "+ numRes
             + " del "+ fechaCadenaRes
             + " se, resuelve: Aprobar el ascenso a " + mostrarTipoDesplazamiento(d.getTip())
             + " de "+ dependencia
             + " a partir del "+ fechaEfec;
             
			 
             }
             cese !! es incorporar???
             Por $TipoDocumento $Ndocumetno del $fechadedocumento , se resuelve: dar por concluida su encargatura de la $dependencia por motivos $motivos, 
             a partir del $fechaCese.*/

            //Meritos, felicitaciones y reconocimientos
            /*if ((Boolean) seleccionados.get(r_reconocimientos) && !reconocimientos.isEmpty()) {
                for (Reconocimiento m : reconocimientos) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = m.getFecDoc() == null ? "" : sdf.format(m.getFecDoc());
                    //tipo de documento
                    String tipDoc = m.getTipDoc() == null ? "" : m.getTipDoc();
                    //numero de resolución
                    String numRes = m.getNumDoc() == null ? "" : m.getNumDoc();
                    //dependencia
                    String dependencia = "";
                    if (m.getDep() != null) {
                        claseDependencia = dependenciaDao.buscarPorId(Integer.parseInt(m.getDep()));
                        dependencia = claseDependencia.getNomDep();
                    }
                    //Motivo
                    String motivo = mostrarMotivo(m.getMot());
                    //Entidad
                    String entidEm = m.getEntEmi() == null ? "" : m.getEntEmi();
                    //FechaEfectiva
                    //String fecEfec = m.getFecEfe() == null ? "" : m.getFecEfe();

                    String lineaRec = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: autorizar su reconocimiento por " + motivo
                            + " de " + dependencia
                            //+ ", del "+ fechaEfec;
                            + " y la entidad emisora correspondiente " + entidEm + ".";
                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaRec)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaRec);
                }
            }*/
            //Demeritos
            /*if ((Boolean) seleccionados.get(r_demeritos) && !demeritos.isEmpty()) {
                for (Demerito d : demeritos) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                    //fecha de resolución
                    String fechaCadenaRes = d.getFecDoc() == null ? "" : sdf.format(d.getFecDoc());
                    //tipo de documento
                    String tipDoc = d.getTipDoc() == null ? "" : d.getTipDoc();
                    //numero de resolución
                    String numRes = d.getNumDoc() == null ? "" : d.getNumDoc();
                    //dependencia
                    String dependencia = "";
                    if (d.getDep() != null) {
                        claseDependencia = dependenciaDao.buscarPorId(Integer.parseInt(d.getDep()));
                        dependencia = claseDependencia.getNomDep();
                    }
                    //Motivo
                    String motivo = d.getMot() == null ? "" : d.getMot();
                    //Entidad
                    String entidEm = d.getEntEmi() == null ? "" : d.getEntEmi();
                    //FechaInicio
                    String fecIni = d.getFecIni() == null ? "" : sdf.format(d.getFecIni());
                    //FechaFin
                    String fecFin = d.getFecFin() == null ? "" : sdf.format(d.getFecFin());
                    //Separacion
                    String sep = d.getSep() == true ? "separación del cargo" : "sanción del cargo";

                    String lineaDem = "Por " + tipDoc
                            + " N° " + numRes
                            + " del " + fechaCadenaRes
                            + " se, resuelve: autorizar su " + sep
                            + " de " + dependencia
                            + " por motivos " + motivo
                            + ", desde el " + fecIni
                            + " al " + fecFin
                            + " y la entidad emisora correspondiente " + entidEm + ".";
                    head.addCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph(lineaDem)).setTextAlignment(TextAlignment.JUSTIFIED));
                    cuerpo.add(lineaDem);
                }
            }*/
            mitext.agregarTabla(head);
            mitext.newLine(6);
            mitext.agregarParrafo("");
            Date fecha = new Date();
            mitext.agregarNotaParrafo("NOTA : * El Presente Formato será utilizado para Reporte Personalizado ESCALAFON");
            piePag.add("NOTA : * El Presente Formato será utilizado para Reporte Personalizado ESCALAFON");
            String dat = new SimpleDateFormat("yyyy-MM-dd").format(fecha);
            //mitext.agregarParrafo("** Reporte generado con Datos a la Fecha: " + dat);
            piePag.add("** Reporte generado con Datos a la Fecha: " + dat);
            //piePag.add("Realizado por: " + nombreDeSolicita);
            ////////////////////
            mitext.agregarParrafo("");
            ///////////////////////////////////
            /*ReportXls myReportXls = new ReportXls();
            try {
                //
                //set header
                myReportXls.addHeader("ESCALAFON UNSA");

                //set title1
                //myReportXls.setTitle1("SISTEMA DE ESCALAFON");
                //set title2
                //myReportXls.setTitle2("REPORTE RETRASOS");
                //Show image
                myReportXls.setImageTop(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png");

                //add fecha
                myReportXls.setDate();

                //create subtitles
                //System.out.println(">>>>>>>>: "+cabecera.toString());
                myReportXls.setSubtitles(cabecera);

                //add directorio
                //myReportXls.fillData(labels2, key2, peso, lista);
                //agregando tabla
                if (!cuerpo.isEmpty()) {
                    myReportXls.tablaSimple(cuerpo);
                }
                //close book
                //myReportXls.closeReport();

            } catch (Exception e) {
                System.out.println("No se pudo generar reporte en xls \n" + e);
            }*/
            ///////////////////////////////////
            ReportDoc myReportDoc = new ReportDoc();
            try {
                //
                //set header
                //myReportXls.addHeader("ESCALAFON UNSA");

                //set title1
                //myReportXls.setTitle1("SISTEMA DE ESCALAFON");
                //set title2
                //myReportXls.setTitle2("REPORTE RETRASOS");
                //Show image
                myReportDoc.setImageTop(ServicioREST.PATH_SIGESMED + "/recursos/img/unsa_logo.png");

                //add fecha
                myReportDoc.setDate();

                //create subtitles
                //System.out.println(">>>>>>>>: "+cabecera.toString());
                myReportDoc.cabeceraTabla(cabecera);
                System.out.println("XXXXXXXXXXXX" + cabecera.toString());
                //add directorio
                //myReportXls.fillData(labels2, key2, peso, lista);
                //agregando tabla
                if (!cuerpo.isEmpty()) {
                    myReportDoc.contenido(cuerpo);
                }
                myReportDoc.piePagina(piePag);
                //close book
                //myReportDoc.closeReport();

            } catch (Exception e) {
                System.out.println("No se pudo generar reporte en doc \n" + e);
            }

            mitext.cerrarDocumento();
            //return myReportXls.encodeToBase64();
            response.append("file", mitext.encodeToBase64());
            //response.append("reporteXls", myReportXls.encodeToBase64());
            response.append("reporteDoc", myReportDoc.encodeToBase64());
            //System.out.print("XLS:\n" + myReportXls.encodeToBase64());
            //System.out.print("DOC:\n" + myReportDoc.encodeToBase64());
            //System.out.print("PDF:\n" + mitext.encodeToBase64());

        } catch (Exception ex) {
            Logger.getLogger(ReportePersonalizadoTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }

        return WebResponse.crearWebResponseExito("El reporte se realizo con éxito", response);
    }

    private String mostrarMotivo(Character m) {
        switch (m) {
            case '1':
                return "Mérito";
            case '2':
                return "Felicitación";
            case '3':
                return "25 años de servicios";
            case '4':
                return "30 años de servicios";
            case '5':
                return "Luto y sepelio";
            default:
                return "Otros";
        }
    }

    private String mostrarTipoIngresoSalida(Character t) {
        switch (t) {
            case '1':
                return "Ingreso";
            case '2':
                return "Salida";
            case '3':
                return "Reingreso";
            default:
                return "Otros";
        }
    }
    private String mostrarTipoDesplazamiento(Character t) {
        switch (t) {
            case '1':
                return "Rotación";
            case '2':
                return "Cambio de Regimen";
            case '3':
                return "Nombramiento";
                //////Otros
            case '4':
                return "Permuta";
            case '5':
                return "Encargo";
            case '6':
                return "Comisión de servicio";
            case '7':
                return "Transferencia";
            default:
                return "Otros";
        }
    }

    private String mostrarTipoEstudioComplementario(Character ec) {
        switch (ec) {
            case '1':
                return "Informatica";
            case '2':
                return "Idiomas";
            case '3':
                return "Certificación";
            case '4':
                return "Diplomado";
            case '5':
                return "Especialización";
            default:
                return "Otros";
        }
    }

    private String mostrarNivel(Character n) {
        switch (n) {
            case 'B':
                return "Básico";
            case 'I':
                return "Intermedio";
            case 'A':
                return "Avanzado";
            default:
                return "Ninguno";
        }
    }

    private String mostrarTipoFormacionEducativa(Character t) {
        switch (t) {
            case '1':
                return "Estudios Basicos Regulares";
            case '2':
                return "Estudios Tecnicos";
            case '3':
                return "Bachiller";
            case '4':
                return "Universitario";
            case '5':
                return "Licenciatura";
            case '6':
                return "Maestria";
            case '7':
                return "Doctorado";
            default:
                return "Otros";
        }
    }

    private Integer calcularEdad(Date fecNac) {
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fecha_nac = formato.format(fecNac);
        String hoy = formato.format(fechaActual);
        String[] dat1 = fecha_nac.split("/");
        String[] dat2 = hoy.split("/");
        int anos = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]);
        int mes = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);
        if (mes < 0) {
            anos = anos - 1;
        } else if (mes == 0) {
            int dia = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
            if (dia > 0) {
                anos = anos - 1;
            }
        }
        return anos;
    }

}
