package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Administrador on 29/08/2016.
 */
public class ResultadoEstadistica {
    private Object clave;
    private Object valor;
    private String type;
    public Object getClave() {
        return clave;
    }

    public Object getValor() {
        return valor;
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String toGroupString(List<ResultadoEstadistica> result) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("[");
        Iterator<ResultadoEstadistica> ite = result.iterator();
        while(ite.hasNext()){
            ResultadoEstadistica r = ite.next();
            buffer.append("{" + "\"label\":" + "\"" + r.getClave() + "\"" + ",\"value\":" + r.getValor()+ '}');
            if(ite.hasNext()) buffer.append(",");
        }
        buffer.append("]");
        return buffer.toString();
    }

}
