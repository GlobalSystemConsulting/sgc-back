package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "adjunto_evaluacion_desarrollo", schema = "pedagogico")
public class AdjuntoEvaluacionDesarrollo implements Serializable {
    @Id
    @Column(name = "adj_eva_des_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_adjunto_evaluacion_desarrollo", sequenceName = "pedagogico.adjunto_evaluacion_desarrollo_adj_eva_des_id_seq")
    @GeneratedValue(generator = "secuencia_adjunto_evaluacion_desarrollo")
    private int adjEvaDesId;
    
    @Column(name = "nom", nullable = false)
    private String nom;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "des_tem_cap_id")
    private DesarrolloTemaCapacitacion desarrollo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "des_tem_cap_id", updatable = false, insertable = false),
        @JoinColumn(name = "per_id", updatable = false, insertable = false)
    })
    private EvaluacionDesarrollo evaluacionDesarrollo;

    public AdjuntoEvaluacionDesarrollo() {}

    public AdjuntoEvaluacionDesarrollo(String nom, EvaluacionDesarrollo evaluacionDesarrollo) {
        this.nom = nom;
        this.evaluacionDesarrollo = evaluacionDesarrollo;
        this.persona = this.evaluacionDesarrollo.getPersona();
        this.desarrollo = this.evaluacionDesarrollo.getDesarrollo();
    }

    public int getAdjEvaDesId() {
        return adjEvaDesId;
    }

    public void setAdjEvaDesId(int adjEvaDesId) {
        this.adjEvaDesId = adjEvaDesId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public EvaluacionDesarrollo getEvaluacionDesarrollo() {
        return evaluacionDesarrollo;
    }

    public void setEvaluacionDesarrollo(EvaluacionDesarrollo evaluacionDesarrollo) {
        this.evaluacionDesarrollo = evaluacionDesarrollo;
    }

    public DesarrolloTemaCapacitacion getDesarrollo() {
        return desarrollo;
    }

    public void setDesarrollo(DesarrolloTemaCapacitacion desarrollo) {
        this.desarrollo = desarrollo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
