package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSessionPersona;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.bandeja_mensaje" )
public class BandejaMensaje  implements java.io.Serializable {

    @Id
    @Column(name="ban_men_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_bandejamensaje", sequenceName="pedagogico.bandeja_mensaje_ban_men_id_seq" )
    @GeneratedValue(generator="secuencia_bandejamensaje")
    private int banMenId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_vis", nullable=false, length=29)
    private Date fecVis;    
    
    @Column(name="men_ele_id")
    private int menEleId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="men_ele_id",insertable = false,updatable = false)
    private MensajeElectronico mensaje;
    
    @Column(name="est_men")
    private char estado;
    
    @Column(name="usu_ses_id")
    private int usuSesId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_ses_id",insertable = false,updatable = false)
    private UsuarioSessionPersona session;

    public BandejaMensaje() {
    }
    public BandejaMensaje(int banMenId) {
        this.banMenId = banMenId;
    }
    public BandejaMensaje(int banMenId,Date fecVis,char estado,int menEleId,int usuSesId) {
       this.banMenId = banMenId;
       this.fecVis = fecVis;
       this.estado = estado;
       this.menEleId = menEleId;
       this.usuSesId = usuSesId;
    }
   
     
    public int getBanMenId() {
        return this.banMenId;
    }    
    public void setBanmenId(int banMenId) {
        this.banMenId = banMenId;
    }
    
    public Date getFecVis() {
        return this.fecVis;
    }
    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }
    
    public int getMenEleId() {
        return this.menEleId;
    }    
    public void setMenEleId(int menEleId) {
        this.menEleId = menEleId;
    }
    
    public MensajeElectronico getMensaje() {
        return this.mensaje;
    }
    public void setMensaje(MensajeElectronico mensaje) {
        this.mensaje = mensaje;
    }
    
    public char getEstado(){
        return this.estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
    
    public int getUsuSesId() {
        return this.usuSesId;
    }    
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }
    
    public UsuarioSessionPersona getSession() {
        return this.session;
    }    
    public void setSession(UsuarioSessionPersona session) {
        this.session = session;
    }
    
}


