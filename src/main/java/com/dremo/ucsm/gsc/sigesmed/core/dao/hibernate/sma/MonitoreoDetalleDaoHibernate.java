/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import org.hibernate.Session;

/**
 *
 * @author gscadmin
 */
public class MonitoreoDetalleDaoHibernate extends GenericDaoHibernate<MonitoreoDetalle> implements MonitoreoDetalleDao{
    @Override
    public MonitoreoDetalle buscarPorId(Integer monDetId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        MonitoreoDetalle a = (MonitoreoDetalle)session.get(MonitoreoDetalle.class, monDetId);
        session.close();
        return a;
    }
}
