/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author abel
 */
public class ModuloSistemaDaoHibernate extends GenericDaoHibernate<ModuloSistema> implements ModuloSistemaDao {
    
    @Override
    public List<ModuloSistema> listarConSubModulos() {
        List<ModuloSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Modulos con sus Submodulos
            String hql = "SELECT DISTINCT m FROM ModuloSistema m LEFT JOIN FETCH m.subModuloSistemas sm WHERE m.estReg='A' and ( sm.estReg!='E' or sm IS NULL )";
                
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los modulos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar llos modulos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }

}
