/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.NacionalidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Nacionalidad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 
 */
public class ListarNacionalidadTx implements ITransaction {
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        List<Nacionalidad> nacionalidades = null;
        NacionalidadDao nacionalidadesDao = (NacionalidadDao)FactoryDao.buildDao("se.NacionalidadDao");
        
        try{
            nacionalidades = nacionalidadesDao.listar(Nacionalidad.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Nacionalidades\n "+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Nacionalidades ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Nacionalidad nac:nacionalidades ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("nacId",nac.getNacId());
            oResponse.put("nacNom",nac.getNacNom());                       
            miArray.put(oResponse);
        }
        System.out.println("||||||||"+miArray);
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray); 
    }
}
