/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

/**
 *
 * @author abel
 */
public class Mensaje {
      
    public static final String BANDEJA_MENSAJE_PATH = "/mensaje/";
    
    public static final char ESTADO_NUEVO = 'N';    //sirve par notificar al usuario que tiene nuevos menssajes en su bandeja
    public static final char ESTADO_VISTO = 'V';    //mensajes que no ha sido leidos por el usuario
    public static final char ESTADO_SIN_VER = 'S';  //mensajes que fueron leidos
    public static final char ESTADO_REENVIADO = 'R';//mensajes reenviados
    public static final char ESTADO_ELIMINADO = 'E';//mensajes eliminados
    
}
