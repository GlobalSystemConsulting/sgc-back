package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;

import java.util.List;

/**
 * Created by Administrador on 19/10/2016.
 */
public interface AreaCurricularDao extends GenericDao<AreaCurricular> {
    AreaCurricular buscarPorId(int id);
    List<AreaCurricular> buscarAreasConCurricula();
}
