package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarEvaluacionTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EliminarEvaluacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionCursoCapacitacion evaluacion = evaluacionCursoCapacitacionDao.buscarPorId(data.getInt("id"));
            evaluacion.setEstReg('E');
            evaluacionCursoCapacitacionDao.update(evaluacion);
            
            return WebResponse.crearWebResponseExito("Exito al eliminar la evaluacion de la capacitación", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "eliminarEvaluacion", e);
            return WebResponse.crearWebResponseError("Error al eliminar la evaluacion de la capacitación", WebResponse.BAD_RESPONSE);
        }        
    }
}
