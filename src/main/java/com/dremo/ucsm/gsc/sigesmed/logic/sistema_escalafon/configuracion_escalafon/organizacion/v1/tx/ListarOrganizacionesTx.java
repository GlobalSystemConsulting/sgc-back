/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarOrganizacionesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarOrganizacionesTx.class.getName());
    
    @Override
        public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Organizacion> organizaciones = null;
        OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("se.OrganizacionDao");
        
        try{
            organizaciones = organizacionDao.listarxOrganizacion();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar organizaciones",e);
            System.out.println("No se pudo listar las organizaciones.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas organizaciones.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Organizacion o: organizaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("orgId", o.getOrgId());
            oResponse.put("cod", o.getCod());
            oResponse.put("nom", o.getNom());
            oResponse.put("ali", o.getAli());
            oResponse.put("fecMod", o.getFecMod());
            oResponse.put("usuMod", o.getUsuMod());
            oResponse.put("estReg", o.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las organizaciones fueron listadas exitosamente", miArray);
    }
    
}
