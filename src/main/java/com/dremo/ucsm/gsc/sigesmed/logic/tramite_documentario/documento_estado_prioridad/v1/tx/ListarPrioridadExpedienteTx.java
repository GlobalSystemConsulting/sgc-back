/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.PrioridadExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.PrioridadExpediente;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarPrioridadExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<PrioridadExpediente> prioridadExpedientes = null;
        PrioridadExpedienteDao prioridadDao = (PrioridadExpedienteDao)FactoryDao.buildDao("std.PrioridadExpedienteDao");
        try{
            prioridadExpedientes = prioridadDao.buscarTodos(PrioridadExpediente.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Prioridades\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Prioridades", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(PrioridadExpediente prioridad:prioridadExpedientes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("prioridadExpedienteID",prioridad.getPriExpId() );
            oResponse.put("nombre",prioridad.getNom());
            oResponse.put("descripcion",prioridad.getDes());
            oResponse.put("fecha",prioridad.getFecMod().toString());
            oResponse.put("estado",""+prioridad.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

