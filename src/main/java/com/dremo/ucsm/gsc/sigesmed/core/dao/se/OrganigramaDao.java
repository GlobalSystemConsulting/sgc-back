/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import java.util.List;

/**
 *
 * @author felipe
 */
public interface OrganigramaDao extends GenericDao<Organigrama>{
    public List<Organigrama> listarXOrganigrama();
    public List<Organigrama> listarXCampo(String campo);
    public List<Organigrama> listarXCodigo(String codigo);
    public Organigrama buscarXId(Integer orgiId);
    public List<Organigrama> listarxTipo(int tipoOrganismoId);
    public Organigrama buscarXCodigo(Integer codigo);
    public Organigrama buscarXCodigo(String codigo);
    public Organigrama obtenerDetalle(int orgiId);
}
