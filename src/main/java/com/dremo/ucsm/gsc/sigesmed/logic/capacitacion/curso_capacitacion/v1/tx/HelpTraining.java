package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class HelpTraining {

    public static String attachments_Development = "capacitacion_desarrollo_adjuntos";
    public static String attachments_D_Address = File.separator + HelpTraining.attachments_Development + File.separator;
    public static String attachments_Evaluation_Development = "capacitacion_evaluacion_desarrollo_adjuntos";
    public static String attachments_ED_Address = File.separator + HelpTraining.attachments_Evaluation_Development + File.separator;
    public static String attachments_Comment = "capacitacion_comentarios_adjuntos";
    public static String attachments_C_Address = File.separator + HelpTraining.attachments_Comment + File.separator;
    public static String attachments_Justification = "capacitacion_justificacion_inasistencia";
    public static String attachments_J_Address = File.separator + HelpTraining.attachments_Justification + File.separator;
    
    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        
        return calendar.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
    }
    
    public static Date getStartOfTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
    }
}
