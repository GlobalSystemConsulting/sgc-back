/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

/**
 *
 * @author Administrador
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarConsultaFrecuenteTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarConsultaFrecuenteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        ConsultaCertificada consultaCertificada = null;
        
        
        try {
            /*    
                CREATE DATA OBJECT
            */
            JSONObject data = (JSONObject) wr.getData();
            String atributos = data.optString("atributosUsados");
            String catalogo = data.optString("catalogo");
            String logica = data.optString("logica");
            String filtros = data.optString("filtros");
            String titulo = data.optString("titulo");
            String observaciones = data.optString("observaciones");
            Date fecEmision = new Date();
            int idUsuEmisor = data.optInt("idUsuarioEmisor");
            String usuEmisor = data.optString("usuarioEmisor");
            Date fecCertificacion = null;
            Character estReg = 'F';
            String consultaSql = data.optString("consultaSql");//Es necesario agregar la consulta sql
            
            consultaCertificada = new ConsultaCertificada( atributos, catalogo, logica, filtros, titulo, observaciones, fecEmision, idUsuEmisor, usuEmisor, fecCertificacion, estReg, consultaSql);
            ConsultaCertificadaDao consultaCertificadaDao = (ConsultaCertificadaDao) FactoryDao.buildDao("ConsultaCertificadaDao");
            consultaCertificadaDao.insert(consultaCertificada);
            ///////////AUDITORIA/////////////
            //AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego una consulta Frecuente", "Titulo Consulta Frecuente: "+titulo, idUsuEmisor , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar la consulta frecuente",e);
            System.out.println(e);
        }
        
        //Fin       
        return WebResponse.crearWebResponseExito("Registro de consulta frecuente EXITO: ");
        //Fin
    }
    
}
