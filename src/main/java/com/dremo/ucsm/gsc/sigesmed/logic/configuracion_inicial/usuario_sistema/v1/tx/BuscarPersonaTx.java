/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class BuscarPersonaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String dni = "";
        Long usuarioID = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dni = requestData.getString("dni");
            usuarioID = requestData.optLong("usuarioID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("El dni ingresado es incorrecto" );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Persona persona = null;
        try{
            if( usuarioID > 0 )
                persona = ((PersonaDao)FactoryDao.buildDao("PersonaDao")).buscarPorID(usuarioID.intValue());
            else                
                persona = ((PersonaDao)FactoryDao.buildDao("PersonaDao")).buscarPorDNI(dni);
        
        }catch(Exception e){
            System.out.println("\n"+e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage() );
        }
        
        //validar el usuario
        if(persona != null ){
            
            JSONObject oRes = new JSONObject();
            
            JSONObject oPersona = new JSONObject();
            
            oPersona.put("personaID", persona.getPerId());
            oPersona.put("dni", ""+persona.getDni());
            oPersona.put("materno", persona.getApeMat());
            oPersona.put("paterno", persona.getApePat());
            oPersona.put("nombre", persona.getNom());
            oPersona.put("email", persona.getEmail());
            oPersona.put("numero1", persona.getNum1());
            oPersona.put("numero2", persona.getNum2());
            oPersona.put("existe", true);
            
            oRes.put("persona", oPersona);
            
            Usuario usuario = persona.getUsuario();
            JSONObject oUsuario = new JSONObject();
            if( usuario!=null){
                oUsuario.put("usuarioID", usuario.getUsuId());
                oUsuario.put("nombre", usuario.getNom());
                oUsuario.put("password", usuario.getPas());
                oUsuario.put("estado", ""+usuario.getEstReg());
                oUsuario.put("existe", true);
                
                oRes.put("usuario", oUsuario);
            }
            
            return WebResponse.crearWebResponseExito("El usuario ya se encuentra registrado", oRes);
        }else{
            return WebResponse.crearWebResponseError("El usuario no esta registrado en el sistema");
        }
    }
    
}
