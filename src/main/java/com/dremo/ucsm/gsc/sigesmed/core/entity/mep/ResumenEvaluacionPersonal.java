package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * @author geank
 */
@Entity
@Table(name="resumen_evaluacion_personal",schema="pedagogico")
@DynamicUpdate(value=true)
public class ResumenEvaluacionPersonal implements java.io.Serializable {


    @Id
    @Column(name="res_eva_per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_resumen_evaluacion_personal", sequenceName="pedagogico.resumen_evaluacion_personal_res_eva_per_id_seq" )
    @GeneratedValue(generator="secuencia_resumen_evaluacion_personal")
    private int resEvaPerId;
    @Temporal(TemporalType.DATE)
    @Column(name="fec_eva", nullable=false, length=13)
    private Date fecEva;
    @Column(name="pun", nullable=false, precision=5 ,scale = 2)
    private BigDecimal pun;
    @Column(name="eta", nullable=false, length=1)
    private char eta;
    @Column(name="res", nullable=false)
    private String res;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="est_reg", length=1)
    private Character estReg;
    @Column(name="es_com")
    private Boolean esCom;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tra_id")
    private Trabajador trabajador;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="resumenEvaluacionPersonal")
    private Set<IndicadoresEvaluarPersonal> indicadoresEvaluarPersonals = new HashSet(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resEvaPer", cascade = CascadeType.PERSIST)
    //@Fetch(FetchMode.JOIN)
    private Set<DetalleResumenEvaluacionPersonal> detallesResumen = new HashSet<>(0);
    public ResumenEvaluacionPersonal() {
    }

	
    public ResumenEvaluacionPersonal(Date fecEva, BigDecimal pun, char eta, String res) {
        this.fecEva = fecEva;
        this.pun = pun;
        this.eta = eta;
        this.res = res;
    }
    public ResumenEvaluacionPersonal(int resEvaPerId, Date fecEva, BigDecimal pun, char eta, String res, Integer usuMod, Date fecMod, Character estReg, Set indicadoresEvaluarPersonals) {
       this.resEvaPerId = resEvaPerId;
       this.fecEva = fecEva;
       this.pun = pun;
       this.eta = eta;
       this.res = res;
       this.usuMod = usuMod;
       this.fecMod = fecMod;
       this.estReg = estReg;
       this.indicadoresEvaluarPersonals = indicadoresEvaluarPersonals;
    }

    public int getResEvaPerId() {
        return resEvaPerId;
    }

    public void setResEvaPerId(int resEvaPerId) {
        this.resEvaPerId = resEvaPerId;
    }

    public void setresEvaPerId(int resEvaPerId) {
        this.resEvaPerId = resEvaPerId;
    }
    
    public Date getFecEva() {
        return this.fecEva;
    }
    
    public void setFecEva(Date fecEva) {
        this.fecEva = fecEva;
    }

    public BigDecimal getPun() {
        return this.pun;
    }
    
    public void setPun(BigDecimal pun) {
        this.pun = pun;
    }

    public char getEta() {
        return this.eta;
    }
    
    public void setEta(char eta) {
        this.eta = eta;
    }

    public String getRes() {
        return this.res;
    }
    
    public void setRes(String res) {
        this.res = res;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    public Set<IndicadoresEvaluarPersonal> getIndicadoresEvaluarPersonals() {
        return this.indicadoresEvaluarPersonals;
    }
    
    public void setIndicadoresEvaluarPersonals(Set<IndicadoresEvaluarPersonal> indicadoresEvaluarPersonals) {
        this.indicadoresEvaluarPersonals = indicadoresEvaluarPersonals;
    }

    public Boolean getEsCom() {
        return esCom;
    }

    public void setEsCom(Boolean esCom) {
        this.esCom = esCom;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Set<DetalleResumenEvaluacionPersonal> getDetallesResumen() {
        return detallesResumen;
    }

    public void setDetallesResumen(Set<DetalleResumenEvaluacionPersonal> detallesResumen) {
        this.detallesResumen = detallesResumen;
    }
    @Override
    public String toString (){
        return EntityUtil.objectToJSONString(new String[]{"resEvaPerId","fecEva","eta","pun","res"},new String[]{"det","fec","eta","pun","res"},this);
    }
}


