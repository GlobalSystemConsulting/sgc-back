/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Objetivos;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface ObjetivosDao extends GenericDao<Objetivos>{
    public String buscarUltimoCodigo();
    public List<Objetivos> listar(int iteide, char tipo);
}
