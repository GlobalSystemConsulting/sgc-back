/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "estudio_complementario", schema="administrativo")
public class EstudioComplementario implements Serializable {
    @Id
    @Column(name = "est_com_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_estudio_complementario", sequenceName="administrativo.estudio_complementario_est_com_id_seq" )
    @GeneratedValue(generator="secuencia_estudio_complementario")
    private Integer estComId;
    
    @Column(name = "tip")
    private Character tip;
    
    @Column(name = "tip_id")
    private Integer tipId;
    
    @Column(name = "des")
    private String des;
    
    @Column(name = "niv")
    private Character niv;
    
    @Column(name = "niv_id")
    private Integer nivId;
    
    @Column(name = "int_cer")
    private String insCer;
    
    @Column(name = "tip_par")
    private String tipPar;
    
    @Column(name = "pais")
    private String pais;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_ter")
    @Temporal(TemporalType.DATE)
    private Date fecTer;
    
    @Column(name = "hor_lec")
    private Integer horLec;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public EstudioComplementario() {
    }

    public EstudioComplementario(Integer estComId) {
        this.estComId = estComId;
    }
    
    public EstudioComplementario(Persona persona, Integer tipId, String des, Integer nivId, String insCer, String tipPar, Date fecIni, Date fecTer, Integer horLec, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.tipId = tipId;
        this.des = des;
        this.nivId = nivId;
        this.insCer = insCer;
        this.tipPar = tipPar;
        this.insCer = insCer;
        this.tipPar = tipPar;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.horLec = horLec;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getEstComId() {
        return estComId;
    }

    public void setEstComId(Integer estComId) {
        this.estComId = estComId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Integer getTipId() {
        return tipId;
    }

    public void setTipId(Integer tipId) {
        this.tipId = tipId;
    }

    public Integer getNivId() {
        return nivId;
    }

    public void setNivId(Integer nivId) {
        this.nivId = nivId;
    }
    
    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
    
    public Character getNiv() {
        return niv;
    }

    public void setNiv(Character niv) {
        this.niv = niv;
    }

    public String getInsCer() {
        return insCer;
    }

    public void setInsCer(String insCer) {
        this.insCer = insCer;
    }

    public String getTipPar() {
        return tipPar;
    }

    public void setTipPar(String tipPar) {
        this.tipPar = tipPar;
    }
    
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecTer() {
        return fecTer;
    }

    public void setFecTer(Date fecTer) {
        this.fecTer = fecTer;
    }

    public Integer getHorLec() {
        return horLec;
    }

    public void setHorLec(Integer horLec) {
        this.horLec = horLec;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estComId != null ? estComId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudioComplementario)) {
            return false;
        }
        EstudioComplementario other = (EstudioComplementario) object;
        if ((this.estComId == null && other.estComId != null) || (this.estComId != null && !this.estComId.equals(other.estComId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EstudioComplementario{" + "estComId=" + estComId + ", tip=" + tip + ", des=" + des + ", niv=" + niv + ", insCer=" + insCer + ", tipPar=" + tipPar + ", fecIni=" + fecIni + ", fecTer=" + fecTer + ", horLec=" + horLec + ", estReg=" + estReg + '}';
    }

    

}
