/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface FormacionEducativaDao extends GenericDao<FormacionEducativa>{
    public List<FormacionEducativa> listarxFichaEscalafonaria(int ficEscId);
    public FormacionEducativa buscarForEduPorId(Integer forEduId);
}
