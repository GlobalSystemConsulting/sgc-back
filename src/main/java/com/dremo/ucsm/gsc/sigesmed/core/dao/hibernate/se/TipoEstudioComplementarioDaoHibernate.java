/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoEstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoEstudioComplementario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author alex
 */
public class TipoEstudioComplementarioDaoHibernate extends GenericDaoHibernate<TipoEstudioComplementario> implements TipoEstudioComplementarioDao{
    @Override
    public List<TipoEstudioComplementario> listarAll() {
        List<TipoEstudioComplementario> tiposEstCom = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT t from TipoEstudioComplementario as t "
                    + "WHERE t.estReg='A'";
            Query query = session.createQuery(hql);
            tiposEstCom = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los tipos de estudio complementario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los tipos de estudio complementario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return tiposEstCom;
    }

    @Override
    public TipoEstudioComplementario buscarPorId(Integer tipEstComId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoEstudioComplementario des = (TipoEstudioComplementario)session.get(TipoEstudioComplementario.class, tipEstComId);
        session.close();
        return des;
    }
}
