/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class ExposicionDaoHibernate extends GenericDaoHibernate<Exposicion> implements ExposicionDao{

    @Override
    public List<Exposicion> listarxFichaEscalafonaria(int perId) {
        List<Exposicion> expPon = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT expPon from Exposicion as expPon "
                    + "join fetch expPon.persona as fe "
                    + "WHERE fe.perId=" + perId + " AND expPon.estReg='A'";
            Query query = session.createQuery(hql);
            expPon = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las exposiciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las exposiciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return expPon;
    }

    @Override
    public Exposicion buscarPorId(Integer expId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Exposicion exp = (Exposicion)session.get(Exposicion.class, expId);
        session.close();
        return exp;
    }
    
}
