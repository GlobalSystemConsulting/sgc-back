package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import java.util.List;

public interface DesarrolloTemaCapacitacionDao extends GenericDao<DesarrolloTemaCapacitacion> {
    List<DesarrolloTemaCapacitacion> buscarPorTema(int temCod);
    DesarrolloTemaCapacitacion buscarPorId(int desCod);
    int totalEvaluaciones(int codSed);
    int totalTareas(int codSed);
}
