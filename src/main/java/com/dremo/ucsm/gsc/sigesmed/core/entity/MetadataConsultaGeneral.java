/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="consulta_frecuente_metadata" ,schema="public" )
public class MetadataConsultaGeneral implements java.io.Serializable {
    @Id
    @Column(name="id_met", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_consulta_frecuente_metadata", sequenceName="public.consulta_frecuente_metadata_id_met_seq" )
    @GeneratedValue(generator="secuencia_consulta_frecuente_metadata")
    private int id;
    @Column(name="entity_name")
    private String nombreTabla;
    @Column(name="atrib_name")
    private String nombreAtributo;
    @Column(name="alias_name")
    private String aliasAtributo;
    @Column(name="related_function")
    private String funcionRelacionada;
    @Column(name="data_type")
    private String tipoDato;
    @Column(name="mod")
    private String modulo;
    @Column(name="est")
    private Character estReg;
    
    /****************constructors********************/
    public MetadataConsultaGeneral() {
    }

    public MetadataConsultaGeneral(String nombreTabla, String nombreAtributo, String aliasAtributo, String funcionRelacionada, String tipoDato, String modulo, Character estReg) {
        this.nombreTabla = nombreTabla;
        this.nombreAtributo = nombreAtributo;
        this.aliasAtributo = aliasAtributo;
        this.funcionRelacionada = funcionRelacionada;
        this.tipoDato = tipoDato;
        this.modulo = modulo;
        this.estReg = estReg;
    }

    public MetadataConsultaGeneral(int id) {
        this.id = id;
    }
    
    /************GETTERS*************/
    public int getId() {
        return id;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public String getNombreAtributo() {
        return nombreAtributo;
    }

    public String getAliasAtributo() {
        return aliasAtributo;
    }

    public String getFuncionRelacionada() {
        return funcionRelacionada;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public String getModulo() {
        return modulo;
    }

    public Character getEstReg() {
        return estReg;
    }

    /****************SETTERS*************/
    
    
    public void setId(int id) {
        this.id = id;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public void setNombreAtributo(String nombreAtributo) {
        this.nombreAtributo = nombreAtributo;
    }

    public void setAliasAtributo(String aliasAtributo) {
        this.aliasAtributo = aliasAtributo;
    }

    public void setFuncionRelacionada(String funcionRelacionada) {
        this.funcionRelacionada = funcionRelacionada;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
}
