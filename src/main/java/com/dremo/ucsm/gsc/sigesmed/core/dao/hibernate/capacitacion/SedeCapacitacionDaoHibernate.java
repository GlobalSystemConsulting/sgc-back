package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;

public class SedeCapacitacionDaoHibernate extends GenericDaoHibernate<SedeCapacitacion> implements SedeCapacitacionDao{
    private static final Logger logger = Logger.getLogger(SedeCapacitacionDaoHibernate.class.getName());
    @Override
    public SedeCapacitacion buscarSedePorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            return (SedeCapacitacion) session.get(SedeCapacitacion.class,id);
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarSedePorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SedeCapacitacion buscarConCronograma(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SedeCapacitacion.class)
                    .setFetchMode("cronograma", FetchMode.JOIN)
                    .add(Restrictions.eq("sedCapId",id));
            return (SedeCapacitacion)query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarConCronograma",e);
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public SedeCapacitacion buscarConCronogramaHorario(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SedeCapacitacion.class)
                    .setFetchMode("cronograma", FetchMode.JOIN)
                    .setFetchMode("horarioSede", FetchMode.JOIN)
                    .add(Restrictions.eq("sedCapId",id));
            return (SedeCapacitacion)query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarConCronograma",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SedeCapacitacion> buscarSedesPorCapacitacion(int idCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SedeCapacitacion.class)
                    .createCriteria("cursoCapacitacion").add(Restrictions.eq("curCapId",idCap));
            
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarSedesPorCapacitacion",e);
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public SedeCapacitacion buscarSedeRegPar(int id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SedeCapacitacion.class)
                    .setFetchMode("cronograma", FetchMode.JOIN)
                    .setFetchMode("horarios", FetchMode.JOIN)
                    .setFetchMode("cursoCapacitacion", FetchMode.JOIN)
                    .add(Restrictions.eq("sedCapId", id));
            return (SedeCapacitacion)query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarConCronograma",e);
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public SedeCapacitacion buscarSedeReporte(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {            
            String hql = "SELECT s FROM SedeCapacitacion s "
                            + "LEFT OUTER JOIN FETCH s.cronograma "
                            + "LEFT OUTER JOIN FETCH s.horarios h "
                            + "LEFT OUTER JOIN FETCH s.capacitadoresCursoCapacitacion u "
                            + "LEFT OUTER JOIN FETCH u.per "
                            + "WHERE s.sedCapId = :sedCapId "
                            + "ORDER BY h.dia";
            
            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", codSed);

            return (SedeCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarSedeResporte", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
