package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "motivo_incumplimiento_criterio", schema = "pedagogico")
public class MotivoIncumplimiento implements Serializable {
    
    @Id
    @Column(name = "mot_inc_cri_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_motivo_incumplimiento_criterio", sequenceName = "pedagogico.motivo_incumplimiento_criterio_mot_inc_cri_id_seq")
    @GeneratedValue(generator = "secuencia_motivo_incumplimiento_criterio")
    private int motIncCriId;
    
    @Column(name = "des")
    private String des;
    
    @Column(name = "jus")
    private String jus;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "crit_cer_cap_id")
    private CriterioCertificacion criterio;

    public MotivoIncumplimiento() {}

    public MotivoIncumplimiento(String des, String jus, CriterioCertificacion criterio) {
        this.des = des;
        this.jus = jus;
        this.criterio = criterio;
    }

    public int getMotIncCriId() {
        return motIncCriId;
    }

    public void setMotIncCriId(int motIncCriId) {
        this.motIncCriId = motIncCriId;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getJus() {
        return jus;
    }

    public void setJus(String jus) {
        this.jus = jus;
    }

    public CriterioCertificacion getCriterio() {
        return criterio;
    }

    public void setCriterio(CriterioCertificacion criterio) {
        this.criterio = criterio;
    }
}
