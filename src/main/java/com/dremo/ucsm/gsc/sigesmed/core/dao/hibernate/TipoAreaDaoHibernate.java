/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoAreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;

/**
 *
 * @author abel
 */
public class TipoAreaDaoHibernate extends GenericDaoHibernate<TipoArea> implements TipoAreaDao {

}
