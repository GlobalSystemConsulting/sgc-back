/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

/**
 *
 * @author abel
 */
public class EntidadCantidadModel {
    public int ID;
    public String nombre;
    public long num1;
    public long num2;
    public long num3;
    public long num4;
    
    public EntidadCantidadModel(int ID, String nombre, long num1){
        this.ID = ID;
        this.nombre = nombre;
        this.num1 = num1;
    }
    public EntidadCantidadModel(int ID, String nombre,long num1, long num2){
        this.ID = ID;
        this.nombre = nombre;
        this.num1 = num1;
        this.num2 = num2;
    }
    public EntidadCantidadModel(int ID, String nombre,long num1, long num2, long num3){
        this.ID = ID;
        this.nombre = nombre;
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
    }
    public EntidadCantidadModel(int ID, String nombre,long num1, long num2, long num3, long num4){
        this.ID = ID;
        this.nombre = nombre;
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
        this.num4 = num4;
    }
}
