/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sdc;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.PropiedadLetra;


import java.util.List;

/**
 *
 * @author abel
 */
public interface PlantillaDao extends GenericDao<Plantilla>{
    
    public Plantilla buscarPorID(int clave);
   
    public List<Plantilla> listarPlantillas(Organizacion organizacion);
    public List<ImagenPlantilla> listarImagenes(Plantilla plantilla);
    public Boolean registrarPlantilla(Integer plantillaId);
    public Boolean eliminarPlantilla(Integer plantillaId);
    public Boolean plantillaEstado2editar(Integer plantillaId);
    public List<PropiedadLetra> listarNombresLetras();
    public Integer buscarUltimoCodigo();
    
}
