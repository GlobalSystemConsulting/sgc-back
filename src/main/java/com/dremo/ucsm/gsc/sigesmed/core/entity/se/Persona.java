/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */

@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.se.Persona")
@Table(name = "persona", schema= "pedagogico")
public class Persona implements Serializable {

    @Id
    @Column(name="per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_persona", sequenceName="pedagogico.persona_per_id_seq" )
    @GeneratedValue(generator="secuencia_persona")
    private Integer perId;
    
    @Column(name = "per_cod")
    private String perCod;
    
    @Column(name = "ape_mat", nullable=false, length=60)
    private String apeMat;
    
    @Column(name = "ape_pat", nullable=false, length=60)
    private String apePat;
    
    @Column(name = "nom", nullable=false, length=60)
    private String nom;
    
    @Column(name = "fec_nac", nullable=false)
    @Temporal(TemporalType.DATE)
    private Date fecNac;
    
    @Column(name = "dni", nullable=false, length=8)
    private String dni;
    
    @Column(name = "email", length=60)
    private String email;
    
    @Column(name = "num_1", length=10)
    private String num1;
    
    @Column(name = "num_2", length=10)
    private String num2;
    
    @Column(name = "fij", length=10)
    private String fij;
    
    @Column(name = "per_dir")
    private String perDir;
    
    @Column(name = "sex")
    private Character sex;
    
    @Column(name = "est_civ")
    private Character estCiv;
    
    @Column(name = "est_civ_id")
    private Integer estCivId;
    
    @Column(name = "dep_nac", length=2)
    private String depNac;
    
    @Column(name = "pro_nac", length=2)
    private String proNac;
    
    @Column(name = "dis_nac", length=2)
    private String disNac;
    
    @Column(name = "eda")
    private Integer eda;
    
    @Column(name = "nac")
    private String nac;
        
    @Column(name = "pas")
    private String pas;
    
    @Column(name = "lic_cond")
    private String licCond;
    
    @Column(name = "bon_caf")
    private String bonCaf;
    
    @Column(name = "idiom")
    private String idioma;  
    
    @Column(name = "ret_jud")
    private String retJud;
    
    @Column(name = "bool_salud")
    private Boolean boolSalud;
    
    @Column(name = "aut_ess", length=15)
    private String autEss;    
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "sis_pen")
    private String sisPen;

    @Column(name = "nom_afp")
    private String nomAfp;
    
    @Column(name = "cod_cuspp", length=12)
    private String codCuspp;
    
    @Column(name = "fec_ing_afp")
    @Temporal(TemporalType.DATE)
    private Date fecIngAfp;
    
    @Column(name = "per_dis")
    private Boolean perDis;
    
    @Column(name = "reg_con")
    private String regCon;

    @Column(name = "fec_tra_afp")
    @Temporal(TemporalType.DATE)
    private Date fecTraAfp;
    
    @Column(name = "tip_afp")
    private String tipAfp;

    @Column(name = "per_foto")
    private String foto;
    
    @Column(name = "dia_fec_nac")
    private Integer diaFecNac;
    
    @Column(name = "mes_fec_nac")
    private Integer mesFecNac;
    
    @Column(name = "anio_fec_nac")
    private Integer anioFecNac;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="per_id")
    private Usuario usuario;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Direccion> direcciones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Trabajador> trabajadores = new HashSet(0);
    
        @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Demerito> demeritos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<EstudioComplementario> estudiosComplementarios = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<EstudioPostgrado> estudiosMaestrias = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Exposicion> expocisiones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<FormacionEducativa> formacionesEducativas = new HashSet(0);
      
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Publicacion> publicaciones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Reconocimiento> reconocimientos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Colegiatura> colegiaturas = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "persona")
    Set<Capacitacion> capacitaciones = new HashSet(0);
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="nac_id", nullable=false)
    private Nacionalidad nacionalidad;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idi_id", nullable=false)
    private Idioma idiomas;
    
    public Persona() {
    }

    public Persona(Integer perId) {
        this.perId = perId;
    }

    //Constructor para insertar un trabajador
    public Persona(String apePat, String apeMat,  String nom, String dni, Date fecNac, String num1, String num2, String fijo, String email, Character sex, Character estCiv, String depNac, String proNac, String disNac, Integer usuMod, Date fecMod, Character estReg) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.dni = dni;
        this.fecNac = fecNac;
        this.num1 = num1;
        this.num2 = num2;
        this.fij = fijo;
        this.email = email;
        this.sex = sex;
        this.estCiv = estCiv;
        this.depNac = depNac;
        this.proNac = proNac;
        this.disNac = disNac;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    public Persona(String apePat, String apeMat,  String nom, String dni, Date fecNac, String num1, String num2, String fijo, String email, Character sex, Integer estCivId, String depNac, String proNac, String disNac, Integer usuMod, Date fecMod, Character estReg) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.dni = dni;
        this.fecNac = fecNac;
        this.num1 = num1;
        this.num2 = num2;
        this.fij = fijo;
        this.email = email;
        this.sex = sex;
        this.estCivId = estCivId;
        this.depNac = depNac;
        this.proNac = proNac;
        this.disNac = disNac;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    
    //Constructor para insertar un pariente
    public Persona(String apePat, String apeMat, String nom, String dni, Date fecNac, String num1, String num2, String fijo, String email, Character sex, Integer usuMod, Date fecMod, Character estReg) {
        this.apePat = apePat;
        this.apeMat = apeMat;
        this.nom = nom;
        this.dni = dni;
        this.fecNac = fecNac;
        this.num1 = num1;
        this.num2 = num2;
        this.fij = fijo;
        this.email = email;
        this.sex = sex;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPerCod() {
        return perCod;
    }

    public void setPerCod(String perCod) {
        this.perCod = perCod;
    }
    
    public String getLicCond() {
        return licCond;
    }

    public void setLicCond(String licCond) {
        this.licCond = licCond;
    }
    
    public String getBonCaf() {
        return bonCaf;
    }

    public void setBonCaf(String bonCaf) {
        this.bonCaf = bonCaf;
    }
    
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
    
    public String getIdioma() {
        return idioma;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    // Only for relatives
    public void setRetJud(String retJud) {
        this.retJud = retJud;
    }
    
    public String getRetJud() {
        return retJud;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public Integer getDiaFecNac() {
        return diaFecNac;
    }

    public void setDiaFecNac(Integer diaFecNac) {
        this.diaFecNac = diaFecNac;
    }

    public Integer getMesFecNac() {
        return mesFecNac;
    }

    public void setMesFecNac(Integer mesFecNac) {
        this.mesFecNac = mesFecNac;
    }

    public Integer getAnioFecNac() {
        return anioFecNac;
    }

    public void setAnioFecNac(Integer anioFecNac) {
        this.anioFecNac = anioFecNac;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getFij() {
        return fij;
    }

    public void setFij(String fij) {
        this.fij = fij;
    }

    public String getPerDir() {
        return perDir;
    }

    public void setPerDir(String perDir) {
        this.perDir = perDir;
    }

    public Character getSex() {
        return sex;
    }

    public void setSex(Character sex) {
        this.sex = sex;
    }
    
    public Integer getEstCivId() {
        return estCivId;
    }

    public void setEstCivId(Integer estCivId) {
        this.estCivId = estCivId;
    }
    
    public Character getEstCiv() {
        return estCiv;
    }

    public void setEstCiv(Character estCiv) {
        this.estCiv = estCiv;
    }
    
    public String getDepNac() {
        return depNac;
    }

    public void setDepNac(String depNac) {
        this.depNac = depNac;
    }
    
    public String getProNac() {
        return proNac;
    }

    public void setProNac(String proNac) {
        this.proNac = proNac;
    }
    
    public String getDisNac() {
        return disNac;
    }

    public void setDisNac(String disNac) {
        this.disNac = disNac;
    }
    
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public Usuario getUsuario() {
        return this.usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getEda() {
        return eda;
    }

    public void setEda(Integer eda) {
        this.eda = eda;
    }

    public String getNac() {
        return nac;
    }

    public void setNac(String nac) {
        this.nac = nac;
    }

    public String getPas() {
        return pas;
    }

    public void setPas(String pas) {
        this.pas = pas;
    }

    public Set<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(Set<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    public Set<Direccion> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(Set<Direccion> direcciones) {
        this.direcciones = direcciones;
    }

    public Boolean getBoolSalud() {
        return boolSalud;
    }

    public void setBoolSalud(Boolean boolSalud) {
        this.boolSalud = boolSalud;
    }
    
    public String getAutEss() {
        return autEss;
    }

    public void setAutEss(String autEss) {
        this.autEss = autEss;
    }

    public String getSisPen() {
        return sisPen;
    }

    public void setSisPen(String sisPen) {
        this.sisPen = sisPen;
    }

    public String getNomAfp() {
        return nomAfp;
    }

    public void setNomAfp(String nomAfp) {
        this.nomAfp = nomAfp;
    }

    public String getCodCuspp() {
        return codCuspp;
    }

    public void setCodCuspp(String codCuspp) {
        this.codCuspp = codCuspp;
    }

    public Date getFecIngAfp() {
        return fecIngAfp;
    }

    public void setFecIngAfp(Date fecIngAfp) {
        this.fecIngAfp = fecIngAfp;
    }

    public Boolean getPerDis() {
        return perDis;
    }

    public void setPerDis(Boolean perDis) {
        this.perDis = perDis;
    }

    public String getRegCon() {
        return regCon;
    }

    public void setRegCon(String regCon) {
        this.regCon = regCon;
    }

    public Date getFecTraAfp() {
        return fecTraAfp;
    }

    public void setFecTraAfp(Date fecTraAfp) {
        this.fecTraAfp = fecTraAfp;
    }

    public String getTipAfp() {
        return tipAfp;
    }

    public void setTipAfp(String tipAfp) {
        this.tipAfp = tipAfp;
    }

    public Set<Demerito> getDemeritos() {
        return demeritos;
    }

    public void setDemeritos(Set<Demerito> demeritos) {
        this.demeritos = demeritos;
    }

    public Set<EstudioComplementario> getEstudiosComplementarios() {
        return estudiosComplementarios;
    }

    public void setEstudiosComplementarios(Set<EstudioComplementario> estudiosComplementarios) {
        this.estudiosComplementarios = estudiosComplementarios;
    }

    public Set<EstudioPostgrado> getEstudiosMaestrias() {
        return estudiosMaestrias;
    }

    public void setEstudiosMaestrias(Set<EstudioPostgrado> estudiosMaestrias) {
        this.estudiosMaestrias = estudiosMaestrias;
    }

    public Set<Exposicion> getExpocisiones() {
        return expocisiones;
    }

    public void setExpocisiones(Set<Exposicion> expocisiones) {
        this.expocisiones = expocisiones;
    }

    public Set<FormacionEducativa> getFormacionesEducativas() {
        return formacionesEducativas;
    }

    public void setFormacionesEducativas(Set<FormacionEducativa> formacionesEducativas) {
        this.formacionesEducativas = formacionesEducativas;
    }

    public Set<Publicacion> getPublicaciones() {
        return publicaciones;
    }

    public void setPublicaciones(Set<Publicacion> publicaciones) {
        this.publicaciones = publicaciones;
    }

    public Set<Reconocimiento> getReconocimientos() {
        return reconocimientos;
    }

    public void setReconocimientos(Set<Reconocimiento> reconocimientos) {
        this.reconocimientos = reconocimientos;
    }

    public Set<Colegiatura> getColegiaturas() {
        return colegiaturas;
    }

    public void setColegiaturas(Set<Colegiatura> colegiaturas) {
        this.colegiaturas = colegiaturas;
    }

    public Set<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(Set<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    public Nacionalidad getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Nacionalidad nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Idioma getIdiomas() {
        return idiomas;
    }

    public void setIdiomas(Idioma idiomas) {
        this.idiomas = idiomas;
    }
    
    

    @Override
    public String toString() {
        return "Persona{" + "perId=" + perId + '}';
    }
    
}
