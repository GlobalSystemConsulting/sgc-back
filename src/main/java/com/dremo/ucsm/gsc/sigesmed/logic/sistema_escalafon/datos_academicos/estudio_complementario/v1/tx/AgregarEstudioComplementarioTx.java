/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarEstudioComplementarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(AgregarEstudioComplementarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        EstudioComplementario estCom = null;

        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            Integer tipId = requestData.getInt("tipId");
            String des = requestData.getString("des");
            Integer nivId = requestData.getInt("nivId");
            String insCer = requestData.getString("insCer");
            String tipPar = requestData.getString("tipPar");
            Date fecIni = requestData.getString("fecIni").equals("") ? null : sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = requestData.getString("fecTer").equals("") ? null : sdi.parse(requestData.getString("fecTer").substring(0, 10));
            Integer horLec = requestData.getInt("horLec");
            String pais = requestData.getString("pais");

            estCom = new EstudioComplementario(new Persona(perId), tipId, des, nivId, insCer, tipPar, fecIni, fecTer, horLec, wr.getIdUsuario(), new Date(), 'A');
            estCom.setPais(pais);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Datos nuevo estudio complementario", e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        EstudioComplementarioDao estComDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        try {
            estComDao.insert(estCom);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego estudio comple", "Detalle: " + perId + " ficha ", wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA///////////// 
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Agregar nuevo estudio complementario", e);
            System.out.println(e);
        }

        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estComId", estCom.getEstComId());
        oResponse.put("tipId", estCom.getTipId() == null ? 0 : estCom.getTipId());
        oResponse.put("des", estCom.getDes() == null ? "" : estCom.getDes());
        oResponse.put("nivId", estCom.getNivId() == null ? 0 : estCom.getNivId());
        oResponse.put("insCer", estCom.getInsCer() == null ? "" : estCom.getInsCer());
        oResponse.put("tipPar", estCom.getTipPar() == null ? "" : estCom.getTipPar());
        oResponse.put("fecIni", estCom.getFecIni() == null ? "" : sdi.format(estCom.getFecIni()));
        oResponse.put("fecTer", estCom.getFecTer() == null ? "" : sdi.format(estCom.getFecTer()));
        oResponse.put("horLec", estCom.getHorLec() == null ? "" : estCom.getHorLec());
        oResponse.put("pais", estCom.getPais() == null ? "" : estCom.getPais());

        return WebResponse.crearWebResponseExito("El registro del estudio complementario se realizo correctamente", oResponse);
        //Fin

    }

}
