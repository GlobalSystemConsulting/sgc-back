/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class AscensoDaoHibernate extends GenericDaoHibernate<Ascenso> implements AscensoDao{

    @Override
    public List<Ascenso> listarxFichaEscalafonaria(int ficEscId) {
        List<Ascenso> ascensos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT ascenso from Ascenso as ascenso "
                    + "join fetch ascenso.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND ascenso.estReg = 'A'";
            Query query = session.createQuery(hql);
            ascensos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los ascensos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los ascensos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return ascensos;
    }

    @Override
    public Ascenso buscarPorId(Integer ascId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Ascenso a = (Ascenso)session.get(Ascenso.class, ascId);
        session.close();
        return a;
    }
    
}
