package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 15/09/16.
 */
@Entity
@Table(name = "actas_reunion_comision", schema = "pedagogico")
@DynamicUpdate(value = true)
public class ActasReunionComision implements java.io.Serializable {
    @Id
    @Column(name = "act_com_id", nullable = false)
    @SequenceGenerator(name = "act_com_sequece", sequenceName = "pedagogico.actas_reunion_comision_act_com_id_seq")
    @GeneratedValue(generator = "act_com_sequece")
    private int actComId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reu_id")
    private ReunionComision reunionComision;

    @Column(name = "des")
    private String des;

    @Column(name = "arc_adj", length = 200)
    private String arcAdj;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_reg_act")
    private Date fecRegAct;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "actaReunion", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AcuerdosActaComision> acuerdos = new ArrayList<>();
    public ActasReunionComision() {
    }

    public ActasReunionComision(String des) {
        this.des = des;

        this.fecRegAct = new Date();
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ActasReunionComision(String des, ReunionComision reunionComision) {
        this.des = des;
        this.reunionComision = reunionComision;

        this.fecRegAct = new Date();
        this.fecMod = new Date();
        this.estReg = 'A';
    }


    public ActasReunionComision(ReunionComision reunionComision, Date fecRegAct) {
        this.reunionComision = reunionComision;
        this.fecRegAct = fecRegAct;
    }

    public ActasReunionComision(Date fecRegAct) {
        this.fecRegAct = fecRegAct;
    }

    public int getActComId() {
        return actComId;
    }

    public void setActComId(int actComId) {
        this.actComId = actComId;
    }

    public ReunionComision getReunionComision() {
        return reunionComision;
    }

    public void setReunionComision(ReunionComision reunionComision) {
        this.reunionComision = reunionComision;
    }

    public Date getFecRegAct() {
        return fecRegAct;
    }

    public void setFecRegAct(Date fecRegAct) {
        this.fecRegAct = fecRegAct;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public List<AcuerdosActaComision> getAcuerdos() {
        return acuerdos;
    }

    public void setAcuerdos(List<AcuerdosActaComision> acuerdos) {
        this.acuerdos = acuerdos;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getArcAdj() {
        return arcAdj;
    }

    public void setArcAdj(String arcAdj) {
        this.arcAdj = arcAdj;
    }
}
