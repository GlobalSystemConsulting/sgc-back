/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.se.NivelAcademico;
import java.util.List;

/**
 *
 * @author alex
 */
public interface NivelAcademicoDao {
    public List<NivelAcademico> listarAll();
    public List<NivelAcademico> listarxTipoFormacion(Integer nivAcaPad);
    public NivelAcademico buscarPorId(Integer nivAcaId);
}
