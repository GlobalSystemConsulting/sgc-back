/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public interface ConsultaGeneralDao extends GenericDao<MetadataConsultaGeneral>{   
    public MetadataConsultaGeneral buscarPorID(int id_met);
    public  JSONArray listarxCatalogo();
    public  List<String> listarTablasCatalogo();
    public  JSONArray listarxMetaDataConstraints();
    public  List<String[]> listarxConsultaGeneral(String consulta);
    public List<MetadataConsultaGeneral> listarTodaMetadata();
    public JSONArray listAllSchema();
}
