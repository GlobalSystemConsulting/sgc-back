package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="rol")
public class Rol  implements java.io.Serializable {


    @Id
    @Column(name="rol_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_rol", sequenceName="rol_rol_id_seq")
    @GeneratedValue(generator="secuencia_rol")
    private int rolId;
    
    @Column(name="abr", length=8)
    private String abr;
    
    @Column(name="nom", nullable=false, length=32)
    private String nom;
    
    @Column(name="des", length=256)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Column(name="est_reg", length=1)
    private char estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="claveRolFuncion.rol",cascade=CascadeType.ALL)
    private List<RolFuncion> rolFunciones;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="rol")
    private List<UsuarioSession> usuarios ;

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "rol")
    List<Trabajador> trabajadores;
    
    public Rol() {
    }
    public Rol(int rolId) {
        this.rolId = rolId;
    }
	
    public Rol(int rolId, String nom) {
        this.rolId = rolId;
        this.nom = nom;
    }
    public Rol(int rolId, String abr, String nom, String des, Date fecMod, Integer usuMod, char estReg, List<RolFuncion> rolFunciones) {
       this.rolId = rolId;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.rolFunciones = rolFunciones;
    }
   
    public int getRolId() {
        return this.rolId;
    }
    
    public void setRolId(int rolId) {
        this.rolId = rolId;
    }
    
    public String getAbr() {
        return this.abr;
    }
    
    public void setAbr(String abr) {
        this.abr = abr;
    }

    //INICIO -usado para el catalogo
    
    public int getId(){
        return this.rolId;
    }
    public void setId(int rolId){
        this.rolId=rolId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public char getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public List<RolFuncion> getRolFunciones() {
        return this.rolFunciones;
    }
    public void setRolFunciones(List<RolFuncion> rolFunciones) {
        this.rolFunciones = rolFunciones;
    }
    
    public List<UsuarioSession> getUsuarios() {
        return this.usuarios;
    }
    public void setUsuarios(List<UsuarioSession> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

}


