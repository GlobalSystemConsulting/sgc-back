/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoJornadaLaboral;

/**
 *
 * @author zpfmg
 */
public interface TipoJornadaLaboralDao {
    public List<TipoJornadaLaboral> listarAll();
    public TipoJornadaLaboral buscarPorId(Integer jorLabId);
}
