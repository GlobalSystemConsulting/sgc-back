/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class CargoDaoHibernate extends GenericDaoHibernate<Cargo> implements CargoDao{

    @Override
    public List<Cargo> listarxCargo() {
        List<Cargo> cargos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT car from Cargo as car "
                    + "WHERE car.estReg='A'";
            Query query = session.createQuery(hql);
            cargos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los cargos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las cargos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return cargos;
    }
    @Override
    public Cargo buscarPorId(Integer carId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Cargo item = (Cargo)session.get(Cargo.class, carId);
        session.close();
        return item;
    }
}
