/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

/**
 *
 * @author Administrador
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ActualizarAtributoMetadataTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarAtributoMetadataTx.class.getName());

     @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject)wr.getData();  
            System.out.println(data);
            return actualizarMetadata(data);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar Estado de consulta",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar atributo de metadata", e.getMessage());
        } 
    }
    private WebResponse actualizarMetadata(JSONObject data) {
        try{
            int rMetadataConsultaGeneral = data.optInt("id_met");
            String MetadataConsultaGeneral_entity_name = data.optString("entity_name");
            String MetadataConsultaGeneral_atrib_name= data.optString("atrib_name");
            String MetadataConsultaGeneral_alias_name= data.optString("alias_name");
            String MetadataConsultaGeneral_data_type= data.optString("data_type");
            String MetadataConsultaGeneral_mod= data.optString("mod");
            ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao) FactoryDao.buildDao("ConsultaGeneralDao");
            MetadataConsultaGeneral metadataConsultaGeneral = consultaGeneralDao.buscarPorID(rMetadataConsultaGeneral);
            metadataConsultaGeneral.setNombreTabla(MetadataConsultaGeneral_entity_name);
            metadataConsultaGeneral.setNombreAtributo(MetadataConsultaGeneral_atrib_name);
            metadataConsultaGeneral.setAliasAtributo(MetadataConsultaGeneral_alias_name);
            metadataConsultaGeneral.setTipoDato(MetadataConsultaGeneral_data_type);
            metadataConsultaGeneral.setModulo(MetadataConsultaGeneral_mod);
            consultaGeneralDao.update(metadataConsultaGeneral);
            
            JSONObject oResponse = new JSONObject();  
            JSONObject metadataConsultaGeneralRes = new JSONObject();
            oResponse.put("metadataConsultaGeneral", metadataConsultaGeneralRes);
            
            
            System.out.println(oResponse);
            
            return WebResponse.crearWebResponseExito("Estado de metadata actualizada exitosamente", oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarAtributoMetadata",e);
            return WebResponse.crearWebResponseError("Error, el estado de la metadata no fue actualizado");
        }
    }
    
}
