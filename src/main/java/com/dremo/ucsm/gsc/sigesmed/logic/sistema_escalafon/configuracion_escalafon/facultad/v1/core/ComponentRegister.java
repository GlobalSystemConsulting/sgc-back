/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx.ActualizarFacultadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx.AgregarFacultadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx.EliminarFacultadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx.ListarFacultadesTx;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("facultadConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarFacultades", ListarFacultadesTx.class);
        seComponent.addTransactionPUT("actualizarFacultades", ActualizarFacultadesTx.class);
        seComponent.addTransactionPOST("agregarFacultades", AgregarFacultadesTx.class);
        seComponent.addTransactionDELETE("eliminarFacultades", EliminarFacultadesTx.class);
        return seComponent;
    }
}
