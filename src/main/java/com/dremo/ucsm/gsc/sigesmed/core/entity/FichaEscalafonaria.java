/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;


import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author gscadmin
 */
@Entity(name = "FichaEsc")
@Table(name = "ficha_escalafonaria", schema="administrativo")

public class FichaEscalafonaria implements Serializable {
    
    @Id
    @Column(name = "fic_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ficha_escalafonaria", sequenceName="administrativo.ficha_escalafonaria_fic_esc_id_seq")
    @GeneratedValue(generator="secuencia_ficha_escalafonaria")
    private Integer ficEscId;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tra_id")
    private Trabajador trabajador;
            
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_doc_id", nullable=false)
    private TipoDoc tipoDoc;


    public FichaEscalafonaria() {
    }
    
    public FichaEscalafonaria(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, Boolean perDis, String regCon, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, String nomAfp, String codCuspp, Date fecIngAfp, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }


    public int getFicEscId() {
        return ficEscId;
    }

    public void setFicEscId(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
   
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public int getTraId() {
        return trabajador.getTraId();
    }

    public void setTraId(int traId) {
        this.trabajador.setTraId(traId);
    }

    @Override
    public String toString() {
        return "FichaEscalafonaria{" + "ficEscId=" + ficEscId + '}';
    }
   
    
}
