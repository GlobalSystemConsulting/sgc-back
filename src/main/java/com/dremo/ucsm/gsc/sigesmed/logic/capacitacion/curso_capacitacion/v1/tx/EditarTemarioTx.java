package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class EditarTemarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EditarTemarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        WebResponse response = null;
        
        switch(data.getInt("opt")) {
            case 0: response = editarTema(data);
                    break;
                
            case 1: response = finalizarTema(data.getJSONArray("cods"),data.getInt("usuMod"));
                    break;
        }
        
        return response;
    }
    
    private WebResponse editarTema(JSONObject data) {
        try {
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            TemarioCursoCapacitacion tema = temarioCursoCapacitacionDao.buscarPorId(data.getJSONObject("tema").getInt("cod"));
            
            tema.setFecFin(HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(data.getJSONObject("tema").getString("fin")).getTime()));
            tema.setFecIni(HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(data.getJSONObject("tema").getString("ini")).getTime()));
            tema.setTem(data.getJSONObject("tema").getString("nom"));
            tema.setUsuMod(data.getInt("usuMod"));
            tema.setFecMod(new Date());
            
            temarioCursoCapacitacionDao.update(tema);

            return WebResponse.crearWebResponseExito("Exito al editar el tema", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarTema", e);
            return WebResponse.crearWebResponseError("Error al editar el tema", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse finalizarTema(JSONArray temas,int usuMod) {
        try {
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            
            for(int i = 0;i < temas.length();i++) {
                TemarioCursoCapacitacion tema = temarioCursoCapacitacionDao.buscarPorId(temas.getInt(i));
                tema.setUsuMod(usuMod);
                tema.setFecMod(new Date());
                tema.setEstReg('F');
                
                temarioCursoCapacitacionDao.update(tema);
            }

            return WebResponse.crearWebResponseExito("Exito al editar el avance", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "finalizarTema", e);
            return WebResponse.crearWebResponseError("Error al editar el avance", WebResponse.BAD_RESPONSE);
        }
    }
}
