/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "desplazamiento", schema="administrativo")
public class Desplazamiento implements Serializable {
    @Id
    @Column(name = "des_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_desplazamiento", sequenceName="administrativo.desplazamiento_des_id_seq" )
    @GeneratedValue(generator="secuencia_desplazamiento")
    private Integer desId;
    
    @Column(name = "tip")
    private Character tip;

    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Column(name = "ins_edu")
    private String insEdu;
    
    @Column(name = "car")
    private String car;
    
    @Column(name = "jor_lab")
    private String jorLab;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_ter")
    @Temporal(TemporalType.DATE)
    private Date fecTer;
    
    @Column(name = "ent_emi_res")
    private String entEmiRes;
    
    @Column(name = "sit_lab")
    private String sitLab;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "fec_doc_ter")
    @Temporal(TemporalType.DATE)
    private Date fecDocTer;
    
    @Column(name = "num_doc_ter")
    private String numDocTer;
    
    @Column(name = "tip_doc_ter")
    private String tipDocTer;
    
    @Size(max = 2147483647)
    @Column(name = "mot_ret")
    private String motRet;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;
    
    @Column(name = "dat_orgi_id")
    private Integer datOrgiId;
    
    @Column(name = "tip_orgi_id")
    private String tipOrgiId;
        
    @Column(name = "orgi_id")
    private String orgiId;

    @Column(name = "ing_id")
    private Integer ingId;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "jor_lab_id")
    private Integer jorLabId;
    
    @Column(name = "orgi_act_id")
    private Integer orgiActId;

    @Column(name = "cat_act_id")
    private Integer catActId;

    @Column(name = "car_act_id")
    private Integer carActId; 

    public Desplazamiento() {
    }

    public Desplazamiento(Integer desId) {
        this.desId = desId;
    }
    
    public Desplazamiento(FichaEscalafonaria fichaEscalafonaria, Character tip, String numDoc, Date fecDoc, String tipDoc, String insEdu, String car, String jorLab, Date fecIni, Date fecTer, Integer usuMod, Date fecMod, Character estReg, Date fecDocTer, String numDocTer) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tip = tip;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.tipDoc = tipDoc;
        this.insEdu = insEdu;
        this.car = car;
        this.jorLab = jorLab;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        if(fecDocTer != null) this.fecDocTer =  fecDocTer;
        if(numDocTer != "") this.numDocTer = numDocTer;
    }
    
    public Desplazamiento(FichaEscalafonaria fichaEscalafonaria, Character tip, String numDoc, Date fecDoc, String insEdu, String car, Date fecIni, Date fecTer, Integer usuMod, Date fecMod, Character estReg, Date fecDocTer, String numDocTer, int tipDocId, int jorLabId) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tip = tip;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.insEdu = insEdu;
        this.car = car;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        if(fecDocTer != null) this.fecDocTer =  fecDocTer;
        if(numDocTer != "") this.numDocTer = numDocTer;
        this.tipDocId = tipDocId;
        this.jorLabId = jorLabId;
    }

    public Desplazamiento(Character tip, FichaEscalafonaria fichaEscalafonaria, int tipDocId) {
        this.tip = tip;
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tipDocId = tipDocId;
    }
    public Desplazamiento(FichaEscalafonaria fichaEscalafonaria, Character tip, int tipDocId, String numDoc, Date fecDoc, int jorLabId, Date fecIni, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tip = tip;
        this.tipDocId = tipDocId;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.jorLabId = jorLabId;
        this.fecIni = fecIni;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public Integer getDesId() {
        return desId;
    }

    public void setDesId(Integer desId) {
        this.desId = desId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }
    
    public String getMotRet() {
        return motRet;
    }

    public void setMotRet(String motRet) {
        this.motRet = motRet;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }
    
    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }
    
    public String getNumDocTer() {
        return numDocTer;
    }

    public void setNumDocTer(String numDoc) {
        this.numDocTer = numDoc;
    }

    public Date getFecDocTer() {
        return fecDocTer;
    }

    public void setFecDocTer(Date fecDoc) {
        this.fecDocTer = fecDoc;
    }
    
    public String getTipDocTer() {
        return tipDocTer;
    }

    public void setTipDocTer(String tipDocTer) {
        this.tipDocTer = tipDocTer;
    }

    public String getInsEdu() {
        return insEdu;
    }

    public void setInsEdu(String insEdu) {
        this.insEdu = insEdu;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getJorLab() {
        return jorLab;
    }

    public void setJorLab(String jorLab) {
        this.jorLab = jorLab;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecTer() {
        return fecTer;
    }

    public void setFecTer(Date fecTer) {
        this.fecTer = fecTer;
    }

    public String getEntEmiRes() {
        return entEmiRes;
    }

    public void setEntEmiRes(String entEmiRes) {
        this.entEmiRes = entEmiRes;
    }

    public String getSitLab() {
        return sitLab;
    }

    public void setSitLab(String sitLab) {
        this.sitLab = sitLab;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setDatEscId(FichaEscalafonaria datosEscalafon) {
        this.fichaEscalafonaria = datosEscalafon;
    }

    public Integer getDatOrgiId() {
        return datOrgiId;
    }

    public void setDatOrgiId(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public String getTipOrgiId() {
        return tipOrgiId;
    }

    public void setTipOrgiId(String tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public String getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(String orgiId) {
        this.orgiId = orgiId;
    }

    public Integer getIngId() {
        return ingId;
    }

    public void setIngId(Integer ingId) {
        this.ingId = ingId;
    }

    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(int tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getJorLabId() {
        return jorLabId;
    }

    public void setJorLabId(int jorLabId) {
        this.jorLabId = jorLabId;
    }

    public Integer getOrgiActId() {
        return orgiActId;
    }

    public void setOrgiActId(Integer orgiActId) {
        this.orgiActId = orgiActId;
    }

    public Integer getCatActId() {
        return catActId;
    }

    public void setCatActId(Integer catActId) {
        this.catActId = catActId;
    }

    public Integer getCarActId() {
        return carActId;
    }

    public void setCarActId(Integer carActId) {
        this.carActId = carActId;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (desId != null ? desId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desplazamiento)) {
            return false;
        }
        Desplazamiento other = (Desplazamiento) object;
        if ((this.desId == null && other.desId != null) || (this.desId != null && !this.desId.equals(other.desId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Desplazamiento{" + "desId=" + desId + ", tip=" + tip + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", insEdu=" + insEdu + ", car=" + car + ", jorLab=" + jorLab + ", fecIni=" + fecIni + ", fecTer=" + fecTer + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }

    
    
}
