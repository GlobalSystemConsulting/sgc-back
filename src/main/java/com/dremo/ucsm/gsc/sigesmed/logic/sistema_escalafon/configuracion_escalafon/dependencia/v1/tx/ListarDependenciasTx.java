/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarDependenciasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarDependenciasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Dependencia> dependencias = null;
        DependenciaDao dependenciaDao = (DependenciaDao)FactoryDao.buildDao("se.DependenciaDao");
        
        try{
            dependencias = dependenciaDao.listarxDependencia();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar dependencias",e);
            System.out.println("No se pudo listar las dependencias.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas dependencias.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Dependencia c: dependencias ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("depId",c.getDepId());
            oResponse.put("codDep",c.getCodDep());
            oResponse.put("nomDep",c.getNomDep());
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            oResponse.put("facId",c.getFacultad().getFacId());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las dependencias fueron listadas exitosamente", miArray);
    }
}
