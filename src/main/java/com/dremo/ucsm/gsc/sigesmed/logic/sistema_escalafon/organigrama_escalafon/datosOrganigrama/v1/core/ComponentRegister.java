/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.core;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx.ActualizarDatosOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx.AgregarDatosOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx.EliminarDatosOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx.ListarDatosOrganigramaTx;



/**
 *
 * @author forev
 */
public class ComponentRegister implements IComponentRegister {
    
     @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("datosOrgaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarDatosOrg", ListarDatosOrganigramaTx.class);
        seComponent.addTransactionPUT("actualizarDatosOrg", ActualizarDatosOrganigramaTx.class);
        seComponent.addTransactionPOST("agregarDatosOrg", AgregarDatosOrganigramaTx.class);
        seComponent.addTransactionDELETE("eliminarDatosOrg", EliminarDatosOrganigramaTx.class);
        return seComponent;
    }
    
}
