/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "estudio_postgrado", schema="administrativo")

public class EstudioPostgrado implements Serializable {
    
    @Id
    @Column(name = "est_pos_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_estudio_postgrado", sequenceName="administrativo.estudio_postgrado_est_pos_id_seq" )    
    @GeneratedValue(generator="secuencia_estudio_postgrado")
    private Integer estPosId;
    
    @Column(name = "tip")
    private Character tip;
    
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "pais")
    private String pais;
    
    @Size(max = 2147483647)
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Column(name = "fec_ini_est")
    @Temporal(TemporalType.DATE)
    private Date fecIniEst;
    
    @Column(name = "fec_ter_est")
    @Temporal(TemporalType.DATE)
    private Date fecTerEst;
    
    @Column(name = "ins")
    private String ins;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public EstudioPostgrado() {
    }

    public EstudioPostgrado(Integer estPosId) {
        this.estPosId = estPosId;
    }
    
    
    public EstudioPostgrado(Persona persona, Character tip, String numDoc, Date fecDoc, Integer tipDocId, Date fecIniEst, Date fecTerEst, String ins, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.tip = tip;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.fecIniEst = fecIniEst;
        this.fecTerEst = fecTerEst;
        this.ins = ins;
        this.tipDocId=tipDocId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getEstPosId() {
        return estPosId;
    }

    public void setEstPosId(Integer estPosId) {
        this.estPosId = estPosId;
    }
    
     public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }
    
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }
    
    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public Date getFecIniEst() {
        return fecIniEst;
    }

    public void setFecIniEst(Date fecIniEst) {
        this.fecIniEst = fecIniEst;
    }

    public Date getFecTerEst() {
        return fecTerEst;
    }

    public void setFecTerEst(Date fecTerEst) {
        this.fecTerEst = fecTerEst;
    }

    public String getIns() {
        return ins;
    }

    public void setIns(String ins) {
        this.ins = ins;
    }
    
    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estPosId != null ? estPosId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudioPostgrado)) {
            return false;
        }
        EstudioPostgrado other = (EstudioPostgrado) object;
        if ((this.estPosId == null && other.estPosId != null) || (this.estPosId != null && !this.estPosId.equals(other.estPosId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EstudioPostgrado{" + "estPosId=" + estPosId + ", tip=" + tip + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", fecIniEst=" + fecIniEst + ", fecTerEst=" + fecTerEst + ", ins=" + ins + ", tipDocId=" + tipDocId +", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + persona + '}';
    }

    
}
