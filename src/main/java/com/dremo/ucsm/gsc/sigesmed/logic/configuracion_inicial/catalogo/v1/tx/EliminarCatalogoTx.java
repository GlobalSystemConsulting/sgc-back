/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarCatalogoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int CatalogoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            CatalogoID = requestData.getInt("catalogoID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        try{
            catalogoTablaDao.delete(new CatalogoTabla(CatalogoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Catalgo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Catalogo ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Catalogo se elimino correctamente");
        //Fin
    }
}
