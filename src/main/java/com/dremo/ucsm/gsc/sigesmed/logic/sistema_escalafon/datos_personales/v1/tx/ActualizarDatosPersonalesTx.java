/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Idioma;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Nacionalidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarDatosPersonalesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarDatosPersonalesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject)wr.getData();  
            System.out.println(data);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarCambioAuditoria("Se modifico datos de persona",data.getJSONObject("persona").getString("nom")+" "+data.getJSONObject("persona").getString("apePat")+" "+data.getJSONObject("persona").getString("apeMat") ,wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
            return actualizarDatosPersonales(data,wr.getIdUsuario());
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar datos personales",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarDatosPersonales(JSONObject data, int usumod) {
        try{
            JSONObject rPersona = data.getJSONObject("persona");
            // rTrabajador = data.getJSONObject("trabajador");
            //JSONObject rFicha = data.getJSONObject("fichaEscalafonaria");
            JSONArray rDirecciones = data.getJSONArray("direcciones");
            
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
            //TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
            //FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao"); 
            DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
            
            Persona persona = personaDao.buscarPersonaPorId(rPersona.getInt("perId"));
            
            if(!"".equals(rPersona.getString("apePat"))){
                persona.setApePat(rPersona.getString("apePat").toUpperCase());
            }
            //if(!"".equals(rPersona.getString("apeMat"))){
            persona.setApeMat(rPersona.getString("apeMat").toUpperCase());
            //}
            if(!"".equals(rPersona.getString("nom"))){
                persona.setNom(rPersona.getString("nom").toUpperCase());
            }
            if(!"".equals(rPersona.getString("sex"))){
                persona.setSex(rPersona.getString("sex").charAt(0));
            }
            
            if(rPersona.getInt("estCivId")!=0){
                persona.setEstCivId(rPersona.getInt("estCivId"));
            }
            
            if(!"".equals(rPersona.getString("pas"))){
                persona.setPas(rPersona.getString("pas"));
            }
            persona.setBoolSalud(rPersona.getBoolean("estAseEss"));
            if (rPersona.getBoolean("estAseEss")) {
                if (!"".equals(rPersona.getString("autEss"))) {
                    persona.setAutEss(rPersona.getString("autEss"));
                }
            } else {
                persona.setAutEss(null);
            }
            
            if(!"".equals(rPersona.getString("fij"))){
                persona.setFij(rPersona.getString("fij"));
            }
            if(!"".equals(rPersona.getString("num1"))){
                persona.setNum1(rPersona.getString("num1"));
            }
            if(!"".equals(rPersona.getString("num2"))){
                persona.setNum2(rPersona.getString("num2"));
            }
            if(!"".equals(rPersona.getString("email"))){
                persona.setEmail(rPersona.getString("email"));
            }
            if(rPersona.getInt("nacId")!=0){
                persona.setNacionalidad(new Nacionalidad(rPersona.getInt("nacId")));
            }
            if(!"".equals(rPersona.getString("depNac"))){
                persona.setDepNac(rPersona.getString("depNac"));
            }
            if(!"".equals(rPersona.getString("proNac"))){
                persona.setProNac(rPersona.getString("proNac"));
            }
            if(!"".equals(rPersona.getString("disNac"))){
                persona.setDisNac(rPersona.getString("disNac"));
            }
            if(!"".equals(rPersona.getString("fecNac"))){
                Date miFecNac = sdi.parse(rPersona.getString("fecNac").substring(0,10));
                persona.setFecNac(miFecNac);
                persona.setDiaFecNac(Integer.parseInt(new SimpleDateFormat("dd").format(miFecNac)));
                persona.setMesFecNac(Integer.parseInt(new SimpleDateFormat("MM").format(miFecNac)));
                persona.setAnioFecNac(Integer.parseInt(new SimpleDateFormat("yyyy").format(miFecNac)));
            }
            
            
            persona.setSisPen(rPersona.getString("sisPen"));
            if (rPersona.getString("sisPen").equals("AFP")) {
                if (!"".equals(rPersona.getString("tipAfp"))) {
                    persona.setTipAfp(rPersona.getString("tipAfp"));
                }
                if (!"".equals(rPersona.getString("codCuspp"))) {
                    persona.setCodCuspp(rPersona.getString("codCuspp"));
                }
                if (!"".equals(rPersona.getString("fecIngAfp"))) {
                    persona.setFecIngAfp(sdi.parse(rPersona.getString("fecIngAfp").substring(0, 10)));
                }
                if (!"".equals(rPersona.getString("fecTraAfp"))) {
                    persona.setFecTraAfp(sdi.parse(rPersona.getString("fecTraAfp").substring(0, 10)));
                }
            } else {
                persona.setTipAfp(null);
                persona.setCodCuspp(null);
                persona.setFecIngAfp(null);
                persona.setFecTraAfp(null);
            }
            
            persona.setPerDis(rPersona.getBoolean("perDis"));
            if(rPersona.getBoolean("perDis")){
                persona.setRegCon(rPersona.getString("regCon"));
            }
            else{
               persona.setRegCon(null);
            }
            if(rPersona.getInt("idiomId")!=0){
                persona.setIdiomas(new Idioma(rPersona.getInt("idiomId")));
            }
            if(!"".equals(rPersona.getString("licCond"))){
                persona.setLicCond(rPersona.getString("licCond"));
            }
            if(!"".equals(rPersona.getString("bonCaf"))){
                persona.setBonCaf(rPersona.getString("bonCaf"));
            }
            persona.setUsuMod(usumod);
            persona.setFecMod(new Date());
            personaDao.update(persona);
            
            //Direcciones
            //List<Direccion> direcciones = new ArrayList<>();
            for (int i = 0; i < rDirecciones.length(); i++) {
                Direccion direccion = direccionDao.buscarPorId(rDirecciones.getJSONObject(i).getInt("dirId"));
                if (direccion == null)//dirId==0
                {
                    Direccion dir = new Direccion(persona,
                            rDirecciones.getJSONObject(i).getString("tipDir").charAt(0),
                            rDirecciones.getJSONObject(i).getString("nomDir"),
                            rDirecciones.getJSONObject(i).getString("depDir"),
                            rDirecciones.getJSONObject(i).getString("proDir"),
                            rDirecciones.getJSONObject(i).getString("disDir"),
                            rDirecciones.getJSONObject(i).getString("nomZon"),
                            rDirecciones.getJSONObject(i).getString("desRef"),usumod,new Date(),
                            'A'
                    );
                    direccionDao.insert(dir);   
                } else {
                    direccion.setPersona(persona);
                    direccion.setTip(rDirecciones.getJSONObject(i).getString("tipDir").charAt(0));
                    direccion.setNom(rDirecciones.getJSONObject(i).getString("nomDir"));
                    direccion.setDep(rDirecciones.getJSONObject(i).getString("depDir"));
                    direccion.setPro(rDirecciones.getJSONObject(i).getString("proDir"));
                    direccion.setDis(rDirecciones.getJSONObject(i).getString("disDir"));
                    direccion.setNomZon(rDirecciones.getJSONObject(i).getString("nomZon"));
                    direccion.setDesRef(rDirecciones.getJSONObject(i).getString("desRef"));
                    direccion.setUsuMod(usumod);
                    direccion.setFecMod(new Date());
                    direccionDao.update(direccion);
                    //direcciones.add(direccion);
                }
            }
            
            JSONObject oResponse = new JSONObject();  
            JSONObject personaRes = new JSONObject();
            JSONArray direccionesRes  = new JSONArray();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            
            personaRes.put("perId", persona.getPerId());
            personaRes.put("apePat", persona.getApePat()==null?"":persona.getApeMat());
            personaRes.put("apeMat", persona.getApeMat()==null?"":persona.getApeMat());
            personaRes.put("nom", persona.getNom()==null?"":persona.getNom());
            personaRes.put("sex", persona.getSex()==null?"":persona.getSex());
            personaRes.put("estCivId", persona.getEstCivId()==null?0:persona.getEstCivId());
            personaRes.put("dni", persona.getDni());
            personaRes.put("pas", persona.getPas()==null?"":persona.getPas());
            personaRes.put("estAseEss", persona.getBoolSalud()==null?"":persona.getBoolSalud());
            personaRes.put("autEss", persona.getAutEss()==null?"":persona.getAutEss());
            personaRes.put("fij", persona.getFij()==null?"":persona.getFij());
            personaRes.put("num1", persona.getNum1()==null?"":persona.getNum1());
            personaRes.put("num2", persona.getNum2()==null?"":persona.getNum2());
            personaRes.put("email", persona.getEmail()==null?"":persona.getEmail());
            personaRes.put("nacId", persona.getNacionalidad()==null?0:persona.getNacionalidad().getNacId());
            personaRes.put("depNac", persona.getDepNac()==null?"":persona.getDepNac());
            personaRes.put("proNac", persona.getProNac()==null?"":persona.getProNac());
            personaRes.put("disNac", persona.getDisNac()==null?"":persona.getDisNac());
            personaRes.put("fecNac", persona.getFecNac()==null?"":sdo.format(persona.getFecNac()));
            //Direcciones?2
            personaRes.put("sisPen", persona.getSisPen()==null?"NUL":persona.getSisPen());
            personaRes.put("tipAfp", persona.getNomAfp()==null?"":persona.getNomAfp());
            personaRes.put("codCuspp", persona.getCodCuspp()==null?"":persona.getCodCuspp());
            personaRes.put("fecIngAfp", persona.getFecIngAfp()==null?"":sdo.format(persona.getFecIngAfp()));
            personaRes.put("fecTraAfp", persona.getFecTraAfp()==null?"":sdo.format(persona.getFecTraAfp()));
            personaRes.put("perDis", persona.getPerDis());
            personaRes.put("regCon", persona.getRegCon()==null?"":persona.getRegCon());
            personaRes.put("idiomId", persona.getIdiomas()==null?0:persona.getIdiomas().getIdiId());
            personaRes.put("licCond", persona.getLicCond()==null?"":persona.getLicCond());
            personaRes.put("bonCaf  ", persona.getBonCaf()==null?"":persona.getBonCaf());
            oResponse.put("persona", personaRes);
            
            /*for(Direccion dir:direcciones ){
                JSONObject aux = new JSONObject();
                aux.put("dirId", dir.getDirId());
                aux.put("tipDir", dir.getTip());
                aux.put("nomDir", dir.getNom());
                aux.put("depDir", dir.getDep());
                aux.put("proDir", dir.getPro());
                aux.put("disDir", dir.getDis());
                aux.put("nomZon", dir.getNomZon());
                aux.put("desRef", dir.getDesRef());
                direccionesRes.put(aux);
            }*/
            
            //oResponse.put("direcciones", direccionesRes);
            System.out.println(oResponse);
            return WebResponse.crearWebResponseExito("Datos personales actualizados exitosamente", oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDatosPersonales",e);
            return WebResponse.crearWebResponseError("Error, los datos personales no fueron actualizados");
        }
    }
    
}
