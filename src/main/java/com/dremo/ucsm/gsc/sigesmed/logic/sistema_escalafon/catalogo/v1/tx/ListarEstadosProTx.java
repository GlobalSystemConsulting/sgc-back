/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstadoProcesoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstadoProceso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarEstadosProTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarEstadosProTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        List<EstadoProceso> estados = null;
        EstadoProcesoDao estProDao = (EstadoProcesoDao)FactoryDao.buildDao("se.EstadoProcesoDao");
        
        try{
            estados = estProDao.listarAll();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar los estados de proceso",e);
            System.out.println("No se pudo listar los estados de proceso.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listar los estados de proceso.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(EstadoProceso d: estados ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id", d.getId());
            oResponse.put("nom", d.getNom());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los estados de proceso fueron listados exitosamente", miArray);
    }
    
}
