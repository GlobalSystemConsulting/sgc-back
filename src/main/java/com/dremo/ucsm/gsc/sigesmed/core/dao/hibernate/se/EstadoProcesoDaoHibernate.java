/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstadoProcesoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstadoProceso;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EstadoProcesoDaoHibernate extends GenericDaoHibernate<EstadoProceso> implements EstadoProcesoDao{

    @Override
    public List<EstadoProceso> listarAll() {
        List<EstadoProceso> estados = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from EstadoProceso as d "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            estados = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los estados de proceso \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los estados de proceso \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return estados;
    }

    @Override
    public EstadoProceso buscarPorId(Integer estProId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstadoProceso des = (EstadoProceso)session.get(EstadoProceso.class, estProId);
        session.close();
        return des;
    }
    
}
