package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;
// Generated 04/08/2016 12:21:42 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ControlPeriodicoLibroId generated by hbm2java
 */
@Embeddable
public class ControlPeriodicoLibroId  implements java.io.Serializable {

    @Column(name="con_per_lib_id", nullable=false)
     private short conPerLibId;
    
    @Column(name="lib_caj_id", nullable=false)
     private int libCajId;

    public ControlPeriodicoLibroId() {
    }

    public ControlPeriodicoLibroId(short conPerLibId, int libCajId) {
       this.conPerLibId = conPerLibId;
       this.libCajId = libCajId;
    }
      
    public short getConPerLibId() {
        return this.conPerLibId;
    }
    
    public void setConPerLibId(short conPerLibId) {
        this.conPerLibId = conPerLibId;
    }


    
    public int getLibCajId() {
        return this.libCajId;
    }
    
    public void setLibCajId(int libCajId) {
        this.libCajId = libCajId;
    }


     @Override
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ControlPeriodicoLibroId) ) return false;
		 ControlPeriodicoLibroId castOther = ( ControlPeriodicoLibroId ) other; 
         
		 return (this.getConPerLibId()==castOther.getConPerLibId())
 && (this.getLibCajId()==castOther.getLibCajId());
   }
   
     @Override
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getConPerLibId();
         result = 37 * result + this.getLibCajId();
         return result;
   }   


}


