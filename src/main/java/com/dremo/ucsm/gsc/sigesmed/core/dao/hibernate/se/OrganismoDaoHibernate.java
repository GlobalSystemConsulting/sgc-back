/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class OrganismoDaoHibernate extends GenericDaoHibernate<Organismo> implements OrganismoDao{
    
    @Override
    public List<Organismo> listarxOrganismo() {
        List<Organismo> organismos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT orga from Organismo as orga "
                    + "WHERE orga.estReg='A'";
            Query query = session.createQuery(hql);
            organismos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organismos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los organismos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organismos;
    }
    @Override
    public Organismo buscarPorId(Integer orgId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Organismo item = (Organismo)session.get(Organismo.class, orgId);
        session.close();
        return item;
    }
    
}
