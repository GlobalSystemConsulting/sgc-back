/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author christian
 */
public class TrabajadorCargoDaoHibernate extends GenericDaoHibernate<TrabajadorCargo> implements TrabajadorCargoDao{

    @Override
    public TrabajadorCargo buscarByCod(int cod) {
        TrabajadorCargo res = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Query query = session.getNamedQuery("TrabajadorCargo.findByCrgTraIde").setInteger("crgTraIde", cod);
            query.setMaxResults(1);
            res = ((TrabajadorCargo)query.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el Cargo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar por cod del cargo \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return res;
    }
    
}
