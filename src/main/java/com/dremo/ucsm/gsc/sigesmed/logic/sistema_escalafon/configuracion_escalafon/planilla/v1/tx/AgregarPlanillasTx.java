/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarPlanillasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarPlanillasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Planilla planilla=null;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String abrPla = requestData.optString("abrPla");
            //String camPla = requestData.optString("camPla");
            String codPla = requestData.optString("codPla");
            //Character estReg = requestData.optString("estReg").charAt(0);
            //Date fecMod = (Date)requestData.getString("fecMod");
            //////////HitorialCambiosPlanillas
            String nomPla = requestData.optString("nomPla");
            //Integer plaId = requestData.getInt("plaId");
            //////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");
            planilla =new Planilla();
            planilla.setCodPla(codPla);
            planilla.setAbrPla(abrPla);
            planilla.setNomPla(nomPla);
            planilla.setEstReg('A');
            planilla.setFecMod(new Date());
            planilla.setUsuMod(wr.getIdUsuario());
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva planilla",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        PlanillaDao planillaDao = (PlanillaDao) FactoryDao.buildDao("se.PlanillaDao");
        try {
            planillaDao.insert(planilla);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva planilla",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("codPla", planilla.getCamPla());
        oResponse.put("abrPla", planilla.getAbrPla());
        oResponse.put("nomPla", planilla.getNomPla());
        oResponse.put("plaId", planilla.getPlaId());
       
                
        return WebResponse.crearWebResponseExito("El registro de la planilla se realizo correctamente", oResponse);

    }
    
}
